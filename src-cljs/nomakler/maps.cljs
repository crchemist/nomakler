(ns nomakler.maps
  (:require-macros  [reagent.ratom :refer  [reaction]])
  (:require [reagent.core :as reagent :refer [atom]]
            [dommy.core :refer-macros [sel sel1]]
            [reagent.session :as session]
            [ajax.core :refer [GET POST]]
            [re-frame.core :refer [register-handler subscribe
                                   dispatch dispatch-sync
                                   register-sub]]
            [nomakler.state :refer [app-state initial-state]]))


(def littleton  (.bindPopup (.marker js/L #js [39.61, -105.02]) "This is Littleton, CO."))
(def cities (.layerGroup js/L #js [littleton] ))

(def overlay-map {:Cities cities})
(def rent-marker (atom nil))



(defn map-add [leaflet-map leaflet-obj]
  (if (some? leaflet-obj)
    (.addTo leaflet-obj leaflet-map)))

(defn add-marker-to-layer [layer-group marker-data]
  (let [wkt (get marker-data "location")
        latlng (get (js->clj (.parse js/wellknown (clj->js wkt))) "coordinates")
        marker (.marker js/L (clj->js latlng))]
    (do
      (.addLayer layer-group marker)
      )))

(defn add-markers-to-map [leaflet-map markers appartments]
  (let []
    (do
      (.clearLayers appartments)
      (.log js/console "Clear appartments")
      (doseq [marker markers]
        (do
          (add-marker-to-layer appartments marker)))
      (map-add leaflet-map appartments))))


;; -- Event handlers --
(register-handler
  :initialize
  (fn [db _]
    (merge db initial-state))) ;; what it returns becomes the new state

(register-handler
  :put-markers-on-map
  (fn [db  [_ leaflet-map resp]]
    (let [markers (get resp "data")]
      (do
        (add-markers-to-map leaflet-map markers (:appartments db))
        ;; add markers to map
        (assoc db :markers resp))))) ;; return the new version of db


(register-handler
  :update-appartments
  (fn [db [_ leaflet-map]]
    (do
      (GET "/appartments/" {:handler #(dispatch [:put-markers-on-map leaflet-map %1])
                            :response-format :json})
      db)))



;; -- Subsription handlers --
(register-sub
  :markers
  (fn [db _]
    (reaction  (:markers @db))))

(register-sub
  :appartments
  (fn [db _]
    (reaction  (:appartments @db))))

(defn set-rent-marker [marker]
  (let []
    (if (nil? @rent-marker)
      (let []
        (reset! rent-marker marker)
        ))))

(defn clear-rent-marker [leaflet-map]
  (do
    (if (not (nil? @rent-marker))
      (do
        (.log js/console "Layer removed: " @rent-marker)
        (.removeLayer leaflet-map (clj->js @rent-marker))))

    (reset! rent-marker nil)))

(defn update-appartments [leaflet-map resp]
  (let[]
    (do
      (.log js/console "Update markers: " leaflet-map resp)
      (dispatch  [:markers (get resp "data")]))))


(defn map-did-mount []
  (let [leaflet-map (.setView (.map js/L "map" (clj->js {:layers cities})) #js [49.83, 24.07] 12)
        geocoder (.nominatim js/L.Control.Geocoder)
        geocoder-control (.geocoder js/L.Control (clj->js {:geocoder geocoder}))
        markers (subscribe [:markers])
        appartments (subscribe [:appartments])]
    (do
      ;; trigger a dispatch every 10 seconds
      (dispatch [:update-appartments leaflet-map])
      (js/setInterval #(dispatch  [:update-appartments  leaflet-map]) 5000)

      (.on leaflet-map "click"
           (fn [e]
             (map-add leaflet-map
                      (let [latlng (.-latlng e)]
                        (.marker js/L (clj->js [(.-lat latlng) (.-lng latlng)]))))))

      (.on leaflet-map "layeradd"
           (fn [e]
             (let [obj (.-layer e)]
               (do
                 (if (and (= (type obj) js/L.Marker) (not (.hasLayer @appartments obj)) )
                   (do
                     (.log js/console "Marker added: " (.-_latlng obj))
                     (clear-rent-marker leaflet-map)
                     (set-rent-marker obj)))
                 ))))
      (map-add leaflet-map
               (.tileLayer js/L "https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}"
                           (clj->js {:attribution "Map data &copy; <a href=\"http://openstreetmap.org\">OpenStreetMap</a> contributors, <a href=\"http://creativecommons.org/licenses/by-sa/2.0/\">CC-BY-SA</a>, Imagery © <a href=\"http://mapbox.com\">Mapbox</a>"
                                     :maxZoom 18
                                     :id "crchemist.odi3aamj"
                                     :accessToken "pk.eyJ1IjoiY3JjaGVtaXN0IiwiYSI6ImNpaTE2MnF5cDAwMzF0bmtxMDFoYWVlNmYifQ.HvpKx0YEjteZ9m0P7G7ypg"})))

      (map-add leaflet-map geocoder-control)

      (map-add leaflet-map
               (.layers js/L.control
                        (clj->js nil),
                        (clj->js overlay-map)
                        (clj->js {:collapsed false})))
      )))


(defn map-render []
  (do
    [:div#map]))


(defn leaflet-component []
  (reagent/create-class {:reagent-render map-render
                         :component-did-mount map-did-mount}))
