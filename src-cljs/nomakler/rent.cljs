(ns nomakler.rent
  (:require [reagent.core :as reagent :refer [atom]]
            [reagent.session :as session]
            [ajax.core :refer [GET POST]]
            [reagent-forms.core :refer [bind-fields]]
            [re-frame.core :refer [register-handler subscribe
                                   dispatch dispatch-sync
                                   register-sub]]
            [nomakler.maps :as maps :refer [rent-marker]]
            [nomakler.state :refer [app-state]]))


(def steps-count 2)

(def page-number (atom 0))

(defn page-put-marker []
  [:div
   [:p "Please specify place where appartment located."]
   (if (some? @maps/rent-marker)
     (let [latlng (.-_latlng @maps/rent-marker)]
       [:div
        [:p "Lat: "
         [:span (.-lat latlng)]]
        [:p "Lng: "
         [:span (.-lng latlng)]]]))
   [:p "Note: put marker on map by left click."]])

(defn page-add-details []
  (let [form  (bind-fields
                [:div
                 [:h4 "Appartment details:"]
                 [:p "Price: " ]
                 [:input {:field :numeric :id :price}]
                 [:p "Description: "]
                 [:textarea  {:field :textarea :id :description}]] (:rent-wizard @app-state))]
    form))

(defn page-last []
  [:div
   [:p "Appartment info already collected. Presss 'Finish' to publish data"]])


(defn publish-rent-data [ev]
  (let [latlng (.-_latlng @rent-marker)]
    (do
      ;; send appartment data to server.
      (POST "/appartments/"
            {:params (assoc (deref (:rent-wizard @app-state))
                            :location (str "POINT("
                                           (.-lat latlng ) " "
                                           (.-lng latlng) ")"))
             :handler #(dispatch [:show-alert "Data stored" "alert-success" true])})
      ;; reset wizard page to start.
      (reset! page-number 0))))

(defn wizard-next []
  (let [d (.log js/console (clj->js  (deref (:rent-wizard @app-state))))]
    (if (< @page-number steps-count)
      (if (nil? @rent-marker)
        [:button.btn.btn-primary.disabled "Continue"]
        [:button.btn.btn-primary {:on-click #(swap! page-number inc)} "Continue"])
      [:button.btn.btn-primary {:on-click publish-rent-data} "Finish!"])))

(defn rent-wizard []
  (let [page (cond
               (= @page-number 0) #'page-put-marker
               (= @page-number 1) #'page-add-details
               :else #'page-last)]
    [:div
     [page]
     [wizard-next]]))
