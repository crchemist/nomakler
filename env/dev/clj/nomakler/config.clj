(ns nomakler.config
  (:require [selmer.parser :as parser]
            [taoensso.timbre :as timbre]
            [nomakler.dev-middleware :refer [wrap-dev]]))

(def defaults
  {:init
   (fn []
     (parser/cache-off!)
     (timbre/info "\n-=[nomakler started successfully using the development profile]=-"))
   :middleware wrap-dev})
