(ns nomakler.config
  (:require [taoensso.timbre :as timbre]))

(def defaults
  {:init
   (fn []
     (timbre/info "\n-=[nomakler started successfully]=-"))
   :middleware identity})
