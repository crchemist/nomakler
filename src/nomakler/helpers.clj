(ns nomakler.helpers
  (:require [clj-http.client :as http]
            [cheshire.core :as chjson]
            [clojure.data.json :as json]
	    [nomakler.db.core :as db]))

(defn call-vk
  [endpoint access-token]
  (-> (format "https://api.vk.com/method/%s?v=5.41&access_token=%s"
        endpoint
        access-token)
    http/get
    :body
    (chjson/parse-string (fn [^String s] (keyword (.replace s \_ \-))))))


(def get-vk-user-info (memoize (partial call-vk "users.get")))

(defn get-or-create-user [vk_user_id user_name]
  (let [user (db/get-vk-user {:vk_id vk_user_id})]
    (if (empty? user)
      (do
        (db/create-user! {:vk_id vk_user_id
	                  :name user_name})
	(first (db/get-vk-user {:vk_id vk_user_id})))
      (first user))))

(defn context-data [req]
  (let [s (:session req)
        sd (println req)
        data {:name (:username s)
              :id (:identity req)
              :server-name (:server-name req)
              :server-port (if (= (str (:server-port req)) (System/getenv "OPENSHIFT_DIY_PORT"))
                             ""
                             (:server-port req))}]
        data))
