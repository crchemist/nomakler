(ns nomakler.routes.uploads
  (:require [nomakler.db.core :as db]
            [compojure.core :refer [defroutes GET POST]]
            [environ.core :refer [env]]
            [ring.util.response :refer  [redirect file-response]]
            [nomakler.layout :as layout])
  (:import  [java.io File FileInputStream FileOutputStream]) )

(def upload-path  (if (empty? (env :media-path)) (System/getenv "OPENSHIFT_DATA_DIR") (env :media-path)))

(defn file-path  [path &  [filename]]
  (java.net.URLDecoder/decode
    (str path File/separator filename "-" (System/currentTimeMillis))
    "utf-8"))


(defn file-upload [{:keys [tempfile size filename]}]
  (try
    (with-open [in  (new FileInputStream tempfile)
                out (new FileOutputStream (file-path upload-path filename))]
      (let  [source (.getChannel in)
             dest (.getChannel out)]
        (.transferFrom dest source 0  (.size source))
        (.flush out)
        {:body {:status 200}}))))

(defroutes uploads-routes
  (POST "/upload/" [file] (file-upload file)))
