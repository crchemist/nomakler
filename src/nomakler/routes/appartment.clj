(ns nomakler.routes.appartment
  (:require [nomakler.db.core :as db]
            [compojure.core :refer [defroutes GET POST]]
            [nomakler.layout :as layout]))


(defn appartments-list [req]
  (let [data (db/appartments-list)]
    (do
      {:body {:status 200
              :data data}})))

(defn appartment-create [req]
  (if (nil? layout/*identity*)
    {:body {:status 403}}
    (do
      (db/appartment-create! (merge (req :params) {:owner_id layout/*identity*}))
      {:body {:status 200}})))

(defroutes appartment-routes
  (GET "/appartments/" [ :as req] (appartments-list req))
  (POST "/appartments/" [ :as req] (appartment-create req)))
