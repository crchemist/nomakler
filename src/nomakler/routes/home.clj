(ns nomakler.routes.home
  (:require [nomakler.layout :as layout]
            [compojure.core :refer [defroutes GET]]
            [ring.util.http-response :refer [ok]]
            [clj-http.client :as http]
            [ring.util.response :refer [redirect]]
            [nomakler.helpers :as h :refer [call-vk get-vk-user-info get-or-create-user]]
            [clojure.java.io :as io]
            [cemerick.url :refer (url query->map)]
            [clojure.walk :refer [keywordize-keys]]
            [clojure.data.json :as json]
            [nomakler.db.core :as db]
            [buddy.auth :refer [authenticated? throw-unauthorized]]))


(defn home-page [req]
  (let [context_data (json/write-str (h/context-data req)
                                     :key-fn name)]
    (layout/render "home.html"
                   {:context_data context_data})))


(defn user-logout [req]
  (let [session (:session req)
        user_id (:identity session)
	user (db/get-user {:id user_id})]
    (-> (redirect "/")
        (assoc :session {}))))


(defn vk-callback [code req]
  (let [get_token_url (format "https://oauth.vk.com/access_token?client_id=4230443&client_secret=OUV5eaSkjC9QDtGs7Yu0&redirect_uri=http://localhost:3000/vk-callback&code=%s"
                              code)
        vk_resp ((http/get get_token_url {:as :json}) :body)
        access_token (vk_resp :access_token)
        vk_user_id (str (vk_resp :user_id))
        session (:session req)
        user_info (first ((get-vk-user-info access_token) :response))
        xxx (println user_info)
        user_name (clojure.string/join " "
	                               [(user_info :first-name)
				        (user_info :last-name)])
	user (get-or-create-user vk_user_id user_name)]
    (do
      (println (str "VK User data: " vk_user_id " " user_name))
      (-> (redirect "/")
          (assoc :session
	         (assoc session
		        :identity (:id user)
			:username (:name user)))))))


(defroutes home-routes
  (GET "/" [ :as req] (home-page req))
  (GET "/user/logout/" [ :as req] (user-logout req))
  (GET "/vk-callback" [code :as req] (vk-callback code req)))
