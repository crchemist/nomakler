CREATE SEQUENCE users_id_seq;
alter table users alter column id SET default nextval('users_id_seq');
ALTER SEQUENCE users_id_seq OWNED BY users.id;
