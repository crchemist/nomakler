CREATE TABLE users
(id VARCHAR(20) PRIMARY KEY,
 name VARCHAR(100),
 admin BOOLEAN,
 last_login TIME,
 is_active BOOLEAN);
