CREATE TABLE appartment
(id SERIAL NOT NULL PRIMARY KEY,
 description TEXT,
 price NUMERIC,
 location GEOGRAPHY(POINT,4326),
 created_at TIMESTAMP NOT NULL DEFAULT (now() AT TIME ZONE 'utc'),
 renewed_date TIMESTAMP NOT NULL DEFAULT (now() AT TIME ZONE 'utc'),
 owner_id BIGINT REFERENCES users(id));

