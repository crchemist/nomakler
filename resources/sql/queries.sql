-- name: create-user!
-- creates a new user record
INSERT INTO users
(vk_id, name)
VALUES (:vk_id, :name)

-- name: update-user!
-- update an existing user record
UPDATE users
SET name = :name
WHERE id = :id

-- name: get-user
-- retrieve a user given the id.
SELECT * FROM users
WHERE id = :id

-- name: delete-user!
-- delete a user given the id
DELETE FROM users
WHERE id = :id


-- name: get-vk-user
-- retrieve a user given the vk_id.
SELECT * FROM users
WHERE vk_id = :vk_id

-- name: appartment-create!
-- creates a new appartment record
INSERT INTO appartments (description, price, location, owner_id)
VALUES (:description, :price, st_geomfromtext(:location), :owner_id)


-- name: appartments-list
-- retrive all apparments
SELECT description, price, ST_AsText(location) AS location, owner_id, created_at, renewed_date
FROM appartments
