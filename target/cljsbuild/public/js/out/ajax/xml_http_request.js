// Compiled by ClojureScript 1.7.170 {}
goog.provide('ajax.xml_http_request');
goog.require('cljs.core');
goog.require('ajax.protocols');
ajax.xml_http_request.ready_state = (function ajax$xml_http_request$ready_state(e){
return new cljs.core.PersistentArrayMap(null, 5, [(0),new cljs.core.Keyword(null,"not-initialized","not-initialized",-1937378906),(1),new cljs.core.Keyword(null,"connection-established","connection-established",-1403749733),(2),new cljs.core.Keyword(null,"request-received","request-received",2110590540),(3),new cljs.core.Keyword(null,"processing-request","processing-request",-264947221),(4),new cljs.core.Keyword(null,"response-ready","response-ready",245208276)], null).call(null,e.target.readyState);
});
XMLHttpRequest.prototype.ajax$protocols$AjaxImpl$ = true;

XMLHttpRequest.prototype.ajax$protocols$AjaxImpl$_js_ajax_request$arity$3 = (function (this$,p__25009,handler){
var map__25010 = p__25009;
var map__25010__$1 = ((((!((map__25010 == null)))?((((map__25010.cljs$lang$protocol_mask$partition0$ & (64))) || (map__25010.cljs$core$ISeq$))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__25010):map__25010);
var uri = cljs.core.get.call(null,map__25010__$1,new cljs.core.Keyword(null,"uri","uri",-774711847));
var method = cljs.core.get.call(null,map__25010__$1,new cljs.core.Keyword(null,"method","method",55703592));
var body = cljs.core.get.call(null,map__25010__$1,new cljs.core.Keyword(null,"body","body",-2049205669));
var headers = cljs.core.get.call(null,map__25010__$1,new cljs.core.Keyword(null,"headers","headers",-835030129));
var timeout = cljs.core.get.call(null,map__25010__$1,new cljs.core.Keyword(null,"timeout","timeout",-318625318),(0));
var with_credentials = cljs.core.get.call(null,map__25010__$1,new cljs.core.Keyword(null,"with-credentials","with-credentials",-1163127235),false);
var response_format = cljs.core.get.call(null,map__25010__$1,new cljs.core.Keyword(null,"response-format","response-format",1664465322));
var this$__$1 = this;
this$__$1.withCredentials = with_credentials;

this$__$1.onreadystatechange = ((function (this$__$1,map__25010,map__25010__$1,uri,method,body,headers,timeout,with_credentials,response_format){
return (function (p1__25008_SHARP_){
if(cljs.core._EQ_.call(null,new cljs.core.Keyword(null,"response-ready","response-ready",245208276),ajax.xml_http_request.ready_state.call(null,p1__25008_SHARP_))){
return handler.call(null,this$__$1);
} else {
return null;
}
});})(this$__$1,map__25010,map__25010__$1,uri,method,body,headers,timeout,with_credentials,response_format))
;

this$__$1.open(method,uri,true);

this$__$1.timeout = timeout;

var temp__4657__auto___25018 = new cljs.core.Keyword(null,"type","type",1174270348).cljs$core$IFn$_invoke$arity$1(response_format);
if(cljs.core.truth_(temp__4657__auto___25018)){
var response_type_25019 = temp__4657__auto___25018;
this$__$1.responseType = cljs.core.name.call(null,response_type_25019);
} else {
}

var seq__25012_25020 = cljs.core.seq.call(null,headers);
var chunk__25013_25021 = null;
var count__25014_25022 = (0);
var i__25015_25023 = (0);
while(true){
if((i__25015_25023 < count__25014_25022)){
var vec__25016_25024 = cljs.core._nth.call(null,chunk__25013_25021,i__25015_25023);
var k_25025 = cljs.core.nth.call(null,vec__25016_25024,(0),null);
var v_25026 = cljs.core.nth.call(null,vec__25016_25024,(1),null);
this$__$1.setRequestHeader(k_25025,v_25026);

var G__25027 = seq__25012_25020;
var G__25028 = chunk__25013_25021;
var G__25029 = count__25014_25022;
var G__25030 = (i__25015_25023 + (1));
seq__25012_25020 = G__25027;
chunk__25013_25021 = G__25028;
count__25014_25022 = G__25029;
i__25015_25023 = G__25030;
continue;
} else {
var temp__4657__auto___25031 = cljs.core.seq.call(null,seq__25012_25020);
if(temp__4657__auto___25031){
var seq__25012_25032__$1 = temp__4657__auto___25031;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__25012_25032__$1)){
var c__23946__auto___25033 = cljs.core.chunk_first.call(null,seq__25012_25032__$1);
var G__25034 = cljs.core.chunk_rest.call(null,seq__25012_25032__$1);
var G__25035 = c__23946__auto___25033;
var G__25036 = cljs.core.count.call(null,c__23946__auto___25033);
var G__25037 = (0);
seq__25012_25020 = G__25034;
chunk__25013_25021 = G__25035;
count__25014_25022 = G__25036;
i__25015_25023 = G__25037;
continue;
} else {
var vec__25017_25038 = cljs.core.first.call(null,seq__25012_25032__$1);
var k_25039 = cljs.core.nth.call(null,vec__25017_25038,(0),null);
var v_25040 = cljs.core.nth.call(null,vec__25017_25038,(1),null);
this$__$1.setRequestHeader(k_25039,v_25040);

var G__25041 = cljs.core.next.call(null,seq__25012_25032__$1);
var G__25042 = null;
var G__25043 = (0);
var G__25044 = (0);
seq__25012_25020 = G__25041;
chunk__25013_25021 = G__25042;
count__25014_25022 = G__25043;
i__25015_25023 = G__25044;
continue;
}
} else {
}
}
break;
}

this$__$1.send((function (){var or__23143__auto__ = body;
if(cljs.core.truth_(or__23143__auto__)){
return or__23143__auto__;
} else {
return "";
}
})());

return this$__$1;
});

XMLHttpRequest.prototype.ajax$protocols$AjaxRequest$ = true;

XMLHttpRequest.prototype.ajax$protocols$AjaxRequest$_abort$arity$1 = (function (this$){
var this$__$1 = this;
return this$__$1.abort();
});

XMLHttpRequest.prototype.ajax$protocols$AjaxResponse$ = true;

XMLHttpRequest.prototype.ajax$protocols$AjaxResponse$_body$arity$1 = (function (this$){
var this$__$1 = this;
return this$__$1.response;
});

XMLHttpRequest.prototype.ajax$protocols$AjaxResponse$_status$arity$1 = (function (this$){
var this$__$1 = this;
return this$__$1.status;
});

XMLHttpRequest.prototype.ajax$protocols$AjaxResponse$_status_text$arity$1 = (function (this$){
var this$__$1 = this;
return this$__$1.statusText;
});

XMLHttpRequest.prototype.ajax$protocols$AjaxResponse$_get_response_header$arity$2 = (function (this$,header){
var this$__$1 = this;
return this$__$1.getResponseHeader(header);
});

XMLHttpRequest.prototype.ajax$protocols$AjaxResponse$_was_aborted$arity$1 = (function (this$){
var this$__$1 = this;
return cljs.core._EQ_.call(null,(0),this$__$1.readyState);
});

//# sourceMappingURL=xml_http_request.js.map