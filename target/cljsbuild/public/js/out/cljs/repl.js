// Compiled by ClojureScript 1.7.170 {}
goog.provide('cljs.repl');
goog.require('cljs.core');
cljs.repl.print_doc = (function cljs$repl$print_doc(m){
cljs.core.println.call(null,"-------------------------");

cljs.core.println.call(null,[cljs.core.str((function (){var temp__4657__auto__ = new cljs.core.Keyword(null,"ns","ns",441598760).cljs$core$IFn$_invoke$arity$1(m);
if(cljs.core.truth_(temp__4657__auto__)){
var ns = temp__4657__auto__;
return [cljs.core.str(ns),cljs.core.str("/")].join('');
} else {
return null;
}
})()),cljs.core.str(new cljs.core.Keyword(null,"name","name",1843675177).cljs$core$IFn$_invoke$arity$1(m))].join(''));

if(cljs.core.truth_(new cljs.core.Keyword(null,"protocol","protocol",652470118).cljs$core$IFn$_invoke$arity$1(m))){
cljs.core.println.call(null,"Protocol");
} else {
}

if(cljs.core.truth_(new cljs.core.Keyword(null,"forms","forms",2045992350).cljs$core$IFn$_invoke$arity$1(m))){
var seq__32471_32485 = cljs.core.seq.call(null,new cljs.core.Keyword(null,"forms","forms",2045992350).cljs$core$IFn$_invoke$arity$1(m));
var chunk__32472_32486 = null;
var count__32473_32487 = (0);
var i__32474_32488 = (0);
while(true){
if((i__32474_32488 < count__32473_32487)){
var f_32489 = cljs.core._nth.call(null,chunk__32472_32486,i__32474_32488);
cljs.core.println.call(null,"  ",f_32489);

var G__32490 = seq__32471_32485;
var G__32491 = chunk__32472_32486;
var G__32492 = count__32473_32487;
var G__32493 = (i__32474_32488 + (1));
seq__32471_32485 = G__32490;
chunk__32472_32486 = G__32491;
count__32473_32487 = G__32492;
i__32474_32488 = G__32493;
continue;
} else {
var temp__4657__auto___32494 = cljs.core.seq.call(null,seq__32471_32485);
if(temp__4657__auto___32494){
var seq__32471_32495__$1 = temp__4657__auto___32494;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__32471_32495__$1)){
var c__23946__auto___32496 = cljs.core.chunk_first.call(null,seq__32471_32495__$1);
var G__32497 = cljs.core.chunk_rest.call(null,seq__32471_32495__$1);
var G__32498 = c__23946__auto___32496;
var G__32499 = cljs.core.count.call(null,c__23946__auto___32496);
var G__32500 = (0);
seq__32471_32485 = G__32497;
chunk__32472_32486 = G__32498;
count__32473_32487 = G__32499;
i__32474_32488 = G__32500;
continue;
} else {
var f_32501 = cljs.core.first.call(null,seq__32471_32495__$1);
cljs.core.println.call(null,"  ",f_32501);

var G__32502 = cljs.core.next.call(null,seq__32471_32495__$1);
var G__32503 = null;
var G__32504 = (0);
var G__32505 = (0);
seq__32471_32485 = G__32502;
chunk__32472_32486 = G__32503;
count__32473_32487 = G__32504;
i__32474_32488 = G__32505;
continue;
}
} else {
}
}
break;
}
} else {
if(cljs.core.truth_(new cljs.core.Keyword(null,"arglists","arglists",1661989754).cljs$core$IFn$_invoke$arity$1(m))){
var arglists_32506 = new cljs.core.Keyword(null,"arglists","arglists",1661989754).cljs$core$IFn$_invoke$arity$1(m);
if(cljs.core.truth_((function (){var or__23143__auto__ = new cljs.core.Keyword(null,"macro","macro",-867863404).cljs$core$IFn$_invoke$arity$1(m);
if(cljs.core.truth_(or__23143__auto__)){
return or__23143__auto__;
} else {
return new cljs.core.Keyword(null,"repl-special-function","repl-special-function",1262603725).cljs$core$IFn$_invoke$arity$1(m);
}
})())){
cljs.core.prn.call(null,arglists_32506);
} else {
cljs.core.prn.call(null,((cljs.core._EQ_.call(null,new cljs.core.Symbol(null,"quote","quote",1377916282,null),cljs.core.first.call(null,arglists_32506)))?cljs.core.second.call(null,arglists_32506):arglists_32506));
}
} else {
}
}

if(cljs.core.truth_(new cljs.core.Keyword(null,"special-form","special-form",-1326536374).cljs$core$IFn$_invoke$arity$1(m))){
cljs.core.println.call(null,"Special Form");

cljs.core.println.call(null," ",new cljs.core.Keyword(null,"doc","doc",1913296891).cljs$core$IFn$_invoke$arity$1(m));

if(cljs.core.contains_QMARK_.call(null,m,new cljs.core.Keyword(null,"url","url",276297046))){
if(cljs.core.truth_(new cljs.core.Keyword(null,"url","url",276297046).cljs$core$IFn$_invoke$arity$1(m))){
return cljs.core.println.call(null,[cljs.core.str("\n  Please see http://clojure.org/"),cljs.core.str(new cljs.core.Keyword(null,"url","url",276297046).cljs$core$IFn$_invoke$arity$1(m))].join(''));
} else {
return null;
}
} else {
return cljs.core.println.call(null,[cljs.core.str("\n  Please see http://clojure.org/special_forms#"),cljs.core.str(new cljs.core.Keyword(null,"name","name",1843675177).cljs$core$IFn$_invoke$arity$1(m))].join(''));
}
} else {
if(cljs.core.truth_(new cljs.core.Keyword(null,"macro","macro",-867863404).cljs$core$IFn$_invoke$arity$1(m))){
cljs.core.println.call(null,"Macro");
} else {
}

if(cljs.core.truth_(new cljs.core.Keyword(null,"repl-special-function","repl-special-function",1262603725).cljs$core$IFn$_invoke$arity$1(m))){
cljs.core.println.call(null,"REPL Special Function");
} else {
}

cljs.core.println.call(null," ",new cljs.core.Keyword(null,"doc","doc",1913296891).cljs$core$IFn$_invoke$arity$1(m));

if(cljs.core.truth_(new cljs.core.Keyword(null,"protocol","protocol",652470118).cljs$core$IFn$_invoke$arity$1(m))){
var seq__32475 = cljs.core.seq.call(null,new cljs.core.Keyword(null,"methods","methods",453930866).cljs$core$IFn$_invoke$arity$1(m));
var chunk__32476 = null;
var count__32477 = (0);
var i__32478 = (0);
while(true){
if((i__32478 < count__32477)){
var vec__32479 = cljs.core._nth.call(null,chunk__32476,i__32478);
var name = cljs.core.nth.call(null,vec__32479,(0),null);
var map__32480 = cljs.core.nth.call(null,vec__32479,(1),null);
var map__32480__$1 = ((((!((map__32480 == null)))?((((map__32480.cljs$lang$protocol_mask$partition0$ & (64))) || (map__32480.cljs$core$ISeq$))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__32480):map__32480);
var doc = cljs.core.get.call(null,map__32480__$1,new cljs.core.Keyword(null,"doc","doc",1913296891));
var arglists = cljs.core.get.call(null,map__32480__$1,new cljs.core.Keyword(null,"arglists","arglists",1661989754));
cljs.core.println.call(null);

cljs.core.println.call(null," ",name);

cljs.core.println.call(null," ",arglists);

if(cljs.core.truth_(doc)){
cljs.core.println.call(null," ",doc);
} else {
}

var G__32507 = seq__32475;
var G__32508 = chunk__32476;
var G__32509 = count__32477;
var G__32510 = (i__32478 + (1));
seq__32475 = G__32507;
chunk__32476 = G__32508;
count__32477 = G__32509;
i__32478 = G__32510;
continue;
} else {
var temp__4657__auto__ = cljs.core.seq.call(null,seq__32475);
if(temp__4657__auto__){
var seq__32475__$1 = temp__4657__auto__;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__32475__$1)){
var c__23946__auto__ = cljs.core.chunk_first.call(null,seq__32475__$1);
var G__32511 = cljs.core.chunk_rest.call(null,seq__32475__$1);
var G__32512 = c__23946__auto__;
var G__32513 = cljs.core.count.call(null,c__23946__auto__);
var G__32514 = (0);
seq__32475 = G__32511;
chunk__32476 = G__32512;
count__32477 = G__32513;
i__32478 = G__32514;
continue;
} else {
var vec__32482 = cljs.core.first.call(null,seq__32475__$1);
var name = cljs.core.nth.call(null,vec__32482,(0),null);
var map__32483 = cljs.core.nth.call(null,vec__32482,(1),null);
var map__32483__$1 = ((((!((map__32483 == null)))?((((map__32483.cljs$lang$protocol_mask$partition0$ & (64))) || (map__32483.cljs$core$ISeq$))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__32483):map__32483);
var doc = cljs.core.get.call(null,map__32483__$1,new cljs.core.Keyword(null,"doc","doc",1913296891));
var arglists = cljs.core.get.call(null,map__32483__$1,new cljs.core.Keyword(null,"arglists","arglists",1661989754));
cljs.core.println.call(null);

cljs.core.println.call(null," ",name);

cljs.core.println.call(null," ",arglists);

if(cljs.core.truth_(doc)){
cljs.core.println.call(null," ",doc);
} else {
}

var G__32515 = cljs.core.next.call(null,seq__32475__$1);
var G__32516 = null;
var G__32517 = (0);
var G__32518 = (0);
seq__32475 = G__32515;
chunk__32476 = G__32516;
count__32477 = G__32517;
i__32478 = G__32518;
continue;
}
} else {
return null;
}
}
break;
}
} else {
return null;
}
}
});

//# sourceMappingURL=repl.js.map