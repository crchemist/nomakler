// Compiled by ClojureScript 1.7.170 {}
goog.provide('cljs.core.async');
goog.require('cljs.core');
goog.require('cljs.core.async.impl.channels');
goog.require('cljs.core.async.impl.dispatch');
goog.require('cljs.core.async.impl.ioc_helpers');
goog.require('cljs.core.async.impl.protocols');
goog.require('cljs.core.async.impl.buffers');
goog.require('cljs.core.async.impl.timers');
cljs.core.async.fn_handler = (function cljs$core$async$fn_handler(var_args){
var args28534 = [];
var len__24201__auto___28540 = arguments.length;
var i__24202__auto___28541 = (0);
while(true){
if((i__24202__auto___28541 < len__24201__auto___28540)){
args28534.push((arguments[i__24202__auto___28541]));

var G__28542 = (i__24202__auto___28541 + (1));
i__24202__auto___28541 = G__28542;
continue;
} else {
}
break;
}

var G__28536 = args28534.length;
switch (G__28536) {
case 1:
return cljs.core.async.fn_handler.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return cljs.core.async.fn_handler.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error([cljs.core.str("Invalid arity: "),cljs.core.str(args28534.length)].join('')));

}
});

cljs.core.async.fn_handler.cljs$core$IFn$_invoke$arity$1 = (function (f){
return cljs.core.async.fn_handler.call(null,f,true);
});

cljs.core.async.fn_handler.cljs$core$IFn$_invoke$arity$2 = (function (f,blockable){
if(typeof cljs.core.async.t_cljs$core$async28537 !== 'undefined'){
} else {

/**
* @constructor
 * @implements {cljs.core.async.impl.protocols.Handler}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async28537 = (function (f,blockable,meta28538){
this.f = f;
this.blockable = blockable;
this.meta28538 = meta28538;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
})
cljs.core.async.t_cljs$core$async28537.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_28539,meta28538__$1){
var self__ = this;
var _28539__$1 = this;
return (new cljs.core.async.t_cljs$core$async28537(self__.f,self__.blockable,meta28538__$1));
});

cljs.core.async.t_cljs$core$async28537.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_28539){
var self__ = this;
var _28539__$1 = this;
return self__.meta28538;
});

cljs.core.async.t_cljs$core$async28537.prototype.cljs$core$async$impl$protocols$Handler$ = true;

cljs.core.async.t_cljs$core$async28537.prototype.cljs$core$async$impl$protocols$Handler$active_QMARK_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return true;
});

cljs.core.async.t_cljs$core$async28537.prototype.cljs$core$async$impl$protocols$Handler$blockable_QMARK_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return self__.blockable;
});

cljs.core.async.t_cljs$core$async28537.prototype.cljs$core$async$impl$protocols$Handler$commit$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return self__.f;
});

cljs.core.async.t_cljs$core$async28537.getBasis = (function (){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"f","f",43394975,null),new cljs.core.Symbol(null,"blockable","blockable",-28395259,null),new cljs.core.Symbol(null,"meta28538","meta28538",-730959098,null)], null);
});

cljs.core.async.t_cljs$core$async28537.cljs$lang$type = true;

cljs.core.async.t_cljs$core$async28537.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async28537";

cljs.core.async.t_cljs$core$async28537.cljs$lang$ctorPrWriter = (function (this__23741__auto__,writer__23742__auto__,opt__23743__auto__){
return cljs.core._write.call(null,writer__23742__auto__,"cljs.core.async/t_cljs$core$async28537");
});

cljs.core.async.__GT_t_cljs$core$async28537 = (function cljs$core$async$__GT_t_cljs$core$async28537(f__$1,blockable__$1,meta28538){
return (new cljs.core.async.t_cljs$core$async28537(f__$1,blockable__$1,meta28538));
});

}

return (new cljs.core.async.t_cljs$core$async28537(f,blockable,cljs.core.PersistentArrayMap.EMPTY));
});

cljs.core.async.fn_handler.cljs$lang$maxFixedArity = 2;
/**
 * Returns a fixed buffer of size n. When full, puts will block/park.
 */
cljs.core.async.buffer = (function cljs$core$async$buffer(n){
return cljs.core.async.impl.buffers.fixed_buffer.call(null,n);
});
/**
 * Returns a buffer of size n. When full, puts will complete but
 *   val will be dropped (no transfer).
 */
cljs.core.async.dropping_buffer = (function cljs$core$async$dropping_buffer(n){
return cljs.core.async.impl.buffers.dropping_buffer.call(null,n);
});
/**
 * Returns a buffer of size n. When full, puts will complete, and be
 *   buffered, but oldest elements in buffer will be dropped (not
 *   transferred).
 */
cljs.core.async.sliding_buffer = (function cljs$core$async$sliding_buffer(n){
return cljs.core.async.impl.buffers.sliding_buffer.call(null,n);
});
/**
 * Returns true if a channel created with buff will never block. That is to say,
 * puts into this buffer will never cause the buffer to be full. 
 */
cljs.core.async.unblocking_buffer_QMARK_ = (function cljs$core$async$unblocking_buffer_QMARK_(buff){
if(!((buff == null))){
if((false) || (buff.cljs$core$async$impl$protocols$UnblockingBuffer$)){
return true;
} else {
if((!buff.cljs$lang$protocol_mask$partition$)){
return cljs.core.native_satisfies_QMARK_.call(null,cljs.core.async.impl.protocols.UnblockingBuffer,buff);
} else {
return false;
}
}
} else {
return cljs.core.native_satisfies_QMARK_.call(null,cljs.core.async.impl.protocols.UnblockingBuffer,buff);
}
});
/**
 * Creates a channel with an optional buffer, an optional transducer (like (map f),
 *   (filter p) etc or a composition thereof), and an optional exception handler.
 *   If buf-or-n is a number, will create and use a fixed buffer of that size. If a
 *   transducer is supplied a buffer must be specified. ex-handler must be a
 *   fn of one argument - if an exception occurs during transformation it will be called
 *   with the thrown value as an argument, and any non-nil return value will be placed
 *   in the channel.
 */
cljs.core.async.chan = (function cljs$core$async$chan(var_args){
var args28546 = [];
var len__24201__auto___28549 = arguments.length;
var i__24202__auto___28550 = (0);
while(true){
if((i__24202__auto___28550 < len__24201__auto___28549)){
args28546.push((arguments[i__24202__auto___28550]));

var G__28551 = (i__24202__auto___28550 + (1));
i__24202__auto___28550 = G__28551;
continue;
} else {
}
break;
}

var G__28548 = args28546.length;
switch (G__28548) {
case 0:
return cljs.core.async.chan.cljs$core$IFn$_invoke$arity$0();

break;
case 1:
return cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return cljs.core.async.chan.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.chan.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error([cljs.core.str("Invalid arity: "),cljs.core.str(args28546.length)].join('')));

}
});

cljs.core.async.chan.cljs$core$IFn$_invoke$arity$0 = (function (){
return cljs.core.async.chan.call(null,null);
});

cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1 = (function (buf_or_n){
return cljs.core.async.chan.call(null,buf_or_n,null,null);
});

cljs.core.async.chan.cljs$core$IFn$_invoke$arity$2 = (function (buf_or_n,xform){
return cljs.core.async.chan.call(null,buf_or_n,xform,null);
});

cljs.core.async.chan.cljs$core$IFn$_invoke$arity$3 = (function (buf_or_n,xform,ex_handler){
var buf_or_n__$1 = ((cljs.core._EQ_.call(null,buf_or_n,(0)))?null:buf_or_n);
if(cljs.core.truth_(xform)){
if(cljs.core.truth_(buf_or_n__$1)){
} else {
throw (new Error([cljs.core.str("Assert failed: "),cljs.core.str("buffer must be supplied when transducer is"),cljs.core.str("\n"),cljs.core.str(cljs.core.pr_str.call(null,new cljs.core.Symbol(null,"buf-or-n","buf-or-n",-1646815050,null)))].join('')));
}
} else {
}

return cljs.core.async.impl.channels.chan.call(null,((typeof buf_or_n__$1 === 'number')?cljs.core.async.buffer.call(null,buf_or_n__$1):buf_or_n__$1),xform,ex_handler);
});

cljs.core.async.chan.cljs$lang$maxFixedArity = 3;
/**
 * Creates a promise channel with an optional transducer, and an optional
 *   exception-handler. A promise channel can take exactly one value that consumers
 *   will receive. Once full, puts complete but val is dropped (no transfer).
 *   Consumers will block until either a value is placed in the channel or the
 *   channel is closed. See chan for the semantics of xform and ex-handler.
 */
cljs.core.async.promise_chan = (function cljs$core$async$promise_chan(var_args){
var args28553 = [];
var len__24201__auto___28556 = arguments.length;
var i__24202__auto___28557 = (0);
while(true){
if((i__24202__auto___28557 < len__24201__auto___28556)){
args28553.push((arguments[i__24202__auto___28557]));

var G__28558 = (i__24202__auto___28557 + (1));
i__24202__auto___28557 = G__28558;
continue;
} else {
}
break;
}

var G__28555 = args28553.length;
switch (G__28555) {
case 0:
return cljs.core.async.promise_chan.cljs$core$IFn$_invoke$arity$0();

break;
case 1:
return cljs.core.async.promise_chan.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return cljs.core.async.promise_chan.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error([cljs.core.str("Invalid arity: "),cljs.core.str(args28553.length)].join('')));

}
});

cljs.core.async.promise_chan.cljs$core$IFn$_invoke$arity$0 = (function (){
return cljs.core.async.promise_chan.call(null,null);
});

cljs.core.async.promise_chan.cljs$core$IFn$_invoke$arity$1 = (function (xform){
return cljs.core.async.promise_chan.call(null,xform,null);
});

cljs.core.async.promise_chan.cljs$core$IFn$_invoke$arity$2 = (function (xform,ex_handler){
return cljs.core.async.chan.call(null,cljs.core.async.impl.buffers.promise_buffer.call(null),xform,ex_handler);
});

cljs.core.async.promise_chan.cljs$lang$maxFixedArity = 2;
/**
 * Returns a channel that will close after msecs
 */
cljs.core.async.timeout = (function cljs$core$async$timeout(msecs){
return cljs.core.async.impl.timers.timeout.call(null,msecs);
});
/**
 * takes a val from port. Must be called inside a (go ...) block. Will
 *   return nil if closed. Will park if nothing is available.
 *   Returns true unless port is already closed
 */
cljs.core.async._LT__BANG_ = (function cljs$core$async$_LT__BANG_(port){
throw (new Error("<! used not in (go ...) block"));
});
/**
 * Asynchronously takes a val from port, passing to fn1. Will pass nil
 * if closed. If on-caller? (default true) is true, and value is
 * immediately available, will call fn1 on calling thread.
 * Returns nil.
 */
cljs.core.async.take_BANG_ = (function cljs$core$async$take_BANG_(var_args){
var args28560 = [];
var len__24201__auto___28563 = arguments.length;
var i__24202__auto___28564 = (0);
while(true){
if((i__24202__auto___28564 < len__24201__auto___28563)){
args28560.push((arguments[i__24202__auto___28564]));

var G__28565 = (i__24202__auto___28564 + (1));
i__24202__auto___28564 = G__28565;
continue;
} else {
}
break;
}

var G__28562 = args28560.length;
switch (G__28562) {
case 2:
return cljs.core.async.take_BANG_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.take_BANG_.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error([cljs.core.str("Invalid arity: "),cljs.core.str(args28560.length)].join('')));

}
});

cljs.core.async.take_BANG_.cljs$core$IFn$_invoke$arity$2 = (function (port,fn1){
return cljs.core.async.take_BANG_.call(null,port,fn1,true);
});

cljs.core.async.take_BANG_.cljs$core$IFn$_invoke$arity$3 = (function (port,fn1,on_caller_QMARK_){
var ret = cljs.core.async.impl.protocols.take_BANG_.call(null,port,cljs.core.async.fn_handler.call(null,fn1));
if(cljs.core.truth_(ret)){
var val_28567 = cljs.core.deref.call(null,ret);
if(cljs.core.truth_(on_caller_QMARK_)){
fn1.call(null,val_28567);
} else {
cljs.core.async.impl.dispatch.run.call(null,((function (val_28567,ret){
return (function (){
return fn1.call(null,val_28567);
});})(val_28567,ret))
);
}
} else {
}

return null;
});

cljs.core.async.take_BANG_.cljs$lang$maxFixedArity = 3;
cljs.core.async.nop = (function cljs$core$async$nop(_){
return null;
});
cljs.core.async.fhnop = cljs.core.async.fn_handler.call(null,cljs.core.async.nop);
/**
 * puts a val into port. nil values are not allowed. Must be called
 *   inside a (go ...) block. Will park if no buffer space is available.
 *   Returns true unless port is already closed.
 */
cljs.core.async._GT__BANG_ = (function cljs$core$async$_GT__BANG_(port,val){
throw (new Error(">! used not in (go ...) block"));
});
/**
 * Asynchronously puts a val into port, calling fn0 (if supplied) when
 * complete. nil values are not allowed. Will throw if closed. If
 * on-caller? (default true) is true, and the put is immediately
 * accepted, will call fn0 on calling thread.  Returns nil.
 */
cljs.core.async.put_BANG_ = (function cljs$core$async$put_BANG_(var_args){
var args28568 = [];
var len__24201__auto___28571 = arguments.length;
var i__24202__auto___28572 = (0);
while(true){
if((i__24202__auto___28572 < len__24201__auto___28571)){
args28568.push((arguments[i__24202__auto___28572]));

var G__28573 = (i__24202__auto___28572 + (1));
i__24202__auto___28572 = G__28573;
continue;
} else {
}
break;
}

var G__28570 = args28568.length;
switch (G__28570) {
case 2:
return cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
case 4:
return cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$4((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]));

break;
default:
throw (new Error([cljs.core.str("Invalid arity: "),cljs.core.str(args28568.length)].join('')));

}
});

cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$2 = (function (port,val){
var temp__4655__auto__ = cljs.core.async.impl.protocols.put_BANG_.call(null,port,val,cljs.core.async.fhnop);
if(cljs.core.truth_(temp__4655__auto__)){
var ret = temp__4655__auto__;
return cljs.core.deref.call(null,ret);
} else {
return true;
}
});

cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$3 = (function (port,val,fn1){
return cljs.core.async.put_BANG_.call(null,port,val,fn1,true);
});

cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$4 = (function (port,val,fn1,on_caller_QMARK_){
var temp__4655__auto__ = cljs.core.async.impl.protocols.put_BANG_.call(null,port,val,cljs.core.async.fn_handler.call(null,fn1));
if(cljs.core.truth_(temp__4655__auto__)){
var retb = temp__4655__auto__;
var ret = cljs.core.deref.call(null,retb);
if(cljs.core.truth_(on_caller_QMARK_)){
fn1.call(null,ret);
} else {
cljs.core.async.impl.dispatch.run.call(null,((function (ret,retb,temp__4655__auto__){
return (function (){
return fn1.call(null,ret);
});})(ret,retb,temp__4655__auto__))
);
}

return ret;
} else {
return true;
}
});

cljs.core.async.put_BANG_.cljs$lang$maxFixedArity = 4;
cljs.core.async.close_BANG_ = (function cljs$core$async$close_BANG_(port){
return cljs.core.async.impl.protocols.close_BANG_.call(null,port);
});
cljs.core.async.random_array = (function cljs$core$async$random_array(n){
var a = (new Array(n));
var n__24046__auto___28575 = n;
var x_28576 = (0);
while(true){
if((x_28576 < n__24046__auto___28575)){
(a[x_28576] = (0));

var G__28577 = (x_28576 + (1));
x_28576 = G__28577;
continue;
} else {
}
break;
}

var i = (1);
while(true){
if(cljs.core._EQ_.call(null,i,n)){
return a;
} else {
var j = cljs.core.rand_int.call(null,i);
(a[i] = (a[j]));

(a[j] = i);

var G__28578 = (i + (1));
i = G__28578;
continue;
}
break;
}
});
cljs.core.async.alt_flag = (function cljs$core$async$alt_flag(){
var flag = cljs.core.atom.call(null,true);
if(typeof cljs.core.async.t_cljs$core$async28582 !== 'undefined'){
} else {

/**
* @constructor
 * @implements {cljs.core.async.impl.protocols.Handler}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async28582 = (function (alt_flag,flag,meta28583){
this.alt_flag = alt_flag;
this.flag = flag;
this.meta28583 = meta28583;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
})
cljs.core.async.t_cljs$core$async28582.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = ((function (flag){
return (function (_28584,meta28583__$1){
var self__ = this;
var _28584__$1 = this;
return (new cljs.core.async.t_cljs$core$async28582(self__.alt_flag,self__.flag,meta28583__$1));
});})(flag))
;

cljs.core.async.t_cljs$core$async28582.prototype.cljs$core$IMeta$_meta$arity$1 = ((function (flag){
return (function (_28584){
var self__ = this;
var _28584__$1 = this;
return self__.meta28583;
});})(flag))
;

cljs.core.async.t_cljs$core$async28582.prototype.cljs$core$async$impl$protocols$Handler$ = true;

cljs.core.async.t_cljs$core$async28582.prototype.cljs$core$async$impl$protocols$Handler$active_QMARK_$arity$1 = ((function (flag){
return (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.deref.call(null,self__.flag);
});})(flag))
;

cljs.core.async.t_cljs$core$async28582.prototype.cljs$core$async$impl$protocols$Handler$blockable_QMARK_$arity$1 = ((function (flag){
return (function (_){
var self__ = this;
var ___$1 = this;
return true;
});})(flag))
;

cljs.core.async.t_cljs$core$async28582.prototype.cljs$core$async$impl$protocols$Handler$commit$arity$1 = ((function (flag){
return (function (_){
var self__ = this;
var ___$1 = this;
cljs.core.reset_BANG_.call(null,self__.flag,null);

return true;
});})(flag))
;

cljs.core.async.t_cljs$core$async28582.getBasis = ((function (flag){
return (function (){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.with_meta(new cljs.core.Symbol(null,"alt-flag","alt-flag",-1794972754,null),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"private","private",-558947994),true,new cljs.core.Keyword(null,"arglists","arglists",1661989754),cljs.core.list(new cljs.core.Symbol(null,"quote","quote",1377916282,null),cljs.core.list(cljs.core.PersistentVector.EMPTY))], null)),new cljs.core.Symbol(null,"flag","flag",-1565787888,null),new cljs.core.Symbol(null,"meta28583","meta28583",239408109,null)], null);
});})(flag))
;

cljs.core.async.t_cljs$core$async28582.cljs$lang$type = true;

cljs.core.async.t_cljs$core$async28582.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async28582";

cljs.core.async.t_cljs$core$async28582.cljs$lang$ctorPrWriter = ((function (flag){
return (function (this__23741__auto__,writer__23742__auto__,opt__23743__auto__){
return cljs.core._write.call(null,writer__23742__auto__,"cljs.core.async/t_cljs$core$async28582");
});})(flag))
;

cljs.core.async.__GT_t_cljs$core$async28582 = ((function (flag){
return (function cljs$core$async$alt_flag_$___GT_t_cljs$core$async28582(alt_flag__$1,flag__$1,meta28583){
return (new cljs.core.async.t_cljs$core$async28582(alt_flag__$1,flag__$1,meta28583));
});})(flag))
;

}

return (new cljs.core.async.t_cljs$core$async28582(cljs$core$async$alt_flag,flag,cljs.core.PersistentArrayMap.EMPTY));
});
cljs.core.async.alt_handler = (function cljs$core$async$alt_handler(flag,cb){
if(typeof cljs.core.async.t_cljs$core$async28588 !== 'undefined'){
} else {

/**
* @constructor
 * @implements {cljs.core.async.impl.protocols.Handler}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async28588 = (function (alt_handler,flag,cb,meta28589){
this.alt_handler = alt_handler;
this.flag = flag;
this.cb = cb;
this.meta28589 = meta28589;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
})
cljs.core.async.t_cljs$core$async28588.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_28590,meta28589__$1){
var self__ = this;
var _28590__$1 = this;
return (new cljs.core.async.t_cljs$core$async28588(self__.alt_handler,self__.flag,self__.cb,meta28589__$1));
});

cljs.core.async.t_cljs$core$async28588.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_28590){
var self__ = this;
var _28590__$1 = this;
return self__.meta28589;
});

cljs.core.async.t_cljs$core$async28588.prototype.cljs$core$async$impl$protocols$Handler$ = true;

cljs.core.async.t_cljs$core$async28588.prototype.cljs$core$async$impl$protocols$Handler$active_QMARK_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.active_QMARK_.call(null,self__.flag);
});

cljs.core.async.t_cljs$core$async28588.prototype.cljs$core$async$impl$protocols$Handler$blockable_QMARK_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return true;
});

cljs.core.async.t_cljs$core$async28588.prototype.cljs$core$async$impl$protocols$Handler$commit$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
cljs.core.async.impl.protocols.commit.call(null,self__.flag);

return self__.cb;
});

cljs.core.async.t_cljs$core$async28588.getBasis = (function (){
return new cljs.core.PersistentVector(null, 4, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.with_meta(new cljs.core.Symbol(null,"alt-handler","alt-handler",963786170,null),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"private","private",-558947994),true,new cljs.core.Keyword(null,"arglists","arglists",1661989754),cljs.core.list(new cljs.core.Symbol(null,"quote","quote",1377916282,null),cljs.core.list(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"flag","flag",-1565787888,null),new cljs.core.Symbol(null,"cb","cb",-2064487928,null)], null)))], null)),new cljs.core.Symbol(null,"flag","flag",-1565787888,null),new cljs.core.Symbol(null,"cb","cb",-2064487928,null),new cljs.core.Symbol(null,"meta28589","meta28589",-944164925,null)], null);
});

cljs.core.async.t_cljs$core$async28588.cljs$lang$type = true;

cljs.core.async.t_cljs$core$async28588.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async28588";

cljs.core.async.t_cljs$core$async28588.cljs$lang$ctorPrWriter = (function (this__23741__auto__,writer__23742__auto__,opt__23743__auto__){
return cljs.core._write.call(null,writer__23742__auto__,"cljs.core.async/t_cljs$core$async28588");
});

cljs.core.async.__GT_t_cljs$core$async28588 = (function cljs$core$async$alt_handler_$___GT_t_cljs$core$async28588(alt_handler__$1,flag__$1,cb__$1,meta28589){
return (new cljs.core.async.t_cljs$core$async28588(alt_handler__$1,flag__$1,cb__$1,meta28589));
});

}

return (new cljs.core.async.t_cljs$core$async28588(cljs$core$async$alt_handler,flag,cb,cljs.core.PersistentArrayMap.EMPTY));
});
/**
 * returns derefable [val port] if immediate, nil if enqueued
 */
cljs.core.async.do_alts = (function cljs$core$async$do_alts(fret,ports,opts){
var flag = cljs.core.async.alt_flag.call(null);
var n = cljs.core.count.call(null,ports);
var idxs = cljs.core.async.random_array.call(null,n);
var priority = new cljs.core.Keyword(null,"priority","priority",1431093715).cljs$core$IFn$_invoke$arity$1(opts);
var ret = (function (){var i = (0);
while(true){
if((i < n)){
var idx = (cljs.core.truth_(priority)?i:(idxs[i]));
var port = cljs.core.nth.call(null,ports,idx);
var wport = ((cljs.core.vector_QMARK_.call(null,port))?port.call(null,(0)):null);
var vbox = (cljs.core.truth_(wport)?(function (){var val = port.call(null,(1));
return cljs.core.async.impl.protocols.put_BANG_.call(null,wport,val,cljs.core.async.alt_handler.call(null,flag,((function (i,val,idx,port,wport,flag,n,idxs,priority){
return (function (p1__28591_SHARP_){
return fret.call(null,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [p1__28591_SHARP_,wport], null));
});})(i,val,idx,port,wport,flag,n,idxs,priority))
));
})():cljs.core.async.impl.protocols.take_BANG_.call(null,port,cljs.core.async.alt_handler.call(null,flag,((function (i,idx,port,wport,flag,n,idxs,priority){
return (function (p1__28592_SHARP_){
return fret.call(null,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [p1__28592_SHARP_,port], null));
});})(i,idx,port,wport,flag,n,idxs,priority))
)));
if(cljs.core.truth_(vbox)){
return cljs.core.async.impl.channels.box.call(null,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.deref.call(null,vbox),(function (){var or__23143__auto__ = wport;
if(cljs.core.truth_(or__23143__auto__)){
return or__23143__auto__;
} else {
return port;
}
})()], null));
} else {
var G__28593 = (i + (1));
i = G__28593;
continue;
}
} else {
return null;
}
break;
}
})();
var or__23143__auto__ = ret;
if(cljs.core.truth_(or__23143__auto__)){
return or__23143__auto__;
} else {
if(cljs.core.contains_QMARK_.call(null,opts,new cljs.core.Keyword(null,"default","default",-1987822328))){
var temp__4657__auto__ = (function (){var and__23131__auto__ = cljs.core.async.impl.protocols.active_QMARK_.call(null,flag);
if(cljs.core.truth_(and__23131__auto__)){
return cljs.core.async.impl.protocols.commit.call(null,flag);
} else {
return and__23131__auto__;
}
})();
if(cljs.core.truth_(temp__4657__auto__)){
var got = temp__4657__auto__;
return cljs.core.async.impl.channels.box.call(null,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"default","default",-1987822328).cljs$core$IFn$_invoke$arity$1(opts),new cljs.core.Keyword(null,"default","default",-1987822328)], null));
} else {
return null;
}
} else {
return null;
}
}
});
/**
 * Completes at most one of several channel operations. Must be called
 * inside a (go ...) block. ports is a vector of channel endpoints,
 * which can be either a channel to take from or a vector of
 *   [channel-to-put-to val-to-put], in any combination. Takes will be
 *   made as if by <!, and puts will be made as if by >!. Unless
 *   the :priority option is true, if more than one port operation is
 *   ready a non-deterministic choice will be made. If no operation is
 *   ready and a :default value is supplied, [default-val :default] will
 *   be returned, otherwise alts! will park until the first operation to
 *   become ready completes. Returns [val port] of the completed
 *   operation, where val is the value taken for takes, and a
 *   boolean (true unless already closed, as per put!) for puts.
 * 
 *   opts are passed as :key val ... Supported options:
 * 
 *   :default val - the value to use if none of the operations are immediately ready
 *   :priority true - (default nil) when true, the operations will be tried in order.
 * 
 *   Note: there is no guarantee that the port exps or val exprs will be
 *   used, nor in what order should they be, so they should not be
 *   depended upon for side effects.
 */
cljs.core.async.alts_BANG_ = (function cljs$core$async$alts_BANG_(var_args){
var args__24208__auto__ = [];
var len__24201__auto___28599 = arguments.length;
var i__24202__auto___28600 = (0);
while(true){
if((i__24202__auto___28600 < len__24201__auto___28599)){
args__24208__auto__.push((arguments[i__24202__auto___28600]));

var G__28601 = (i__24202__auto___28600 + (1));
i__24202__auto___28600 = G__28601;
continue;
} else {
}
break;
}

var argseq__24209__auto__ = ((((1) < args__24208__auto__.length))?(new cljs.core.IndexedSeq(args__24208__auto__.slice((1)),(0))):null);
return cljs.core.async.alts_BANG_.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),argseq__24209__auto__);
});

cljs.core.async.alts_BANG_.cljs$core$IFn$_invoke$arity$variadic = (function (ports,p__28596){
var map__28597 = p__28596;
var map__28597__$1 = ((((!((map__28597 == null)))?((((map__28597.cljs$lang$protocol_mask$partition0$ & (64))) || (map__28597.cljs$core$ISeq$))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__28597):map__28597);
var opts = map__28597__$1;
throw (new Error("alts! used not in (go ...) block"));
});

cljs.core.async.alts_BANG_.cljs$lang$maxFixedArity = (1);

cljs.core.async.alts_BANG_.cljs$lang$applyTo = (function (seq28594){
var G__28595 = cljs.core.first.call(null,seq28594);
var seq28594__$1 = cljs.core.next.call(null,seq28594);
return cljs.core.async.alts_BANG_.cljs$core$IFn$_invoke$arity$variadic(G__28595,seq28594__$1);
});
/**
 * Puts a val into port if it's possible to do so immediately.
 *   nil values are not allowed. Never blocks. Returns true if offer succeeds.
 */
cljs.core.async.offer_BANG_ = (function cljs$core$async$offer_BANG_(port,val){
var ret = cljs.core.async.impl.protocols.put_BANG_.call(null,port,val,cljs.core.async.fn_handler.call(null,cljs.core.async.nop,false));
if(cljs.core.truth_(ret)){
return cljs.core.deref.call(null,ret);
} else {
return null;
}
});
/**
 * Takes a val from port if it's possible to do so immediately.
 *   Never blocks. Returns value if successful, nil otherwise.
 */
cljs.core.async.poll_BANG_ = (function cljs$core$async$poll_BANG_(port){
var ret = cljs.core.async.impl.protocols.take_BANG_.call(null,port,cljs.core.async.fn_handler.call(null,cljs.core.async.nop,false));
if(cljs.core.truth_(ret)){
return cljs.core.deref.call(null,ret);
} else {
return null;
}
});
/**
 * Takes elements from the from channel and supplies them to the to
 * channel. By default, the to channel will be closed when the from
 * channel closes, but can be determined by the close?  parameter. Will
 * stop consuming the from channel if the to channel closes
 */
cljs.core.async.pipe = (function cljs$core$async$pipe(var_args){
var args28602 = [];
var len__24201__auto___28652 = arguments.length;
var i__24202__auto___28653 = (0);
while(true){
if((i__24202__auto___28653 < len__24201__auto___28652)){
args28602.push((arguments[i__24202__auto___28653]));

var G__28654 = (i__24202__auto___28653 + (1));
i__24202__auto___28653 = G__28654;
continue;
} else {
}
break;
}

var G__28604 = args28602.length;
switch (G__28604) {
case 2:
return cljs.core.async.pipe.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.pipe.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error([cljs.core.str("Invalid arity: "),cljs.core.str(args28602.length)].join('')));

}
});

cljs.core.async.pipe.cljs$core$IFn$_invoke$arity$2 = (function (from,to){
return cljs.core.async.pipe.call(null,from,to,true);
});

cljs.core.async.pipe.cljs$core$IFn$_invoke$arity$3 = (function (from,to,close_QMARK_){
var c__28489__auto___28656 = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__28489__auto___28656){
return (function (){
var f__28490__auto__ = (function (){var switch__28377__auto__ = ((function (c__28489__auto___28656){
return (function (state_28628){
var state_val_28629 = (state_28628[(1)]);
if((state_val_28629 === (7))){
var inst_28624 = (state_28628[(2)]);
var state_28628__$1 = state_28628;
var statearr_28630_28657 = state_28628__$1;
(statearr_28630_28657[(2)] = inst_28624);

(statearr_28630_28657[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_28629 === (1))){
var state_28628__$1 = state_28628;
var statearr_28631_28658 = state_28628__$1;
(statearr_28631_28658[(2)] = null);

(statearr_28631_28658[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_28629 === (4))){
var inst_28607 = (state_28628[(7)]);
var inst_28607__$1 = (state_28628[(2)]);
var inst_28608 = (inst_28607__$1 == null);
var state_28628__$1 = (function (){var statearr_28632 = state_28628;
(statearr_28632[(7)] = inst_28607__$1);

return statearr_28632;
})();
if(cljs.core.truth_(inst_28608)){
var statearr_28633_28659 = state_28628__$1;
(statearr_28633_28659[(1)] = (5));

} else {
var statearr_28634_28660 = state_28628__$1;
(statearr_28634_28660[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_28629 === (13))){
var state_28628__$1 = state_28628;
var statearr_28635_28661 = state_28628__$1;
(statearr_28635_28661[(2)] = null);

(statearr_28635_28661[(1)] = (14));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_28629 === (6))){
var inst_28607 = (state_28628[(7)]);
var state_28628__$1 = state_28628;
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_28628__$1,(11),to,inst_28607);
} else {
if((state_val_28629 === (3))){
var inst_28626 = (state_28628[(2)]);
var state_28628__$1 = state_28628;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_28628__$1,inst_28626);
} else {
if((state_val_28629 === (12))){
var state_28628__$1 = state_28628;
var statearr_28636_28662 = state_28628__$1;
(statearr_28636_28662[(2)] = null);

(statearr_28636_28662[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_28629 === (2))){
var state_28628__$1 = state_28628;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_28628__$1,(4),from);
} else {
if((state_val_28629 === (11))){
var inst_28617 = (state_28628[(2)]);
var state_28628__$1 = state_28628;
if(cljs.core.truth_(inst_28617)){
var statearr_28637_28663 = state_28628__$1;
(statearr_28637_28663[(1)] = (12));

} else {
var statearr_28638_28664 = state_28628__$1;
(statearr_28638_28664[(1)] = (13));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_28629 === (9))){
var state_28628__$1 = state_28628;
var statearr_28639_28665 = state_28628__$1;
(statearr_28639_28665[(2)] = null);

(statearr_28639_28665[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_28629 === (5))){
var state_28628__$1 = state_28628;
if(cljs.core.truth_(close_QMARK_)){
var statearr_28640_28666 = state_28628__$1;
(statearr_28640_28666[(1)] = (8));

} else {
var statearr_28641_28667 = state_28628__$1;
(statearr_28641_28667[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_28629 === (14))){
var inst_28622 = (state_28628[(2)]);
var state_28628__$1 = state_28628;
var statearr_28642_28668 = state_28628__$1;
(statearr_28642_28668[(2)] = inst_28622);

(statearr_28642_28668[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_28629 === (10))){
var inst_28614 = (state_28628[(2)]);
var state_28628__$1 = state_28628;
var statearr_28643_28669 = state_28628__$1;
(statearr_28643_28669[(2)] = inst_28614);

(statearr_28643_28669[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_28629 === (8))){
var inst_28611 = cljs.core.async.close_BANG_.call(null,to);
var state_28628__$1 = state_28628;
var statearr_28644_28670 = state_28628__$1;
(statearr_28644_28670[(2)] = inst_28611);

(statearr_28644_28670[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__28489__auto___28656))
;
return ((function (switch__28377__auto__,c__28489__auto___28656){
return (function() {
var cljs$core$async$state_machine__28378__auto__ = null;
var cljs$core$async$state_machine__28378__auto____0 = (function (){
var statearr_28648 = [null,null,null,null,null,null,null,null];
(statearr_28648[(0)] = cljs$core$async$state_machine__28378__auto__);

(statearr_28648[(1)] = (1));

return statearr_28648;
});
var cljs$core$async$state_machine__28378__auto____1 = (function (state_28628){
while(true){
var ret_value__28379__auto__ = (function (){try{while(true){
var result__28380__auto__ = switch__28377__auto__.call(null,state_28628);
if(cljs.core.keyword_identical_QMARK_.call(null,result__28380__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__28380__auto__;
}
break;
}
}catch (e28649){if((e28649 instanceof Object)){
var ex__28381__auto__ = e28649;
var statearr_28650_28671 = state_28628;
(statearr_28650_28671[(5)] = ex__28381__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_28628);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e28649;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__28379__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__28672 = state_28628;
state_28628 = G__28672;
continue;
} else {
return ret_value__28379__auto__;
}
break;
}
});
cljs$core$async$state_machine__28378__auto__ = function(state_28628){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__28378__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__28378__auto____1.call(this,state_28628);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__28378__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__28378__auto____0;
cljs$core$async$state_machine__28378__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__28378__auto____1;
return cljs$core$async$state_machine__28378__auto__;
})()
;})(switch__28377__auto__,c__28489__auto___28656))
})();
var state__28491__auto__ = (function (){var statearr_28651 = f__28490__auto__.call(null);
(statearr_28651[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__28489__auto___28656);

return statearr_28651;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__28491__auto__);
});})(c__28489__auto___28656))
);


return to;
});

cljs.core.async.pipe.cljs$lang$maxFixedArity = 3;
cljs.core.async.pipeline_STAR_ = (function cljs$core$async$pipeline_STAR_(n,to,xf,from,close_QMARK_,ex_handler,type){
if((n > (0))){
} else {
throw (new Error([cljs.core.str("Assert failed: "),cljs.core.str(cljs.core.pr_str.call(null,cljs.core.list(new cljs.core.Symbol(null,"pos?","pos?",-244377722,null),new cljs.core.Symbol(null,"n","n",-2092305744,null))))].join('')));
}

var jobs = cljs.core.async.chan.call(null,n);
var results = cljs.core.async.chan.call(null,n);
var process = ((function (jobs,results){
return (function (p__28856){
var vec__28857 = p__28856;
var v = cljs.core.nth.call(null,vec__28857,(0),null);
var p = cljs.core.nth.call(null,vec__28857,(1),null);
var job = vec__28857;
if((job == null)){
cljs.core.async.close_BANG_.call(null,results);

return null;
} else {
var res = cljs.core.async.chan.call(null,(1),xf,ex_handler);
var c__28489__auto___29039 = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__28489__auto___29039,res,vec__28857,v,p,job,jobs,results){
return (function (){
var f__28490__auto__ = (function (){var switch__28377__auto__ = ((function (c__28489__auto___29039,res,vec__28857,v,p,job,jobs,results){
return (function (state_28862){
var state_val_28863 = (state_28862[(1)]);
if((state_val_28863 === (1))){
var state_28862__$1 = state_28862;
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_28862__$1,(2),res,v);
} else {
if((state_val_28863 === (2))){
var inst_28859 = (state_28862[(2)]);
var inst_28860 = cljs.core.async.close_BANG_.call(null,res);
var state_28862__$1 = (function (){var statearr_28864 = state_28862;
(statearr_28864[(7)] = inst_28859);

return statearr_28864;
})();
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_28862__$1,inst_28860);
} else {
return null;
}
}
});})(c__28489__auto___29039,res,vec__28857,v,p,job,jobs,results))
;
return ((function (switch__28377__auto__,c__28489__auto___29039,res,vec__28857,v,p,job,jobs,results){
return (function() {
var cljs$core$async$pipeline_STAR__$_state_machine__28378__auto__ = null;
var cljs$core$async$pipeline_STAR__$_state_machine__28378__auto____0 = (function (){
var statearr_28868 = [null,null,null,null,null,null,null,null];
(statearr_28868[(0)] = cljs$core$async$pipeline_STAR__$_state_machine__28378__auto__);

(statearr_28868[(1)] = (1));

return statearr_28868;
});
var cljs$core$async$pipeline_STAR__$_state_machine__28378__auto____1 = (function (state_28862){
while(true){
var ret_value__28379__auto__ = (function (){try{while(true){
var result__28380__auto__ = switch__28377__auto__.call(null,state_28862);
if(cljs.core.keyword_identical_QMARK_.call(null,result__28380__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__28380__auto__;
}
break;
}
}catch (e28869){if((e28869 instanceof Object)){
var ex__28381__auto__ = e28869;
var statearr_28870_29040 = state_28862;
(statearr_28870_29040[(5)] = ex__28381__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_28862);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e28869;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__28379__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__29041 = state_28862;
state_28862 = G__29041;
continue;
} else {
return ret_value__28379__auto__;
}
break;
}
});
cljs$core$async$pipeline_STAR__$_state_machine__28378__auto__ = function(state_28862){
switch(arguments.length){
case 0:
return cljs$core$async$pipeline_STAR__$_state_machine__28378__auto____0.call(this);
case 1:
return cljs$core$async$pipeline_STAR__$_state_machine__28378__auto____1.call(this,state_28862);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$pipeline_STAR__$_state_machine__28378__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$pipeline_STAR__$_state_machine__28378__auto____0;
cljs$core$async$pipeline_STAR__$_state_machine__28378__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$pipeline_STAR__$_state_machine__28378__auto____1;
return cljs$core$async$pipeline_STAR__$_state_machine__28378__auto__;
})()
;})(switch__28377__auto__,c__28489__auto___29039,res,vec__28857,v,p,job,jobs,results))
})();
var state__28491__auto__ = (function (){var statearr_28871 = f__28490__auto__.call(null);
(statearr_28871[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__28489__auto___29039);

return statearr_28871;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__28491__auto__);
});})(c__28489__auto___29039,res,vec__28857,v,p,job,jobs,results))
);


cljs.core.async.put_BANG_.call(null,p,res);

return true;
}
});})(jobs,results))
;
var async = ((function (jobs,results,process){
return (function (p__28872){
var vec__28873 = p__28872;
var v = cljs.core.nth.call(null,vec__28873,(0),null);
var p = cljs.core.nth.call(null,vec__28873,(1),null);
var job = vec__28873;
if((job == null)){
cljs.core.async.close_BANG_.call(null,results);

return null;
} else {
var res = cljs.core.async.chan.call(null,(1));
xf.call(null,v,res);

cljs.core.async.put_BANG_.call(null,p,res);

return true;
}
});})(jobs,results,process))
;
var n__24046__auto___29042 = n;
var __29043 = (0);
while(true){
if((__29043 < n__24046__auto___29042)){
var G__28874_29044 = (((type instanceof cljs.core.Keyword))?type.fqn:null);
switch (G__28874_29044) {
case "compute":
var c__28489__auto___29046 = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (__29043,c__28489__auto___29046,G__28874_29044,n__24046__auto___29042,jobs,results,process,async){
return (function (){
var f__28490__auto__ = (function (){var switch__28377__auto__ = ((function (__29043,c__28489__auto___29046,G__28874_29044,n__24046__auto___29042,jobs,results,process,async){
return (function (state_28887){
var state_val_28888 = (state_28887[(1)]);
if((state_val_28888 === (1))){
var state_28887__$1 = state_28887;
var statearr_28889_29047 = state_28887__$1;
(statearr_28889_29047[(2)] = null);

(statearr_28889_29047[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_28888 === (2))){
var state_28887__$1 = state_28887;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_28887__$1,(4),jobs);
} else {
if((state_val_28888 === (3))){
var inst_28885 = (state_28887[(2)]);
var state_28887__$1 = state_28887;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_28887__$1,inst_28885);
} else {
if((state_val_28888 === (4))){
var inst_28877 = (state_28887[(2)]);
var inst_28878 = process.call(null,inst_28877);
var state_28887__$1 = state_28887;
if(cljs.core.truth_(inst_28878)){
var statearr_28890_29048 = state_28887__$1;
(statearr_28890_29048[(1)] = (5));

} else {
var statearr_28891_29049 = state_28887__$1;
(statearr_28891_29049[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_28888 === (5))){
var state_28887__$1 = state_28887;
var statearr_28892_29050 = state_28887__$1;
(statearr_28892_29050[(2)] = null);

(statearr_28892_29050[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_28888 === (6))){
var state_28887__$1 = state_28887;
var statearr_28893_29051 = state_28887__$1;
(statearr_28893_29051[(2)] = null);

(statearr_28893_29051[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_28888 === (7))){
var inst_28883 = (state_28887[(2)]);
var state_28887__$1 = state_28887;
var statearr_28894_29052 = state_28887__$1;
(statearr_28894_29052[(2)] = inst_28883);

(statearr_28894_29052[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
});})(__29043,c__28489__auto___29046,G__28874_29044,n__24046__auto___29042,jobs,results,process,async))
;
return ((function (__29043,switch__28377__auto__,c__28489__auto___29046,G__28874_29044,n__24046__auto___29042,jobs,results,process,async){
return (function() {
var cljs$core$async$pipeline_STAR__$_state_machine__28378__auto__ = null;
var cljs$core$async$pipeline_STAR__$_state_machine__28378__auto____0 = (function (){
var statearr_28898 = [null,null,null,null,null,null,null];
(statearr_28898[(0)] = cljs$core$async$pipeline_STAR__$_state_machine__28378__auto__);

(statearr_28898[(1)] = (1));

return statearr_28898;
});
var cljs$core$async$pipeline_STAR__$_state_machine__28378__auto____1 = (function (state_28887){
while(true){
var ret_value__28379__auto__ = (function (){try{while(true){
var result__28380__auto__ = switch__28377__auto__.call(null,state_28887);
if(cljs.core.keyword_identical_QMARK_.call(null,result__28380__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__28380__auto__;
}
break;
}
}catch (e28899){if((e28899 instanceof Object)){
var ex__28381__auto__ = e28899;
var statearr_28900_29053 = state_28887;
(statearr_28900_29053[(5)] = ex__28381__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_28887);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e28899;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__28379__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__29054 = state_28887;
state_28887 = G__29054;
continue;
} else {
return ret_value__28379__auto__;
}
break;
}
});
cljs$core$async$pipeline_STAR__$_state_machine__28378__auto__ = function(state_28887){
switch(arguments.length){
case 0:
return cljs$core$async$pipeline_STAR__$_state_machine__28378__auto____0.call(this);
case 1:
return cljs$core$async$pipeline_STAR__$_state_machine__28378__auto____1.call(this,state_28887);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$pipeline_STAR__$_state_machine__28378__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$pipeline_STAR__$_state_machine__28378__auto____0;
cljs$core$async$pipeline_STAR__$_state_machine__28378__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$pipeline_STAR__$_state_machine__28378__auto____1;
return cljs$core$async$pipeline_STAR__$_state_machine__28378__auto__;
})()
;})(__29043,switch__28377__auto__,c__28489__auto___29046,G__28874_29044,n__24046__auto___29042,jobs,results,process,async))
})();
var state__28491__auto__ = (function (){var statearr_28901 = f__28490__auto__.call(null);
(statearr_28901[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__28489__auto___29046);

return statearr_28901;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__28491__auto__);
});})(__29043,c__28489__auto___29046,G__28874_29044,n__24046__auto___29042,jobs,results,process,async))
);


break;
case "async":
var c__28489__auto___29055 = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (__29043,c__28489__auto___29055,G__28874_29044,n__24046__auto___29042,jobs,results,process,async){
return (function (){
var f__28490__auto__ = (function (){var switch__28377__auto__ = ((function (__29043,c__28489__auto___29055,G__28874_29044,n__24046__auto___29042,jobs,results,process,async){
return (function (state_28914){
var state_val_28915 = (state_28914[(1)]);
if((state_val_28915 === (1))){
var state_28914__$1 = state_28914;
var statearr_28916_29056 = state_28914__$1;
(statearr_28916_29056[(2)] = null);

(statearr_28916_29056[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_28915 === (2))){
var state_28914__$1 = state_28914;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_28914__$1,(4),jobs);
} else {
if((state_val_28915 === (3))){
var inst_28912 = (state_28914[(2)]);
var state_28914__$1 = state_28914;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_28914__$1,inst_28912);
} else {
if((state_val_28915 === (4))){
var inst_28904 = (state_28914[(2)]);
var inst_28905 = async.call(null,inst_28904);
var state_28914__$1 = state_28914;
if(cljs.core.truth_(inst_28905)){
var statearr_28917_29057 = state_28914__$1;
(statearr_28917_29057[(1)] = (5));

} else {
var statearr_28918_29058 = state_28914__$1;
(statearr_28918_29058[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_28915 === (5))){
var state_28914__$1 = state_28914;
var statearr_28919_29059 = state_28914__$1;
(statearr_28919_29059[(2)] = null);

(statearr_28919_29059[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_28915 === (6))){
var state_28914__$1 = state_28914;
var statearr_28920_29060 = state_28914__$1;
(statearr_28920_29060[(2)] = null);

(statearr_28920_29060[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_28915 === (7))){
var inst_28910 = (state_28914[(2)]);
var state_28914__$1 = state_28914;
var statearr_28921_29061 = state_28914__$1;
(statearr_28921_29061[(2)] = inst_28910);

(statearr_28921_29061[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
});})(__29043,c__28489__auto___29055,G__28874_29044,n__24046__auto___29042,jobs,results,process,async))
;
return ((function (__29043,switch__28377__auto__,c__28489__auto___29055,G__28874_29044,n__24046__auto___29042,jobs,results,process,async){
return (function() {
var cljs$core$async$pipeline_STAR__$_state_machine__28378__auto__ = null;
var cljs$core$async$pipeline_STAR__$_state_machine__28378__auto____0 = (function (){
var statearr_28925 = [null,null,null,null,null,null,null];
(statearr_28925[(0)] = cljs$core$async$pipeline_STAR__$_state_machine__28378__auto__);

(statearr_28925[(1)] = (1));

return statearr_28925;
});
var cljs$core$async$pipeline_STAR__$_state_machine__28378__auto____1 = (function (state_28914){
while(true){
var ret_value__28379__auto__ = (function (){try{while(true){
var result__28380__auto__ = switch__28377__auto__.call(null,state_28914);
if(cljs.core.keyword_identical_QMARK_.call(null,result__28380__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__28380__auto__;
}
break;
}
}catch (e28926){if((e28926 instanceof Object)){
var ex__28381__auto__ = e28926;
var statearr_28927_29062 = state_28914;
(statearr_28927_29062[(5)] = ex__28381__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_28914);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e28926;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__28379__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__29063 = state_28914;
state_28914 = G__29063;
continue;
} else {
return ret_value__28379__auto__;
}
break;
}
});
cljs$core$async$pipeline_STAR__$_state_machine__28378__auto__ = function(state_28914){
switch(arguments.length){
case 0:
return cljs$core$async$pipeline_STAR__$_state_machine__28378__auto____0.call(this);
case 1:
return cljs$core$async$pipeline_STAR__$_state_machine__28378__auto____1.call(this,state_28914);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$pipeline_STAR__$_state_machine__28378__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$pipeline_STAR__$_state_machine__28378__auto____0;
cljs$core$async$pipeline_STAR__$_state_machine__28378__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$pipeline_STAR__$_state_machine__28378__auto____1;
return cljs$core$async$pipeline_STAR__$_state_machine__28378__auto__;
})()
;})(__29043,switch__28377__auto__,c__28489__auto___29055,G__28874_29044,n__24046__auto___29042,jobs,results,process,async))
})();
var state__28491__auto__ = (function (){var statearr_28928 = f__28490__auto__.call(null);
(statearr_28928[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__28489__auto___29055);

return statearr_28928;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__28491__auto__);
});})(__29043,c__28489__auto___29055,G__28874_29044,n__24046__auto___29042,jobs,results,process,async))
);


break;
default:
throw (new Error([cljs.core.str("No matching clause: "),cljs.core.str(type)].join('')));

}

var G__29064 = (__29043 + (1));
__29043 = G__29064;
continue;
} else {
}
break;
}

var c__28489__auto___29065 = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__28489__auto___29065,jobs,results,process,async){
return (function (){
var f__28490__auto__ = (function (){var switch__28377__auto__ = ((function (c__28489__auto___29065,jobs,results,process,async){
return (function (state_28950){
var state_val_28951 = (state_28950[(1)]);
if((state_val_28951 === (1))){
var state_28950__$1 = state_28950;
var statearr_28952_29066 = state_28950__$1;
(statearr_28952_29066[(2)] = null);

(statearr_28952_29066[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_28951 === (2))){
var state_28950__$1 = state_28950;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_28950__$1,(4),from);
} else {
if((state_val_28951 === (3))){
var inst_28948 = (state_28950[(2)]);
var state_28950__$1 = state_28950;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_28950__$1,inst_28948);
} else {
if((state_val_28951 === (4))){
var inst_28931 = (state_28950[(7)]);
var inst_28931__$1 = (state_28950[(2)]);
var inst_28932 = (inst_28931__$1 == null);
var state_28950__$1 = (function (){var statearr_28953 = state_28950;
(statearr_28953[(7)] = inst_28931__$1);

return statearr_28953;
})();
if(cljs.core.truth_(inst_28932)){
var statearr_28954_29067 = state_28950__$1;
(statearr_28954_29067[(1)] = (5));

} else {
var statearr_28955_29068 = state_28950__$1;
(statearr_28955_29068[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_28951 === (5))){
var inst_28934 = cljs.core.async.close_BANG_.call(null,jobs);
var state_28950__$1 = state_28950;
var statearr_28956_29069 = state_28950__$1;
(statearr_28956_29069[(2)] = inst_28934);

(statearr_28956_29069[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_28951 === (6))){
var inst_28931 = (state_28950[(7)]);
var inst_28936 = (state_28950[(8)]);
var inst_28936__$1 = cljs.core.async.chan.call(null,(1));
var inst_28937 = cljs.core.PersistentVector.EMPTY_NODE;
var inst_28938 = [inst_28931,inst_28936__$1];
var inst_28939 = (new cljs.core.PersistentVector(null,2,(5),inst_28937,inst_28938,null));
var state_28950__$1 = (function (){var statearr_28957 = state_28950;
(statearr_28957[(8)] = inst_28936__$1);

return statearr_28957;
})();
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_28950__$1,(8),jobs,inst_28939);
} else {
if((state_val_28951 === (7))){
var inst_28946 = (state_28950[(2)]);
var state_28950__$1 = state_28950;
var statearr_28958_29070 = state_28950__$1;
(statearr_28958_29070[(2)] = inst_28946);

(statearr_28958_29070[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_28951 === (8))){
var inst_28936 = (state_28950[(8)]);
var inst_28941 = (state_28950[(2)]);
var state_28950__$1 = (function (){var statearr_28959 = state_28950;
(statearr_28959[(9)] = inst_28941);

return statearr_28959;
})();
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_28950__$1,(9),results,inst_28936);
} else {
if((state_val_28951 === (9))){
var inst_28943 = (state_28950[(2)]);
var state_28950__$1 = (function (){var statearr_28960 = state_28950;
(statearr_28960[(10)] = inst_28943);

return statearr_28960;
})();
var statearr_28961_29071 = state_28950__$1;
(statearr_28961_29071[(2)] = null);

(statearr_28961_29071[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
});})(c__28489__auto___29065,jobs,results,process,async))
;
return ((function (switch__28377__auto__,c__28489__auto___29065,jobs,results,process,async){
return (function() {
var cljs$core$async$pipeline_STAR__$_state_machine__28378__auto__ = null;
var cljs$core$async$pipeline_STAR__$_state_machine__28378__auto____0 = (function (){
var statearr_28965 = [null,null,null,null,null,null,null,null,null,null,null];
(statearr_28965[(0)] = cljs$core$async$pipeline_STAR__$_state_machine__28378__auto__);

(statearr_28965[(1)] = (1));

return statearr_28965;
});
var cljs$core$async$pipeline_STAR__$_state_machine__28378__auto____1 = (function (state_28950){
while(true){
var ret_value__28379__auto__ = (function (){try{while(true){
var result__28380__auto__ = switch__28377__auto__.call(null,state_28950);
if(cljs.core.keyword_identical_QMARK_.call(null,result__28380__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__28380__auto__;
}
break;
}
}catch (e28966){if((e28966 instanceof Object)){
var ex__28381__auto__ = e28966;
var statearr_28967_29072 = state_28950;
(statearr_28967_29072[(5)] = ex__28381__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_28950);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e28966;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__28379__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__29073 = state_28950;
state_28950 = G__29073;
continue;
} else {
return ret_value__28379__auto__;
}
break;
}
});
cljs$core$async$pipeline_STAR__$_state_machine__28378__auto__ = function(state_28950){
switch(arguments.length){
case 0:
return cljs$core$async$pipeline_STAR__$_state_machine__28378__auto____0.call(this);
case 1:
return cljs$core$async$pipeline_STAR__$_state_machine__28378__auto____1.call(this,state_28950);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$pipeline_STAR__$_state_machine__28378__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$pipeline_STAR__$_state_machine__28378__auto____0;
cljs$core$async$pipeline_STAR__$_state_machine__28378__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$pipeline_STAR__$_state_machine__28378__auto____1;
return cljs$core$async$pipeline_STAR__$_state_machine__28378__auto__;
})()
;})(switch__28377__auto__,c__28489__auto___29065,jobs,results,process,async))
})();
var state__28491__auto__ = (function (){var statearr_28968 = f__28490__auto__.call(null);
(statearr_28968[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__28489__auto___29065);

return statearr_28968;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__28491__auto__);
});})(c__28489__auto___29065,jobs,results,process,async))
);


var c__28489__auto__ = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__28489__auto__,jobs,results,process,async){
return (function (){
var f__28490__auto__ = (function (){var switch__28377__auto__ = ((function (c__28489__auto__,jobs,results,process,async){
return (function (state_29006){
var state_val_29007 = (state_29006[(1)]);
if((state_val_29007 === (7))){
var inst_29002 = (state_29006[(2)]);
var state_29006__$1 = state_29006;
var statearr_29008_29074 = state_29006__$1;
(statearr_29008_29074[(2)] = inst_29002);

(statearr_29008_29074[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_29007 === (20))){
var state_29006__$1 = state_29006;
var statearr_29009_29075 = state_29006__$1;
(statearr_29009_29075[(2)] = null);

(statearr_29009_29075[(1)] = (21));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_29007 === (1))){
var state_29006__$1 = state_29006;
var statearr_29010_29076 = state_29006__$1;
(statearr_29010_29076[(2)] = null);

(statearr_29010_29076[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_29007 === (4))){
var inst_28971 = (state_29006[(7)]);
var inst_28971__$1 = (state_29006[(2)]);
var inst_28972 = (inst_28971__$1 == null);
var state_29006__$1 = (function (){var statearr_29011 = state_29006;
(statearr_29011[(7)] = inst_28971__$1);

return statearr_29011;
})();
if(cljs.core.truth_(inst_28972)){
var statearr_29012_29077 = state_29006__$1;
(statearr_29012_29077[(1)] = (5));

} else {
var statearr_29013_29078 = state_29006__$1;
(statearr_29013_29078[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_29007 === (15))){
var inst_28984 = (state_29006[(8)]);
var state_29006__$1 = state_29006;
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_29006__$1,(18),to,inst_28984);
} else {
if((state_val_29007 === (21))){
var inst_28997 = (state_29006[(2)]);
var state_29006__$1 = state_29006;
var statearr_29014_29079 = state_29006__$1;
(statearr_29014_29079[(2)] = inst_28997);

(statearr_29014_29079[(1)] = (13));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_29007 === (13))){
var inst_28999 = (state_29006[(2)]);
var state_29006__$1 = (function (){var statearr_29015 = state_29006;
(statearr_29015[(9)] = inst_28999);

return statearr_29015;
})();
var statearr_29016_29080 = state_29006__$1;
(statearr_29016_29080[(2)] = null);

(statearr_29016_29080[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_29007 === (6))){
var inst_28971 = (state_29006[(7)]);
var state_29006__$1 = state_29006;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_29006__$1,(11),inst_28971);
} else {
if((state_val_29007 === (17))){
var inst_28992 = (state_29006[(2)]);
var state_29006__$1 = state_29006;
if(cljs.core.truth_(inst_28992)){
var statearr_29017_29081 = state_29006__$1;
(statearr_29017_29081[(1)] = (19));

} else {
var statearr_29018_29082 = state_29006__$1;
(statearr_29018_29082[(1)] = (20));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_29007 === (3))){
var inst_29004 = (state_29006[(2)]);
var state_29006__$1 = state_29006;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_29006__$1,inst_29004);
} else {
if((state_val_29007 === (12))){
var inst_28981 = (state_29006[(10)]);
var state_29006__$1 = state_29006;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_29006__$1,(14),inst_28981);
} else {
if((state_val_29007 === (2))){
var state_29006__$1 = state_29006;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_29006__$1,(4),results);
} else {
if((state_val_29007 === (19))){
var state_29006__$1 = state_29006;
var statearr_29019_29083 = state_29006__$1;
(statearr_29019_29083[(2)] = null);

(statearr_29019_29083[(1)] = (12));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_29007 === (11))){
var inst_28981 = (state_29006[(2)]);
var state_29006__$1 = (function (){var statearr_29020 = state_29006;
(statearr_29020[(10)] = inst_28981);

return statearr_29020;
})();
var statearr_29021_29084 = state_29006__$1;
(statearr_29021_29084[(2)] = null);

(statearr_29021_29084[(1)] = (12));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_29007 === (9))){
var state_29006__$1 = state_29006;
var statearr_29022_29085 = state_29006__$1;
(statearr_29022_29085[(2)] = null);

(statearr_29022_29085[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_29007 === (5))){
var state_29006__$1 = state_29006;
if(cljs.core.truth_(close_QMARK_)){
var statearr_29023_29086 = state_29006__$1;
(statearr_29023_29086[(1)] = (8));

} else {
var statearr_29024_29087 = state_29006__$1;
(statearr_29024_29087[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_29007 === (14))){
var inst_28986 = (state_29006[(11)]);
var inst_28984 = (state_29006[(8)]);
var inst_28984__$1 = (state_29006[(2)]);
var inst_28985 = (inst_28984__$1 == null);
var inst_28986__$1 = cljs.core.not.call(null,inst_28985);
var state_29006__$1 = (function (){var statearr_29025 = state_29006;
(statearr_29025[(11)] = inst_28986__$1);

(statearr_29025[(8)] = inst_28984__$1);

return statearr_29025;
})();
if(inst_28986__$1){
var statearr_29026_29088 = state_29006__$1;
(statearr_29026_29088[(1)] = (15));

} else {
var statearr_29027_29089 = state_29006__$1;
(statearr_29027_29089[(1)] = (16));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_29007 === (16))){
var inst_28986 = (state_29006[(11)]);
var state_29006__$1 = state_29006;
var statearr_29028_29090 = state_29006__$1;
(statearr_29028_29090[(2)] = inst_28986);

(statearr_29028_29090[(1)] = (17));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_29007 === (10))){
var inst_28978 = (state_29006[(2)]);
var state_29006__$1 = state_29006;
var statearr_29029_29091 = state_29006__$1;
(statearr_29029_29091[(2)] = inst_28978);

(statearr_29029_29091[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_29007 === (18))){
var inst_28989 = (state_29006[(2)]);
var state_29006__$1 = state_29006;
var statearr_29030_29092 = state_29006__$1;
(statearr_29030_29092[(2)] = inst_28989);

(statearr_29030_29092[(1)] = (17));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_29007 === (8))){
var inst_28975 = cljs.core.async.close_BANG_.call(null,to);
var state_29006__$1 = state_29006;
var statearr_29031_29093 = state_29006__$1;
(statearr_29031_29093[(2)] = inst_28975);

(statearr_29031_29093[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__28489__auto__,jobs,results,process,async))
;
return ((function (switch__28377__auto__,c__28489__auto__,jobs,results,process,async){
return (function() {
var cljs$core$async$pipeline_STAR__$_state_machine__28378__auto__ = null;
var cljs$core$async$pipeline_STAR__$_state_machine__28378__auto____0 = (function (){
var statearr_29035 = [null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_29035[(0)] = cljs$core$async$pipeline_STAR__$_state_machine__28378__auto__);

(statearr_29035[(1)] = (1));

return statearr_29035;
});
var cljs$core$async$pipeline_STAR__$_state_machine__28378__auto____1 = (function (state_29006){
while(true){
var ret_value__28379__auto__ = (function (){try{while(true){
var result__28380__auto__ = switch__28377__auto__.call(null,state_29006);
if(cljs.core.keyword_identical_QMARK_.call(null,result__28380__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__28380__auto__;
}
break;
}
}catch (e29036){if((e29036 instanceof Object)){
var ex__28381__auto__ = e29036;
var statearr_29037_29094 = state_29006;
(statearr_29037_29094[(5)] = ex__28381__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_29006);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e29036;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__28379__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__29095 = state_29006;
state_29006 = G__29095;
continue;
} else {
return ret_value__28379__auto__;
}
break;
}
});
cljs$core$async$pipeline_STAR__$_state_machine__28378__auto__ = function(state_29006){
switch(arguments.length){
case 0:
return cljs$core$async$pipeline_STAR__$_state_machine__28378__auto____0.call(this);
case 1:
return cljs$core$async$pipeline_STAR__$_state_machine__28378__auto____1.call(this,state_29006);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$pipeline_STAR__$_state_machine__28378__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$pipeline_STAR__$_state_machine__28378__auto____0;
cljs$core$async$pipeline_STAR__$_state_machine__28378__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$pipeline_STAR__$_state_machine__28378__auto____1;
return cljs$core$async$pipeline_STAR__$_state_machine__28378__auto__;
})()
;})(switch__28377__auto__,c__28489__auto__,jobs,results,process,async))
})();
var state__28491__auto__ = (function (){var statearr_29038 = f__28490__auto__.call(null);
(statearr_29038[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__28489__auto__);

return statearr_29038;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__28491__auto__);
});})(c__28489__auto__,jobs,results,process,async))
);

return c__28489__auto__;
});
/**
 * Takes elements from the from channel and supplies them to the to
 *   channel, subject to the async function af, with parallelism n. af
 *   must be a function of two arguments, the first an input value and
 *   the second a channel on which to place the result(s). af must close!
 *   the channel before returning.  The presumption is that af will
 *   return immediately, having launched some asynchronous operation
 *   whose completion/callback will manipulate the result channel. Outputs
 *   will be returned in order relative to  the inputs. By default, the to
 *   channel will be closed when the from channel closes, but can be
 *   determined by the close?  parameter. Will stop consuming the from
 *   channel if the to channel closes.
 */
cljs.core.async.pipeline_async = (function cljs$core$async$pipeline_async(var_args){
var args29096 = [];
var len__24201__auto___29099 = arguments.length;
var i__24202__auto___29100 = (0);
while(true){
if((i__24202__auto___29100 < len__24201__auto___29099)){
args29096.push((arguments[i__24202__auto___29100]));

var G__29101 = (i__24202__auto___29100 + (1));
i__24202__auto___29100 = G__29101;
continue;
} else {
}
break;
}

var G__29098 = args29096.length;
switch (G__29098) {
case 4:
return cljs.core.async.pipeline_async.cljs$core$IFn$_invoke$arity$4((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]));

break;
case 5:
return cljs.core.async.pipeline_async.cljs$core$IFn$_invoke$arity$5((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]),(arguments[(4)]));

break;
default:
throw (new Error([cljs.core.str("Invalid arity: "),cljs.core.str(args29096.length)].join('')));

}
});

cljs.core.async.pipeline_async.cljs$core$IFn$_invoke$arity$4 = (function (n,to,af,from){
return cljs.core.async.pipeline_async.call(null,n,to,af,from,true);
});

cljs.core.async.pipeline_async.cljs$core$IFn$_invoke$arity$5 = (function (n,to,af,from,close_QMARK_){
return cljs.core.async.pipeline_STAR_.call(null,n,to,af,from,close_QMARK_,null,new cljs.core.Keyword(null,"async","async",1050769601));
});

cljs.core.async.pipeline_async.cljs$lang$maxFixedArity = 5;
/**
 * Takes elements from the from channel and supplies them to the to
 *   channel, subject to the transducer xf, with parallelism n. Because
 *   it is parallel, the transducer will be applied independently to each
 *   element, not across elements, and may produce zero or more outputs
 *   per input.  Outputs will be returned in order relative to the
 *   inputs. By default, the to channel will be closed when the from
 *   channel closes, but can be determined by the close?  parameter. Will
 *   stop consuming the from channel if the to channel closes.
 * 
 *   Note this is supplied for API compatibility with the Clojure version.
 *   Values of N > 1 will not result in actual concurrency in a
 *   single-threaded runtime.
 */
cljs.core.async.pipeline = (function cljs$core$async$pipeline(var_args){
var args29103 = [];
var len__24201__auto___29106 = arguments.length;
var i__24202__auto___29107 = (0);
while(true){
if((i__24202__auto___29107 < len__24201__auto___29106)){
args29103.push((arguments[i__24202__auto___29107]));

var G__29108 = (i__24202__auto___29107 + (1));
i__24202__auto___29107 = G__29108;
continue;
} else {
}
break;
}

var G__29105 = args29103.length;
switch (G__29105) {
case 4:
return cljs.core.async.pipeline.cljs$core$IFn$_invoke$arity$4((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]));

break;
case 5:
return cljs.core.async.pipeline.cljs$core$IFn$_invoke$arity$5((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]),(arguments[(4)]));

break;
case 6:
return cljs.core.async.pipeline.cljs$core$IFn$_invoke$arity$6((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]),(arguments[(4)]),(arguments[(5)]));

break;
default:
throw (new Error([cljs.core.str("Invalid arity: "),cljs.core.str(args29103.length)].join('')));

}
});

cljs.core.async.pipeline.cljs$core$IFn$_invoke$arity$4 = (function (n,to,xf,from){
return cljs.core.async.pipeline.call(null,n,to,xf,from,true);
});

cljs.core.async.pipeline.cljs$core$IFn$_invoke$arity$5 = (function (n,to,xf,from,close_QMARK_){
return cljs.core.async.pipeline.call(null,n,to,xf,from,close_QMARK_,null);
});

cljs.core.async.pipeline.cljs$core$IFn$_invoke$arity$6 = (function (n,to,xf,from,close_QMARK_,ex_handler){
return cljs.core.async.pipeline_STAR_.call(null,n,to,xf,from,close_QMARK_,ex_handler,new cljs.core.Keyword(null,"compute","compute",1555393130));
});

cljs.core.async.pipeline.cljs$lang$maxFixedArity = 6;
/**
 * Takes a predicate and a source channel and returns a vector of two
 *   channels, the first of which will contain the values for which the
 *   predicate returned true, the second those for which it returned
 *   false.
 * 
 *   The out channels will be unbuffered by default, or two buf-or-ns can
 *   be supplied. The channels will close after the source channel has
 *   closed.
 */
cljs.core.async.split = (function cljs$core$async$split(var_args){
var args29110 = [];
var len__24201__auto___29163 = arguments.length;
var i__24202__auto___29164 = (0);
while(true){
if((i__24202__auto___29164 < len__24201__auto___29163)){
args29110.push((arguments[i__24202__auto___29164]));

var G__29165 = (i__24202__auto___29164 + (1));
i__24202__auto___29164 = G__29165;
continue;
} else {
}
break;
}

var G__29112 = args29110.length;
switch (G__29112) {
case 2:
return cljs.core.async.split.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 4:
return cljs.core.async.split.cljs$core$IFn$_invoke$arity$4((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]));

break;
default:
throw (new Error([cljs.core.str("Invalid arity: "),cljs.core.str(args29110.length)].join('')));

}
});

cljs.core.async.split.cljs$core$IFn$_invoke$arity$2 = (function (p,ch){
return cljs.core.async.split.call(null,p,ch,null,null);
});

cljs.core.async.split.cljs$core$IFn$_invoke$arity$4 = (function (p,ch,t_buf_or_n,f_buf_or_n){
var tc = cljs.core.async.chan.call(null,t_buf_or_n);
var fc = cljs.core.async.chan.call(null,f_buf_or_n);
var c__28489__auto___29167 = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__28489__auto___29167,tc,fc){
return (function (){
var f__28490__auto__ = (function (){var switch__28377__auto__ = ((function (c__28489__auto___29167,tc,fc){
return (function (state_29138){
var state_val_29139 = (state_29138[(1)]);
if((state_val_29139 === (7))){
var inst_29134 = (state_29138[(2)]);
var state_29138__$1 = state_29138;
var statearr_29140_29168 = state_29138__$1;
(statearr_29140_29168[(2)] = inst_29134);

(statearr_29140_29168[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_29139 === (1))){
var state_29138__$1 = state_29138;
var statearr_29141_29169 = state_29138__$1;
(statearr_29141_29169[(2)] = null);

(statearr_29141_29169[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_29139 === (4))){
var inst_29115 = (state_29138[(7)]);
var inst_29115__$1 = (state_29138[(2)]);
var inst_29116 = (inst_29115__$1 == null);
var state_29138__$1 = (function (){var statearr_29142 = state_29138;
(statearr_29142[(7)] = inst_29115__$1);

return statearr_29142;
})();
if(cljs.core.truth_(inst_29116)){
var statearr_29143_29170 = state_29138__$1;
(statearr_29143_29170[(1)] = (5));

} else {
var statearr_29144_29171 = state_29138__$1;
(statearr_29144_29171[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_29139 === (13))){
var state_29138__$1 = state_29138;
var statearr_29145_29172 = state_29138__$1;
(statearr_29145_29172[(2)] = null);

(statearr_29145_29172[(1)] = (14));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_29139 === (6))){
var inst_29115 = (state_29138[(7)]);
var inst_29121 = p.call(null,inst_29115);
var state_29138__$1 = state_29138;
if(cljs.core.truth_(inst_29121)){
var statearr_29146_29173 = state_29138__$1;
(statearr_29146_29173[(1)] = (9));

} else {
var statearr_29147_29174 = state_29138__$1;
(statearr_29147_29174[(1)] = (10));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_29139 === (3))){
var inst_29136 = (state_29138[(2)]);
var state_29138__$1 = state_29138;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_29138__$1,inst_29136);
} else {
if((state_val_29139 === (12))){
var state_29138__$1 = state_29138;
var statearr_29148_29175 = state_29138__$1;
(statearr_29148_29175[(2)] = null);

(statearr_29148_29175[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_29139 === (2))){
var state_29138__$1 = state_29138;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_29138__$1,(4),ch);
} else {
if((state_val_29139 === (11))){
var inst_29115 = (state_29138[(7)]);
var inst_29125 = (state_29138[(2)]);
var state_29138__$1 = state_29138;
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_29138__$1,(8),inst_29125,inst_29115);
} else {
if((state_val_29139 === (9))){
var state_29138__$1 = state_29138;
var statearr_29149_29176 = state_29138__$1;
(statearr_29149_29176[(2)] = tc);

(statearr_29149_29176[(1)] = (11));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_29139 === (5))){
var inst_29118 = cljs.core.async.close_BANG_.call(null,tc);
var inst_29119 = cljs.core.async.close_BANG_.call(null,fc);
var state_29138__$1 = (function (){var statearr_29150 = state_29138;
(statearr_29150[(8)] = inst_29118);

return statearr_29150;
})();
var statearr_29151_29177 = state_29138__$1;
(statearr_29151_29177[(2)] = inst_29119);

(statearr_29151_29177[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_29139 === (14))){
var inst_29132 = (state_29138[(2)]);
var state_29138__$1 = state_29138;
var statearr_29152_29178 = state_29138__$1;
(statearr_29152_29178[(2)] = inst_29132);

(statearr_29152_29178[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_29139 === (10))){
var state_29138__$1 = state_29138;
var statearr_29153_29179 = state_29138__$1;
(statearr_29153_29179[(2)] = fc);

(statearr_29153_29179[(1)] = (11));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_29139 === (8))){
var inst_29127 = (state_29138[(2)]);
var state_29138__$1 = state_29138;
if(cljs.core.truth_(inst_29127)){
var statearr_29154_29180 = state_29138__$1;
(statearr_29154_29180[(1)] = (12));

} else {
var statearr_29155_29181 = state_29138__$1;
(statearr_29155_29181[(1)] = (13));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__28489__auto___29167,tc,fc))
;
return ((function (switch__28377__auto__,c__28489__auto___29167,tc,fc){
return (function() {
var cljs$core$async$state_machine__28378__auto__ = null;
var cljs$core$async$state_machine__28378__auto____0 = (function (){
var statearr_29159 = [null,null,null,null,null,null,null,null,null];
(statearr_29159[(0)] = cljs$core$async$state_machine__28378__auto__);

(statearr_29159[(1)] = (1));

return statearr_29159;
});
var cljs$core$async$state_machine__28378__auto____1 = (function (state_29138){
while(true){
var ret_value__28379__auto__ = (function (){try{while(true){
var result__28380__auto__ = switch__28377__auto__.call(null,state_29138);
if(cljs.core.keyword_identical_QMARK_.call(null,result__28380__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__28380__auto__;
}
break;
}
}catch (e29160){if((e29160 instanceof Object)){
var ex__28381__auto__ = e29160;
var statearr_29161_29182 = state_29138;
(statearr_29161_29182[(5)] = ex__28381__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_29138);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e29160;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__28379__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__29183 = state_29138;
state_29138 = G__29183;
continue;
} else {
return ret_value__28379__auto__;
}
break;
}
});
cljs$core$async$state_machine__28378__auto__ = function(state_29138){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__28378__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__28378__auto____1.call(this,state_29138);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__28378__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__28378__auto____0;
cljs$core$async$state_machine__28378__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__28378__auto____1;
return cljs$core$async$state_machine__28378__auto__;
})()
;})(switch__28377__auto__,c__28489__auto___29167,tc,fc))
})();
var state__28491__auto__ = (function (){var statearr_29162 = f__28490__auto__.call(null);
(statearr_29162[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__28489__auto___29167);

return statearr_29162;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__28491__auto__);
});})(c__28489__auto___29167,tc,fc))
);


return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [tc,fc], null);
});

cljs.core.async.split.cljs$lang$maxFixedArity = 4;
/**
 * f should be a function of 2 arguments. Returns a channel containing
 *   the single result of applying f to init and the first item from the
 *   channel, then applying f to that result and the 2nd item, etc. If
 *   the channel closes without yielding items, returns init and f is not
 *   called. ch must close before reduce produces a result.
 */
cljs.core.async.reduce = (function cljs$core$async$reduce(f,init,ch){
var c__28489__auto__ = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__28489__auto__){
return (function (){
var f__28490__auto__ = (function (){var switch__28377__auto__ = ((function (c__28489__auto__){
return (function (state_29247){
var state_val_29248 = (state_29247[(1)]);
if((state_val_29248 === (7))){
var inst_29243 = (state_29247[(2)]);
var state_29247__$1 = state_29247;
var statearr_29249_29270 = state_29247__$1;
(statearr_29249_29270[(2)] = inst_29243);

(statearr_29249_29270[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_29248 === (1))){
var inst_29227 = init;
var state_29247__$1 = (function (){var statearr_29250 = state_29247;
(statearr_29250[(7)] = inst_29227);

return statearr_29250;
})();
var statearr_29251_29271 = state_29247__$1;
(statearr_29251_29271[(2)] = null);

(statearr_29251_29271[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_29248 === (4))){
var inst_29230 = (state_29247[(8)]);
var inst_29230__$1 = (state_29247[(2)]);
var inst_29231 = (inst_29230__$1 == null);
var state_29247__$1 = (function (){var statearr_29252 = state_29247;
(statearr_29252[(8)] = inst_29230__$1);

return statearr_29252;
})();
if(cljs.core.truth_(inst_29231)){
var statearr_29253_29272 = state_29247__$1;
(statearr_29253_29272[(1)] = (5));

} else {
var statearr_29254_29273 = state_29247__$1;
(statearr_29254_29273[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_29248 === (6))){
var inst_29227 = (state_29247[(7)]);
var inst_29230 = (state_29247[(8)]);
var inst_29234 = (state_29247[(9)]);
var inst_29234__$1 = f.call(null,inst_29227,inst_29230);
var inst_29235 = cljs.core.reduced_QMARK_.call(null,inst_29234__$1);
var state_29247__$1 = (function (){var statearr_29255 = state_29247;
(statearr_29255[(9)] = inst_29234__$1);

return statearr_29255;
})();
if(inst_29235){
var statearr_29256_29274 = state_29247__$1;
(statearr_29256_29274[(1)] = (8));

} else {
var statearr_29257_29275 = state_29247__$1;
(statearr_29257_29275[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_29248 === (3))){
var inst_29245 = (state_29247[(2)]);
var state_29247__$1 = state_29247;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_29247__$1,inst_29245);
} else {
if((state_val_29248 === (2))){
var state_29247__$1 = state_29247;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_29247__$1,(4),ch);
} else {
if((state_val_29248 === (9))){
var inst_29234 = (state_29247[(9)]);
var inst_29227 = inst_29234;
var state_29247__$1 = (function (){var statearr_29258 = state_29247;
(statearr_29258[(7)] = inst_29227);

return statearr_29258;
})();
var statearr_29259_29276 = state_29247__$1;
(statearr_29259_29276[(2)] = null);

(statearr_29259_29276[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_29248 === (5))){
var inst_29227 = (state_29247[(7)]);
var state_29247__$1 = state_29247;
var statearr_29260_29277 = state_29247__$1;
(statearr_29260_29277[(2)] = inst_29227);

(statearr_29260_29277[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_29248 === (10))){
var inst_29241 = (state_29247[(2)]);
var state_29247__$1 = state_29247;
var statearr_29261_29278 = state_29247__$1;
(statearr_29261_29278[(2)] = inst_29241);

(statearr_29261_29278[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_29248 === (8))){
var inst_29234 = (state_29247[(9)]);
var inst_29237 = cljs.core.deref.call(null,inst_29234);
var state_29247__$1 = state_29247;
var statearr_29262_29279 = state_29247__$1;
(statearr_29262_29279[(2)] = inst_29237);

(statearr_29262_29279[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
});})(c__28489__auto__))
;
return ((function (switch__28377__auto__,c__28489__auto__){
return (function() {
var cljs$core$async$reduce_$_state_machine__28378__auto__ = null;
var cljs$core$async$reduce_$_state_machine__28378__auto____0 = (function (){
var statearr_29266 = [null,null,null,null,null,null,null,null,null,null];
(statearr_29266[(0)] = cljs$core$async$reduce_$_state_machine__28378__auto__);

(statearr_29266[(1)] = (1));

return statearr_29266;
});
var cljs$core$async$reduce_$_state_machine__28378__auto____1 = (function (state_29247){
while(true){
var ret_value__28379__auto__ = (function (){try{while(true){
var result__28380__auto__ = switch__28377__auto__.call(null,state_29247);
if(cljs.core.keyword_identical_QMARK_.call(null,result__28380__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__28380__auto__;
}
break;
}
}catch (e29267){if((e29267 instanceof Object)){
var ex__28381__auto__ = e29267;
var statearr_29268_29280 = state_29247;
(statearr_29268_29280[(5)] = ex__28381__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_29247);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e29267;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__28379__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__29281 = state_29247;
state_29247 = G__29281;
continue;
} else {
return ret_value__28379__auto__;
}
break;
}
});
cljs$core$async$reduce_$_state_machine__28378__auto__ = function(state_29247){
switch(arguments.length){
case 0:
return cljs$core$async$reduce_$_state_machine__28378__auto____0.call(this);
case 1:
return cljs$core$async$reduce_$_state_machine__28378__auto____1.call(this,state_29247);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$reduce_$_state_machine__28378__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$reduce_$_state_machine__28378__auto____0;
cljs$core$async$reduce_$_state_machine__28378__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$reduce_$_state_machine__28378__auto____1;
return cljs$core$async$reduce_$_state_machine__28378__auto__;
})()
;})(switch__28377__auto__,c__28489__auto__))
})();
var state__28491__auto__ = (function (){var statearr_29269 = f__28490__auto__.call(null);
(statearr_29269[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__28489__auto__);

return statearr_29269;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__28491__auto__);
});})(c__28489__auto__))
);

return c__28489__auto__;
});
/**
 * Puts the contents of coll into the supplied channel.
 * 
 *   By default the channel will be closed after the items are copied,
 *   but can be determined by the close? parameter.
 * 
 *   Returns a channel which will close after the items are copied.
 */
cljs.core.async.onto_chan = (function cljs$core$async$onto_chan(var_args){
var args29282 = [];
var len__24201__auto___29334 = arguments.length;
var i__24202__auto___29335 = (0);
while(true){
if((i__24202__auto___29335 < len__24201__auto___29334)){
args29282.push((arguments[i__24202__auto___29335]));

var G__29336 = (i__24202__auto___29335 + (1));
i__24202__auto___29335 = G__29336;
continue;
} else {
}
break;
}

var G__29284 = args29282.length;
switch (G__29284) {
case 2:
return cljs.core.async.onto_chan.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.onto_chan.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error([cljs.core.str("Invalid arity: "),cljs.core.str(args29282.length)].join('')));

}
});

cljs.core.async.onto_chan.cljs$core$IFn$_invoke$arity$2 = (function (ch,coll){
return cljs.core.async.onto_chan.call(null,ch,coll,true);
});

cljs.core.async.onto_chan.cljs$core$IFn$_invoke$arity$3 = (function (ch,coll,close_QMARK_){
var c__28489__auto__ = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__28489__auto__){
return (function (){
var f__28490__auto__ = (function (){var switch__28377__auto__ = ((function (c__28489__auto__){
return (function (state_29309){
var state_val_29310 = (state_29309[(1)]);
if((state_val_29310 === (7))){
var inst_29291 = (state_29309[(2)]);
var state_29309__$1 = state_29309;
var statearr_29311_29338 = state_29309__$1;
(statearr_29311_29338[(2)] = inst_29291);

(statearr_29311_29338[(1)] = (6));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_29310 === (1))){
var inst_29285 = cljs.core.seq.call(null,coll);
var inst_29286 = inst_29285;
var state_29309__$1 = (function (){var statearr_29312 = state_29309;
(statearr_29312[(7)] = inst_29286);

return statearr_29312;
})();
var statearr_29313_29339 = state_29309__$1;
(statearr_29313_29339[(2)] = null);

(statearr_29313_29339[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_29310 === (4))){
var inst_29286 = (state_29309[(7)]);
var inst_29289 = cljs.core.first.call(null,inst_29286);
var state_29309__$1 = state_29309;
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_29309__$1,(7),ch,inst_29289);
} else {
if((state_val_29310 === (13))){
var inst_29303 = (state_29309[(2)]);
var state_29309__$1 = state_29309;
var statearr_29314_29340 = state_29309__$1;
(statearr_29314_29340[(2)] = inst_29303);

(statearr_29314_29340[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_29310 === (6))){
var inst_29294 = (state_29309[(2)]);
var state_29309__$1 = state_29309;
if(cljs.core.truth_(inst_29294)){
var statearr_29315_29341 = state_29309__$1;
(statearr_29315_29341[(1)] = (8));

} else {
var statearr_29316_29342 = state_29309__$1;
(statearr_29316_29342[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_29310 === (3))){
var inst_29307 = (state_29309[(2)]);
var state_29309__$1 = state_29309;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_29309__$1,inst_29307);
} else {
if((state_val_29310 === (12))){
var state_29309__$1 = state_29309;
var statearr_29317_29343 = state_29309__$1;
(statearr_29317_29343[(2)] = null);

(statearr_29317_29343[(1)] = (13));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_29310 === (2))){
var inst_29286 = (state_29309[(7)]);
var state_29309__$1 = state_29309;
if(cljs.core.truth_(inst_29286)){
var statearr_29318_29344 = state_29309__$1;
(statearr_29318_29344[(1)] = (4));

} else {
var statearr_29319_29345 = state_29309__$1;
(statearr_29319_29345[(1)] = (5));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_29310 === (11))){
var inst_29300 = cljs.core.async.close_BANG_.call(null,ch);
var state_29309__$1 = state_29309;
var statearr_29320_29346 = state_29309__$1;
(statearr_29320_29346[(2)] = inst_29300);

(statearr_29320_29346[(1)] = (13));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_29310 === (9))){
var state_29309__$1 = state_29309;
if(cljs.core.truth_(close_QMARK_)){
var statearr_29321_29347 = state_29309__$1;
(statearr_29321_29347[(1)] = (11));

} else {
var statearr_29322_29348 = state_29309__$1;
(statearr_29322_29348[(1)] = (12));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_29310 === (5))){
var inst_29286 = (state_29309[(7)]);
var state_29309__$1 = state_29309;
var statearr_29323_29349 = state_29309__$1;
(statearr_29323_29349[(2)] = inst_29286);

(statearr_29323_29349[(1)] = (6));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_29310 === (10))){
var inst_29305 = (state_29309[(2)]);
var state_29309__$1 = state_29309;
var statearr_29324_29350 = state_29309__$1;
(statearr_29324_29350[(2)] = inst_29305);

(statearr_29324_29350[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_29310 === (8))){
var inst_29286 = (state_29309[(7)]);
var inst_29296 = cljs.core.next.call(null,inst_29286);
var inst_29286__$1 = inst_29296;
var state_29309__$1 = (function (){var statearr_29325 = state_29309;
(statearr_29325[(7)] = inst_29286__$1);

return statearr_29325;
})();
var statearr_29326_29351 = state_29309__$1;
(statearr_29326_29351[(2)] = null);

(statearr_29326_29351[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__28489__auto__))
;
return ((function (switch__28377__auto__,c__28489__auto__){
return (function() {
var cljs$core$async$state_machine__28378__auto__ = null;
var cljs$core$async$state_machine__28378__auto____0 = (function (){
var statearr_29330 = [null,null,null,null,null,null,null,null];
(statearr_29330[(0)] = cljs$core$async$state_machine__28378__auto__);

(statearr_29330[(1)] = (1));

return statearr_29330;
});
var cljs$core$async$state_machine__28378__auto____1 = (function (state_29309){
while(true){
var ret_value__28379__auto__ = (function (){try{while(true){
var result__28380__auto__ = switch__28377__auto__.call(null,state_29309);
if(cljs.core.keyword_identical_QMARK_.call(null,result__28380__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__28380__auto__;
}
break;
}
}catch (e29331){if((e29331 instanceof Object)){
var ex__28381__auto__ = e29331;
var statearr_29332_29352 = state_29309;
(statearr_29332_29352[(5)] = ex__28381__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_29309);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e29331;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__28379__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__29353 = state_29309;
state_29309 = G__29353;
continue;
} else {
return ret_value__28379__auto__;
}
break;
}
});
cljs$core$async$state_machine__28378__auto__ = function(state_29309){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__28378__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__28378__auto____1.call(this,state_29309);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__28378__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__28378__auto____0;
cljs$core$async$state_machine__28378__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__28378__auto____1;
return cljs$core$async$state_machine__28378__auto__;
})()
;})(switch__28377__auto__,c__28489__auto__))
})();
var state__28491__auto__ = (function (){var statearr_29333 = f__28490__auto__.call(null);
(statearr_29333[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__28489__auto__);

return statearr_29333;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__28491__auto__);
});})(c__28489__auto__))
);

return c__28489__auto__;
});

cljs.core.async.onto_chan.cljs$lang$maxFixedArity = 3;
/**
 * Creates and returns a channel which contains the contents of coll,
 *   closing when exhausted.
 */
cljs.core.async.to_chan = (function cljs$core$async$to_chan(coll){
var ch = cljs.core.async.chan.call(null,cljs.core.bounded_count.call(null,(100),coll));
cljs.core.async.onto_chan.call(null,ch,coll);

return ch;
});

/**
 * @interface
 */
cljs.core.async.Mux = function(){};

cljs.core.async.muxch_STAR_ = (function cljs$core$async$muxch_STAR_(_){
if((!((_ == null))) && (!((_.cljs$core$async$Mux$muxch_STAR_$arity$1 == null)))){
return _.cljs$core$async$Mux$muxch_STAR_$arity$1(_);
} else {
var x__23798__auto__ = (((_ == null))?null:_);
var m__23799__auto__ = (cljs.core.async.muxch_STAR_[goog.typeOf(x__23798__auto__)]);
if(!((m__23799__auto__ == null))){
return m__23799__auto__.call(null,_);
} else {
var m__23799__auto____$1 = (cljs.core.async.muxch_STAR_["_"]);
if(!((m__23799__auto____$1 == null))){
return m__23799__auto____$1.call(null,_);
} else {
throw cljs.core.missing_protocol.call(null,"Mux.muxch*",_);
}
}
}
});


/**
 * @interface
 */
cljs.core.async.Mult = function(){};

cljs.core.async.tap_STAR_ = (function cljs$core$async$tap_STAR_(m,ch,close_QMARK_){
if((!((m == null))) && (!((m.cljs$core$async$Mult$tap_STAR_$arity$3 == null)))){
return m.cljs$core$async$Mult$tap_STAR_$arity$3(m,ch,close_QMARK_);
} else {
var x__23798__auto__ = (((m == null))?null:m);
var m__23799__auto__ = (cljs.core.async.tap_STAR_[goog.typeOf(x__23798__auto__)]);
if(!((m__23799__auto__ == null))){
return m__23799__auto__.call(null,m,ch,close_QMARK_);
} else {
var m__23799__auto____$1 = (cljs.core.async.tap_STAR_["_"]);
if(!((m__23799__auto____$1 == null))){
return m__23799__auto____$1.call(null,m,ch,close_QMARK_);
} else {
throw cljs.core.missing_protocol.call(null,"Mult.tap*",m);
}
}
}
});

cljs.core.async.untap_STAR_ = (function cljs$core$async$untap_STAR_(m,ch){
if((!((m == null))) && (!((m.cljs$core$async$Mult$untap_STAR_$arity$2 == null)))){
return m.cljs$core$async$Mult$untap_STAR_$arity$2(m,ch);
} else {
var x__23798__auto__ = (((m == null))?null:m);
var m__23799__auto__ = (cljs.core.async.untap_STAR_[goog.typeOf(x__23798__auto__)]);
if(!((m__23799__auto__ == null))){
return m__23799__auto__.call(null,m,ch);
} else {
var m__23799__auto____$1 = (cljs.core.async.untap_STAR_["_"]);
if(!((m__23799__auto____$1 == null))){
return m__23799__auto____$1.call(null,m,ch);
} else {
throw cljs.core.missing_protocol.call(null,"Mult.untap*",m);
}
}
}
});

cljs.core.async.untap_all_STAR_ = (function cljs$core$async$untap_all_STAR_(m){
if((!((m == null))) && (!((m.cljs$core$async$Mult$untap_all_STAR_$arity$1 == null)))){
return m.cljs$core$async$Mult$untap_all_STAR_$arity$1(m);
} else {
var x__23798__auto__ = (((m == null))?null:m);
var m__23799__auto__ = (cljs.core.async.untap_all_STAR_[goog.typeOf(x__23798__auto__)]);
if(!((m__23799__auto__ == null))){
return m__23799__auto__.call(null,m);
} else {
var m__23799__auto____$1 = (cljs.core.async.untap_all_STAR_["_"]);
if(!((m__23799__auto____$1 == null))){
return m__23799__auto____$1.call(null,m);
} else {
throw cljs.core.missing_protocol.call(null,"Mult.untap-all*",m);
}
}
}
});

/**
 * Creates and returns a mult(iple) of the supplied channel. Channels
 *   containing copies of the channel can be created with 'tap', and
 *   detached with 'untap'.
 * 
 *   Each item is distributed to all taps in parallel and synchronously,
 *   i.e. each tap must accept before the next item is distributed. Use
 *   buffering/windowing to prevent slow taps from holding up the mult.
 * 
 *   Items received when there are no taps get dropped.
 * 
 *   If a tap puts to a closed channel, it will be removed from the mult.
 */
cljs.core.async.mult = (function cljs$core$async$mult(ch){
var cs = cljs.core.atom.call(null,cljs.core.PersistentArrayMap.EMPTY);
var m = (function (){
if(typeof cljs.core.async.t_cljs$core$async29575 !== 'undefined'){
} else {

/**
* @constructor
 * @implements {cljs.core.async.Mult}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.async.Mux}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async29575 = (function (mult,ch,cs,meta29576){
this.mult = mult;
this.ch = ch;
this.cs = cs;
this.meta29576 = meta29576;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
})
cljs.core.async.t_cljs$core$async29575.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = ((function (cs){
return (function (_29577,meta29576__$1){
var self__ = this;
var _29577__$1 = this;
return (new cljs.core.async.t_cljs$core$async29575(self__.mult,self__.ch,self__.cs,meta29576__$1));
});})(cs))
;

cljs.core.async.t_cljs$core$async29575.prototype.cljs$core$IMeta$_meta$arity$1 = ((function (cs){
return (function (_29577){
var self__ = this;
var _29577__$1 = this;
return self__.meta29576;
});})(cs))
;

cljs.core.async.t_cljs$core$async29575.prototype.cljs$core$async$Mux$ = true;

cljs.core.async.t_cljs$core$async29575.prototype.cljs$core$async$Mux$muxch_STAR_$arity$1 = ((function (cs){
return (function (_){
var self__ = this;
var ___$1 = this;
return self__.ch;
});})(cs))
;

cljs.core.async.t_cljs$core$async29575.prototype.cljs$core$async$Mult$ = true;

cljs.core.async.t_cljs$core$async29575.prototype.cljs$core$async$Mult$tap_STAR_$arity$3 = ((function (cs){
return (function (_,ch__$1,close_QMARK_){
var self__ = this;
var ___$1 = this;
cljs.core.swap_BANG_.call(null,self__.cs,cljs.core.assoc,ch__$1,close_QMARK_);

return null;
});})(cs))
;

cljs.core.async.t_cljs$core$async29575.prototype.cljs$core$async$Mult$untap_STAR_$arity$2 = ((function (cs){
return (function (_,ch__$1){
var self__ = this;
var ___$1 = this;
cljs.core.swap_BANG_.call(null,self__.cs,cljs.core.dissoc,ch__$1);

return null;
});})(cs))
;

cljs.core.async.t_cljs$core$async29575.prototype.cljs$core$async$Mult$untap_all_STAR_$arity$1 = ((function (cs){
return (function (_){
var self__ = this;
var ___$1 = this;
cljs.core.reset_BANG_.call(null,self__.cs,cljs.core.PersistentArrayMap.EMPTY);

return null;
});})(cs))
;

cljs.core.async.t_cljs$core$async29575.getBasis = ((function (cs){
return (function (){
return new cljs.core.PersistentVector(null, 4, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.with_meta(new cljs.core.Symbol(null,"mult","mult",-1187640995,null),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"arglists","arglists",1661989754),cljs.core.list(new cljs.core.Symbol(null,"quote","quote",1377916282,null),cljs.core.list(new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"ch","ch",1085813622,null)], null))),new cljs.core.Keyword(null,"doc","doc",1913296891),"Creates and returns a mult(iple) of the supplied channel. Channels\n  containing copies of the channel can be created with 'tap', and\n  detached with 'untap'.\n\n  Each item is distributed to all taps in parallel and synchronously,\n  i.e. each tap must accept before the next item is distributed. Use\n  buffering/windowing to prevent slow taps from holding up the mult.\n\n  Items received when there are no taps get dropped.\n\n  If a tap puts to a closed channel, it will be removed from the mult."], null)),new cljs.core.Symbol(null,"ch","ch",1085813622,null),new cljs.core.Symbol(null,"cs","cs",-117024463,null),new cljs.core.Symbol(null,"meta29576","meta29576",1012996552,null)], null);
});})(cs))
;

cljs.core.async.t_cljs$core$async29575.cljs$lang$type = true;

cljs.core.async.t_cljs$core$async29575.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async29575";

cljs.core.async.t_cljs$core$async29575.cljs$lang$ctorPrWriter = ((function (cs){
return (function (this__23741__auto__,writer__23742__auto__,opt__23743__auto__){
return cljs.core._write.call(null,writer__23742__auto__,"cljs.core.async/t_cljs$core$async29575");
});})(cs))
;

cljs.core.async.__GT_t_cljs$core$async29575 = ((function (cs){
return (function cljs$core$async$mult_$___GT_t_cljs$core$async29575(mult__$1,ch__$1,cs__$1,meta29576){
return (new cljs.core.async.t_cljs$core$async29575(mult__$1,ch__$1,cs__$1,meta29576));
});})(cs))
;

}

return (new cljs.core.async.t_cljs$core$async29575(cljs$core$async$mult,ch,cs,cljs.core.PersistentArrayMap.EMPTY));
})()
;
var dchan = cljs.core.async.chan.call(null,(1));
var dctr = cljs.core.atom.call(null,null);
var done = ((function (cs,m,dchan,dctr){
return (function (_){
if((cljs.core.swap_BANG_.call(null,dctr,cljs.core.dec) === (0))){
return cljs.core.async.put_BANG_.call(null,dchan,true);
} else {
return null;
}
});})(cs,m,dchan,dctr))
;
var c__28489__auto___29796 = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__28489__auto___29796,cs,m,dchan,dctr,done){
return (function (){
var f__28490__auto__ = (function (){var switch__28377__auto__ = ((function (c__28489__auto___29796,cs,m,dchan,dctr,done){
return (function (state_29708){
var state_val_29709 = (state_29708[(1)]);
if((state_val_29709 === (7))){
var inst_29704 = (state_29708[(2)]);
var state_29708__$1 = state_29708;
var statearr_29710_29797 = state_29708__$1;
(statearr_29710_29797[(2)] = inst_29704);

(statearr_29710_29797[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_29709 === (20))){
var inst_29609 = (state_29708[(7)]);
var inst_29619 = cljs.core.first.call(null,inst_29609);
var inst_29620 = cljs.core.nth.call(null,inst_29619,(0),null);
var inst_29621 = cljs.core.nth.call(null,inst_29619,(1),null);
var state_29708__$1 = (function (){var statearr_29711 = state_29708;
(statearr_29711[(8)] = inst_29620);

return statearr_29711;
})();
if(cljs.core.truth_(inst_29621)){
var statearr_29712_29798 = state_29708__$1;
(statearr_29712_29798[(1)] = (22));

} else {
var statearr_29713_29799 = state_29708__$1;
(statearr_29713_29799[(1)] = (23));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_29709 === (27))){
var inst_29649 = (state_29708[(9)]);
var inst_29656 = (state_29708[(10)]);
var inst_29651 = (state_29708[(11)]);
var inst_29580 = (state_29708[(12)]);
var inst_29656__$1 = cljs.core._nth.call(null,inst_29649,inst_29651);
var inst_29657 = cljs.core.async.put_BANG_.call(null,inst_29656__$1,inst_29580,done);
var state_29708__$1 = (function (){var statearr_29714 = state_29708;
(statearr_29714[(10)] = inst_29656__$1);

return statearr_29714;
})();
if(cljs.core.truth_(inst_29657)){
var statearr_29715_29800 = state_29708__$1;
(statearr_29715_29800[(1)] = (30));

} else {
var statearr_29716_29801 = state_29708__$1;
(statearr_29716_29801[(1)] = (31));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_29709 === (1))){
var state_29708__$1 = state_29708;
var statearr_29717_29802 = state_29708__$1;
(statearr_29717_29802[(2)] = null);

(statearr_29717_29802[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_29709 === (24))){
var inst_29609 = (state_29708[(7)]);
var inst_29626 = (state_29708[(2)]);
var inst_29627 = cljs.core.next.call(null,inst_29609);
var inst_29589 = inst_29627;
var inst_29590 = null;
var inst_29591 = (0);
var inst_29592 = (0);
var state_29708__$1 = (function (){var statearr_29718 = state_29708;
(statearr_29718[(13)] = inst_29590);

(statearr_29718[(14)] = inst_29592);

(statearr_29718[(15)] = inst_29591);

(statearr_29718[(16)] = inst_29589);

(statearr_29718[(17)] = inst_29626);

return statearr_29718;
})();
var statearr_29719_29803 = state_29708__$1;
(statearr_29719_29803[(2)] = null);

(statearr_29719_29803[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_29709 === (39))){
var state_29708__$1 = state_29708;
var statearr_29723_29804 = state_29708__$1;
(statearr_29723_29804[(2)] = null);

(statearr_29723_29804[(1)] = (41));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_29709 === (4))){
var inst_29580 = (state_29708[(12)]);
var inst_29580__$1 = (state_29708[(2)]);
var inst_29581 = (inst_29580__$1 == null);
var state_29708__$1 = (function (){var statearr_29724 = state_29708;
(statearr_29724[(12)] = inst_29580__$1);

return statearr_29724;
})();
if(cljs.core.truth_(inst_29581)){
var statearr_29725_29805 = state_29708__$1;
(statearr_29725_29805[(1)] = (5));

} else {
var statearr_29726_29806 = state_29708__$1;
(statearr_29726_29806[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_29709 === (15))){
var inst_29590 = (state_29708[(13)]);
var inst_29592 = (state_29708[(14)]);
var inst_29591 = (state_29708[(15)]);
var inst_29589 = (state_29708[(16)]);
var inst_29605 = (state_29708[(2)]);
var inst_29606 = (inst_29592 + (1));
var tmp29720 = inst_29590;
var tmp29721 = inst_29591;
var tmp29722 = inst_29589;
var inst_29589__$1 = tmp29722;
var inst_29590__$1 = tmp29720;
var inst_29591__$1 = tmp29721;
var inst_29592__$1 = inst_29606;
var state_29708__$1 = (function (){var statearr_29727 = state_29708;
(statearr_29727[(13)] = inst_29590__$1);

(statearr_29727[(18)] = inst_29605);

(statearr_29727[(14)] = inst_29592__$1);

(statearr_29727[(15)] = inst_29591__$1);

(statearr_29727[(16)] = inst_29589__$1);

return statearr_29727;
})();
var statearr_29728_29807 = state_29708__$1;
(statearr_29728_29807[(2)] = null);

(statearr_29728_29807[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_29709 === (21))){
var inst_29630 = (state_29708[(2)]);
var state_29708__$1 = state_29708;
var statearr_29732_29808 = state_29708__$1;
(statearr_29732_29808[(2)] = inst_29630);

(statearr_29732_29808[(1)] = (18));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_29709 === (31))){
var inst_29656 = (state_29708[(10)]);
var inst_29660 = done.call(null,null);
var inst_29661 = cljs.core.async.untap_STAR_.call(null,m,inst_29656);
var state_29708__$1 = (function (){var statearr_29733 = state_29708;
(statearr_29733[(19)] = inst_29660);

return statearr_29733;
})();
var statearr_29734_29809 = state_29708__$1;
(statearr_29734_29809[(2)] = inst_29661);

(statearr_29734_29809[(1)] = (32));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_29709 === (32))){
var inst_29649 = (state_29708[(9)]);
var inst_29648 = (state_29708[(20)]);
var inst_29650 = (state_29708[(21)]);
var inst_29651 = (state_29708[(11)]);
var inst_29663 = (state_29708[(2)]);
var inst_29664 = (inst_29651 + (1));
var tmp29729 = inst_29649;
var tmp29730 = inst_29648;
var tmp29731 = inst_29650;
var inst_29648__$1 = tmp29730;
var inst_29649__$1 = tmp29729;
var inst_29650__$1 = tmp29731;
var inst_29651__$1 = inst_29664;
var state_29708__$1 = (function (){var statearr_29735 = state_29708;
(statearr_29735[(9)] = inst_29649__$1);

(statearr_29735[(20)] = inst_29648__$1);

(statearr_29735[(21)] = inst_29650__$1);

(statearr_29735[(22)] = inst_29663);

(statearr_29735[(11)] = inst_29651__$1);

return statearr_29735;
})();
var statearr_29736_29810 = state_29708__$1;
(statearr_29736_29810[(2)] = null);

(statearr_29736_29810[(1)] = (25));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_29709 === (40))){
var inst_29676 = (state_29708[(23)]);
var inst_29680 = done.call(null,null);
var inst_29681 = cljs.core.async.untap_STAR_.call(null,m,inst_29676);
var state_29708__$1 = (function (){var statearr_29737 = state_29708;
(statearr_29737[(24)] = inst_29680);

return statearr_29737;
})();
var statearr_29738_29811 = state_29708__$1;
(statearr_29738_29811[(2)] = inst_29681);

(statearr_29738_29811[(1)] = (41));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_29709 === (33))){
var inst_29667 = (state_29708[(25)]);
var inst_29669 = cljs.core.chunked_seq_QMARK_.call(null,inst_29667);
var state_29708__$1 = state_29708;
if(inst_29669){
var statearr_29739_29812 = state_29708__$1;
(statearr_29739_29812[(1)] = (36));

} else {
var statearr_29740_29813 = state_29708__$1;
(statearr_29740_29813[(1)] = (37));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_29709 === (13))){
var inst_29599 = (state_29708[(26)]);
var inst_29602 = cljs.core.async.close_BANG_.call(null,inst_29599);
var state_29708__$1 = state_29708;
var statearr_29741_29814 = state_29708__$1;
(statearr_29741_29814[(2)] = inst_29602);

(statearr_29741_29814[(1)] = (15));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_29709 === (22))){
var inst_29620 = (state_29708[(8)]);
var inst_29623 = cljs.core.async.close_BANG_.call(null,inst_29620);
var state_29708__$1 = state_29708;
var statearr_29742_29815 = state_29708__$1;
(statearr_29742_29815[(2)] = inst_29623);

(statearr_29742_29815[(1)] = (24));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_29709 === (36))){
var inst_29667 = (state_29708[(25)]);
var inst_29671 = cljs.core.chunk_first.call(null,inst_29667);
var inst_29672 = cljs.core.chunk_rest.call(null,inst_29667);
var inst_29673 = cljs.core.count.call(null,inst_29671);
var inst_29648 = inst_29672;
var inst_29649 = inst_29671;
var inst_29650 = inst_29673;
var inst_29651 = (0);
var state_29708__$1 = (function (){var statearr_29743 = state_29708;
(statearr_29743[(9)] = inst_29649);

(statearr_29743[(20)] = inst_29648);

(statearr_29743[(21)] = inst_29650);

(statearr_29743[(11)] = inst_29651);

return statearr_29743;
})();
var statearr_29744_29816 = state_29708__$1;
(statearr_29744_29816[(2)] = null);

(statearr_29744_29816[(1)] = (25));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_29709 === (41))){
var inst_29667 = (state_29708[(25)]);
var inst_29683 = (state_29708[(2)]);
var inst_29684 = cljs.core.next.call(null,inst_29667);
var inst_29648 = inst_29684;
var inst_29649 = null;
var inst_29650 = (0);
var inst_29651 = (0);
var state_29708__$1 = (function (){var statearr_29745 = state_29708;
(statearr_29745[(9)] = inst_29649);

(statearr_29745[(20)] = inst_29648);

(statearr_29745[(27)] = inst_29683);

(statearr_29745[(21)] = inst_29650);

(statearr_29745[(11)] = inst_29651);

return statearr_29745;
})();
var statearr_29746_29817 = state_29708__$1;
(statearr_29746_29817[(2)] = null);

(statearr_29746_29817[(1)] = (25));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_29709 === (43))){
var state_29708__$1 = state_29708;
var statearr_29747_29818 = state_29708__$1;
(statearr_29747_29818[(2)] = null);

(statearr_29747_29818[(1)] = (44));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_29709 === (29))){
var inst_29692 = (state_29708[(2)]);
var state_29708__$1 = state_29708;
var statearr_29748_29819 = state_29708__$1;
(statearr_29748_29819[(2)] = inst_29692);

(statearr_29748_29819[(1)] = (26));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_29709 === (44))){
var inst_29701 = (state_29708[(2)]);
var state_29708__$1 = (function (){var statearr_29749 = state_29708;
(statearr_29749[(28)] = inst_29701);

return statearr_29749;
})();
var statearr_29750_29820 = state_29708__$1;
(statearr_29750_29820[(2)] = null);

(statearr_29750_29820[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_29709 === (6))){
var inst_29640 = (state_29708[(29)]);
var inst_29639 = cljs.core.deref.call(null,cs);
var inst_29640__$1 = cljs.core.keys.call(null,inst_29639);
var inst_29641 = cljs.core.count.call(null,inst_29640__$1);
var inst_29642 = cljs.core.reset_BANG_.call(null,dctr,inst_29641);
var inst_29647 = cljs.core.seq.call(null,inst_29640__$1);
var inst_29648 = inst_29647;
var inst_29649 = null;
var inst_29650 = (0);
var inst_29651 = (0);
var state_29708__$1 = (function (){var statearr_29751 = state_29708;
(statearr_29751[(9)] = inst_29649);

(statearr_29751[(20)] = inst_29648);

(statearr_29751[(30)] = inst_29642);

(statearr_29751[(21)] = inst_29650);

(statearr_29751[(11)] = inst_29651);

(statearr_29751[(29)] = inst_29640__$1);

return statearr_29751;
})();
var statearr_29752_29821 = state_29708__$1;
(statearr_29752_29821[(2)] = null);

(statearr_29752_29821[(1)] = (25));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_29709 === (28))){
var inst_29648 = (state_29708[(20)]);
var inst_29667 = (state_29708[(25)]);
var inst_29667__$1 = cljs.core.seq.call(null,inst_29648);
var state_29708__$1 = (function (){var statearr_29753 = state_29708;
(statearr_29753[(25)] = inst_29667__$1);

return statearr_29753;
})();
if(inst_29667__$1){
var statearr_29754_29822 = state_29708__$1;
(statearr_29754_29822[(1)] = (33));

} else {
var statearr_29755_29823 = state_29708__$1;
(statearr_29755_29823[(1)] = (34));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_29709 === (25))){
var inst_29650 = (state_29708[(21)]);
var inst_29651 = (state_29708[(11)]);
var inst_29653 = (inst_29651 < inst_29650);
var inst_29654 = inst_29653;
var state_29708__$1 = state_29708;
if(cljs.core.truth_(inst_29654)){
var statearr_29756_29824 = state_29708__$1;
(statearr_29756_29824[(1)] = (27));

} else {
var statearr_29757_29825 = state_29708__$1;
(statearr_29757_29825[(1)] = (28));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_29709 === (34))){
var state_29708__$1 = state_29708;
var statearr_29758_29826 = state_29708__$1;
(statearr_29758_29826[(2)] = null);

(statearr_29758_29826[(1)] = (35));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_29709 === (17))){
var state_29708__$1 = state_29708;
var statearr_29759_29827 = state_29708__$1;
(statearr_29759_29827[(2)] = null);

(statearr_29759_29827[(1)] = (18));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_29709 === (3))){
var inst_29706 = (state_29708[(2)]);
var state_29708__$1 = state_29708;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_29708__$1,inst_29706);
} else {
if((state_val_29709 === (12))){
var inst_29635 = (state_29708[(2)]);
var state_29708__$1 = state_29708;
var statearr_29760_29828 = state_29708__$1;
(statearr_29760_29828[(2)] = inst_29635);

(statearr_29760_29828[(1)] = (9));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_29709 === (2))){
var state_29708__$1 = state_29708;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_29708__$1,(4),ch);
} else {
if((state_val_29709 === (23))){
var state_29708__$1 = state_29708;
var statearr_29761_29829 = state_29708__$1;
(statearr_29761_29829[(2)] = null);

(statearr_29761_29829[(1)] = (24));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_29709 === (35))){
var inst_29690 = (state_29708[(2)]);
var state_29708__$1 = state_29708;
var statearr_29762_29830 = state_29708__$1;
(statearr_29762_29830[(2)] = inst_29690);

(statearr_29762_29830[(1)] = (29));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_29709 === (19))){
var inst_29609 = (state_29708[(7)]);
var inst_29613 = cljs.core.chunk_first.call(null,inst_29609);
var inst_29614 = cljs.core.chunk_rest.call(null,inst_29609);
var inst_29615 = cljs.core.count.call(null,inst_29613);
var inst_29589 = inst_29614;
var inst_29590 = inst_29613;
var inst_29591 = inst_29615;
var inst_29592 = (0);
var state_29708__$1 = (function (){var statearr_29763 = state_29708;
(statearr_29763[(13)] = inst_29590);

(statearr_29763[(14)] = inst_29592);

(statearr_29763[(15)] = inst_29591);

(statearr_29763[(16)] = inst_29589);

return statearr_29763;
})();
var statearr_29764_29831 = state_29708__$1;
(statearr_29764_29831[(2)] = null);

(statearr_29764_29831[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_29709 === (11))){
var inst_29609 = (state_29708[(7)]);
var inst_29589 = (state_29708[(16)]);
var inst_29609__$1 = cljs.core.seq.call(null,inst_29589);
var state_29708__$1 = (function (){var statearr_29765 = state_29708;
(statearr_29765[(7)] = inst_29609__$1);

return statearr_29765;
})();
if(inst_29609__$1){
var statearr_29766_29832 = state_29708__$1;
(statearr_29766_29832[(1)] = (16));

} else {
var statearr_29767_29833 = state_29708__$1;
(statearr_29767_29833[(1)] = (17));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_29709 === (9))){
var inst_29637 = (state_29708[(2)]);
var state_29708__$1 = state_29708;
var statearr_29768_29834 = state_29708__$1;
(statearr_29768_29834[(2)] = inst_29637);

(statearr_29768_29834[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_29709 === (5))){
var inst_29587 = cljs.core.deref.call(null,cs);
var inst_29588 = cljs.core.seq.call(null,inst_29587);
var inst_29589 = inst_29588;
var inst_29590 = null;
var inst_29591 = (0);
var inst_29592 = (0);
var state_29708__$1 = (function (){var statearr_29769 = state_29708;
(statearr_29769[(13)] = inst_29590);

(statearr_29769[(14)] = inst_29592);

(statearr_29769[(15)] = inst_29591);

(statearr_29769[(16)] = inst_29589);

return statearr_29769;
})();
var statearr_29770_29835 = state_29708__$1;
(statearr_29770_29835[(2)] = null);

(statearr_29770_29835[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_29709 === (14))){
var state_29708__$1 = state_29708;
var statearr_29771_29836 = state_29708__$1;
(statearr_29771_29836[(2)] = null);

(statearr_29771_29836[(1)] = (15));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_29709 === (45))){
var inst_29698 = (state_29708[(2)]);
var state_29708__$1 = state_29708;
var statearr_29772_29837 = state_29708__$1;
(statearr_29772_29837[(2)] = inst_29698);

(statearr_29772_29837[(1)] = (44));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_29709 === (26))){
var inst_29640 = (state_29708[(29)]);
var inst_29694 = (state_29708[(2)]);
var inst_29695 = cljs.core.seq.call(null,inst_29640);
var state_29708__$1 = (function (){var statearr_29773 = state_29708;
(statearr_29773[(31)] = inst_29694);

return statearr_29773;
})();
if(inst_29695){
var statearr_29774_29838 = state_29708__$1;
(statearr_29774_29838[(1)] = (42));

} else {
var statearr_29775_29839 = state_29708__$1;
(statearr_29775_29839[(1)] = (43));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_29709 === (16))){
var inst_29609 = (state_29708[(7)]);
var inst_29611 = cljs.core.chunked_seq_QMARK_.call(null,inst_29609);
var state_29708__$1 = state_29708;
if(inst_29611){
var statearr_29776_29840 = state_29708__$1;
(statearr_29776_29840[(1)] = (19));

} else {
var statearr_29777_29841 = state_29708__$1;
(statearr_29777_29841[(1)] = (20));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_29709 === (38))){
var inst_29687 = (state_29708[(2)]);
var state_29708__$1 = state_29708;
var statearr_29778_29842 = state_29708__$1;
(statearr_29778_29842[(2)] = inst_29687);

(statearr_29778_29842[(1)] = (35));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_29709 === (30))){
var state_29708__$1 = state_29708;
var statearr_29779_29843 = state_29708__$1;
(statearr_29779_29843[(2)] = null);

(statearr_29779_29843[(1)] = (32));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_29709 === (10))){
var inst_29590 = (state_29708[(13)]);
var inst_29592 = (state_29708[(14)]);
var inst_29598 = cljs.core._nth.call(null,inst_29590,inst_29592);
var inst_29599 = cljs.core.nth.call(null,inst_29598,(0),null);
var inst_29600 = cljs.core.nth.call(null,inst_29598,(1),null);
var state_29708__$1 = (function (){var statearr_29780 = state_29708;
(statearr_29780[(26)] = inst_29599);

return statearr_29780;
})();
if(cljs.core.truth_(inst_29600)){
var statearr_29781_29844 = state_29708__$1;
(statearr_29781_29844[(1)] = (13));

} else {
var statearr_29782_29845 = state_29708__$1;
(statearr_29782_29845[(1)] = (14));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_29709 === (18))){
var inst_29633 = (state_29708[(2)]);
var state_29708__$1 = state_29708;
var statearr_29783_29846 = state_29708__$1;
(statearr_29783_29846[(2)] = inst_29633);

(statearr_29783_29846[(1)] = (12));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_29709 === (42))){
var state_29708__$1 = state_29708;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_29708__$1,(45),dchan);
} else {
if((state_val_29709 === (37))){
var inst_29676 = (state_29708[(23)]);
var inst_29580 = (state_29708[(12)]);
var inst_29667 = (state_29708[(25)]);
var inst_29676__$1 = cljs.core.first.call(null,inst_29667);
var inst_29677 = cljs.core.async.put_BANG_.call(null,inst_29676__$1,inst_29580,done);
var state_29708__$1 = (function (){var statearr_29784 = state_29708;
(statearr_29784[(23)] = inst_29676__$1);

return statearr_29784;
})();
if(cljs.core.truth_(inst_29677)){
var statearr_29785_29847 = state_29708__$1;
(statearr_29785_29847[(1)] = (39));

} else {
var statearr_29786_29848 = state_29708__$1;
(statearr_29786_29848[(1)] = (40));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_29709 === (8))){
var inst_29592 = (state_29708[(14)]);
var inst_29591 = (state_29708[(15)]);
var inst_29594 = (inst_29592 < inst_29591);
var inst_29595 = inst_29594;
var state_29708__$1 = state_29708;
if(cljs.core.truth_(inst_29595)){
var statearr_29787_29849 = state_29708__$1;
(statearr_29787_29849[(1)] = (10));

} else {
var statearr_29788_29850 = state_29708__$1;
(statearr_29788_29850[(1)] = (11));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__28489__auto___29796,cs,m,dchan,dctr,done))
;
return ((function (switch__28377__auto__,c__28489__auto___29796,cs,m,dchan,dctr,done){
return (function() {
var cljs$core$async$mult_$_state_machine__28378__auto__ = null;
var cljs$core$async$mult_$_state_machine__28378__auto____0 = (function (){
var statearr_29792 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_29792[(0)] = cljs$core$async$mult_$_state_machine__28378__auto__);

(statearr_29792[(1)] = (1));

return statearr_29792;
});
var cljs$core$async$mult_$_state_machine__28378__auto____1 = (function (state_29708){
while(true){
var ret_value__28379__auto__ = (function (){try{while(true){
var result__28380__auto__ = switch__28377__auto__.call(null,state_29708);
if(cljs.core.keyword_identical_QMARK_.call(null,result__28380__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__28380__auto__;
}
break;
}
}catch (e29793){if((e29793 instanceof Object)){
var ex__28381__auto__ = e29793;
var statearr_29794_29851 = state_29708;
(statearr_29794_29851[(5)] = ex__28381__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_29708);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e29793;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__28379__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__29852 = state_29708;
state_29708 = G__29852;
continue;
} else {
return ret_value__28379__auto__;
}
break;
}
});
cljs$core$async$mult_$_state_machine__28378__auto__ = function(state_29708){
switch(arguments.length){
case 0:
return cljs$core$async$mult_$_state_machine__28378__auto____0.call(this);
case 1:
return cljs$core$async$mult_$_state_machine__28378__auto____1.call(this,state_29708);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$mult_$_state_machine__28378__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$mult_$_state_machine__28378__auto____0;
cljs$core$async$mult_$_state_machine__28378__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$mult_$_state_machine__28378__auto____1;
return cljs$core$async$mult_$_state_machine__28378__auto__;
})()
;})(switch__28377__auto__,c__28489__auto___29796,cs,m,dchan,dctr,done))
})();
var state__28491__auto__ = (function (){var statearr_29795 = f__28490__auto__.call(null);
(statearr_29795[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__28489__auto___29796);

return statearr_29795;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__28491__auto__);
});})(c__28489__auto___29796,cs,m,dchan,dctr,done))
);


return m;
});
/**
 * Copies the mult source onto the supplied channel.
 * 
 *   By default the channel will be closed when the source closes,
 *   but can be determined by the close? parameter.
 */
cljs.core.async.tap = (function cljs$core$async$tap(var_args){
var args29853 = [];
var len__24201__auto___29856 = arguments.length;
var i__24202__auto___29857 = (0);
while(true){
if((i__24202__auto___29857 < len__24201__auto___29856)){
args29853.push((arguments[i__24202__auto___29857]));

var G__29858 = (i__24202__auto___29857 + (1));
i__24202__auto___29857 = G__29858;
continue;
} else {
}
break;
}

var G__29855 = args29853.length;
switch (G__29855) {
case 2:
return cljs.core.async.tap.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.tap.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error([cljs.core.str("Invalid arity: "),cljs.core.str(args29853.length)].join('')));

}
});

cljs.core.async.tap.cljs$core$IFn$_invoke$arity$2 = (function (mult,ch){
return cljs.core.async.tap.call(null,mult,ch,true);
});

cljs.core.async.tap.cljs$core$IFn$_invoke$arity$3 = (function (mult,ch,close_QMARK_){
cljs.core.async.tap_STAR_.call(null,mult,ch,close_QMARK_);

return ch;
});

cljs.core.async.tap.cljs$lang$maxFixedArity = 3;
/**
 * Disconnects a target channel from a mult
 */
cljs.core.async.untap = (function cljs$core$async$untap(mult,ch){
return cljs.core.async.untap_STAR_.call(null,mult,ch);
});
/**
 * Disconnects all target channels from a mult
 */
cljs.core.async.untap_all = (function cljs$core$async$untap_all(mult){
return cljs.core.async.untap_all_STAR_.call(null,mult);
});

/**
 * @interface
 */
cljs.core.async.Mix = function(){};

cljs.core.async.admix_STAR_ = (function cljs$core$async$admix_STAR_(m,ch){
if((!((m == null))) && (!((m.cljs$core$async$Mix$admix_STAR_$arity$2 == null)))){
return m.cljs$core$async$Mix$admix_STAR_$arity$2(m,ch);
} else {
var x__23798__auto__ = (((m == null))?null:m);
var m__23799__auto__ = (cljs.core.async.admix_STAR_[goog.typeOf(x__23798__auto__)]);
if(!((m__23799__auto__ == null))){
return m__23799__auto__.call(null,m,ch);
} else {
var m__23799__auto____$1 = (cljs.core.async.admix_STAR_["_"]);
if(!((m__23799__auto____$1 == null))){
return m__23799__auto____$1.call(null,m,ch);
} else {
throw cljs.core.missing_protocol.call(null,"Mix.admix*",m);
}
}
}
});

cljs.core.async.unmix_STAR_ = (function cljs$core$async$unmix_STAR_(m,ch){
if((!((m == null))) && (!((m.cljs$core$async$Mix$unmix_STAR_$arity$2 == null)))){
return m.cljs$core$async$Mix$unmix_STAR_$arity$2(m,ch);
} else {
var x__23798__auto__ = (((m == null))?null:m);
var m__23799__auto__ = (cljs.core.async.unmix_STAR_[goog.typeOf(x__23798__auto__)]);
if(!((m__23799__auto__ == null))){
return m__23799__auto__.call(null,m,ch);
} else {
var m__23799__auto____$1 = (cljs.core.async.unmix_STAR_["_"]);
if(!((m__23799__auto____$1 == null))){
return m__23799__auto____$1.call(null,m,ch);
} else {
throw cljs.core.missing_protocol.call(null,"Mix.unmix*",m);
}
}
}
});

cljs.core.async.unmix_all_STAR_ = (function cljs$core$async$unmix_all_STAR_(m){
if((!((m == null))) && (!((m.cljs$core$async$Mix$unmix_all_STAR_$arity$1 == null)))){
return m.cljs$core$async$Mix$unmix_all_STAR_$arity$1(m);
} else {
var x__23798__auto__ = (((m == null))?null:m);
var m__23799__auto__ = (cljs.core.async.unmix_all_STAR_[goog.typeOf(x__23798__auto__)]);
if(!((m__23799__auto__ == null))){
return m__23799__auto__.call(null,m);
} else {
var m__23799__auto____$1 = (cljs.core.async.unmix_all_STAR_["_"]);
if(!((m__23799__auto____$1 == null))){
return m__23799__auto____$1.call(null,m);
} else {
throw cljs.core.missing_protocol.call(null,"Mix.unmix-all*",m);
}
}
}
});

cljs.core.async.toggle_STAR_ = (function cljs$core$async$toggle_STAR_(m,state_map){
if((!((m == null))) && (!((m.cljs$core$async$Mix$toggle_STAR_$arity$2 == null)))){
return m.cljs$core$async$Mix$toggle_STAR_$arity$2(m,state_map);
} else {
var x__23798__auto__ = (((m == null))?null:m);
var m__23799__auto__ = (cljs.core.async.toggle_STAR_[goog.typeOf(x__23798__auto__)]);
if(!((m__23799__auto__ == null))){
return m__23799__auto__.call(null,m,state_map);
} else {
var m__23799__auto____$1 = (cljs.core.async.toggle_STAR_["_"]);
if(!((m__23799__auto____$1 == null))){
return m__23799__auto____$1.call(null,m,state_map);
} else {
throw cljs.core.missing_protocol.call(null,"Mix.toggle*",m);
}
}
}
});

cljs.core.async.solo_mode_STAR_ = (function cljs$core$async$solo_mode_STAR_(m,mode){
if((!((m == null))) && (!((m.cljs$core$async$Mix$solo_mode_STAR_$arity$2 == null)))){
return m.cljs$core$async$Mix$solo_mode_STAR_$arity$2(m,mode);
} else {
var x__23798__auto__ = (((m == null))?null:m);
var m__23799__auto__ = (cljs.core.async.solo_mode_STAR_[goog.typeOf(x__23798__auto__)]);
if(!((m__23799__auto__ == null))){
return m__23799__auto__.call(null,m,mode);
} else {
var m__23799__auto____$1 = (cljs.core.async.solo_mode_STAR_["_"]);
if(!((m__23799__auto____$1 == null))){
return m__23799__auto____$1.call(null,m,mode);
} else {
throw cljs.core.missing_protocol.call(null,"Mix.solo-mode*",m);
}
}
}
});

cljs.core.async.ioc_alts_BANG_ = (function cljs$core$async$ioc_alts_BANG_(var_args){
var args__24208__auto__ = [];
var len__24201__auto___29870 = arguments.length;
var i__24202__auto___29871 = (0);
while(true){
if((i__24202__auto___29871 < len__24201__auto___29870)){
args__24208__auto__.push((arguments[i__24202__auto___29871]));

var G__29872 = (i__24202__auto___29871 + (1));
i__24202__auto___29871 = G__29872;
continue;
} else {
}
break;
}

var argseq__24209__auto__ = ((((3) < args__24208__auto__.length))?(new cljs.core.IndexedSeq(args__24208__auto__.slice((3)),(0))):null);
return cljs.core.async.ioc_alts_BANG_.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),argseq__24209__auto__);
});

cljs.core.async.ioc_alts_BANG_.cljs$core$IFn$_invoke$arity$variadic = (function (state,cont_block,ports,p__29864){
var map__29865 = p__29864;
var map__29865__$1 = ((((!((map__29865 == null)))?((((map__29865.cljs$lang$protocol_mask$partition0$ & (64))) || (map__29865.cljs$core$ISeq$))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__29865):map__29865);
var opts = map__29865__$1;
var statearr_29867_29873 = state;
(statearr_29867_29873[cljs.core.async.impl.ioc_helpers.STATE_IDX] = cont_block);


var temp__4657__auto__ = cljs.core.async.do_alts.call(null,((function (map__29865,map__29865__$1,opts){
return (function (val){
var statearr_29868_29874 = state;
(statearr_29868_29874[cljs.core.async.impl.ioc_helpers.VALUE_IDX] = val);


return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state);
});})(map__29865,map__29865__$1,opts))
,ports,opts);
if(cljs.core.truth_(temp__4657__auto__)){
var cb = temp__4657__auto__;
var statearr_29869_29875 = state;
(statearr_29869_29875[cljs.core.async.impl.ioc_helpers.VALUE_IDX] = cljs.core.deref.call(null,cb));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
});

cljs.core.async.ioc_alts_BANG_.cljs$lang$maxFixedArity = (3);

cljs.core.async.ioc_alts_BANG_.cljs$lang$applyTo = (function (seq29860){
var G__29861 = cljs.core.first.call(null,seq29860);
var seq29860__$1 = cljs.core.next.call(null,seq29860);
var G__29862 = cljs.core.first.call(null,seq29860__$1);
var seq29860__$2 = cljs.core.next.call(null,seq29860__$1);
var G__29863 = cljs.core.first.call(null,seq29860__$2);
var seq29860__$3 = cljs.core.next.call(null,seq29860__$2);
return cljs.core.async.ioc_alts_BANG_.cljs$core$IFn$_invoke$arity$variadic(G__29861,G__29862,G__29863,seq29860__$3);
});
/**
 * Creates and returns a mix of one or more input channels which will
 *   be put on the supplied out channel. Input sources can be added to
 *   the mix with 'admix', and removed with 'unmix'. A mix supports
 *   soloing, muting and pausing multiple inputs atomically using
 *   'toggle', and can solo using either muting or pausing as determined
 *   by 'solo-mode'.
 * 
 *   Each channel can have zero or more boolean modes set via 'toggle':
 * 
 *   :solo - when true, only this (ond other soloed) channel(s) will appear
 *        in the mix output channel. :mute and :pause states of soloed
 *        channels are ignored. If solo-mode is :mute, non-soloed
 *        channels are muted, if :pause, non-soloed channels are
 *        paused.
 * 
 *   :mute - muted channels will have their contents consumed but not included in the mix
 *   :pause - paused channels will not have their contents consumed (and thus also not included in the mix)
 */
cljs.core.async.mix = (function cljs$core$async$mix(out){
var cs = cljs.core.atom.call(null,cljs.core.PersistentArrayMap.EMPTY);
var solo_modes = new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"pause","pause",-2095325672),null,new cljs.core.Keyword(null,"mute","mute",1151223646),null], null), null);
var attrs = cljs.core.conj.call(null,solo_modes,new cljs.core.Keyword(null,"solo","solo",-316350075));
var solo_mode = cljs.core.atom.call(null,new cljs.core.Keyword(null,"mute","mute",1151223646));
var change = cljs.core.async.chan.call(null);
var changed = ((function (cs,solo_modes,attrs,solo_mode,change){
return (function (){
return cljs.core.async.put_BANG_.call(null,change,true);
});})(cs,solo_modes,attrs,solo_mode,change))
;
var pick = ((function (cs,solo_modes,attrs,solo_mode,change,changed){
return (function (attr,chs){
return cljs.core.reduce_kv.call(null,((function (cs,solo_modes,attrs,solo_mode,change,changed){
return (function (ret,c,v){
if(cljs.core.truth_(attr.call(null,v))){
return cljs.core.conj.call(null,ret,c);
} else {
return ret;
}
});})(cs,solo_modes,attrs,solo_mode,change,changed))
,cljs.core.PersistentHashSet.EMPTY,chs);
});})(cs,solo_modes,attrs,solo_mode,change,changed))
;
var calc_state = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick){
return (function (){
var chs = cljs.core.deref.call(null,cs);
var mode = cljs.core.deref.call(null,solo_mode);
var solos = pick.call(null,new cljs.core.Keyword(null,"solo","solo",-316350075),chs);
var pauses = pick.call(null,new cljs.core.Keyword(null,"pause","pause",-2095325672),chs);
return new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"solos","solos",1441458643),solos,new cljs.core.Keyword(null,"mutes","mutes",1068806309),pick.call(null,new cljs.core.Keyword(null,"mute","mute",1151223646),chs),new cljs.core.Keyword(null,"reads","reads",-1215067361),cljs.core.conj.call(null,(((cljs.core._EQ_.call(null,mode,new cljs.core.Keyword(null,"pause","pause",-2095325672))) && (!(cljs.core.empty_QMARK_.call(null,solos))))?cljs.core.vec.call(null,solos):cljs.core.vec.call(null,cljs.core.remove.call(null,pauses,cljs.core.keys.call(null,chs)))),change)], null);
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick))
;
var m = (function (){
if(typeof cljs.core.async.t_cljs$core$async30039 !== 'undefined'){
} else {

/**
* @constructor
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.async.Mix}
 * @implements {cljs.core.async.Mux}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async30039 = (function (change,mix,solo_mode,pick,cs,calc_state,out,changed,solo_modes,attrs,meta30040){
this.change = change;
this.mix = mix;
this.solo_mode = solo_mode;
this.pick = pick;
this.cs = cs;
this.calc_state = calc_state;
this.out = out;
this.changed = changed;
this.solo_modes = solo_modes;
this.attrs = attrs;
this.meta30040 = meta30040;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
})
cljs.core.async.t_cljs$core$async30039.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state){
return (function (_30041,meta30040__$1){
var self__ = this;
var _30041__$1 = this;
return (new cljs.core.async.t_cljs$core$async30039(self__.change,self__.mix,self__.solo_mode,self__.pick,self__.cs,self__.calc_state,self__.out,self__.changed,self__.solo_modes,self__.attrs,meta30040__$1));
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state))
;

cljs.core.async.t_cljs$core$async30039.prototype.cljs$core$IMeta$_meta$arity$1 = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state){
return (function (_30041){
var self__ = this;
var _30041__$1 = this;
return self__.meta30040;
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state))
;

cljs.core.async.t_cljs$core$async30039.prototype.cljs$core$async$Mux$ = true;

cljs.core.async.t_cljs$core$async30039.prototype.cljs$core$async$Mux$muxch_STAR_$arity$1 = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state){
return (function (_){
var self__ = this;
var ___$1 = this;
return self__.out;
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state))
;

cljs.core.async.t_cljs$core$async30039.prototype.cljs$core$async$Mix$ = true;

cljs.core.async.t_cljs$core$async30039.prototype.cljs$core$async$Mix$admix_STAR_$arity$2 = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state){
return (function (_,ch){
var self__ = this;
var ___$1 = this;
cljs.core.swap_BANG_.call(null,self__.cs,cljs.core.assoc,ch,cljs.core.PersistentArrayMap.EMPTY);

return self__.changed.call(null);
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state))
;

cljs.core.async.t_cljs$core$async30039.prototype.cljs$core$async$Mix$unmix_STAR_$arity$2 = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state){
return (function (_,ch){
var self__ = this;
var ___$1 = this;
cljs.core.swap_BANG_.call(null,self__.cs,cljs.core.dissoc,ch);

return self__.changed.call(null);
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state))
;

cljs.core.async.t_cljs$core$async30039.prototype.cljs$core$async$Mix$unmix_all_STAR_$arity$1 = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state){
return (function (_){
var self__ = this;
var ___$1 = this;
cljs.core.reset_BANG_.call(null,self__.cs,cljs.core.PersistentArrayMap.EMPTY);

return self__.changed.call(null);
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state))
;

cljs.core.async.t_cljs$core$async30039.prototype.cljs$core$async$Mix$toggle_STAR_$arity$2 = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state){
return (function (_,state_map){
var self__ = this;
var ___$1 = this;
cljs.core.swap_BANG_.call(null,self__.cs,cljs.core.partial.call(null,cljs.core.merge_with,cljs.core.merge),state_map);

return self__.changed.call(null);
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state))
;

cljs.core.async.t_cljs$core$async30039.prototype.cljs$core$async$Mix$solo_mode_STAR_$arity$2 = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state){
return (function (_,mode){
var self__ = this;
var ___$1 = this;
if(cljs.core.truth_(self__.solo_modes.call(null,mode))){
} else {
throw (new Error([cljs.core.str("Assert failed: "),cljs.core.str([cljs.core.str("mode must be one of: "),cljs.core.str(self__.solo_modes)].join('')),cljs.core.str("\n"),cljs.core.str(cljs.core.pr_str.call(null,cljs.core.list(new cljs.core.Symbol(null,"solo-modes","solo-modes",882180540,null),new cljs.core.Symbol(null,"mode","mode",-2000032078,null))))].join('')));
}

cljs.core.reset_BANG_.call(null,self__.solo_mode,mode);

return self__.changed.call(null);
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state))
;

cljs.core.async.t_cljs$core$async30039.getBasis = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state){
return (function (){
return new cljs.core.PersistentVector(null, 11, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"change","change",477485025,null),cljs.core.with_meta(new cljs.core.Symbol(null,"mix","mix",2121373763,null),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"arglists","arglists",1661989754),cljs.core.list(new cljs.core.Symbol(null,"quote","quote",1377916282,null),cljs.core.list(new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"out","out",729986010,null)], null))),new cljs.core.Keyword(null,"doc","doc",1913296891),"Creates and returns a mix of one or more input channels which will\n  be put on the supplied out channel. Input sources can be added to\n  the mix with 'admix', and removed with 'unmix'. A mix supports\n  soloing, muting and pausing multiple inputs atomically using\n  'toggle', and can solo using either muting or pausing as determined\n  by 'solo-mode'.\n\n  Each channel can have zero or more boolean modes set via 'toggle':\n\n  :solo - when true, only this (ond other soloed) channel(s) will appear\n          in the mix output channel. :mute and :pause states of soloed\n          channels are ignored. If solo-mode is :mute, non-soloed\n          channels are muted, if :pause, non-soloed channels are\n          paused.\n\n  :mute - muted channels will have their contents consumed but not included in the mix\n  :pause - paused channels will not have their contents consumed (and thus also not included in the mix)\n"], null)),new cljs.core.Symbol(null,"solo-mode","solo-mode",2031788074,null),new cljs.core.Symbol(null,"pick","pick",1300068175,null),new cljs.core.Symbol(null,"cs","cs",-117024463,null),new cljs.core.Symbol(null,"calc-state","calc-state",-349968968,null),new cljs.core.Symbol(null,"out","out",729986010,null),new cljs.core.Symbol(null,"changed","changed",-2083710852,null),new cljs.core.Symbol(null,"solo-modes","solo-modes",882180540,null),new cljs.core.Symbol(null,"attrs","attrs",-450137186,null),new cljs.core.Symbol(null,"meta30040","meta30040",-1268261511,null)], null);
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state))
;

cljs.core.async.t_cljs$core$async30039.cljs$lang$type = true;

cljs.core.async.t_cljs$core$async30039.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async30039";

cljs.core.async.t_cljs$core$async30039.cljs$lang$ctorPrWriter = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state){
return (function (this__23741__auto__,writer__23742__auto__,opt__23743__auto__){
return cljs.core._write.call(null,writer__23742__auto__,"cljs.core.async/t_cljs$core$async30039");
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state))
;

cljs.core.async.__GT_t_cljs$core$async30039 = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state){
return (function cljs$core$async$mix_$___GT_t_cljs$core$async30039(change__$1,mix__$1,solo_mode__$1,pick__$1,cs__$1,calc_state__$1,out__$1,changed__$1,solo_modes__$1,attrs__$1,meta30040){
return (new cljs.core.async.t_cljs$core$async30039(change__$1,mix__$1,solo_mode__$1,pick__$1,cs__$1,calc_state__$1,out__$1,changed__$1,solo_modes__$1,attrs__$1,meta30040));
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state))
;

}

return (new cljs.core.async.t_cljs$core$async30039(change,cljs$core$async$mix,solo_mode,pick,cs,calc_state,out,changed,solo_modes,attrs,cljs.core.PersistentArrayMap.EMPTY));
})()
;
var c__28489__auto___30202 = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__28489__auto___30202,cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state,m){
return (function (){
var f__28490__auto__ = (function (){var switch__28377__auto__ = ((function (c__28489__auto___30202,cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state,m){
return (function (state_30139){
var state_val_30140 = (state_30139[(1)]);
if((state_val_30140 === (7))){
var inst_30057 = (state_30139[(2)]);
var state_30139__$1 = state_30139;
var statearr_30141_30203 = state_30139__$1;
(statearr_30141_30203[(2)] = inst_30057);

(statearr_30141_30203[(1)] = (4));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30140 === (20))){
var inst_30069 = (state_30139[(7)]);
var state_30139__$1 = state_30139;
var statearr_30142_30204 = state_30139__$1;
(statearr_30142_30204[(2)] = inst_30069);

(statearr_30142_30204[(1)] = (21));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30140 === (27))){
var state_30139__$1 = state_30139;
var statearr_30143_30205 = state_30139__$1;
(statearr_30143_30205[(2)] = null);

(statearr_30143_30205[(1)] = (28));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30140 === (1))){
var inst_30045 = (state_30139[(8)]);
var inst_30045__$1 = calc_state.call(null);
var inst_30047 = (inst_30045__$1 == null);
var inst_30048 = cljs.core.not.call(null,inst_30047);
var state_30139__$1 = (function (){var statearr_30144 = state_30139;
(statearr_30144[(8)] = inst_30045__$1);

return statearr_30144;
})();
if(inst_30048){
var statearr_30145_30206 = state_30139__$1;
(statearr_30145_30206[(1)] = (2));

} else {
var statearr_30146_30207 = state_30139__$1;
(statearr_30146_30207[(1)] = (3));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30140 === (24))){
var inst_30099 = (state_30139[(9)]);
var inst_30092 = (state_30139[(10)]);
var inst_30113 = (state_30139[(11)]);
var inst_30113__$1 = inst_30092.call(null,inst_30099);
var state_30139__$1 = (function (){var statearr_30147 = state_30139;
(statearr_30147[(11)] = inst_30113__$1);

return statearr_30147;
})();
if(cljs.core.truth_(inst_30113__$1)){
var statearr_30148_30208 = state_30139__$1;
(statearr_30148_30208[(1)] = (29));

} else {
var statearr_30149_30209 = state_30139__$1;
(statearr_30149_30209[(1)] = (30));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30140 === (4))){
var inst_30060 = (state_30139[(2)]);
var state_30139__$1 = state_30139;
if(cljs.core.truth_(inst_30060)){
var statearr_30150_30210 = state_30139__$1;
(statearr_30150_30210[(1)] = (8));

} else {
var statearr_30151_30211 = state_30139__$1;
(statearr_30151_30211[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30140 === (15))){
var inst_30086 = (state_30139[(2)]);
var state_30139__$1 = state_30139;
if(cljs.core.truth_(inst_30086)){
var statearr_30152_30212 = state_30139__$1;
(statearr_30152_30212[(1)] = (19));

} else {
var statearr_30153_30213 = state_30139__$1;
(statearr_30153_30213[(1)] = (20));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30140 === (21))){
var inst_30091 = (state_30139[(12)]);
var inst_30091__$1 = (state_30139[(2)]);
var inst_30092 = cljs.core.get.call(null,inst_30091__$1,new cljs.core.Keyword(null,"solos","solos",1441458643));
var inst_30093 = cljs.core.get.call(null,inst_30091__$1,new cljs.core.Keyword(null,"mutes","mutes",1068806309));
var inst_30094 = cljs.core.get.call(null,inst_30091__$1,new cljs.core.Keyword(null,"reads","reads",-1215067361));
var state_30139__$1 = (function (){var statearr_30154 = state_30139;
(statearr_30154[(13)] = inst_30093);

(statearr_30154[(10)] = inst_30092);

(statearr_30154[(12)] = inst_30091__$1);

return statearr_30154;
})();
return cljs.core.async.ioc_alts_BANG_.call(null,state_30139__$1,(22),inst_30094);
} else {
if((state_val_30140 === (31))){
var inst_30121 = (state_30139[(2)]);
var state_30139__$1 = state_30139;
if(cljs.core.truth_(inst_30121)){
var statearr_30155_30214 = state_30139__$1;
(statearr_30155_30214[(1)] = (32));

} else {
var statearr_30156_30215 = state_30139__$1;
(statearr_30156_30215[(1)] = (33));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30140 === (32))){
var inst_30098 = (state_30139[(14)]);
var state_30139__$1 = state_30139;
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_30139__$1,(35),out,inst_30098);
} else {
if((state_val_30140 === (33))){
var inst_30091 = (state_30139[(12)]);
var inst_30069 = inst_30091;
var state_30139__$1 = (function (){var statearr_30157 = state_30139;
(statearr_30157[(7)] = inst_30069);

return statearr_30157;
})();
var statearr_30158_30216 = state_30139__$1;
(statearr_30158_30216[(2)] = null);

(statearr_30158_30216[(1)] = (11));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30140 === (13))){
var inst_30069 = (state_30139[(7)]);
var inst_30076 = inst_30069.cljs$lang$protocol_mask$partition0$;
var inst_30077 = (inst_30076 & (64));
var inst_30078 = inst_30069.cljs$core$ISeq$;
var inst_30079 = (inst_30077) || (inst_30078);
var state_30139__$1 = state_30139;
if(cljs.core.truth_(inst_30079)){
var statearr_30159_30217 = state_30139__$1;
(statearr_30159_30217[(1)] = (16));

} else {
var statearr_30160_30218 = state_30139__$1;
(statearr_30160_30218[(1)] = (17));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30140 === (22))){
var inst_30099 = (state_30139[(9)]);
var inst_30098 = (state_30139[(14)]);
var inst_30097 = (state_30139[(2)]);
var inst_30098__$1 = cljs.core.nth.call(null,inst_30097,(0),null);
var inst_30099__$1 = cljs.core.nth.call(null,inst_30097,(1),null);
var inst_30100 = (inst_30098__$1 == null);
var inst_30101 = cljs.core._EQ_.call(null,inst_30099__$1,change);
var inst_30102 = (inst_30100) || (inst_30101);
var state_30139__$1 = (function (){var statearr_30161 = state_30139;
(statearr_30161[(9)] = inst_30099__$1);

(statearr_30161[(14)] = inst_30098__$1);

return statearr_30161;
})();
if(cljs.core.truth_(inst_30102)){
var statearr_30162_30219 = state_30139__$1;
(statearr_30162_30219[(1)] = (23));

} else {
var statearr_30163_30220 = state_30139__$1;
(statearr_30163_30220[(1)] = (24));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30140 === (36))){
var inst_30091 = (state_30139[(12)]);
var inst_30069 = inst_30091;
var state_30139__$1 = (function (){var statearr_30164 = state_30139;
(statearr_30164[(7)] = inst_30069);

return statearr_30164;
})();
var statearr_30165_30221 = state_30139__$1;
(statearr_30165_30221[(2)] = null);

(statearr_30165_30221[(1)] = (11));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30140 === (29))){
var inst_30113 = (state_30139[(11)]);
var state_30139__$1 = state_30139;
var statearr_30166_30222 = state_30139__$1;
(statearr_30166_30222[(2)] = inst_30113);

(statearr_30166_30222[(1)] = (31));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30140 === (6))){
var state_30139__$1 = state_30139;
var statearr_30167_30223 = state_30139__$1;
(statearr_30167_30223[(2)] = false);

(statearr_30167_30223[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30140 === (28))){
var inst_30109 = (state_30139[(2)]);
var inst_30110 = calc_state.call(null);
var inst_30069 = inst_30110;
var state_30139__$1 = (function (){var statearr_30168 = state_30139;
(statearr_30168[(15)] = inst_30109);

(statearr_30168[(7)] = inst_30069);

return statearr_30168;
})();
var statearr_30169_30224 = state_30139__$1;
(statearr_30169_30224[(2)] = null);

(statearr_30169_30224[(1)] = (11));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30140 === (25))){
var inst_30135 = (state_30139[(2)]);
var state_30139__$1 = state_30139;
var statearr_30170_30225 = state_30139__$1;
(statearr_30170_30225[(2)] = inst_30135);

(statearr_30170_30225[(1)] = (12));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30140 === (34))){
var inst_30133 = (state_30139[(2)]);
var state_30139__$1 = state_30139;
var statearr_30171_30226 = state_30139__$1;
(statearr_30171_30226[(2)] = inst_30133);

(statearr_30171_30226[(1)] = (25));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30140 === (17))){
var state_30139__$1 = state_30139;
var statearr_30172_30227 = state_30139__$1;
(statearr_30172_30227[(2)] = false);

(statearr_30172_30227[(1)] = (18));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30140 === (3))){
var state_30139__$1 = state_30139;
var statearr_30173_30228 = state_30139__$1;
(statearr_30173_30228[(2)] = false);

(statearr_30173_30228[(1)] = (4));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30140 === (12))){
var inst_30137 = (state_30139[(2)]);
var state_30139__$1 = state_30139;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_30139__$1,inst_30137);
} else {
if((state_val_30140 === (2))){
var inst_30045 = (state_30139[(8)]);
var inst_30050 = inst_30045.cljs$lang$protocol_mask$partition0$;
var inst_30051 = (inst_30050 & (64));
var inst_30052 = inst_30045.cljs$core$ISeq$;
var inst_30053 = (inst_30051) || (inst_30052);
var state_30139__$1 = state_30139;
if(cljs.core.truth_(inst_30053)){
var statearr_30174_30229 = state_30139__$1;
(statearr_30174_30229[(1)] = (5));

} else {
var statearr_30175_30230 = state_30139__$1;
(statearr_30175_30230[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30140 === (23))){
var inst_30098 = (state_30139[(14)]);
var inst_30104 = (inst_30098 == null);
var state_30139__$1 = state_30139;
if(cljs.core.truth_(inst_30104)){
var statearr_30176_30231 = state_30139__$1;
(statearr_30176_30231[(1)] = (26));

} else {
var statearr_30177_30232 = state_30139__$1;
(statearr_30177_30232[(1)] = (27));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30140 === (35))){
var inst_30124 = (state_30139[(2)]);
var state_30139__$1 = state_30139;
if(cljs.core.truth_(inst_30124)){
var statearr_30178_30233 = state_30139__$1;
(statearr_30178_30233[(1)] = (36));

} else {
var statearr_30179_30234 = state_30139__$1;
(statearr_30179_30234[(1)] = (37));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30140 === (19))){
var inst_30069 = (state_30139[(7)]);
var inst_30088 = cljs.core.apply.call(null,cljs.core.hash_map,inst_30069);
var state_30139__$1 = state_30139;
var statearr_30180_30235 = state_30139__$1;
(statearr_30180_30235[(2)] = inst_30088);

(statearr_30180_30235[(1)] = (21));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30140 === (11))){
var inst_30069 = (state_30139[(7)]);
var inst_30073 = (inst_30069 == null);
var inst_30074 = cljs.core.not.call(null,inst_30073);
var state_30139__$1 = state_30139;
if(inst_30074){
var statearr_30181_30236 = state_30139__$1;
(statearr_30181_30236[(1)] = (13));

} else {
var statearr_30182_30237 = state_30139__$1;
(statearr_30182_30237[(1)] = (14));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30140 === (9))){
var inst_30045 = (state_30139[(8)]);
var state_30139__$1 = state_30139;
var statearr_30183_30238 = state_30139__$1;
(statearr_30183_30238[(2)] = inst_30045);

(statearr_30183_30238[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30140 === (5))){
var state_30139__$1 = state_30139;
var statearr_30184_30239 = state_30139__$1;
(statearr_30184_30239[(2)] = true);

(statearr_30184_30239[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30140 === (14))){
var state_30139__$1 = state_30139;
var statearr_30185_30240 = state_30139__$1;
(statearr_30185_30240[(2)] = false);

(statearr_30185_30240[(1)] = (15));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30140 === (26))){
var inst_30099 = (state_30139[(9)]);
var inst_30106 = cljs.core.swap_BANG_.call(null,cs,cljs.core.dissoc,inst_30099);
var state_30139__$1 = state_30139;
var statearr_30186_30241 = state_30139__$1;
(statearr_30186_30241[(2)] = inst_30106);

(statearr_30186_30241[(1)] = (28));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30140 === (16))){
var state_30139__$1 = state_30139;
var statearr_30187_30242 = state_30139__$1;
(statearr_30187_30242[(2)] = true);

(statearr_30187_30242[(1)] = (18));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30140 === (38))){
var inst_30129 = (state_30139[(2)]);
var state_30139__$1 = state_30139;
var statearr_30188_30243 = state_30139__$1;
(statearr_30188_30243[(2)] = inst_30129);

(statearr_30188_30243[(1)] = (34));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30140 === (30))){
var inst_30099 = (state_30139[(9)]);
var inst_30093 = (state_30139[(13)]);
var inst_30092 = (state_30139[(10)]);
var inst_30116 = cljs.core.empty_QMARK_.call(null,inst_30092);
var inst_30117 = inst_30093.call(null,inst_30099);
var inst_30118 = cljs.core.not.call(null,inst_30117);
var inst_30119 = (inst_30116) && (inst_30118);
var state_30139__$1 = state_30139;
var statearr_30189_30244 = state_30139__$1;
(statearr_30189_30244[(2)] = inst_30119);

(statearr_30189_30244[(1)] = (31));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30140 === (10))){
var inst_30045 = (state_30139[(8)]);
var inst_30065 = (state_30139[(2)]);
var inst_30066 = cljs.core.get.call(null,inst_30065,new cljs.core.Keyword(null,"solos","solos",1441458643));
var inst_30067 = cljs.core.get.call(null,inst_30065,new cljs.core.Keyword(null,"mutes","mutes",1068806309));
var inst_30068 = cljs.core.get.call(null,inst_30065,new cljs.core.Keyword(null,"reads","reads",-1215067361));
var inst_30069 = inst_30045;
var state_30139__$1 = (function (){var statearr_30190 = state_30139;
(statearr_30190[(16)] = inst_30066);

(statearr_30190[(17)] = inst_30067);

(statearr_30190[(18)] = inst_30068);

(statearr_30190[(7)] = inst_30069);

return statearr_30190;
})();
var statearr_30191_30245 = state_30139__$1;
(statearr_30191_30245[(2)] = null);

(statearr_30191_30245[(1)] = (11));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30140 === (18))){
var inst_30083 = (state_30139[(2)]);
var state_30139__$1 = state_30139;
var statearr_30192_30246 = state_30139__$1;
(statearr_30192_30246[(2)] = inst_30083);

(statearr_30192_30246[(1)] = (15));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30140 === (37))){
var state_30139__$1 = state_30139;
var statearr_30193_30247 = state_30139__$1;
(statearr_30193_30247[(2)] = null);

(statearr_30193_30247[(1)] = (38));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30140 === (8))){
var inst_30045 = (state_30139[(8)]);
var inst_30062 = cljs.core.apply.call(null,cljs.core.hash_map,inst_30045);
var state_30139__$1 = state_30139;
var statearr_30194_30248 = state_30139__$1;
(statearr_30194_30248[(2)] = inst_30062);

(statearr_30194_30248[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__28489__auto___30202,cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state,m))
;
return ((function (switch__28377__auto__,c__28489__auto___30202,cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state,m){
return (function() {
var cljs$core$async$mix_$_state_machine__28378__auto__ = null;
var cljs$core$async$mix_$_state_machine__28378__auto____0 = (function (){
var statearr_30198 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_30198[(0)] = cljs$core$async$mix_$_state_machine__28378__auto__);

(statearr_30198[(1)] = (1));

return statearr_30198;
});
var cljs$core$async$mix_$_state_machine__28378__auto____1 = (function (state_30139){
while(true){
var ret_value__28379__auto__ = (function (){try{while(true){
var result__28380__auto__ = switch__28377__auto__.call(null,state_30139);
if(cljs.core.keyword_identical_QMARK_.call(null,result__28380__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__28380__auto__;
}
break;
}
}catch (e30199){if((e30199 instanceof Object)){
var ex__28381__auto__ = e30199;
var statearr_30200_30249 = state_30139;
(statearr_30200_30249[(5)] = ex__28381__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_30139);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e30199;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__28379__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__30250 = state_30139;
state_30139 = G__30250;
continue;
} else {
return ret_value__28379__auto__;
}
break;
}
});
cljs$core$async$mix_$_state_machine__28378__auto__ = function(state_30139){
switch(arguments.length){
case 0:
return cljs$core$async$mix_$_state_machine__28378__auto____0.call(this);
case 1:
return cljs$core$async$mix_$_state_machine__28378__auto____1.call(this,state_30139);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$mix_$_state_machine__28378__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$mix_$_state_machine__28378__auto____0;
cljs$core$async$mix_$_state_machine__28378__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$mix_$_state_machine__28378__auto____1;
return cljs$core$async$mix_$_state_machine__28378__auto__;
})()
;})(switch__28377__auto__,c__28489__auto___30202,cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state,m))
})();
var state__28491__auto__ = (function (){var statearr_30201 = f__28490__auto__.call(null);
(statearr_30201[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__28489__auto___30202);

return statearr_30201;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__28491__auto__);
});})(c__28489__auto___30202,cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state,m))
);


return m;
});
/**
 * Adds ch as an input to the mix
 */
cljs.core.async.admix = (function cljs$core$async$admix(mix,ch){
return cljs.core.async.admix_STAR_.call(null,mix,ch);
});
/**
 * Removes ch as an input to the mix
 */
cljs.core.async.unmix = (function cljs$core$async$unmix(mix,ch){
return cljs.core.async.unmix_STAR_.call(null,mix,ch);
});
/**
 * removes all inputs from the mix
 */
cljs.core.async.unmix_all = (function cljs$core$async$unmix_all(mix){
return cljs.core.async.unmix_all_STAR_.call(null,mix);
});
/**
 * Atomically sets the state(s) of one or more channels in a mix. The
 *   state map is a map of channels -> channel-state-map. A
 *   channel-state-map is a map of attrs -> boolean, where attr is one or
 *   more of :mute, :pause or :solo. Any states supplied are merged with
 *   the current state.
 * 
 *   Note that channels can be added to a mix via toggle, which can be
 *   used to add channels in a particular (e.g. paused) state.
 */
cljs.core.async.toggle = (function cljs$core$async$toggle(mix,state_map){
return cljs.core.async.toggle_STAR_.call(null,mix,state_map);
});
/**
 * Sets the solo mode of the mix. mode must be one of :mute or :pause
 */
cljs.core.async.solo_mode = (function cljs$core$async$solo_mode(mix,mode){
return cljs.core.async.solo_mode_STAR_.call(null,mix,mode);
});

/**
 * @interface
 */
cljs.core.async.Pub = function(){};

cljs.core.async.sub_STAR_ = (function cljs$core$async$sub_STAR_(p,v,ch,close_QMARK_){
if((!((p == null))) && (!((p.cljs$core$async$Pub$sub_STAR_$arity$4 == null)))){
return p.cljs$core$async$Pub$sub_STAR_$arity$4(p,v,ch,close_QMARK_);
} else {
var x__23798__auto__ = (((p == null))?null:p);
var m__23799__auto__ = (cljs.core.async.sub_STAR_[goog.typeOf(x__23798__auto__)]);
if(!((m__23799__auto__ == null))){
return m__23799__auto__.call(null,p,v,ch,close_QMARK_);
} else {
var m__23799__auto____$1 = (cljs.core.async.sub_STAR_["_"]);
if(!((m__23799__auto____$1 == null))){
return m__23799__auto____$1.call(null,p,v,ch,close_QMARK_);
} else {
throw cljs.core.missing_protocol.call(null,"Pub.sub*",p);
}
}
}
});

cljs.core.async.unsub_STAR_ = (function cljs$core$async$unsub_STAR_(p,v,ch){
if((!((p == null))) && (!((p.cljs$core$async$Pub$unsub_STAR_$arity$3 == null)))){
return p.cljs$core$async$Pub$unsub_STAR_$arity$3(p,v,ch);
} else {
var x__23798__auto__ = (((p == null))?null:p);
var m__23799__auto__ = (cljs.core.async.unsub_STAR_[goog.typeOf(x__23798__auto__)]);
if(!((m__23799__auto__ == null))){
return m__23799__auto__.call(null,p,v,ch);
} else {
var m__23799__auto____$1 = (cljs.core.async.unsub_STAR_["_"]);
if(!((m__23799__auto____$1 == null))){
return m__23799__auto____$1.call(null,p,v,ch);
} else {
throw cljs.core.missing_protocol.call(null,"Pub.unsub*",p);
}
}
}
});

cljs.core.async.unsub_all_STAR_ = (function cljs$core$async$unsub_all_STAR_(var_args){
var args30251 = [];
var len__24201__auto___30254 = arguments.length;
var i__24202__auto___30255 = (0);
while(true){
if((i__24202__auto___30255 < len__24201__auto___30254)){
args30251.push((arguments[i__24202__auto___30255]));

var G__30256 = (i__24202__auto___30255 + (1));
i__24202__auto___30255 = G__30256;
continue;
} else {
}
break;
}

var G__30253 = args30251.length;
switch (G__30253) {
case 1:
return cljs.core.async.unsub_all_STAR_.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return cljs.core.async.unsub_all_STAR_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error([cljs.core.str("Invalid arity: "),cljs.core.str(args30251.length)].join('')));

}
});

cljs.core.async.unsub_all_STAR_.cljs$core$IFn$_invoke$arity$1 = (function (p){
if((!((p == null))) && (!((p.cljs$core$async$Pub$unsub_all_STAR_$arity$1 == null)))){
return p.cljs$core$async$Pub$unsub_all_STAR_$arity$1(p);
} else {
var x__23798__auto__ = (((p == null))?null:p);
var m__23799__auto__ = (cljs.core.async.unsub_all_STAR_[goog.typeOf(x__23798__auto__)]);
if(!((m__23799__auto__ == null))){
return m__23799__auto__.call(null,p);
} else {
var m__23799__auto____$1 = (cljs.core.async.unsub_all_STAR_["_"]);
if(!((m__23799__auto____$1 == null))){
return m__23799__auto____$1.call(null,p);
} else {
throw cljs.core.missing_protocol.call(null,"Pub.unsub-all*",p);
}
}
}
});

cljs.core.async.unsub_all_STAR_.cljs$core$IFn$_invoke$arity$2 = (function (p,v){
if((!((p == null))) && (!((p.cljs$core$async$Pub$unsub_all_STAR_$arity$2 == null)))){
return p.cljs$core$async$Pub$unsub_all_STAR_$arity$2(p,v);
} else {
var x__23798__auto__ = (((p == null))?null:p);
var m__23799__auto__ = (cljs.core.async.unsub_all_STAR_[goog.typeOf(x__23798__auto__)]);
if(!((m__23799__auto__ == null))){
return m__23799__auto__.call(null,p,v);
} else {
var m__23799__auto____$1 = (cljs.core.async.unsub_all_STAR_["_"]);
if(!((m__23799__auto____$1 == null))){
return m__23799__auto____$1.call(null,p,v);
} else {
throw cljs.core.missing_protocol.call(null,"Pub.unsub-all*",p);
}
}
}
});

cljs.core.async.unsub_all_STAR_.cljs$lang$maxFixedArity = 2;

/**
 * Creates and returns a pub(lication) of the supplied channel,
 *   partitioned into topics by the topic-fn. topic-fn will be applied to
 *   each value on the channel and the result will determine the 'topic'
 *   on which that value will be put. Channels can be subscribed to
 *   receive copies of topics using 'sub', and unsubscribed using
 *   'unsub'. Each topic will be handled by an internal mult on a
 *   dedicated channel. By default these internal channels are
 *   unbuffered, but a buf-fn can be supplied which, given a topic,
 *   creates a buffer with desired properties.
 * 
 *   Each item is distributed to all subs in parallel and synchronously,
 *   i.e. each sub must accept before the next item is distributed. Use
 *   buffering/windowing to prevent slow subs from holding up the pub.
 * 
 *   Items received when there are no matching subs get dropped.
 * 
 *   Note that if buf-fns are used then each topic is handled
 *   asynchronously, i.e. if a channel is subscribed to more than one
 *   topic it should not expect them to be interleaved identically with
 *   the source.
 */
cljs.core.async.pub = (function cljs$core$async$pub(var_args){
var args30259 = [];
var len__24201__auto___30384 = arguments.length;
var i__24202__auto___30385 = (0);
while(true){
if((i__24202__auto___30385 < len__24201__auto___30384)){
args30259.push((arguments[i__24202__auto___30385]));

var G__30386 = (i__24202__auto___30385 + (1));
i__24202__auto___30385 = G__30386;
continue;
} else {
}
break;
}

var G__30261 = args30259.length;
switch (G__30261) {
case 2:
return cljs.core.async.pub.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.pub.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error([cljs.core.str("Invalid arity: "),cljs.core.str(args30259.length)].join('')));

}
});

cljs.core.async.pub.cljs$core$IFn$_invoke$arity$2 = (function (ch,topic_fn){
return cljs.core.async.pub.call(null,ch,topic_fn,cljs.core.constantly.call(null,null));
});

cljs.core.async.pub.cljs$core$IFn$_invoke$arity$3 = (function (ch,topic_fn,buf_fn){
var mults = cljs.core.atom.call(null,cljs.core.PersistentArrayMap.EMPTY);
var ensure_mult = ((function (mults){
return (function (topic){
var or__23143__auto__ = cljs.core.get.call(null,cljs.core.deref.call(null,mults),topic);
if(cljs.core.truth_(or__23143__auto__)){
return or__23143__auto__;
} else {
return cljs.core.get.call(null,cljs.core.swap_BANG_.call(null,mults,((function (or__23143__auto__,mults){
return (function (p1__30258_SHARP_){
if(cljs.core.truth_(p1__30258_SHARP_.call(null,topic))){
return p1__30258_SHARP_;
} else {
return cljs.core.assoc.call(null,p1__30258_SHARP_,topic,cljs.core.async.mult.call(null,cljs.core.async.chan.call(null,buf_fn.call(null,topic))));
}
});})(or__23143__auto__,mults))
),topic);
}
});})(mults))
;
var p = (function (){
if(typeof cljs.core.async.t_cljs$core$async30262 !== 'undefined'){
} else {

/**
* @constructor
 * @implements {cljs.core.async.Pub}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.async.Mux}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async30262 = (function (ch,topic_fn,buf_fn,mults,ensure_mult,meta30263){
this.ch = ch;
this.topic_fn = topic_fn;
this.buf_fn = buf_fn;
this.mults = mults;
this.ensure_mult = ensure_mult;
this.meta30263 = meta30263;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
})
cljs.core.async.t_cljs$core$async30262.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = ((function (mults,ensure_mult){
return (function (_30264,meta30263__$1){
var self__ = this;
var _30264__$1 = this;
return (new cljs.core.async.t_cljs$core$async30262(self__.ch,self__.topic_fn,self__.buf_fn,self__.mults,self__.ensure_mult,meta30263__$1));
});})(mults,ensure_mult))
;

cljs.core.async.t_cljs$core$async30262.prototype.cljs$core$IMeta$_meta$arity$1 = ((function (mults,ensure_mult){
return (function (_30264){
var self__ = this;
var _30264__$1 = this;
return self__.meta30263;
});})(mults,ensure_mult))
;

cljs.core.async.t_cljs$core$async30262.prototype.cljs$core$async$Mux$ = true;

cljs.core.async.t_cljs$core$async30262.prototype.cljs$core$async$Mux$muxch_STAR_$arity$1 = ((function (mults,ensure_mult){
return (function (_){
var self__ = this;
var ___$1 = this;
return self__.ch;
});})(mults,ensure_mult))
;

cljs.core.async.t_cljs$core$async30262.prototype.cljs$core$async$Pub$ = true;

cljs.core.async.t_cljs$core$async30262.prototype.cljs$core$async$Pub$sub_STAR_$arity$4 = ((function (mults,ensure_mult){
return (function (p,topic,ch__$1,close_QMARK_){
var self__ = this;
var p__$1 = this;
var m = self__.ensure_mult.call(null,topic);
return cljs.core.async.tap.call(null,m,ch__$1,close_QMARK_);
});})(mults,ensure_mult))
;

cljs.core.async.t_cljs$core$async30262.prototype.cljs$core$async$Pub$unsub_STAR_$arity$3 = ((function (mults,ensure_mult){
return (function (p,topic,ch__$1){
var self__ = this;
var p__$1 = this;
var temp__4657__auto__ = cljs.core.get.call(null,cljs.core.deref.call(null,self__.mults),topic);
if(cljs.core.truth_(temp__4657__auto__)){
var m = temp__4657__auto__;
return cljs.core.async.untap.call(null,m,ch__$1);
} else {
return null;
}
});})(mults,ensure_mult))
;

cljs.core.async.t_cljs$core$async30262.prototype.cljs$core$async$Pub$unsub_all_STAR_$arity$1 = ((function (mults,ensure_mult){
return (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.reset_BANG_.call(null,self__.mults,cljs.core.PersistentArrayMap.EMPTY);
});})(mults,ensure_mult))
;

cljs.core.async.t_cljs$core$async30262.prototype.cljs$core$async$Pub$unsub_all_STAR_$arity$2 = ((function (mults,ensure_mult){
return (function (_,topic){
var self__ = this;
var ___$1 = this;
return cljs.core.swap_BANG_.call(null,self__.mults,cljs.core.dissoc,topic);
});})(mults,ensure_mult))
;

cljs.core.async.t_cljs$core$async30262.getBasis = ((function (mults,ensure_mult){
return (function (){
return new cljs.core.PersistentVector(null, 6, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"ch","ch",1085813622,null),new cljs.core.Symbol(null,"topic-fn","topic-fn",-862449736,null),new cljs.core.Symbol(null,"buf-fn","buf-fn",-1200281591,null),new cljs.core.Symbol(null,"mults","mults",-461114485,null),new cljs.core.Symbol(null,"ensure-mult","ensure-mult",1796584816,null),new cljs.core.Symbol(null,"meta30263","meta30263",-1055754719,null)], null);
});})(mults,ensure_mult))
;

cljs.core.async.t_cljs$core$async30262.cljs$lang$type = true;

cljs.core.async.t_cljs$core$async30262.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async30262";

cljs.core.async.t_cljs$core$async30262.cljs$lang$ctorPrWriter = ((function (mults,ensure_mult){
return (function (this__23741__auto__,writer__23742__auto__,opt__23743__auto__){
return cljs.core._write.call(null,writer__23742__auto__,"cljs.core.async/t_cljs$core$async30262");
});})(mults,ensure_mult))
;

cljs.core.async.__GT_t_cljs$core$async30262 = ((function (mults,ensure_mult){
return (function cljs$core$async$__GT_t_cljs$core$async30262(ch__$1,topic_fn__$1,buf_fn__$1,mults__$1,ensure_mult__$1,meta30263){
return (new cljs.core.async.t_cljs$core$async30262(ch__$1,topic_fn__$1,buf_fn__$1,mults__$1,ensure_mult__$1,meta30263));
});})(mults,ensure_mult))
;

}

return (new cljs.core.async.t_cljs$core$async30262(ch,topic_fn,buf_fn,mults,ensure_mult,cljs.core.PersistentArrayMap.EMPTY));
})()
;
var c__28489__auto___30388 = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__28489__auto___30388,mults,ensure_mult,p){
return (function (){
var f__28490__auto__ = (function (){var switch__28377__auto__ = ((function (c__28489__auto___30388,mults,ensure_mult,p){
return (function (state_30336){
var state_val_30337 = (state_30336[(1)]);
if((state_val_30337 === (7))){
var inst_30332 = (state_30336[(2)]);
var state_30336__$1 = state_30336;
var statearr_30338_30389 = state_30336__$1;
(statearr_30338_30389[(2)] = inst_30332);

(statearr_30338_30389[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30337 === (20))){
var state_30336__$1 = state_30336;
var statearr_30339_30390 = state_30336__$1;
(statearr_30339_30390[(2)] = null);

(statearr_30339_30390[(1)] = (21));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30337 === (1))){
var state_30336__$1 = state_30336;
var statearr_30340_30391 = state_30336__$1;
(statearr_30340_30391[(2)] = null);

(statearr_30340_30391[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30337 === (24))){
var inst_30315 = (state_30336[(7)]);
var inst_30324 = cljs.core.swap_BANG_.call(null,mults,cljs.core.dissoc,inst_30315);
var state_30336__$1 = state_30336;
var statearr_30341_30392 = state_30336__$1;
(statearr_30341_30392[(2)] = inst_30324);

(statearr_30341_30392[(1)] = (25));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30337 === (4))){
var inst_30267 = (state_30336[(8)]);
var inst_30267__$1 = (state_30336[(2)]);
var inst_30268 = (inst_30267__$1 == null);
var state_30336__$1 = (function (){var statearr_30342 = state_30336;
(statearr_30342[(8)] = inst_30267__$1);

return statearr_30342;
})();
if(cljs.core.truth_(inst_30268)){
var statearr_30343_30393 = state_30336__$1;
(statearr_30343_30393[(1)] = (5));

} else {
var statearr_30344_30394 = state_30336__$1;
(statearr_30344_30394[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30337 === (15))){
var inst_30309 = (state_30336[(2)]);
var state_30336__$1 = state_30336;
var statearr_30345_30395 = state_30336__$1;
(statearr_30345_30395[(2)] = inst_30309);

(statearr_30345_30395[(1)] = (12));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30337 === (21))){
var inst_30329 = (state_30336[(2)]);
var state_30336__$1 = (function (){var statearr_30346 = state_30336;
(statearr_30346[(9)] = inst_30329);

return statearr_30346;
})();
var statearr_30347_30396 = state_30336__$1;
(statearr_30347_30396[(2)] = null);

(statearr_30347_30396[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30337 === (13))){
var inst_30291 = (state_30336[(10)]);
var inst_30293 = cljs.core.chunked_seq_QMARK_.call(null,inst_30291);
var state_30336__$1 = state_30336;
if(inst_30293){
var statearr_30348_30397 = state_30336__$1;
(statearr_30348_30397[(1)] = (16));

} else {
var statearr_30349_30398 = state_30336__$1;
(statearr_30349_30398[(1)] = (17));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30337 === (22))){
var inst_30321 = (state_30336[(2)]);
var state_30336__$1 = state_30336;
if(cljs.core.truth_(inst_30321)){
var statearr_30350_30399 = state_30336__$1;
(statearr_30350_30399[(1)] = (23));

} else {
var statearr_30351_30400 = state_30336__$1;
(statearr_30351_30400[(1)] = (24));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30337 === (6))){
var inst_30317 = (state_30336[(11)]);
var inst_30267 = (state_30336[(8)]);
var inst_30315 = (state_30336[(7)]);
var inst_30315__$1 = topic_fn.call(null,inst_30267);
var inst_30316 = cljs.core.deref.call(null,mults);
var inst_30317__$1 = cljs.core.get.call(null,inst_30316,inst_30315__$1);
var state_30336__$1 = (function (){var statearr_30352 = state_30336;
(statearr_30352[(11)] = inst_30317__$1);

(statearr_30352[(7)] = inst_30315__$1);

return statearr_30352;
})();
if(cljs.core.truth_(inst_30317__$1)){
var statearr_30353_30401 = state_30336__$1;
(statearr_30353_30401[(1)] = (19));

} else {
var statearr_30354_30402 = state_30336__$1;
(statearr_30354_30402[(1)] = (20));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30337 === (25))){
var inst_30326 = (state_30336[(2)]);
var state_30336__$1 = state_30336;
var statearr_30355_30403 = state_30336__$1;
(statearr_30355_30403[(2)] = inst_30326);

(statearr_30355_30403[(1)] = (21));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30337 === (17))){
var inst_30291 = (state_30336[(10)]);
var inst_30300 = cljs.core.first.call(null,inst_30291);
var inst_30301 = cljs.core.async.muxch_STAR_.call(null,inst_30300);
var inst_30302 = cljs.core.async.close_BANG_.call(null,inst_30301);
var inst_30303 = cljs.core.next.call(null,inst_30291);
var inst_30277 = inst_30303;
var inst_30278 = null;
var inst_30279 = (0);
var inst_30280 = (0);
var state_30336__$1 = (function (){var statearr_30356 = state_30336;
(statearr_30356[(12)] = inst_30278);

(statearr_30356[(13)] = inst_30277);

(statearr_30356[(14)] = inst_30279);

(statearr_30356[(15)] = inst_30280);

(statearr_30356[(16)] = inst_30302);

return statearr_30356;
})();
var statearr_30357_30404 = state_30336__$1;
(statearr_30357_30404[(2)] = null);

(statearr_30357_30404[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30337 === (3))){
var inst_30334 = (state_30336[(2)]);
var state_30336__$1 = state_30336;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_30336__$1,inst_30334);
} else {
if((state_val_30337 === (12))){
var inst_30311 = (state_30336[(2)]);
var state_30336__$1 = state_30336;
var statearr_30358_30405 = state_30336__$1;
(statearr_30358_30405[(2)] = inst_30311);

(statearr_30358_30405[(1)] = (9));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30337 === (2))){
var state_30336__$1 = state_30336;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_30336__$1,(4),ch);
} else {
if((state_val_30337 === (23))){
var state_30336__$1 = state_30336;
var statearr_30359_30406 = state_30336__$1;
(statearr_30359_30406[(2)] = null);

(statearr_30359_30406[(1)] = (25));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30337 === (19))){
var inst_30317 = (state_30336[(11)]);
var inst_30267 = (state_30336[(8)]);
var inst_30319 = cljs.core.async.muxch_STAR_.call(null,inst_30317);
var state_30336__$1 = state_30336;
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_30336__$1,(22),inst_30319,inst_30267);
} else {
if((state_val_30337 === (11))){
var inst_30277 = (state_30336[(13)]);
var inst_30291 = (state_30336[(10)]);
var inst_30291__$1 = cljs.core.seq.call(null,inst_30277);
var state_30336__$1 = (function (){var statearr_30360 = state_30336;
(statearr_30360[(10)] = inst_30291__$1);

return statearr_30360;
})();
if(inst_30291__$1){
var statearr_30361_30407 = state_30336__$1;
(statearr_30361_30407[(1)] = (13));

} else {
var statearr_30362_30408 = state_30336__$1;
(statearr_30362_30408[(1)] = (14));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30337 === (9))){
var inst_30313 = (state_30336[(2)]);
var state_30336__$1 = state_30336;
var statearr_30363_30409 = state_30336__$1;
(statearr_30363_30409[(2)] = inst_30313);

(statearr_30363_30409[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30337 === (5))){
var inst_30274 = cljs.core.deref.call(null,mults);
var inst_30275 = cljs.core.vals.call(null,inst_30274);
var inst_30276 = cljs.core.seq.call(null,inst_30275);
var inst_30277 = inst_30276;
var inst_30278 = null;
var inst_30279 = (0);
var inst_30280 = (0);
var state_30336__$1 = (function (){var statearr_30364 = state_30336;
(statearr_30364[(12)] = inst_30278);

(statearr_30364[(13)] = inst_30277);

(statearr_30364[(14)] = inst_30279);

(statearr_30364[(15)] = inst_30280);

return statearr_30364;
})();
var statearr_30365_30410 = state_30336__$1;
(statearr_30365_30410[(2)] = null);

(statearr_30365_30410[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30337 === (14))){
var state_30336__$1 = state_30336;
var statearr_30369_30411 = state_30336__$1;
(statearr_30369_30411[(2)] = null);

(statearr_30369_30411[(1)] = (15));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30337 === (16))){
var inst_30291 = (state_30336[(10)]);
var inst_30295 = cljs.core.chunk_first.call(null,inst_30291);
var inst_30296 = cljs.core.chunk_rest.call(null,inst_30291);
var inst_30297 = cljs.core.count.call(null,inst_30295);
var inst_30277 = inst_30296;
var inst_30278 = inst_30295;
var inst_30279 = inst_30297;
var inst_30280 = (0);
var state_30336__$1 = (function (){var statearr_30370 = state_30336;
(statearr_30370[(12)] = inst_30278);

(statearr_30370[(13)] = inst_30277);

(statearr_30370[(14)] = inst_30279);

(statearr_30370[(15)] = inst_30280);

return statearr_30370;
})();
var statearr_30371_30412 = state_30336__$1;
(statearr_30371_30412[(2)] = null);

(statearr_30371_30412[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30337 === (10))){
var inst_30278 = (state_30336[(12)]);
var inst_30277 = (state_30336[(13)]);
var inst_30279 = (state_30336[(14)]);
var inst_30280 = (state_30336[(15)]);
var inst_30285 = cljs.core._nth.call(null,inst_30278,inst_30280);
var inst_30286 = cljs.core.async.muxch_STAR_.call(null,inst_30285);
var inst_30287 = cljs.core.async.close_BANG_.call(null,inst_30286);
var inst_30288 = (inst_30280 + (1));
var tmp30366 = inst_30278;
var tmp30367 = inst_30277;
var tmp30368 = inst_30279;
var inst_30277__$1 = tmp30367;
var inst_30278__$1 = tmp30366;
var inst_30279__$1 = tmp30368;
var inst_30280__$1 = inst_30288;
var state_30336__$1 = (function (){var statearr_30372 = state_30336;
(statearr_30372[(12)] = inst_30278__$1);

(statearr_30372[(13)] = inst_30277__$1);

(statearr_30372[(17)] = inst_30287);

(statearr_30372[(14)] = inst_30279__$1);

(statearr_30372[(15)] = inst_30280__$1);

return statearr_30372;
})();
var statearr_30373_30413 = state_30336__$1;
(statearr_30373_30413[(2)] = null);

(statearr_30373_30413[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30337 === (18))){
var inst_30306 = (state_30336[(2)]);
var state_30336__$1 = state_30336;
var statearr_30374_30414 = state_30336__$1;
(statearr_30374_30414[(2)] = inst_30306);

(statearr_30374_30414[(1)] = (15));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30337 === (8))){
var inst_30279 = (state_30336[(14)]);
var inst_30280 = (state_30336[(15)]);
var inst_30282 = (inst_30280 < inst_30279);
var inst_30283 = inst_30282;
var state_30336__$1 = state_30336;
if(cljs.core.truth_(inst_30283)){
var statearr_30375_30415 = state_30336__$1;
(statearr_30375_30415[(1)] = (10));

} else {
var statearr_30376_30416 = state_30336__$1;
(statearr_30376_30416[(1)] = (11));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__28489__auto___30388,mults,ensure_mult,p))
;
return ((function (switch__28377__auto__,c__28489__auto___30388,mults,ensure_mult,p){
return (function() {
var cljs$core$async$state_machine__28378__auto__ = null;
var cljs$core$async$state_machine__28378__auto____0 = (function (){
var statearr_30380 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_30380[(0)] = cljs$core$async$state_machine__28378__auto__);

(statearr_30380[(1)] = (1));

return statearr_30380;
});
var cljs$core$async$state_machine__28378__auto____1 = (function (state_30336){
while(true){
var ret_value__28379__auto__ = (function (){try{while(true){
var result__28380__auto__ = switch__28377__auto__.call(null,state_30336);
if(cljs.core.keyword_identical_QMARK_.call(null,result__28380__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__28380__auto__;
}
break;
}
}catch (e30381){if((e30381 instanceof Object)){
var ex__28381__auto__ = e30381;
var statearr_30382_30417 = state_30336;
(statearr_30382_30417[(5)] = ex__28381__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_30336);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e30381;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__28379__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__30418 = state_30336;
state_30336 = G__30418;
continue;
} else {
return ret_value__28379__auto__;
}
break;
}
});
cljs$core$async$state_machine__28378__auto__ = function(state_30336){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__28378__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__28378__auto____1.call(this,state_30336);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__28378__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__28378__auto____0;
cljs$core$async$state_machine__28378__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__28378__auto____1;
return cljs$core$async$state_machine__28378__auto__;
})()
;})(switch__28377__auto__,c__28489__auto___30388,mults,ensure_mult,p))
})();
var state__28491__auto__ = (function (){var statearr_30383 = f__28490__auto__.call(null);
(statearr_30383[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__28489__auto___30388);

return statearr_30383;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__28491__auto__);
});})(c__28489__auto___30388,mults,ensure_mult,p))
);


return p;
});

cljs.core.async.pub.cljs$lang$maxFixedArity = 3;
/**
 * Subscribes a channel to a topic of a pub.
 * 
 *   By default the channel will be closed when the source closes,
 *   but can be determined by the close? parameter.
 */
cljs.core.async.sub = (function cljs$core$async$sub(var_args){
var args30419 = [];
var len__24201__auto___30422 = arguments.length;
var i__24202__auto___30423 = (0);
while(true){
if((i__24202__auto___30423 < len__24201__auto___30422)){
args30419.push((arguments[i__24202__auto___30423]));

var G__30424 = (i__24202__auto___30423 + (1));
i__24202__auto___30423 = G__30424;
continue;
} else {
}
break;
}

var G__30421 = args30419.length;
switch (G__30421) {
case 3:
return cljs.core.async.sub.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
case 4:
return cljs.core.async.sub.cljs$core$IFn$_invoke$arity$4((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]));

break;
default:
throw (new Error([cljs.core.str("Invalid arity: "),cljs.core.str(args30419.length)].join('')));

}
});

cljs.core.async.sub.cljs$core$IFn$_invoke$arity$3 = (function (p,topic,ch){
return cljs.core.async.sub.call(null,p,topic,ch,true);
});

cljs.core.async.sub.cljs$core$IFn$_invoke$arity$4 = (function (p,topic,ch,close_QMARK_){
return cljs.core.async.sub_STAR_.call(null,p,topic,ch,close_QMARK_);
});

cljs.core.async.sub.cljs$lang$maxFixedArity = 4;
/**
 * Unsubscribes a channel from a topic of a pub
 */
cljs.core.async.unsub = (function cljs$core$async$unsub(p,topic,ch){
return cljs.core.async.unsub_STAR_.call(null,p,topic,ch);
});
/**
 * Unsubscribes all channels from a pub, or a topic of a pub
 */
cljs.core.async.unsub_all = (function cljs$core$async$unsub_all(var_args){
var args30426 = [];
var len__24201__auto___30429 = arguments.length;
var i__24202__auto___30430 = (0);
while(true){
if((i__24202__auto___30430 < len__24201__auto___30429)){
args30426.push((arguments[i__24202__auto___30430]));

var G__30431 = (i__24202__auto___30430 + (1));
i__24202__auto___30430 = G__30431;
continue;
} else {
}
break;
}

var G__30428 = args30426.length;
switch (G__30428) {
case 1:
return cljs.core.async.unsub_all.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return cljs.core.async.unsub_all.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error([cljs.core.str("Invalid arity: "),cljs.core.str(args30426.length)].join('')));

}
});

cljs.core.async.unsub_all.cljs$core$IFn$_invoke$arity$1 = (function (p){
return cljs.core.async.unsub_all_STAR_.call(null,p);
});

cljs.core.async.unsub_all.cljs$core$IFn$_invoke$arity$2 = (function (p,topic){
return cljs.core.async.unsub_all_STAR_.call(null,p,topic);
});

cljs.core.async.unsub_all.cljs$lang$maxFixedArity = 2;
/**
 * Takes a function and a collection of source channels, and returns a
 *   channel which contains the values produced by applying f to the set
 *   of first items taken from each source channel, followed by applying
 *   f to the set of second items from each channel, until any one of the
 *   channels is closed, at which point the output channel will be
 *   closed. The returned channel will be unbuffered by default, or a
 *   buf-or-n can be supplied
 */
cljs.core.async.map = (function cljs$core$async$map(var_args){
var args30433 = [];
var len__24201__auto___30504 = arguments.length;
var i__24202__auto___30505 = (0);
while(true){
if((i__24202__auto___30505 < len__24201__auto___30504)){
args30433.push((arguments[i__24202__auto___30505]));

var G__30506 = (i__24202__auto___30505 + (1));
i__24202__auto___30505 = G__30506;
continue;
} else {
}
break;
}

var G__30435 = args30433.length;
switch (G__30435) {
case 2:
return cljs.core.async.map.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.map.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error([cljs.core.str("Invalid arity: "),cljs.core.str(args30433.length)].join('')));

}
});

cljs.core.async.map.cljs$core$IFn$_invoke$arity$2 = (function (f,chs){
return cljs.core.async.map.call(null,f,chs,null);
});

cljs.core.async.map.cljs$core$IFn$_invoke$arity$3 = (function (f,chs,buf_or_n){
var chs__$1 = cljs.core.vec.call(null,chs);
var out = cljs.core.async.chan.call(null,buf_or_n);
var cnt = cljs.core.count.call(null,chs__$1);
var rets = cljs.core.object_array.call(null,cnt);
var dchan = cljs.core.async.chan.call(null,(1));
var dctr = cljs.core.atom.call(null,null);
var done = cljs.core.mapv.call(null,((function (chs__$1,out,cnt,rets,dchan,dctr){
return (function (i){
return ((function (chs__$1,out,cnt,rets,dchan,dctr){
return (function (ret){
(rets[i] = ret);

if((cljs.core.swap_BANG_.call(null,dctr,cljs.core.dec) === (0))){
return cljs.core.async.put_BANG_.call(null,dchan,rets.slice((0)));
} else {
return null;
}
});
;})(chs__$1,out,cnt,rets,dchan,dctr))
});})(chs__$1,out,cnt,rets,dchan,dctr))
,cljs.core.range.call(null,cnt));
var c__28489__auto___30508 = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__28489__auto___30508,chs__$1,out,cnt,rets,dchan,dctr,done){
return (function (){
var f__28490__auto__ = (function (){var switch__28377__auto__ = ((function (c__28489__auto___30508,chs__$1,out,cnt,rets,dchan,dctr,done){
return (function (state_30474){
var state_val_30475 = (state_30474[(1)]);
if((state_val_30475 === (7))){
var state_30474__$1 = state_30474;
var statearr_30476_30509 = state_30474__$1;
(statearr_30476_30509[(2)] = null);

(statearr_30476_30509[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30475 === (1))){
var state_30474__$1 = state_30474;
var statearr_30477_30510 = state_30474__$1;
(statearr_30477_30510[(2)] = null);

(statearr_30477_30510[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30475 === (4))){
var inst_30438 = (state_30474[(7)]);
var inst_30440 = (inst_30438 < cnt);
var state_30474__$1 = state_30474;
if(cljs.core.truth_(inst_30440)){
var statearr_30478_30511 = state_30474__$1;
(statearr_30478_30511[(1)] = (6));

} else {
var statearr_30479_30512 = state_30474__$1;
(statearr_30479_30512[(1)] = (7));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30475 === (15))){
var inst_30470 = (state_30474[(2)]);
var state_30474__$1 = state_30474;
var statearr_30480_30513 = state_30474__$1;
(statearr_30480_30513[(2)] = inst_30470);

(statearr_30480_30513[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30475 === (13))){
var inst_30463 = cljs.core.async.close_BANG_.call(null,out);
var state_30474__$1 = state_30474;
var statearr_30481_30514 = state_30474__$1;
(statearr_30481_30514[(2)] = inst_30463);

(statearr_30481_30514[(1)] = (15));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30475 === (6))){
var state_30474__$1 = state_30474;
var statearr_30482_30515 = state_30474__$1;
(statearr_30482_30515[(2)] = null);

(statearr_30482_30515[(1)] = (11));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30475 === (3))){
var inst_30472 = (state_30474[(2)]);
var state_30474__$1 = state_30474;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_30474__$1,inst_30472);
} else {
if((state_val_30475 === (12))){
var inst_30460 = (state_30474[(8)]);
var inst_30460__$1 = (state_30474[(2)]);
var inst_30461 = cljs.core.some.call(null,cljs.core.nil_QMARK_,inst_30460__$1);
var state_30474__$1 = (function (){var statearr_30483 = state_30474;
(statearr_30483[(8)] = inst_30460__$1);

return statearr_30483;
})();
if(cljs.core.truth_(inst_30461)){
var statearr_30484_30516 = state_30474__$1;
(statearr_30484_30516[(1)] = (13));

} else {
var statearr_30485_30517 = state_30474__$1;
(statearr_30485_30517[(1)] = (14));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30475 === (2))){
var inst_30437 = cljs.core.reset_BANG_.call(null,dctr,cnt);
var inst_30438 = (0);
var state_30474__$1 = (function (){var statearr_30486 = state_30474;
(statearr_30486[(7)] = inst_30438);

(statearr_30486[(9)] = inst_30437);

return statearr_30486;
})();
var statearr_30487_30518 = state_30474__$1;
(statearr_30487_30518[(2)] = null);

(statearr_30487_30518[(1)] = (4));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30475 === (11))){
var inst_30438 = (state_30474[(7)]);
var _ = cljs.core.async.impl.ioc_helpers.add_exception_frame.call(null,state_30474,(10),Object,null,(9));
var inst_30447 = chs__$1.call(null,inst_30438);
var inst_30448 = done.call(null,inst_30438);
var inst_30449 = cljs.core.async.take_BANG_.call(null,inst_30447,inst_30448);
var state_30474__$1 = state_30474;
var statearr_30488_30519 = state_30474__$1;
(statearr_30488_30519[(2)] = inst_30449);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_30474__$1);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30475 === (9))){
var inst_30438 = (state_30474[(7)]);
var inst_30451 = (state_30474[(2)]);
var inst_30452 = (inst_30438 + (1));
var inst_30438__$1 = inst_30452;
var state_30474__$1 = (function (){var statearr_30489 = state_30474;
(statearr_30489[(10)] = inst_30451);

(statearr_30489[(7)] = inst_30438__$1);

return statearr_30489;
})();
var statearr_30490_30520 = state_30474__$1;
(statearr_30490_30520[(2)] = null);

(statearr_30490_30520[(1)] = (4));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30475 === (5))){
var inst_30458 = (state_30474[(2)]);
var state_30474__$1 = (function (){var statearr_30491 = state_30474;
(statearr_30491[(11)] = inst_30458);

return statearr_30491;
})();
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_30474__$1,(12),dchan);
} else {
if((state_val_30475 === (14))){
var inst_30460 = (state_30474[(8)]);
var inst_30465 = cljs.core.apply.call(null,f,inst_30460);
var state_30474__$1 = state_30474;
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_30474__$1,(16),out,inst_30465);
} else {
if((state_val_30475 === (16))){
var inst_30467 = (state_30474[(2)]);
var state_30474__$1 = (function (){var statearr_30492 = state_30474;
(statearr_30492[(12)] = inst_30467);

return statearr_30492;
})();
var statearr_30493_30521 = state_30474__$1;
(statearr_30493_30521[(2)] = null);

(statearr_30493_30521[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30475 === (10))){
var inst_30442 = (state_30474[(2)]);
var inst_30443 = cljs.core.swap_BANG_.call(null,dctr,cljs.core.dec);
var state_30474__$1 = (function (){var statearr_30494 = state_30474;
(statearr_30494[(13)] = inst_30442);

return statearr_30494;
})();
var statearr_30495_30522 = state_30474__$1;
(statearr_30495_30522[(2)] = inst_30443);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_30474__$1);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30475 === (8))){
var inst_30456 = (state_30474[(2)]);
var state_30474__$1 = state_30474;
var statearr_30496_30523 = state_30474__$1;
(statearr_30496_30523[(2)] = inst_30456);

(statearr_30496_30523[(1)] = (5));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__28489__auto___30508,chs__$1,out,cnt,rets,dchan,dctr,done))
;
return ((function (switch__28377__auto__,c__28489__auto___30508,chs__$1,out,cnt,rets,dchan,dctr,done){
return (function() {
var cljs$core$async$state_machine__28378__auto__ = null;
var cljs$core$async$state_machine__28378__auto____0 = (function (){
var statearr_30500 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_30500[(0)] = cljs$core$async$state_machine__28378__auto__);

(statearr_30500[(1)] = (1));

return statearr_30500;
});
var cljs$core$async$state_machine__28378__auto____1 = (function (state_30474){
while(true){
var ret_value__28379__auto__ = (function (){try{while(true){
var result__28380__auto__ = switch__28377__auto__.call(null,state_30474);
if(cljs.core.keyword_identical_QMARK_.call(null,result__28380__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__28380__auto__;
}
break;
}
}catch (e30501){if((e30501 instanceof Object)){
var ex__28381__auto__ = e30501;
var statearr_30502_30524 = state_30474;
(statearr_30502_30524[(5)] = ex__28381__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_30474);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e30501;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__28379__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__30525 = state_30474;
state_30474 = G__30525;
continue;
} else {
return ret_value__28379__auto__;
}
break;
}
});
cljs$core$async$state_machine__28378__auto__ = function(state_30474){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__28378__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__28378__auto____1.call(this,state_30474);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__28378__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__28378__auto____0;
cljs$core$async$state_machine__28378__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__28378__auto____1;
return cljs$core$async$state_machine__28378__auto__;
})()
;})(switch__28377__auto__,c__28489__auto___30508,chs__$1,out,cnt,rets,dchan,dctr,done))
})();
var state__28491__auto__ = (function (){var statearr_30503 = f__28490__auto__.call(null);
(statearr_30503[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__28489__auto___30508);

return statearr_30503;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__28491__auto__);
});})(c__28489__auto___30508,chs__$1,out,cnt,rets,dchan,dctr,done))
);


return out;
});

cljs.core.async.map.cljs$lang$maxFixedArity = 3;
/**
 * Takes a collection of source channels and returns a channel which
 *   contains all values taken from them. The returned channel will be
 *   unbuffered by default, or a buf-or-n can be supplied. The channel
 *   will close after all the source channels have closed.
 */
cljs.core.async.merge = (function cljs$core$async$merge(var_args){
var args30527 = [];
var len__24201__auto___30583 = arguments.length;
var i__24202__auto___30584 = (0);
while(true){
if((i__24202__auto___30584 < len__24201__auto___30583)){
args30527.push((arguments[i__24202__auto___30584]));

var G__30585 = (i__24202__auto___30584 + (1));
i__24202__auto___30584 = G__30585;
continue;
} else {
}
break;
}

var G__30529 = args30527.length;
switch (G__30529) {
case 1:
return cljs.core.async.merge.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return cljs.core.async.merge.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error([cljs.core.str("Invalid arity: "),cljs.core.str(args30527.length)].join('')));

}
});

cljs.core.async.merge.cljs$core$IFn$_invoke$arity$1 = (function (chs){
return cljs.core.async.merge.call(null,chs,null);
});

cljs.core.async.merge.cljs$core$IFn$_invoke$arity$2 = (function (chs,buf_or_n){
var out = cljs.core.async.chan.call(null,buf_or_n);
var c__28489__auto___30587 = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__28489__auto___30587,out){
return (function (){
var f__28490__auto__ = (function (){var switch__28377__auto__ = ((function (c__28489__auto___30587,out){
return (function (state_30559){
var state_val_30560 = (state_30559[(1)]);
if((state_val_30560 === (7))){
var inst_30538 = (state_30559[(7)]);
var inst_30539 = (state_30559[(8)]);
var inst_30538__$1 = (state_30559[(2)]);
var inst_30539__$1 = cljs.core.nth.call(null,inst_30538__$1,(0),null);
var inst_30540 = cljs.core.nth.call(null,inst_30538__$1,(1),null);
var inst_30541 = (inst_30539__$1 == null);
var state_30559__$1 = (function (){var statearr_30561 = state_30559;
(statearr_30561[(9)] = inst_30540);

(statearr_30561[(7)] = inst_30538__$1);

(statearr_30561[(8)] = inst_30539__$1);

return statearr_30561;
})();
if(cljs.core.truth_(inst_30541)){
var statearr_30562_30588 = state_30559__$1;
(statearr_30562_30588[(1)] = (8));

} else {
var statearr_30563_30589 = state_30559__$1;
(statearr_30563_30589[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30560 === (1))){
var inst_30530 = cljs.core.vec.call(null,chs);
var inst_30531 = inst_30530;
var state_30559__$1 = (function (){var statearr_30564 = state_30559;
(statearr_30564[(10)] = inst_30531);

return statearr_30564;
})();
var statearr_30565_30590 = state_30559__$1;
(statearr_30565_30590[(2)] = null);

(statearr_30565_30590[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30560 === (4))){
var inst_30531 = (state_30559[(10)]);
var state_30559__$1 = state_30559;
return cljs.core.async.ioc_alts_BANG_.call(null,state_30559__$1,(7),inst_30531);
} else {
if((state_val_30560 === (6))){
var inst_30555 = (state_30559[(2)]);
var state_30559__$1 = state_30559;
var statearr_30566_30591 = state_30559__$1;
(statearr_30566_30591[(2)] = inst_30555);

(statearr_30566_30591[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30560 === (3))){
var inst_30557 = (state_30559[(2)]);
var state_30559__$1 = state_30559;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_30559__$1,inst_30557);
} else {
if((state_val_30560 === (2))){
var inst_30531 = (state_30559[(10)]);
var inst_30533 = cljs.core.count.call(null,inst_30531);
var inst_30534 = (inst_30533 > (0));
var state_30559__$1 = state_30559;
if(cljs.core.truth_(inst_30534)){
var statearr_30568_30592 = state_30559__$1;
(statearr_30568_30592[(1)] = (4));

} else {
var statearr_30569_30593 = state_30559__$1;
(statearr_30569_30593[(1)] = (5));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30560 === (11))){
var inst_30531 = (state_30559[(10)]);
var inst_30548 = (state_30559[(2)]);
var tmp30567 = inst_30531;
var inst_30531__$1 = tmp30567;
var state_30559__$1 = (function (){var statearr_30570 = state_30559;
(statearr_30570[(11)] = inst_30548);

(statearr_30570[(10)] = inst_30531__$1);

return statearr_30570;
})();
var statearr_30571_30594 = state_30559__$1;
(statearr_30571_30594[(2)] = null);

(statearr_30571_30594[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30560 === (9))){
var inst_30539 = (state_30559[(8)]);
var state_30559__$1 = state_30559;
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_30559__$1,(11),out,inst_30539);
} else {
if((state_val_30560 === (5))){
var inst_30553 = cljs.core.async.close_BANG_.call(null,out);
var state_30559__$1 = state_30559;
var statearr_30572_30595 = state_30559__$1;
(statearr_30572_30595[(2)] = inst_30553);

(statearr_30572_30595[(1)] = (6));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30560 === (10))){
var inst_30551 = (state_30559[(2)]);
var state_30559__$1 = state_30559;
var statearr_30573_30596 = state_30559__$1;
(statearr_30573_30596[(2)] = inst_30551);

(statearr_30573_30596[(1)] = (6));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30560 === (8))){
var inst_30540 = (state_30559[(9)]);
var inst_30538 = (state_30559[(7)]);
var inst_30539 = (state_30559[(8)]);
var inst_30531 = (state_30559[(10)]);
var inst_30543 = (function (){var cs = inst_30531;
var vec__30536 = inst_30538;
var v = inst_30539;
var c = inst_30540;
return ((function (cs,vec__30536,v,c,inst_30540,inst_30538,inst_30539,inst_30531,state_val_30560,c__28489__auto___30587,out){
return (function (p1__30526_SHARP_){
return cljs.core.not_EQ_.call(null,c,p1__30526_SHARP_);
});
;})(cs,vec__30536,v,c,inst_30540,inst_30538,inst_30539,inst_30531,state_val_30560,c__28489__auto___30587,out))
})();
var inst_30544 = cljs.core.filterv.call(null,inst_30543,inst_30531);
var inst_30531__$1 = inst_30544;
var state_30559__$1 = (function (){var statearr_30574 = state_30559;
(statearr_30574[(10)] = inst_30531__$1);

return statearr_30574;
})();
var statearr_30575_30597 = state_30559__$1;
(statearr_30575_30597[(2)] = null);

(statearr_30575_30597[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
});})(c__28489__auto___30587,out))
;
return ((function (switch__28377__auto__,c__28489__auto___30587,out){
return (function() {
var cljs$core$async$state_machine__28378__auto__ = null;
var cljs$core$async$state_machine__28378__auto____0 = (function (){
var statearr_30579 = [null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_30579[(0)] = cljs$core$async$state_machine__28378__auto__);

(statearr_30579[(1)] = (1));

return statearr_30579;
});
var cljs$core$async$state_machine__28378__auto____1 = (function (state_30559){
while(true){
var ret_value__28379__auto__ = (function (){try{while(true){
var result__28380__auto__ = switch__28377__auto__.call(null,state_30559);
if(cljs.core.keyword_identical_QMARK_.call(null,result__28380__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__28380__auto__;
}
break;
}
}catch (e30580){if((e30580 instanceof Object)){
var ex__28381__auto__ = e30580;
var statearr_30581_30598 = state_30559;
(statearr_30581_30598[(5)] = ex__28381__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_30559);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e30580;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__28379__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__30599 = state_30559;
state_30559 = G__30599;
continue;
} else {
return ret_value__28379__auto__;
}
break;
}
});
cljs$core$async$state_machine__28378__auto__ = function(state_30559){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__28378__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__28378__auto____1.call(this,state_30559);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__28378__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__28378__auto____0;
cljs$core$async$state_machine__28378__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__28378__auto____1;
return cljs$core$async$state_machine__28378__auto__;
})()
;})(switch__28377__auto__,c__28489__auto___30587,out))
})();
var state__28491__auto__ = (function (){var statearr_30582 = f__28490__auto__.call(null);
(statearr_30582[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__28489__auto___30587);

return statearr_30582;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__28491__auto__);
});})(c__28489__auto___30587,out))
);


return out;
});

cljs.core.async.merge.cljs$lang$maxFixedArity = 2;
/**
 * Returns a channel containing the single (collection) result of the
 *   items taken from the channel conjoined to the supplied
 *   collection. ch must close before into produces a result.
 */
cljs.core.async.into = (function cljs$core$async$into(coll,ch){
return cljs.core.async.reduce.call(null,cljs.core.conj,coll,ch);
});
/**
 * Returns a channel that will return, at most, n items from ch. After n items
 * have been returned, or ch has been closed, the return chanel will close.
 * 
 *   The output channel is unbuffered by default, unless buf-or-n is given.
 */
cljs.core.async.take = (function cljs$core$async$take(var_args){
var args30600 = [];
var len__24201__auto___30649 = arguments.length;
var i__24202__auto___30650 = (0);
while(true){
if((i__24202__auto___30650 < len__24201__auto___30649)){
args30600.push((arguments[i__24202__auto___30650]));

var G__30651 = (i__24202__auto___30650 + (1));
i__24202__auto___30650 = G__30651;
continue;
} else {
}
break;
}

var G__30602 = args30600.length;
switch (G__30602) {
case 2:
return cljs.core.async.take.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.take.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error([cljs.core.str("Invalid arity: "),cljs.core.str(args30600.length)].join('')));

}
});

cljs.core.async.take.cljs$core$IFn$_invoke$arity$2 = (function (n,ch){
return cljs.core.async.take.call(null,n,ch,null);
});

cljs.core.async.take.cljs$core$IFn$_invoke$arity$3 = (function (n,ch,buf_or_n){
var out = cljs.core.async.chan.call(null,buf_or_n);
var c__28489__auto___30653 = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__28489__auto___30653,out){
return (function (){
var f__28490__auto__ = (function (){var switch__28377__auto__ = ((function (c__28489__auto___30653,out){
return (function (state_30626){
var state_val_30627 = (state_30626[(1)]);
if((state_val_30627 === (7))){
var inst_30608 = (state_30626[(7)]);
var inst_30608__$1 = (state_30626[(2)]);
var inst_30609 = (inst_30608__$1 == null);
var inst_30610 = cljs.core.not.call(null,inst_30609);
var state_30626__$1 = (function (){var statearr_30628 = state_30626;
(statearr_30628[(7)] = inst_30608__$1);

return statearr_30628;
})();
if(inst_30610){
var statearr_30629_30654 = state_30626__$1;
(statearr_30629_30654[(1)] = (8));

} else {
var statearr_30630_30655 = state_30626__$1;
(statearr_30630_30655[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30627 === (1))){
var inst_30603 = (0);
var state_30626__$1 = (function (){var statearr_30631 = state_30626;
(statearr_30631[(8)] = inst_30603);

return statearr_30631;
})();
var statearr_30632_30656 = state_30626__$1;
(statearr_30632_30656[(2)] = null);

(statearr_30632_30656[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30627 === (4))){
var state_30626__$1 = state_30626;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_30626__$1,(7),ch);
} else {
if((state_val_30627 === (6))){
var inst_30621 = (state_30626[(2)]);
var state_30626__$1 = state_30626;
var statearr_30633_30657 = state_30626__$1;
(statearr_30633_30657[(2)] = inst_30621);

(statearr_30633_30657[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30627 === (3))){
var inst_30623 = (state_30626[(2)]);
var inst_30624 = cljs.core.async.close_BANG_.call(null,out);
var state_30626__$1 = (function (){var statearr_30634 = state_30626;
(statearr_30634[(9)] = inst_30623);

return statearr_30634;
})();
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_30626__$1,inst_30624);
} else {
if((state_val_30627 === (2))){
var inst_30603 = (state_30626[(8)]);
var inst_30605 = (inst_30603 < n);
var state_30626__$1 = state_30626;
if(cljs.core.truth_(inst_30605)){
var statearr_30635_30658 = state_30626__$1;
(statearr_30635_30658[(1)] = (4));

} else {
var statearr_30636_30659 = state_30626__$1;
(statearr_30636_30659[(1)] = (5));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30627 === (11))){
var inst_30603 = (state_30626[(8)]);
var inst_30613 = (state_30626[(2)]);
var inst_30614 = (inst_30603 + (1));
var inst_30603__$1 = inst_30614;
var state_30626__$1 = (function (){var statearr_30637 = state_30626;
(statearr_30637[(10)] = inst_30613);

(statearr_30637[(8)] = inst_30603__$1);

return statearr_30637;
})();
var statearr_30638_30660 = state_30626__$1;
(statearr_30638_30660[(2)] = null);

(statearr_30638_30660[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30627 === (9))){
var state_30626__$1 = state_30626;
var statearr_30639_30661 = state_30626__$1;
(statearr_30639_30661[(2)] = null);

(statearr_30639_30661[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30627 === (5))){
var state_30626__$1 = state_30626;
var statearr_30640_30662 = state_30626__$1;
(statearr_30640_30662[(2)] = null);

(statearr_30640_30662[(1)] = (6));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30627 === (10))){
var inst_30618 = (state_30626[(2)]);
var state_30626__$1 = state_30626;
var statearr_30641_30663 = state_30626__$1;
(statearr_30641_30663[(2)] = inst_30618);

(statearr_30641_30663[(1)] = (6));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30627 === (8))){
var inst_30608 = (state_30626[(7)]);
var state_30626__$1 = state_30626;
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_30626__$1,(11),out,inst_30608);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
});})(c__28489__auto___30653,out))
;
return ((function (switch__28377__auto__,c__28489__auto___30653,out){
return (function() {
var cljs$core$async$state_machine__28378__auto__ = null;
var cljs$core$async$state_machine__28378__auto____0 = (function (){
var statearr_30645 = [null,null,null,null,null,null,null,null,null,null,null];
(statearr_30645[(0)] = cljs$core$async$state_machine__28378__auto__);

(statearr_30645[(1)] = (1));

return statearr_30645;
});
var cljs$core$async$state_machine__28378__auto____1 = (function (state_30626){
while(true){
var ret_value__28379__auto__ = (function (){try{while(true){
var result__28380__auto__ = switch__28377__auto__.call(null,state_30626);
if(cljs.core.keyword_identical_QMARK_.call(null,result__28380__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__28380__auto__;
}
break;
}
}catch (e30646){if((e30646 instanceof Object)){
var ex__28381__auto__ = e30646;
var statearr_30647_30664 = state_30626;
(statearr_30647_30664[(5)] = ex__28381__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_30626);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e30646;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__28379__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__30665 = state_30626;
state_30626 = G__30665;
continue;
} else {
return ret_value__28379__auto__;
}
break;
}
});
cljs$core$async$state_machine__28378__auto__ = function(state_30626){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__28378__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__28378__auto____1.call(this,state_30626);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__28378__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__28378__auto____0;
cljs$core$async$state_machine__28378__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__28378__auto____1;
return cljs$core$async$state_machine__28378__auto__;
})()
;})(switch__28377__auto__,c__28489__auto___30653,out))
})();
var state__28491__auto__ = (function (){var statearr_30648 = f__28490__auto__.call(null);
(statearr_30648[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__28489__auto___30653);

return statearr_30648;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__28491__auto__);
});})(c__28489__auto___30653,out))
);


return out;
});

cljs.core.async.take.cljs$lang$maxFixedArity = 3;
/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.map_LT_ = (function cljs$core$async$map_LT_(f,ch){
if(typeof cljs.core.async.t_cljs$core$async30673 !== 'undefined'){
} else {

/**
* @constructor
 * @implements {cljs.core.async.impl.protocols.Channel}
 * @implements {cljs.core.async.impl.protocols.WritePort}
 * @implements {cljs.core.async.impl.protocols.ReadPort}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async30673 = (function (map_LT_,f,ch,meta30674){
this.map_LT_ = map_LT_;
this.f = f;
this.ch = ch;
this.meta30674 = meta30674;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
})
cljs.core.async.t_cljs$core$async30673.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_30675,meta30674__$1){
var self__ = this;
var _30675__$1 = this;
return (new cljs.core.async.t_cljs$core$async30673(self__.map_LT_,self__.f,self__.ch,meta30674__$1));
});

cljs.core.async.t_cljs$core$async30673.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_30675){
var self__ = this;
var _30675__$1 = this;
return self__.meta30674;
});

cljs.core.async.t_cljs$core$async30673.prototype.cljs$core$async$impl$protocols$Channel$ = true;

cljs.core.async.t_cljs$core$async30673.prototype.cljs$core$async$impl$protocols$Channel$close_BANG_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.close_BANG_.call(null,self__.ch);
});

cljs.core.async.t_cljs$core$async30673.prototype.cljs$core$async$impl$protocols$Channel$closed_QMARK_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.closed_QMARK_.call(null,self__.ch);
});

cljs.core.async.t_cljs$core$async30673.prototype.cljs$core$async$impl$protocols$ReadPort$ = true;

cljs.core.async.t_cljs$core$async30673.prototype.cljs$core$async$impl$protocols$ReadPort$take_BANG_$arity$2 = (function (_,fn1){
var self__ = this;
var ___$1 = this;
var ret = cljs.core.async.impl.protocols.take_BANG_.call(null,self__.ch,(function (){
if(typeof cljs.core.async.t_cljs$core$async30676 !== 'undefined'){
} else {

/**
* @constructor
 * @implements {cljs.core.async.impl.protocols.Handler}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async30676 = (function (map_LT_,f,ch,meta30674,_,fn1,meta30677){
this.map_LT_ = map_LT_;
this.f = f;
this.ch = ch;
this.meta30674 = meta30674;
this._ = _;
this.fn1 = fn1;
this.meta30677 = meta30677;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
})
cljs.core.async.t_cljs$core$async30676.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = ((function (___$1){
return (function (_30678,meta30677__$1){
var self__ = this;
var _30678__$1 = this;
return (new cljs.core.async.t_cljs$core$async30676(self__.map_LT_,self__.f,self__.ch,self__.meta30674,self__._,self__.fn1,meta30677__$1));
});})(___$1))
;

cljs.core.async.t_cljs$core$async30676.prototype.cljs$core$IMeta$_meta$arity$1 = ((function (___$1){
return (function (_30678){
var self__ = this;
var _30678__$1 = this;
return self__.meta30677;
});})(___$1))
;

cljs.core.async.t_cljs$core$async30676.prototype.cljs$core$async$impl$protocols$Handler$ = true;

cljs.core.async.t_cljs$core$async30676.prototype.cljs$core$async$impl$protocols$Handler$active_QMARK_$arity$1 = ((function (___$1){
return (function (___$1){
var self__ = this;
var ___$2 = this;
return cljs.core.async.impl.protocols.active_QMARK_.call(null,self__.fn1);
});})(___$1))
;

cljs.core.async.t_cljs$core$async30676.prototype.cljs$core$async$impl$protocols$Handler$blockable_QMARK_$arity$1 = ((function (___$1){
return (function (___$1){
var self__ = this;
var ___$2 = this;
return true;
});})(___$1))
;

cljs.core.async.t_cljs$core$async30676.prototype.cljs$core$async$impl$protocols$Handler$commit$arity$1 = ((function (___$1){
return (function (___$1){
var self__ = this;
var ___$2 = this;
var f1 = cljs.core.async.impl.protocols.commit.call(null,self__.fn1);
return ((function (f1,___$2,___$1){
return (function (p1__30666_SHARP_){
return f1.call(null,(((p1__30666_SHARP_ == null))?null:self__.f.call(null,p1__30666_SHARP_)));
});
;})(f1,___$2,___$1))
});})(___$1))
;

cljs.core.async.t_cljs$core$async30676.getBasis = ((function (___$1){
return (function (){
return new cljs.core.PersistentVector(null, 7, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.with_meta(new cljs.core.Symbol(null,"map<","map<",-1235808357,null),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"arglists","arglists",1661989754),cljs.core.list(new cljs.core.Symbol(null,"quote","quote",1377916282,null),cljs.core.list(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"f","f",43394975,null),new cljs.core.Symbol(null,"ch","ch",1085813622,null)], null))),new cljs.core.Keyword(null,"doc","doc",1913296891),"Deprecated - this function will be removed. Use transducer instead"], null)),new cljs.core.Symbol(null,"f","f",43394975,null),new cljs.core.Symbol(null,"ch","ch",1085813622,null),new cljs.core.Symbol(null,"meta30674","meta30674",2071691845,null),cljs.core.with_meta(new cljs.core.Symbol(null,"_","_",-1201019570,null),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"tag","tag",-1290361223),new cljs.core.Symbol("cljs.core.async","t_cljs$core$async30673","cljs.core.async/t_cljs$core$async30673",1923785027,null)], null)),new cljs.core.Symbol(null,"fn1","fn1",895834444,null),new cljs.core.Symbol(null,"meta30677","meta30677",89401192,null)], null);
});})(___$1))
;

cljs.core.async.t_cljs$core$async30676.cljs$lang$type = true;

cljs.core.async.t_cljs$core$async30676.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async30676";

cljs.core.async.t_cljs$core$async30676.cljs$lang$ctorPrWriter = ((function (___$1){
return (function (this__23741__auto__,writer__23742__auto__,opt__23743__auto__){
return cljs.core._write.call(null,writer__23742__auto__,"cljs.core.async/t_cljs$core$async30676");
});})(___$1))
;

cljs.core.async.__GT_t_cljs$core$async30676 = ((function (___$1){
return (function cljs$core$async$map_LT__$___GT_t_cljs$core$async30676(map_LT___$1,f__$1,ch__$1,meta30674__$1,___$2,fn1__$1,meta30677){
return (new cljs.core.async.t_cljs$core$async30676(map_LT___$1,f__$1,ch__$1,meta30674__$1,___$2,fn1__$1,meta30677));
});})(___$1))
;

}

return (new cljs.core.async.t_cljs$core$async30676(self__.map_LT_,self__.f,self__.ch,self__.meta30674,___$1,fn1,cljs.core.PersistentArrayMap.EMPTY));
})()
);
if(cljs.core.truth_((function (){var and__23131__auto__ = ret;
if(cljs.core.truth_(and__23131__auto__)){
return !((cljs.core.deref.call(null,ret) == null));
} else {
return and__23131__auto__;
}
})())){
return cljs.core.async.impl.channels.box.call(null,self__.f.call(null,cljs.core.deref.call(null,ret)));
} else {
return ret;
}
});

cljs.core.async.t_cljs$core$async30673.prototype.cljs$core$async$impl$protocols$WritePort$ = true;

cljs.core.async.t_cljs$core$async30673.prototype.cljs$core$async$impl$protocols$WritePort$put_BANG_$arity$3 = (function (_,val,fn1){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.put_BANG_.call(null,self__.ch,val,fn1);
});

cljs.core.async.t_cljs$core$async30673.getBasis = (function (){
return new cljs.core.PersistentVector(null, 4, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.with_meta(new cljs.core.Symbol(null,"map<","map<",-1235808357,null),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"arglists","arglists",1661989754),cljs.core.list(new cljs.core.Symbol(null,"quote","quote",1377916282,null),cljs.core.list(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"f","f",43394975,null),new cljs.core.Symbol(null,"ch","ch",1085813622,null)], null))),new cljs.core.Keyword(null,"doc","doc",1913296891),"Deprecated - this function will be removed. Use transducer instead"], null)),new cljs.core.Symbol(null,"f","f",43394975,null),new cljs.core.Symbol(null,"ch","ch",1085813622,null),new cljs.core.Symbol(null,"meta30674","meta30674",2071691845,null)], null);
});

cljs.core.async.t_cljs$core$async30673.cljs$lang$type = true;

cljs.core.async.t_cljs$core$async30673.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async30673";

cljs.core.async.t_cljs$core$async30673.cljs$lang$ctorPrWriter = (function (this__23741__auto__,writer__23742__auto__,opt__23743__auto__){
return cljs.core._write.call(null,writer__23742__auto__,"cljs.core.async/t_cljs$core$async30673");
});

cljs.core.async.__GT_t_cljs$core$async30673 = (function cljs$core$async$map_LT__$___GT_t_cljs$core$async30673(map_LT___$1,f__$1,ch__$1,meta30674){
return (new cljs.core.async.t_cljs$core$async30673(map_LT___$1,f__$1,ch__$1,meta30674));
});

}

return (new cljs.core.async.t_cljs$core$async30673(cljs$core$async$map_LT_,f,ch,cljs.core.PersistentArrayMap.EMPTY));
});
/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.map_GT_ = (function cljs$core$async$map_GT_(f,ch){
if(typeof cljs.core.async.t_cljs$core$async30682 !== 'undefined'){
} else {

/**
* @constructor
 * @implements {cljs.core.async.impl.protocols.Channel}
 * @implements {cljs.core.async.impl.protocols.WritePort}
 * @implements {cljs.core.async.impl.protocols.ReadPort}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async30682 = (function (map_GT_,f,ch,meta30683){
this.map_GT_ = map_GT_;
this.f = f;
this.ch = ch;
this.meta30683 = meta30683;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
})
cljs.core.async.t_cljs$core$async30682.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_30684,meta30683__$1){
var self__ = this;
var _30684__$1 = this;
return (new cljs.core.async.t_cljs$core$async30682(self__.map_GT_,self__.f,self__.ch,meta30683__$1));
});

cljs.core.async.t_cljs$core$async30682.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_30684){
var self__ = this;
var _30684__$1 = this;
return self__.meta30683;
});

cljs.core.async.t_cljs$core$async30682.prototype.cljs$core$async$impl$protocols$Channel$ = true;

cljs.core.async.t_cljs$core$async30682.prototype.cljs$core$async$impl$protocols$Channel$close_BANG_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.close_BANG_.call(null,self__.ch);
});

cljs.core.async.t_cljs$core$async30682.prototype.cljs$core$async$impl$protocols$ReadPort$ = true;

cljs.core.async.t_cljs$core$async30682.prototype.cljs$core$async$impl$protocols$ReadPort$take_BANG_$arity$2 = (function (_,fn1){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.take_BANG_.call(null,self__.ch,fn1);
});

cljs.core.async.t_cljs$core$async30682.prototype.cljs$core$async$impl$protocols$WritePort$ = true;

cljs.core.async.t_cljs$core$async30682.prototype.cljs$core$async$impl$protocols$WritePort$put_BANG_$arity$3 = (function (_,val,fn1){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.put_BANG_.call(null,self__.ch,self__.f.call(null,val),fn1);
});

cljs.core.async.t_cljs$core$async30682.getBasis = (function (){
return new cljs.core.PersistentVector(null, 4, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.with_meta(new cljs.core.Symbol(null,"map>","map>",1676369295,null),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"arglists","arglists",1661989754),cljs.core.list(new cljs.core.Symbol(null,"quote","quote",1377916282,null),cljs.core.list(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"f","f",43394975,null),new cljs.core.Symbol(null,"ch","ch",1085813622,null)], null))),new cljs.core.Keyword(null,"doc","doc",1913296891),"Deprecated - this function will be removed. Use transducer instead"], null)),new cljs.core.Symbol(null,"f","f",43394975,null),new cljs.core.Symbol(null,"ch","ch",1085813622,null),new cljs.core.Symbol(null,"meta30683","meta30683",759794392,null)], null);
});

cljs.core.async.t_cljs$core$async30682.cljs$lang$type = true;

cljs.core.async.t_cljs$core$async30682.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async30682";

cljs.core.async.t_cljs$core$async30682.cljs$lang$ctorPrWriter = (function (this__23741__auto__,writer__23742__auto__,opt__23743__auto__){
return cljs.core._write.call(null,writer__23742__auto__,"cljs.core.async/t_cljs$core$async30682");
});

cljs.core.async.__GT_t_cljs$core$async30682 = (function cljs$core$async$map_GT__$___GT_t_cljs$core$async30682(map_GT___$1,f__$1,ch__$1,meta30683){
return (new cljs.core.async.t_cljs$core$async30682(map_GT___$1,f__$1,ch__$1,meta30683));
});

}

return (new cljs.core.async.t_cljs$core$async30682(cljs$core$async$map_GT_,f,ch,cljs.core.PersistentArrayMap.EMPTY));
});
/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.filter_GT_ = (function cljs$core$async$filter_GT_(p,ch){
if(typeof cljs.core.async.t_cljs$core$async30688 !== 'undefined'){
} else {

/**
* @constructor
 * @implements {cljs.core.async.impl.protocols.Channel}
 * @implements {cljs.core.async.impl.protocols.WritePort}
 * @implements {cljs.core.async.impl.protocols.ReadPort}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async30688 = (function (filter_GT_,p,ch,meta30689){
this.filter_GT_ = filter_GT_;
this.p = p;
this.ch = ch;
this.meta30689 = meta30689;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
})
cljs.core.async.t_cljs$core$async30688.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_30690,meta30689__$1){
var self__ = this;
var _30690__$1 = this;
return (new cljs.core.async.t_cljs$core$async30688(self__.filter_GT_,self__.p,self__.ch,meta30689__$1));
});

cljs.core.async.t_cljs$core$async30688.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_30690){
var self__ = this;
var _30690__$1 = this;
return self__.meta30689;
});

cljs.core.async.t_cljs$core$async30688.prototype.cljs$core$async$impl$protocols$Channel$ = true;

cljs.core.async.t_cljs$core$async30688.prototype.cljs$core$async$impl$protocols$Channel$close_BANG_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.close_BANG_.call(null,self__.ch);
});

cljs.core.async.t_cljs$core$async30688.prototype.cljs$core$async$impl$protocols$Channel$closed_QMARK_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.closed_QMARK_.call(null,self__.ch);
});

cljs.core.async.t_cljs$core$async30688.prototype.cljs$core$async$impl$protocols$ReadPort$ = true;

cljs.core.async.t_cljs$core$async30688.prototype.cljs$core$async$impl$protocols$ReadPort$take_BANG_$arity$2 = (function (_,fn1){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.take_BANG_.call(null,self__.ch,fn1);
});

cljs.core.async.t_cljs$core$async30688.prototype.cljs$core$async$impl$protocols$WritePort$ = true;

cljs.core.async.t_cljs$core$async30688.prototype.cljs$core$async$impl$protocols$WritePort$put_BANG_$arity$3 = (function (_,val,fn1){
var self__ = this;
var ___$1 = this;
if(cljs.core.truth_(self__.p.call(null,val))){
return cljs.core.async.impl.protocols.put_BANG_.call(null,self__.ch,val,fn1);
} else {
return cljs.core.async.impl.channels.box.call(null,cljs.core.not.call(null,cljs.core.async.impl.protocols.closed_QMARK_.call(null,self__.ch)));
}
});

cljs.core.async.t_cljs$core$async30688.getBasis = (function (){
return new cljs.core.PersistentVector(null, 4, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.with_meta(new cljs.core.Symbol(null,"filter>","filter>",-37644455,null),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"arglists","arglists",1661989754),cljs.core.list(new cljs.core.Symbol(null,"quote","quote",1377916282,null),cljs.core.list(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"p","p",1791580836,null),new cljs.core.Symbol(null,"ch","ch",1085813622,null)], null))),new cljs.core.Keyword(null,"doc","doc",1913296891),"Deprecated - this function will be removed. Use transducer instead"], null)),new cljs.core.Symbol(null,"p","p",1791580836,null),new cljs.core.Symbol(null,"ch","ch",1085813622,null),new cljs.core.Symbol(null,"meta30689","meta30689",1896820200,null)], null);
});

cljs.core.async.t_cljs$core$async30688.cljs$lang$type = true;

cljs.core.async.t_cljs$core$async30688.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async30688";

cljs.core.async.t_cljs$core$async30688.cljs$lang$ctorPrWriter = (function (this__23741__auto__,writer__23742__auto__,opt__23743__auto__){
return cljs.core._write.call(null,writer__23742__auto__,"cljs.core.async/t_cljs$core$async30688");
});

cljs.core.async.__GT_t_cljs$core$async30688 = (function cljs$core$async$filter_GT__$___GT_t_cljs$core$async30688(filter_GT___$1,p__$1,ch__$1,meta30689){
return (new cljs.core.async.t_cljs$core$async30688(filter_GT___$1,p__$1,ch__$1,meta30689));
});

}

return (new cljs.core.async.t_cljs$core$async30688(cljs$core$async$filter_GT_,p,ch,cljs.core.PersistentArrayMap.EMPTY));
});
/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.remove_GT_ = (function cljs$core$async$remove_GT_(p,ch){
return cljs.core.async.filter_GT_.call(null,cljs.core.complement.call(null,p),ch);
});
/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.filter_LT_ = (function cljs$core$async$filter_LT_(var_args){
var args30691 = [];
var len__24201__auto___30735 = arguments.length;
var i__24202__auto___30736 = (0);
while(true){
if((i__24202__auto___30736 < len__24201__auto___30735)){
args30691.push((arguments[i__24202__auto___30736]));

var G__30737 = (i__24202__auto___30736 + (1));
i__24202__auto___30736 = G__30737;
continue;
} else {
}
break;
}

var G__30693 = args30691.length;
switch (G__30693) {
case 2:
return cljs.core.async.filter_LT_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.filter_LT_.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error([cljs.core.str("Invalid arity: "),cljs.core.str(args30691.length)].join('')));

}
});

cljs.core.async.filter_LT_.cljs$core$IFn$_invoke$arity$2 = (function (p,ch){
return cljs.core.async.filter_LT_.call(null,p,ch,null);
});

cljs.core.async.filter_LT_.cljs$core$IFn$_invoke$arity$3 = (function (p,ch,buf_or_n){
var out = cljs.core.async.chan.call(null,buf_or_n);
var c__28489__auto___30739 = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__28489__auto___30739,out){
return (function (){
var f__28490__auto__ = (function (){var switch__28377__auto__ = ((function (c__28489__auto___30739,out){
return (function (state_30714){
var state_val_30715 = (state_30714[(1)]);
if((state_val_30715 === (7))){
var inst_30710 = (state_30714[(2)]);
var state_30714__$1 = state_30714;
var statearr_30716_30740 = state_30714__$1;
(statearr_30716_30740[(2)] = inst_30710);

(statearr_30716_30740[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30715 === (1))){
var state_30714__$1 = state_30714;
var statearr_30717_30741 = state_30714__$1;
(statearr_30717_30741[(2)] = null);

(statearr_30717_30741[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30715 === (4))){
var inst_30696 = (state_30714[(7)]);
var inst_30696__$1 = (state_30714[(2)]);
var inst_30697 = (inst_30696__$1 == null);
var state_30714__$1 = (function (){var statearr_30718 = state_30714;
(statearr_30718[(7)] = inst_30696__$1);

return statearr_30718;
})();
if(cljs.core.truth_(inst_30697)){
var statearr_30719_30742 = state_30714__$1;
(statearr_30719_30742[(1)] = (5));

} else {
var statearr_30720_30743 = state_30714__$1;
(statearr_30720_30743[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30715 === (6))){
var inst_30696 = (state_30714[(7)]);
var inst_30701 = p.call(null,inst_30696);
var state_30714__$1 = state_30714;
if(cljs.core.truth_(inst_30701)){
var statearr_30721_30744 = state_30714__$1;
(statearr_30721_30744[(1)] = (8));

} else {
var statearr_30722_30745 = state_30714__$1;
(statearr_30722_30745[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30715 === (3))){
var inst_30712 = (state_30714[(2)]);
var state_30714__$1 = state_30714;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_30714__$1,inst_30712);
} else {
if((state_val_30715 === (2))){
var state_30714__$1 = state_30714;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_30714__$1,(4),ch);
} else {
if((state_val_30715 === (11))){
var inst_30704 = (state_30714[(2)]);
var state_30714__$1 = state_30714;
var statearr_30723_30746 = state_30714__$1;
(statearr_30723_30746[(2)] = inst_30704);

(statearr_30723_30746[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30715 === (9))){
var state_30714__$1 = state_30714;
var statearr_30724_30747 = state_30714__$1;
(statearr_30724_30747[(2)] = null);

(statearr_30724_30747[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30715 === (5))){
var inst_30699 = cljs.core.async.close_BANG_.call(null,out);
var state_30714__$1 = state_30714;
var statearr_30725_30748 = state_30714__$1;
(statearr_30725_30748[(2)] = inst_30699);

(statearr_30725_30748[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30715 === (10))){
var inst_30707 = (state_30714[(2)]);
var state_30714__$1 = (function (){var statearr_30726 = state_30714;
(statearr_30726[(8)] = inst_30707);

return statearr_30726;
})();
var statearr_30727_30749 = state_30714__$1;
(statearr_30727_30749[(2)] = null);

(statearr_30727_30749[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30715 === (8))){
var inst_30696 = (state_30714[(7)]);
var state_30714__$1 = state_30714;
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_30714__$1,(11),out,inst_30696);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
});})(c__28489__auto___30739,out))
;
return ((function (switch__28377__auto__,c__28489__auto___30739,out){
return (function() {
var cljs$core$async$state_machine__28378__auto__ = null;
var cljs$core$async$state_machine__28378__auto____0 = (function (){
var statearr_30731 = [null,null,null,null,null,null,null,null,null];
(statearr_30731[(0)] = cljs$core$async$state_machine__28378__auto__);

(statearr_30731[(1)] = (1));

return statearr_30731;
});
var cljs$core$async$state_machine__28378__auto____1 = (function (state_30714){
while(true){
var ret_value__28379__auto__ = (function (){try{while(true){
var result__28380__auto__ = switch__28377__auto__.call(null,state_30714);
if(cljs.core.keyword_identical_QMARK_.call(null,result__28380__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__28380__auto__;
}
break;
}
}catch (e30732){if((e30732 instanceof Object)){
var ex__28381__auto__ = e30732;
var statearr_30733_30750 = state_30714;
(statearr_30733_30750[(5)] = ex__28381__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_30714);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e30732;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__28379__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__30751 = state_30714;
state_30714 = G__30751;
continue;
} else {
return ret_value__28379__auto__;
}
break;
}
});
cljs$core$async$state_machine__28378__auto__ = function(state_30714){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__28378__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__28378__auto____1.call(this,state_30714);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__28378__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__28378__auto____0;
cljs$core$async$state_machine__28378__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__28378__auto____1;
return cljs$core$async$state_machine__28378__auto__;
})()
;})(switch__28377__auto__,c__28489__auto___30739,out))
})();
var state__28491__auto__ = (function (){var statearr_30734 = f__28490__auto__.call(null);
(statearr_30734[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__28489__auto___30739);

return statearr_30734;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__28491__auto__);
});})(c__28489__auto___30739,out))
);


return out;
});

cljs.core.async.filter_LT_.cljs$lang$maxFixedArity = 3;
/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.remove_LT_ = (function cljs$core$async$remove_LT_(var_args){
var args30752 = [];
var len__24201__auto___30755 = arguments.length;
var i__24202__auto___30756 = (0);
while(true){
if((i__24202__auto___30756 < len__24201__auto___30755)){
args30752.push((arguments[i__24202__auto___30756]));

var G__30757 = (i__24202__auto___30756 + (1));
i__24202__auto___30756 = G__30757;
continue;
} else {
}
break;
}

var G__30754 = args30752.length;
switch (G__30754) {
case 2:
return cljs.core.async.remove_LT_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.remove_LT_.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error([cljs.core.str("Invalid arity: "),cljs.core.str(args30752.length)].join('')));

}
});

cljs.core.async.remove_LT_.cljs$core$IFn$_invoke$arity$2 = (function (p,ch){
return cljs.core.async.remove_LT_.call(null,p,ch,null);
});

cljs.core.async.remove_LT_.cljs$core$IFn$_invoke$arity$3 = (function (p,ch,buf_or_n){
return cljs.core.async.filter_LT_.call(null,cljs.core.complement.call(null,p),ch,buf_or_n);
});

cljs.core.async.remove_LT_.cljs$lang$maxFixedArity = 3;
cljs.core.async.mapcat_STAR_ = (function cljs$core$async$mapcat_STAR_(f,in$,out){
var c__28489__auto__ = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__28489__auto__){
return (function (){
var f__28490__auto__ = (function (){var switch__28377__auto__ = ((function (c__28489__auto__){
return (function (state_30924){
var state_val_30925 = (state_30924[(1)]);
if((state_val_30925 === (7))){
var inst_30920 = (state_30924[(2)]);
var state_30924__$1 = state_30924;
var statearr_30926_30967 = state_30924__$1;
(statearr_30926_30967[(2)] = inst_30920);

(statearr_30926_30967[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30925 === (20))){
var inst_30890 = (state_30924[(7)]);
var inst_30901 = (state_30924[(2)]);
var inst_30902 = cljs.core.next.call(null,inst_30890);
var inst_30876 = inst_30902;
var inst_30877 = null;
var inst_30878 = (0);
var inst_30879 = (0);
var state_30924__$1 = (function (){var statearr_30927 = state_30924;
(statearr_30927[(8)] = inst_30879);

(statearr_30927[(9)] = inst_30876);

(statearr_30927[(10)] = inst_30878);

(statearr_30927[(11)] = inst_30901);

(statearr_30927[(12)] = inst_30877);

return statearr_30927;
})();
var statearr_30928_30968 = state_30924__$1;
(statearr_30928_30968[(2)] = null);

(statearr_30928_30968[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30925 === (1))){
var state_30924__$1 = state_30924;
var statearr_30929_30969 = state_30924__$1;
(statearr_30929_30969[(2)] = null);

(statearr_30929_30969[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30925 === (4))){
var inst_30865 = (state_30924[(13)]);
var inst_30865__$1 = (state_30924[(2)]);
var inst_30866 = (inst_30865__$1 == null);
var state_30924__$1 = (function (){var statearr_30930 = state_30924;
(statearr_30930[(13)] = inst_30865__$1);

return statearr_30930;
})();
if(cljs.core.truth_(inst_30866)){
var statearr_30931_30970 = state_30924__$1;
(statearr_30931_30970[(1)] = (5));

} else {
var statearr_30932_30971 = state_30924__$1;
(statearr_30932_30971[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30925 === (15))){
var state_30924__$1 = state_30924;
var statearr_30936_30972 = state_30924__$1;
(statearr_30936_30972[(2)] = null);

(statearr_30936_30972[(1)] = (16));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30925 === (21))){
var state_30924__$1 = state_30924;
var statearr_30937_30973 = state_30924__$1;
(statearr_30937_30973[(2)] = null);

(statearr_30937_30973[(1)] = (23));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30925 === (13))){
var inst_30879 = (state_30924[(8)]);
var inst_30876 = (state_30924[(9)]);
var inst_30878 = (state_30924[(10)]);
var inst_30877 = (state_30924[(12)]);
var inst_30886 = (state_30924[(2)]);
var inst_30887 = (inst_30879 + (1));
var tmp30933 = inst_30876;
var tmp30934 = inst_30878;
var tmp30935 = inst_30877;
var inst_30876__$1 = tmp30933;
var inst_30877__$1 = tmp30935;
var inst_30878__$1 = tmp30934;
var inst_30879__$1 = inst_30887;
var state_30924__$1 = (function (){var statearr_30938 = state_30924;
(statearr_30938[(8)] = inst_30879__$1);

(statearr_30938[(9)] = inst_30876__$1);

(statearr_30938[(10)] = inst_30878__$1);

(statearr_30938[(14)] = inst_30886);

(statearr_30938[(12)] = inst_30877__$1);

return statearr_30938;
})();
var statearr_30939_30974 = state_30924__$1;
(statearr_30939_30974[(2)] = null);

(statearr_30939_30974[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30925 === (22))){
var state_30924__$1 = state_30924;
var statearr_30940_30975 = state_30924__$1;
(statearr_30940_30975[(2)] = null);

(statearr_30940_30975[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30925 === (6))){
var inst_30865 = (state_30924[(13)]);
var inst_30874 = f.call(null,inst_30865);
var inst_30875 = cljs.core.seq.call(null,inst_30874);
var inst_30876 = inst_30875;
var inst_30877 = null;
var inst_30878 = (0);
var inst_30879 = (0);
var state_30924__$1 = (function (){var statearr_30941 = state_30924;
(statearr_30941[(8)] = inst_30879);

(statearr_30941[(9)] = inst_30876);

(statearr_30941[(10)] = inst_30878);

(statearr_30941[(12)] = inst_30877);

return statearr_30941;
})();
var statearr_30942_30976 = state_30924__$1;
(statearr_30942_30976[(2)] = null);

(statearr_30942_30976[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30925 === (17))){
var inst_30890 = (state_30924[(7)]);
var inst_30894 = cljs.core.chunk_first.call(null,inst_30890);
var inst_30895 = cljs.core.chunk_rest.call(null,inst_30890);
var inst_30896 = cljs.core.count.call(null,inst_30894);
var inst_30876 = inst_30895;
var inst_30877 = inst_30894;
var inst_30878 = inst_30896;
var inst_30879 = (0);
var state_30924__$1 = (function (){var statearr_30943 = state_30924;
(statearr_30943[(8)] = inst_30879);

(statearr_30943[(9)] = inst_30876);

(statearr_30943[(10)] = inst_30878);

(statearr_30943[(12)] = inst_30877);

return statearr_30943;
})();
var statearr_30944_30977 = state_30924__$1;
(statearr_30944_30977[(2)] = null);

(statearr_30944_30977[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30925 === (3))){
var inst_30922 = (state_30924[(2)]);
var state_30924__$1 = state_30924;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_30924__$1,inst_30922);
} else {
if((state_val_30925 === (12))){
var inst_30910 = (state_30924[(2)]);
var state_30924__$1 = state_30924;
var statearr_30945_30978 = state_30924__$1;
(statearr_30945_30978[(2)] = inst_30910);

(statearr_30945_30978[(1)] = (9));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30925 === (2))){
var state_30924__$1 = state_30924;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_30924__$1,(4),in$);
} else {
if((state_val_30925 === (23))){
var inst_30918 = (state_30924[(2)]);
var state_30924__$1 = state_30924;
var statearr_30946_30979 = state_30924__$1;
(statearr_30946_30979[(2)] = inst_30918);

(statearr_30946_30979[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30925 === (19))){
var inst_30905 = (state_30924[(2)]);
var state_30924__$1 = state_30924;
var statearr_30947_30980 = state_30924__$1;
(statearr_30947_30980[(2)] = inst_30905);

(statearr_30947_30980[(1)] = (16));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30925 === (11))){
var inst_30890 = (state_30924[(7)]);
var inst_30876 = (state_30924[(9)]);
var inst_30890__$1 = cljs.core.seq.call(null,inst_30876);
var state_30924__$1 = (function (){var statearr_30948 = state_30924;
(statearr_30948[(7)] = inst_30890__$1);

return statearr_30948;
})();
if(inst_30890__$1){
var statearr_30949_30981 = state_30924__$1;
(statearr_30949_30981[(1)] = (14));

} else {
var statearr_30950_30982 = state_30924__$1;
(statearr_30950_30982[(1)] = (15));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30925 === (9))){
var inst_30912 = (state_30924[(2)]);
var inst_30913 = cljs.core.async.impl.protocols.closed_QMARK_.call(null,out);
var state_30924__$1 = (function (){var statearr_30951 = state_30924;
(statearr_30951[(15)] = inst_30912);

return statearr_30951;
})();
if(cljs.core.truth_(inst_30913)){
var statearr_30952_30983 = state_30924__$1;
(statearr_30952_30983[(1)] = (21));

} else {
var statearr_30953_30984 = state_30924__$1;
(statearr_30953_30984[(1)] = (22));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30925 === (5))){
var inst_30868 = cljs.core.async.close_BANG_.call(null,out);
var state_30924__$1 = state_30924;
var statearr_30954_30985 = state_30924__$1;
(statearr_30954_30985[(2)] = inst_30868);

(statearr_30954_30985[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30925 === (14))){
var inst_30890 = (state_30924[(7)]);
var inst_30892 = cljs.core.chunked_seq_QMARK_.call(null,inst_30890);
var state_30924__$1 = state_30924;
if(inst_30892){
var statearr_30955_30986 = state_30924__$1;
(statearr_30955_30986[(1)] = (17));

} else {
var statearr_30956_30987 = state_30924__$1;
(statearr_30956_30987[(1)] = (18));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30925 === (16))){
var inst_30908 = (state_30924[(2)]);
var state_30924__$1 = state_30924;
var statearr_30957_30988 = state_30924__$1;
(statearr_30957_30988[(2)] = inst_30908);

(statearr_30957_30988[(1)] = (12));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30925 === (10))){
var inst_30879 = (state_30924[(8)]);
var inst_30877 = (state_30924[(12)]);
var inst_30884 = cljs.core._nth.call(null,inst_30877,inst_30879);
var state_30924__$1 = state_30924;
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_30924__$1,(13),out,inst_30884);
} else {
if((state_val_30925 === (18))){
var inst_30890 = (state_30924[(7)]);
var inst_30899 = cljs.core.first.call(null,inst_30890);
var state_30924__$1 = state_30924;
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_30924__$1,(20),out,inst_30899);
} else {
if((state_val_30925 === (8))){
var inst_30879 = (state_30924[(8)]);
var inst_30878 = (state_30924[(10)]);
var inst_30881 = (inst_30879 < inst_30878);
var inst_30882 = inst_30881;
var state_30924__$1 = state_30924;
if(cljs.core.truth_(inst_30882)){
var statearr_30958_30989 = state_30924__$1;
(statearr_30958_30989[(1)] = (10));

} else {
var statearr_30959_30990 = state_30924__$1;
(statearr_30959_30990[(1)] = (11));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__28489__auto__))
;
return ((function (switch__28377__auto__,c__28489__auto__){
return (function() {
var cljs$core$async$mapcat_STAR__$_state_machine__28378__auto__ = null;
var cljs$core$async$mapcat_STAR__$_state_machine__28378__auto____0 = (function (){
var statearr_30963 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_30963[(0)] = cljs$core$async$mapcat_STAR__$_state_machine__28378__auto__);

(statearr_30963[(1)] = (1));

return statearr_30963;
});
var cljs$core$async$mapcat_STAR__$_state_machine__28378__auto____1 = (function (state_30924){
while(true){
var ret_value__28379__auto__ = (function (){try{while(true){
var result__28380__auto__ = switch__28377__auto__.call(null,state_30924);
if(cljs.core.keyword_identical_QMARK_.call(null,result__28380__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__28380__auto__;
}
break;
}
}catch (e30964){if((e30964 instanceof Object)){
var ex__28381__auto__ = e30964;
var statearr_30965_30991 = state_30924;
(statearr_30965_30991[(5)] = ex__28381__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_30924);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e30964;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__28379__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__30992 = state_30924;
state_30924 = G__30992;
continue;
} else {
return ret_value__28379__auto__;
}
break;
}
});
cljs$core$async$mapcat_STAR__$_state_machine__28378__auto__ = function(state_30924){
switch(arguments.length){
case 0:
return cljs$core$async$mapcat_STAR__$_state_machine__28378__auto____0.call(this);
case 1:
return cljs$core$async$mapcat_STAR__$_state_machine__28378__auto____1.call(this,state_30924);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$mapcat_STAR__$_state_machine__28378__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$mapcat_STAR__$_state_machine__28378__auto____0;
cljs$core$async$mapcat_STAR__$_state_machine__28378__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$mapcat_STAR__$_state_machine__28378__auto____1;
return cljs$core$async$mapcat_STAR__$_state_machine__28378__auto__;
})()
;})(switch__28377__auto__,c__28489__auto__))
})();
var state__28491__auto__ = (function (){var statearr_30966 = f__28490__auto__.call(null);
(statearr_30966[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__28489__auto__);

return statearr_30966;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__28491__auto__);
});})(c__28489__auto__))
);

return c__28489__auto__;
});
/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.mapcat_LT_ = (function cljs$core$async$mapcat_LT_(var_args){
var args30993 = [];
var len__24201__auto___30996 = arguments.length;
var i__24202__auto___30997 = (0);
while(true){
if((i__24202__auto___30997 < len__24201__auto___30996)){
args30993.push((arguments[i__24202__auto___30997]));

var G__30998 = (i__24202__auto___30997 + (1));
i__24202__auto___30997 = G__30998;
continue;
} else {
}
break;
}

var G__30995 = args30993.length;
switch (G__30995) {
case 2:
return cljs.core.async.mapcat_LT_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.mapcat_LT_.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error([cljs.core.str("Invalid arity: "),cljs.core.str(args30993.length)].join('')));

}
});

cljs.core.async.mapcat_LT_.cljs$core$IFn$_invoke$arity$2 = (function (f,in$){
return cljs.core.async.mapcat_LT_.call(null,f,in$,null);
});

cljs.core.async.mapcat_LT_.cljs$core$IFn$_invoke$arity$3 = (function (f,in$,buf_or_n){
var out = cljs.core.async.chan.call(null,buf_or_n);
cljs.core.async.mapcat_STAR_.call(null,f,in$,out);

return out;
});

cljs.core.async.mapcat_LT_.cljs$lang$maxFixedArity = 3;
/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.mapcat_GT_ = (function cljs$core$async$mapcat_GT_(var_args){
var args31000 = [];
var len__24201__auto___31003 = arguments.length;
var i__24202__auto___31004 = (0);
while(true){
if((i__24202__auto___31004 < len__24201__auto___31003)){
args31000.push((arguments[i__24202__auto___31004]));

var G__31005 = (i__24202__auto___31004 + (1));
i__24202__auto___31004 = G__31005;
continue;
} else {
}
break;
}

var G__31002 = args31000.length;
switch (G__31002) {
case 2:
return cljs.core.async.mapcat_GT_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.mapcat_GT_.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error([cljs.core.str("Invalid arity: "),cljs.core.str(args31000.length)].join('')));

}
});

cljs.core.async.mapcat_GT_.cljs$core$IFn$_invoke$arity$2 = (function (f,out){
return cljs.core.async.mapcat_GT_.call(null,f,out,null);
});

cljs.core.async.mapcat_GT_.cljs$core$IFn$_invoke$arity$3 = (function (f,out,buf_or_n){
var in$ = cljs.core.async.chan.call(null,buf_or_n);
cljs.core.async.mapcat_STAR_.call(null,f,in$,out);

return in$;
});

cljs.core.async.mapcat_GT_.cljs$lang$maxFixedArity = 3;
/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.unique = (function cljs$core$async$unique(var_args){
var args31007 = [];
var len__24201__auto___31058 = arguments.length;
var i__24202__auto___31059 = (0);
while(true){
if((i__24202__auto___31059 < len__24201__auto___31058)){
args31007.push((arguments[i__24202__auto___31059]));

var G__31060 = (i__24202__auto___31059 + (1));
i__24202__auto___31059 = G__31060;
continue;
} else {
}
break;
}

var G__31009 = args31007.length;
switch (G__31009) {
case 1:
return cljs.core.async.unique.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return cljs.core.async.unique.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error([cljs.core.str("Invalid arity: "),cljs.core.str(args31007.length)].join('')));

}
});

cljs.core.async.unique.cljs$core$IFn$_invoke$arity$1 = (function (ch){
return cljs.core.async.unique.call(null,ch,null);
});

cljs.core.async.unique.cljs$core$IFn$_invoke$arity$2 = (function (ch,buf_or_n){
var out = cljs.core.async.chan.call(null,buf_or_n);
var c__28489__auto___31062 = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__28489__auto___31062,out){
return (function (){
var f__28490__auto__ = (function (){var switch__28377__auto__ = ((function (c__28489__auto___31062,out){
return (function (state_31033){
var state_val_31034 = (state_31033[(1)]);
if((state_val_31034 === (7))){
var inst_31028 = (state_31033[(2)]);
var state_31033__$1 = state_31033;
var statearr_31035_31063 = state_31033__$1;
(statearr_31035_31063[(2)] = inst_31028);

(statearr_31035_31063[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31034 === (1))){
var inst_31010 = null;
var state_31033__$1 = (function (){var statearr_31036 = state_31033;
(statearr_31036[(7)] = inst_31010);

return statearr_31036;
})();
var statearr_31037_31064 = state_31033__$1;
(statearr_31037_31064[(2)] = null);

(statearr_31037_31064[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31034 === (4))){
var inst_31013 = (state_31033[(8)]);
var inst_31013__$1 = (state_31033[(2)]);
var inst_31014 = (inst_31013__$1 == null);
var inst_31015 = cljs.core.not.call(null,inst_31014);
var state_31033__$1 = (function (){var statearr_31038 = state_31033;
(statearr_31038[(8)] = inst_31013__$1);

return statearr_31038;
})();
if(inst_31015){
var statearr_31039_31065 = state_31033__$1;
(statearr_31039_31065[(1)] = (5));

} else {
var statearr_31040_31066 = state_31033__$1;
(statearr_31040_31066[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31034 === (6))){
var state_31033__$1 = state_31033;
var statearr_31041_31067 = state_31033__$1;
(statearr_31041_31067[(2)] = null);

(statearr_31041_31067[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31034 === (3))){
var inst_31030 = (state_31033[(2)]);
var inst_31031 = cljs.core.async.close_BANG_.call(null,out);
var state_31033__$1 = (function (){var statearr_31042 = state_31033;
(statearr_31042[(9)] = inst_31030);

return statearr_31042;
})();
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_31033__$1,inst_31031);
} else {
if((state_val_31034 === (2))){
var state_31033__$1 = state_31033;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_31033__$1,(4),ch);
} else {
if((state_val_31034 === (11))){
var inst_31013 = (state_31033[(8)]);
var inst_31022 = (state_31033[(2)]);
var inst_31010 = inst_31013;
var state_31033__$1 = (function (){var statearr_31043 = state_31033;
(statearr_31043[(7)] = inst_31010);

(statearr_31043[(10)] = inst_31022);

return statearr_31043;
})();
var statearr_31044_31068 = state_31033__$1;
(statearr_31044_31068[(2)] = null);

(statearr_31044_31068[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31034 === (9))){
var inst_31013 = (state_31033[(8)]);
var state_31033__$1 = state_31033;
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_31033__$1,(11),out,inst_31013);
} else {
if((state_val_31034 === (5))){
var inst_31010 = (state_31033[(7)]);
var inst_31013 = (state_31033[(8)]);
var inst_31017 = cljs.core._EQ_.call(null,inst_31013,inst_31010);
var state_31033__$1 = state_31033;
if(inst_31017){
var statearr_31046_31069 = state_31033__$1;
(statearr_31046_31069[(1)] = (8));

} else {
var statearr_31047_31070 = state_31033__$1;
(statearr_31047_31070[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31034 === (10))){
var inst_31025 = (state_31033[(2)]);
var state_31033__$1 = state_31033;
var statearr_31048_31071 = state_31033__$1;
(statearr_31048_31071[(2)] = inst_31025);

(statearr_31048_31071[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31034 === (8))){
var inst_31010 = (state_31033[(7)]);
var tmp31045 = inst_31010;
var inst_31010__$1 = tmp31045;
var state_31033__$1 = (function (){var statearr_31049 = state_31033;
(statearr_31049[(7)] = inst_31010__$1);

return statearr_31049;
})();
var statearr_31050_31072 = state_31033__$1;
(statearr_31050_31072[(2)] = null);

(statearr_31050_31072[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
});})(c__28489__auto___31062,out))
;
return ((function (switch__28377__auto__,c__28489__auto___31062,out){
return (function() {
var cljs$core$async$state_machine__28378__auto__ = null;
var cljs$core$async$state_machine__28378__auto____0 = (function (){
var statearr_31054 = [null,null,null,null,null,null,null,null,null,null,null];
(statearr_31054[(0)] = cljs$core$async$state_machine__28378__auto__);

(statearr_31054[(1)] = (1));

return statearr_31054;
});
var cljs$core$async$state_machine__28378__auto____1 = (function (state_31033){
while(true){
var ret_value__28379__auto__ = (function (){try{while(true){
var result__28380__auto__ = switch__28377__auto__.call(null,state_31033);
if(cljs.core.keyword_identical_QMARK_.call(null,result__28380__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__28380__auto__;
}
break;
}
}catch (e31055){if((e31055 instanceof Object)){
var ex__28381__auto__ = e31055;
var statearr_31056_31073 = state_31033;
(statearr_31056_31073[(5)] = ex__28381__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_31033);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e31055;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__28379__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__31074 = state_31033;
state_31033 = G__31074;
continue;
} else {
return ret_value__28379__auto__;
}
break;
}
});
cljs$core$async$state_machine__28378__auto__ = function(state_31033){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__28378__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__28378__auto____1.call(this,state_31033);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__28378__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__28378__auto____0;
cljs$core$async$state_machine__28378__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__28378__auto____1;
return cljs$core$async$state_machine__28378__auto__;
})()
;})(switch__28377__auto__,c__28489__auto___31062,out))
})();
var state__28491__auto__ = (function (){var statearr_31057 = f__28490__auto__.call(null);
(statearr_31057[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__28489__auto___31062);

return statearr_31057;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__28491__auto__);
});})(c__28489__auto___31062,out))
);


return out;
});

cljs.core.async.unique.cljs$lang$maxFixedArity = 2;
/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.partition = (function cljs$core$async$partition(var_args){
var args31075 = [];
var len__24201__auto___31145 = arguments.length;
var i__24202__auto___31146 = (0);
while(true){
if((i__24202__auto___31146 < len__24201__auto___31145)){
args31075.push((arguments[i__24202__auto___31146]));

var G__31147 = (i__24202__auto___31146 + (1));
i__24202__auto___31146 = G__31147;
continue;
} else {
}
break;
}

var G__31077 = args31075.length;
switch (G__31077) {
case 2:
return cljs.core.async.partition.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.partition.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error([cljs.core.str("Invalid arity: "),cljs.core.str(args31075.length)].join('')));

}
});

cljs.core.async.partition.cljs$core$IFn$_invoke$arity$2 = (function (n,ch){
return cljs.core.async.partition.call(null,n,ch,null);
});

cljs.core.async.partition.cljs$core$IFn$_invoke$arity$3 = (function (n,ch,buf_or_n){
var out = cljs.core.async.chan.call(null,buf_or_n);
var c__28489__auto___31149 = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__28489__auto___31149,out){
return (function (){
var f__28490__auto__ = (function (){var switch__28377__auto__ = ((function (c__28489__auto___31149,out){
return (function (state_31115){
var state_val_31116 = (state_31115[(1)]);
if((state_val_31116 === (7))){
var inst_31111 = (state_31115[(2)]);
var state_31115__$1 = state_31115;
var statearr_31117_31150 = state_31115__$1;
(statearr_31117_31150[(2)] = inst_31111);

(statearr_31117_31150[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31116 === (1))){
var inst_31078 = (new Array(n));
var inst_31079 = inst_31078;
var inst_31080 = (0);
var state_31115__$1 = (function (){var statearr_31118 = state_31115;
(statearr_31118[(7)] = inst_31079);

(statearr_31118[(8)] = inst_31080);

return statearr_31118;
})();
var statearr_31119_31151 = state_31115__$1;
(statearr_31119_31151[(2)] = null);

(statearr_31119_31151[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31116 === (4))){
var inst_31083 = (state_31115[(9)]);
var inst_31083__$1 = (state_31115[(2)]);
var inst_31084 = (inst_31083__$1 == null);
var inst_31085 = cljs.core.not.call(null,inst_31084);
var state_31115__$1 = (function (){var statearr_31120 = state_31115;
(statearr_31120[(9)] = inst_31083__$1);

return statearr_31120;
})();
if(inst_31085){
var statearr_31121_31152 = state_31115__$1;
(statearr_31121_31152[(1)] = (5));

} else {
var statearr_31122_31153 = state_31115__$1;
(statearr_31122_31153[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31116 === (15))){
var inst_31105 = (state_31115[(2)]);
var state_31115__$1 = state_31115;
var statearr_31123_31154 = state_31115__$1;
(statearr_31123_31154[(2)] = inst_31105);

(statearr_31123_31154[(1)] = (14));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31116 === (13))){
var state_31115__$1 = state_31115;
var statearr_31124_31155 = state_31115__$1;
(statearr_31124_31155[(2)] = null);

(statearr_31124_31155[(1)] = (14));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31116 === (6))){
var inst_31080 = (state_31115[(8)]);
var inst_31101 = (inst_31080 > (0));
var state_31115__$1 = state_31115;
if(cljs.core.truth_(inst_31101)){
var statearr_31125_31156 = state_31115__$1;
(statearr_31125_31156[(1)] = (12));

} else {
var statearr_31126_31157 = state_31115__$1;
(statearr_31126_31157[(1)] = (13));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31116 === (3))){
var inst_31113 = (state_31115[(2)]);
var state_31115__$1 = state_31115;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_31115__$1,inst_31113);
} else {
if((state_val_31116 === (12))){
var inst_31079 = (state_31115[(7)]);
var inst_31103 = cljs.core.vec.call(null,inst_31079);
var state_31115__$1 = state_31115;
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_31115__$1,(15),out,inst_31103);
} else {
if((state_val_31116 === (2))){
var state_31115__$1 = state_31115;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_31115__$1,(4),ch);
} else {
if((state_val_31116 === (11))){
var inst_31095 = (state_31115[(2)]);
var inst_31096 = (new Array(n));
var inst_31079 = inst_31096;
var inst_31080 = (0);
var state_31115__$1 = (function (){var statearr_31127 = state_31115;
(statearr_31127[(7)] = inst_31079);

(statearr_31127[(10)] = inst_31095);

(statearr_31127[(8)] = inst_31080);

return statearr_31127;
})();
var statearr_31128_31158 = state_31115__$1;
(statearr_31128_31158[(2)] = null);

(statearr_31128_31158[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31116 === (9))){
var inst_31079 = (state_31115[(7)]);
var inst_31093 = cljs.core.vec.call(null,inst_31079);
var state_31115__$1 = state_31115;
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_31115__$1,(11),out,inst_31093);
} else {
if((state_val_31116 === (5))){
var inst_31083 = (state_31115[(9)]);
var inst_31079 = (state_31115[(7)]);
var inst_31088 = (state_31115[(11)]);
var inst_31080 = (state_31115[(8)]);
var inst_31087 = (inst_31079[inst_31080] = inst_31083);
var inst_31088__$1 = (inst_31080 + (1));
var inst_31089 = (inst_31088__$1 < n);
var state_31115__$1 = (function (){var statearr_31129 = state_31115;
(statearr_31129[(12)] = inst_31087);

(statearr_31129[(11)] = inst_31088__$1);

return statearr_31129;
})();
if(cljs.core.truth_(inst_31089)){
var statearr_31130_31159 = state_31115__$1;
(statearr_31130_31159[(1)] = (8));

} else {
var statearr_31131_31160 = state_31115__$1;
(statearr_31131_31160[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31116 === (14))){
var inst_31108 = (state_31115[(2)]);
var inst_31109 = cljs.core.async.close_BANG_.call(null,out);
var state_31115__$1 = (function (){var statearr_31133 = state_31115;
(statearr_31133[(13)] = inst_31108);

return statearr_31133;
})();
var statearr_31134_31161 = state_31115__$1;
(statearr_31134_31161[(2)] = inst_31109);

(statearr_31134_31161[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31116 === (10))){
var inst_31099 = (state_31115[(2)]);
var state_31115__$1 = state_31115;
var statearr_31135_31162 = state_31115__$1;
(statearr_31135_31162[(2)] = inst_31099);

(statearr_31135_31162[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31116 === (8))){
var inst_31079 = (state_31115[(7)]);
var inst_31088 = (state_31115[(11)]);
var tmp31132 = inst_31079;
var inst_31079__$1 = tmp31132;
var inst_31080 = inst_31088;
var state_31115__$1 = (function (){var statearr_31136 = state_31115;
(statearr_31136[(7)] = inst_31079__$1);

(statearr_31136[(8)] = inst_31080);

return statearr_31136;
})();
var statearr_31137_31163 = state_31115__$1;
(statearr_31137_31163[(2)] = null);

(statearr_31137_31163[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__28489__auto___31149,out))
;
return ((function (switch__28377__auto__,c__28489__auto___31149,out){
return (function() {
var cljs$core$async$state_machine__28378__auto__ = null;
var cljs$core$async$state_machine__28378__auto____0 = (function (){
var statearr_31141 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_31141[(0)] = cljs$core$async$state_machine__28378__auto__);

(statearr_31141[(1)] = (1));

return statearr_31141;
});
var cljs$core$async$state_machine__28378__auto____1 = (function (state_31115){
while(true){
var ret_value__28379__auto__ = (function (){try{while(true){
var result__28380__auto__ = switch__28377__auto__.call(null,state_31115);
if(cljs.core.keyword_identical_QMARK_.call(null,result__28380__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__28380__auto__;
}
break;
}
}catch (e31142){if((e31142 instanceof Object)){
var ex__28381__auto__ = e31142;
var statearr_31143_31164 = state_31115;
(statearr_31143_31164[(5)] = ex__28381__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_31115);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e31142;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__28379__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__31165 = state_31115;
state_31115 = G__31165;
continue;
} else {
return ret_value__28379__auto__;
}
break;
}
});
cljs$core$async$state_machine__28378__auto__ = function(state_31115){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__28378__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__28378__auto____1.call(this,state_31115);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__28378__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__28378__auto____0;
cljs$core$async$state_machine__28378__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__28378__auto____1;
return cljs$core$async$state_machine__28378__auto__;
})()
;})(switch__28377__auto__,c__28489__auto___31149,out))
})();
var state__28491__auto__ = (function (){var statearr_31144 = f__28490__auto__.call(null);
(statearr_31144[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__28489__auto___31149);

return statearr_31144;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__28491__auto__);
});})(c__28489__auto___31149,out))
);


return out;
});

cljs.core.async.partition.cljs$lang$maxFixedArity = 3;
/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.partition_by = (function cljs$core$async$partition_by(var_args){
var args31166 = [];
var len__24201__auto___31240 = arguments.length;
var i__24202__auto___31241 = (0);
while(true){
if((i__24202__auto___31241 < len__24201__auto___31240)){
args31166.push((arguments[i__24202__auto___31241]));

var G__31242 = (i__24202__auto___31241 + (1));
i__24202__auto___31241 = G__31242;
continue;
} else {
}
break;
}

var G__31168 = args31166.length;
switch (G__31168) {
case 2:
return cljs.core.async.partition_by.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.partition_by.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error([cljs.core.str("Invalid arity: "),cljs.core.str(args31166.length)].join('')));

}
});

cljs.core.async.partition_by.cljs$core$IFn$_invoke$arity$2 = (function (f,ch){
return cljs.core.async.partition_by.call(null,f,ch,null);
});

cljs.core.async.partition_by.cljs$core$IFn$_invoke$arity$3 = (function (f,ch,buf_or_n){
var out = cljs.core.async.chan.call(null,buf_or_n);
var c__28489__auto___31244 = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__28489__auto___31244,out){
return (function (){
var f__28490__auto__ = (function (){var switch__28377__auto__ = ((function (c__28489__auto___31244,out){
return (function (state_31210){
var state_val_31211 = (state_31210[(1)]);
if((state_val_31211 === (7))){
var inst_31206 = (state_31210[(2)]);
var state_31210__$1 = state_31210;
var statearr_31212_31245 = state_31210__$1;
(statearr_31212_31245[(2)] = inst_31206);

(statearr_31212_31245[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31211 === (1))){
var inst_31169 = [];
var inst_31170 = inst_31169;
var inst_31171 = new cljs.core.Keyword("cljs.core.async","nothing","cljs.core.async/nothing",-69252123);
var state_31210__$1 = (function (){var statearr_31213 = state_31210;
(statearr_31213[(7)] = inst_31171);

(statearr_31213[(8)] = inst_31170);

return statearr_31213;
})();
var statearr_31214_31246 = state_31210__$1;
(statearr_31214_31246[(2)] = null);

(statearr_31214_31246[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31211 === (4))){
var inst_31174 = (state_31210[(9)]);
var inst_31174__$1 = (state_31210[(2)]);
var inst_31175 = (inst_31174__$1 == null);
var inst_31176 = cljs.core.not.call(null,inst_31175);
var state_31210__$1 = (function (){var statearr_31215 = state_31210;
(statearr_31215[(9)] = inst_31174__$1);

return statearr_31215;
})();
if(inst_31176){
var statearr_31216_31247 = state_31210__$1;
(statearr_31216_31247[(1)] = (5));

} else {
var statearr_31217_31248 = state_31210__$1;
(statearr_31217_31248[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31211 === (15))){
var inst_31200 = (state_31210[(2)]);
var state_31210__$1 = state_31210;
var statearr_31218_31249 = state_31210__$1;
(statearr_31218_31249[(2)] = inst_31200);

(statearr_31218_31249[(1)] = (14));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31211 === (13))){
var state_31210__$1 = state_31210;
var statearr_31219_31250 = state_31210__$1;
(statearr_31219_31250[(2)] = null);

(statearr_31219_31250[(1)] = (14));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31211 === (6))){
var inst_31170 = (state_31210[(8)]);
var inst_31195 = inst_31170.length;
var inst_31196 = (inst_31195 > (0));
var state_31210__$1 = state_31210;
if(cljs.core.truth_(inst_31196)){
var statearr_31220_31251 = state_31210__$1;
(statearr_31220_31251[(1)] = (12));

} else {
var statearr_31221_31252 = state_31210__$1;
(statearr_31221_31252[(1)] = (13));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31211 === (3))){
var inst_31208 = (state_31210[(2)]);
var state_31210__$1 = state_31210;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_31210__$1,inst_31208);
} else {
if((state_val_31211 === (12))){
var inst_31170 = (state_31210[(8)]);
var inst_31198 = cljs.core.vec.call(null,inst_31170);
var state_31210__$1 = state_31210;
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_31210__$1,(15),out,inst_31198);
} else {
if((state_val_31211 === (2))){
var state_31210__$1 = state_31210;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_31210__$1,(4),ch);
} else {
if((state_val_31211 === (11))){
var inst_31174 = (state_31210[(9)]);
var inst_31178 = (state_31210[(10)]);
var inst_31188 = (state_31210[(2)]);
var inst_31189 = [];
var inst_31190 = inst_31189.push(inst_31174);
var inst_31170 = inst_31189;
var inst_31171 = inst_31178;
var state_31210__$1 = (function (){var statearr_31222 = state_31210;
(statearr_31222[(7)] = inst_31171);

(statearr_31222[(11)] = inst_31188);

(statearr_31222[(12)] = inst_31190);

(statearr_31222[(8)] = inst_31170);

return statearr_31222;
})();
var statearr_31223_31253 = state_31210__$1;
(statearr_31223_31253[(2)] = null);

(statearr_31223_31253[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31211 === (9))){
var inst_31170 = (state_31210[(8)]);
var inst_31186 = cljs.core.vec.call(null,inst_31170);
var state_31210__$1 = state_31210;
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_31210__$1,(11),out,inst_31186);
} else {
if((state_val_31211 === (5))){
var inst_31171 = (state_31210[(7)]);
var inst_31174 = (state_31210[(9)]);
var inst_31178 = (state_31210[(10)]);
var inst_31178__$1 = f.call(null,inst_31174);
var inst_31179 = cljs.core._EQ_.call(null,inst_31178__$1,inst_31171);
var inst_31180 = cljs.core.keyword_identical_QMARK_.call(null,inst_31171,new cljs.core.Keyword("cljs.core.async","nothing","cljs.core.async/nothing",-69252123));
var inst_31181 = (inst_31179) || (inst_31180);
var state_31210__$1 = (function (){var statearr_31224 = state_31210;
(statearr_31224[(10)] = inst_31178__$1);

return statearr_31224;
})();
if(cljs.core.truth_(inst_31181)){
var statearr_31225_31254 = state_31210__$1;
(statearr_31225_31254[(1)] = (8));

} else {
var statearr_31226_31255 = state_31210__$1;
(statearr_31226_31255[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31211 === (14))){
var inst_31203 = (state_31210[(2)]);
var inst_31204 = cljs.core.async.close_BANG_.call(null,out);
var state_31210__$1 = (function (){var statearr_31228 = state_31210;
(statearr_31228[(13)] = inst_31203);

return statearr_31228;
})();
var statearr_31229_31256 = state_31210__$1;
(statearr_31229_31256[(2)] = inst_31204);

(statearr_31229_31256[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31211 === (10))){
var inst_31193 = (state_31210[(2)]);
var state_31210__$1 = state_31210;
var statearr_31230_31257 = state_31210__$1;
(statearr_31230_31257[(2)] = inst_31193);

(statearr_31230_31257[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31211 === (8))){
var inst_31174 = (state_31210[(9)]);
var inst_31170 = (state_31210[(8)]);
var inst_31178 = (state_31210[(10)]);
var inst_31183 = inst_31170.push(inst_31174);
var tmp31227 = inst_31170;
var inst_31170__$1 = tmp31227;
var inst_31171 = inst_31178;
var state_31210__$1 = (function (){var statearr_31231 = state_31210;
(statearr_31231[(14)] = inst_31183);

(statearr_31231[(7)] = inst_31171);

(statearr_31231[(8)] = inst_31170__$1);

return statearr_31231;
})();
var statearr_31232_31258 = state_31210__$1;
(statearr_31232_31258[(2)] = null);

(statearr_31232_31258[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__28489__auto___31244,out))
;
return ((function (switch__28377__auto__,c__28489__auto___31244,out){
return (function() {
var cljs$core$async$state_machine__28378__auto__ = null;
var cljs$core$async$state_machine__28378__auto____0 = (function (){
var statearr_31236 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_31236[(0)] = cljs$core$async$state_machine__28378__auto__);

(statearr_31236[(1)] = (1));

return statearr_31236;
});
var cljs$core$async$state_machine__28378__auto____1 = (function (state_31210){
while(true){
var ret_value__28379__auto__ = (function (){try{while(true){
var result__28380__auto__ = switch__28377__auto__.call(null,state_31210);
if(cljs.core.keyword_identical_QMARK_.call(null,result__28380__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__28380__auto__;
}
break;
}
}catch (e31237){if((e31237 instanceof Object)){
var ex__28381__auto__ = e31237;
var statearr_31238_31259 = state_31210;
(statearr_31238_31259[(5)] = ex__28381__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_31210);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e31237;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__28379__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__31260 = state_31210;
state_31210 = G__31260;
continue;
} else {
return ret_value__28379__auto__;
}
break;
}
});
cljs$core$async$state_machine__28378__auto__ = function(state_31210){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__28378__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__28378__auto____1.call(this,state_31210);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__28378__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__28378__auto____0;
cljs$core$async$state_machine__28378__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__28378__auto____1;
return cljs$core$async$state_machine__28378__auto__;
})()
;})(switch__28377__auto__,c__28489__auto___31244,out))
})();
var state__28491__auto__ = (function (){var statearr_31239 = f__28490__auto__.call(null);
(statearr_31239[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__28489__auto___31244);

return statearr_31239;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__28491__auto__);
});})(c__28489__auto___31244,out))
);


return out;
});

cljs.core.async.partition_by.cljs$lang$maxFixedArity = 3;

//# sourceMappingURL=async.js.map