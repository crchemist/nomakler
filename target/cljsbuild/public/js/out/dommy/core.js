// Compiled by ClojureScript 1.7.170 {}
goog.provide('dommy.core');
goog.require('cljs.core');
goog.require('clojure.string');
goog.require('dommy.utils');
/**
 * Returns a selector in string format.
 * Accepts string, keyword, or collection.
 */
dommy.core.selector = (function dommy$core$selector(data){
if(cljs.core.coll_QMARK_.call(null,data)){
return clojure.string.join.call(null," ",cljs.core.map.call(null,dommy$core$selector,data));
} else {
if((typeof data === 'string') || ((data instanceof cljs.core.Keyword))){
return cljs.core.name.call(null,data);
} else {
return null;
}
}
});
dommy.core.text = (function dommy$core$text(elem){
var or__23143__auto__ = elem.textContent;
if(cljs.core.truth_(or__23143__auto__)){
return or__23143__auto__;
} else {
return elem.innerText;
}
});
dommy.core.html = (function dommy$core$html(elem){
return elem.innerHTML;
});
dommy.core.value = (function dommy$core$value(elem){
return elem.value;
});
dommy.core.class$ = (function dommy$core$class(elem){
return elem.className;
});
dommy.core.attr = (function dommy$core$attr(elem,k){
if(cljs.core.truth_(k)){
return elem.getAttribute(dommy.utils.as_str.call(null,k));
} else {
return null;
}
});
/**
 * The computed style of `elem`, optionally specifying the key of
 * a particular style to return
 */
dommy.core.style = (function dommy$core$style(var_args){
var args26117 = [];
var len__24201__auto___26120 = arguments.length;
var i__24202__auto___26121 = (0);
while(true){
if((i__24202__auto___26121 < len__24201__auto___26120)){
args26117.push((arguments[i__24202__auto___26121]));

var G__26122 = (i__24202__auto___26121 + (1));
i__24202__auto___26121 = G__26122;
continue;
} else {
}
break;
}

var G__26119 = args26117.length;
switch (G__26119) {
case 1:
return dommy.core.style.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return dommy.core.style.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error([cljs.core.str("Invalid arity: "),cljs.core.str(args26117.length)].join('')));

}
});

dommy.core.style.cljs$core$IFn$_invoke$arity$1 = (function (elem){
return cljs.core.js__GT_clj.call(null,window.getComputedStyle(elem));
});

dommy.core.style.cljs$core$IFn$_invoke$arity$2 = (function (elem,k){
return (window.getComputedStyle(elem)[dommy.utils.as_str.call(null,k)]);
});

dommy.core.style.cljs$lang$maxFixedArity = 2;
dommy.core.px = (function dommy$core$px(elem,k){

var pixels = dommy.core.style.call(null,elem,k);
if(cljs.core.seq.call(null,pixels)){
return parseInt(pixels);
} else {
return null;
}
});
/**
 * Does `elem` contain `c` in its class list
 */
dommy.core.has_class_QMARK_ = (function dommy$core$has_class_QMARK_(elem,c){
var c__$1 = dommy.utils.as_str.call(null,c);
var temp__4655__auto__ = elem.classList;
if(cljs.core.truth_(temp__4655__auto__)){
var class_list = temp__4655__auto__;
return class_list.contains(c__$1);
} else {
var temp__4657__auto__ = dommy.core.class$.call(null,elem);
if(cljs.core.truth_(temp__4657__auto__)){
var class_name = temp__4657__auto__;
var temp__4657__auto____$1 = dommy.utils.class_index.call(null,class_name,c__$1);
if(cljs.core.truth_(temp__4657__auto____$1)){
var i = temp__4657__auto____$1;
return (i >= (0));
} else {
return null;
}
} else {
return null;
}
}
});
/**
 * Is `elem` hidden (as associated with hide!/show!/toggle!, using display: none)
 */
dommy.core.hidden_QMARK_ = (function dommy$core$hidden_QMARK_(elem){
return (dommy.core.style.call(null,elem,new cljs.core.Keyword(null,"display","display",242065432)) === "none");
});
/**
 * Returns a map of the bounding client rect of `elem`
 * as a map with [:top :left :right :bottom :width :height]
 */
dommy.core.bounding_client_rect = (function dommy$core$bounding_client_rect(elem){
var r = elem.getBoundingClientRect();
return new cljs.core.PersistentArrayMap(null, 6, [new cljs.core.Keyword(null,"top","top",-1856271961),r.top,new cljs.core.Keyword(null,"bottom","bottom",-1550509018),r.bottom,new cljs.core.Keyword(null,"left","left",-399115937),r.left,new cljs.core.Keyword(null,"right","right",-452581833),r.right,new cljs.core.Keyword(null,"width","width",-384071477),r.width,new cljs.core.Keyword(null,"height","height",1025178622),r.height], null);
});
dommy.core.parent = (function dommy$core$parent(elem){
return elem.parentNode;
});
dommy.core.children = (function dommy$core$children(elem){
return elem.children;
});
/**
 * Lazy seq of the ancestors of `elem`
 */
dommy.core.ancestors = (function dommy$core$ancestors(elem){
return cljs.core.take_while.call(null,cljs.core.identity,cljs.core.iterate.call(null,dommy.core.parent,elem));
});
dommy.core.ancestor_nodes = dommy.core.ancestors;
/**
 * Returns a predicate on nodes that match `selector` at the
 * time of this `matches-pred` call (may return outdated results
 * if you fuck with the DOM)
 */
dommy.core.matches_pred = (function dommy$core$matches_pred(var_args){
var args26124 = [];
var len__24201__auto___26127 = arguments.length;
var i__24202__auto___26128 = (0);
while(true){
if((i__24202__auto___26128 < len__24201__auto___26127)){
args26124.push((arguments[i__24202__auto___26128]));

var G__26129 = (i__24202__auto___26128 + (1));
i__24202__auto___26128 = G__26129;
continue;
} else {
}
break;
}

var G__26126 = args26124.length;
switch (G__26126) {
case 2:
return dommy.core.matches_pred.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 1:
return dommy.core.matches_pred.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
default:
throw (new Error([cljs.core.str("Invalid arity: "),cljs.core.str(args26124.length)].join('')));

}
});

dommy.core.matches_pred.cljs$core$IFn$_invoke$arity$2 = (function (base,selector){
var matches = dommy.utils.__GT_Array.call(null,base.querySelectorAll(dommy.core.selector.call(null,selector)));
return ((function (matches){
return (function (elem){
return (matches.indexOf(elem) >= (0));
});
;})(matches))
});

dommy.core.matches_pred.cljs$core$IFn$_invoke$arity$1 = (function (selector){
return dommy.core.matches_pred.call(null,document,selector);
});

dommy.core.matches_pred.cljs$lang$maxFixedArity = 2;
/**
 * Closest ancestor of `elem` (up to `base`, if provided)
 * that matches `selector`
 */
dommy.core.closest = (function dommy$core$closest(var_args){
var args26132 = [];
var len__24201__auto___26135 = arguments.length;
var i__24202__auto___26136 = (0);
while(true){
if((i__24202__auto___26136 < len__24201__auto___26135)){
args26132.push((arguments[i__24202__auto___26136]));

var G__26137 = (i__24202__auto___26136 + (1));
i__24202__auto___26136 = G__26137;
continue;
} else {
}
break;
}

var G__26134 = args26132.length;
switch (G__26134) {
case 3:
return dommy.core.closest.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
case 2:
return dommy.core.closest.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error([cljs.core.str("Invalid arity: "),cljs.core.str(args26132.length)].join('')));

}
});

dommy.core.closest.cljs$core$IFn$_invoke$arity$3 = (function (base,elem,selector){
return cljs.core.first.call(null,cljs.core.filter.call(null,dommy.core.matches_pred.call(null,base,selector),cljs.core.take_while.call(null,(function (p1__26131_SHARP_){
return !((p1__26131_SHARP_ === base));
}),dommy.core.ancestors.call(null,elem))));
});

dommy.core.closest.cljs$core$IFn$_invoke$arity$2 = (function (elem,selector){
return dommy.core.closest.call(null,document.body,elem,selector);
});

dommy.core.closest.cljs$lang$maxFixedArity = 3;
/**
 * Is `descendant` a descendant of `ancestor`?
 * (http://goo.gl/T8pgCX)
 */
dommy.core.descendant_QMARK_ = (function dommy$core$descendant_QMARK_(descendant,ancestor){
if(cljs.core.truth_(ancestor.contains)){
return ancestor.contains(descendant);
} else {
if(cljs.core.truth_(ancestor.compareDocumentPosition)){
return ((ancestor.compareDocumentPosition(descendant) & (1 << (4))) != 0);
} else {
return null;
}
}
});
/**
 * Set the textContent of `elem` to `text`, fall back to innerText
 */
dommy.core.set_text_BANG_ = (function dommy$core$set_text_BANG_(elem,text){
if(!((void 0 === elem.textContent))){
elem.textContent = text;
} else {
elem.innerText = text;
}

return elem;
});
/**
 * Set the innerHTML of `elem` to `html`
 */
dommy.core.set_html_BANG_ = (function dommy$core$set_html_BANG_(elem,html){
elem.innerHTML = html;

return elem;
});
/**
 * Set the value of `elem` to `value`
 */
dommy.core.set_value_BANG_ = (function dommy$core$set_value_BANG_(elem,value){
elem.value = value;

return elem;
});
/**
 * Set the css class of `elem` to `elem`
 */
dommy.core.set_class_BANG_ = (function dommy$core$set_class_BANG_(elem,c){
return elem.className = c;
});
/**
 * Set the style of `elem` using key-value pairs:
 * 
 *    (set-style! elem :display "block" :color "red")
 */
dommy.core.set_style_BANG_ = (function dommy$core$set_style_BANG_(var_args){
var args__24208__auto__ = [];
var len__24201__auto___26147 = arguments.length;
var i__24202__auto___26148 = (0);
while(true){
if((i__24202__auto___26148 < len__24201__auto___26147)){
args__24208__auto__.push((arguments[i__24202__auto___26148]));

var G__26149 = (i__24202__auto___26148 + (1));
i__24202__auto___26148 = G__26149;
continue;
} else {
}
break;
}

var argseq__24209__auto__ = ((((1) < args__24208__auto__.length))?(new cljs.core.IndexedSeq(args__24208__auto__.slice((1)),(0))):null);
return dommy.core.set_style_BANG_.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),argseq__24209__auto__);
});

dommy.core.set_style_BANG_.cljs$core$IFn$_invoke$arity$variadic = (function (elem,kvs){
if(cljs.core.even_QMARK_.call(null,cljs.core.count.call(null,kvs))){
} else {
throw (new Error([cljs.core.str("Assert failed: "),cljs.core.str(cljs.core.pr_str.call(null,cljs.core.list(new cljs.core.Symbol(null,"even?","even?",-1827825394,null),cljs.core.list(new cljs.core.Symbol(null,"count","count",-514511684,null),new cljs.core.Symbol(null,"kvs","kvs",-1695980277,null)))))].join('')));
}

var style = elem.style;
var seq__26141_26150 = cljs.core.seq.call(null,cljs.core.partition.call(null,(2),kvs));
var chunk__26142_26151 = null;
var count__26143_26152 = (0);
var i__26144_26153 = (0);
while(true){
if((i__26144_26153 < count__26143_26152)){
var vec__26145_26154 = cljs.core._nth.call(null,chunk__26142_26151,i__26144_26153);
var k_26155 = cljs.core.nth.call(null,vec__26145_26154,(0),null);
var v_26156 = cljs.core.nth.call(null,vec__26145_26154,(1),null);
style.setProperty(dommy.utils.as_str.call(null,k_26155),v_26156);

var G__26157 = seq__26141_26150;
var G__26158 = chunk__26142_26151;
var G__26159 = count__26143_26152;
var G__26160 = (i__26144_26153 + (1));
seq__26141_26150 = G__26157;
chunk__26142_26151 = G__26158;
count__26143_26152 = G__26159;
i__26144_26153 = G__26160;
continue;
} else {
var temp__4657__auto___26161 = cljs.core.seq.call(null,seq__26141_26150);
if(temp__4657__auto___26161){
var seq__26141_26162__$1 = temp__4657__auto___26161;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__26141_26162__$1)){
var c__23946__auto___26163 = cljs.core.chunk_first.call(null,seq__26141_26162__$1);
var G__26164 = cljs.core.chunk_rest.call(null,seq__26141_26162__$1);
var G__26165 = c__23946__auto___26163;
var G__26166 = cljs.core.count.call(null,c__23946__auto___26163);
var G__26167 = (0);
seq__26141_26150 = G__26164;
chunk__26142_26151 = G__26165;
count__26143_26152 = G__26166;
i__26144_26153 = G__26167;
continue;
} else {
var vec__26146_26168 = cljs.core.first.call(null,seq__26141_26162__$1);
var k_26169 = cljs.core.nth.call(null,vec__26146_26168,(0),null);
var v_26170 = cljs.core.nth.call(null,vec__26146_26168,(1),null);
style.setProperty(dommy.utils.as_str.call(null,k_26169),v_26170);

var G__26171 = cljs.core.next.call(null,seq__26141_26162__$1);
var G__26172 = null;
var G__26173 = (0);
var G__26174 = (0);
seq__26141_26150 = G__26171;
chunk__26142_26151 = G__26172;
count__26143_26152 = G__26173;
i__26144_26153 = G__26174;
continue;
}
} else {
}
}
break;
}

return elem;
});

dommy.core.set_style_BANG_.cljs$lang$maxFixedArity = (1);

dommy.core.set_style_BANG_.cljs$lang$applyTo = (function (seq26139){
var G__26140 = cljs.core.first.call(null,seq26139);
var seq26139__$1 = cljs.core.next.call(null,seq26139);
return dommy.core.set_style_BANG_.cljs$core$IFn$_invoke$arity$variadic(G__26140,seq26139__$1);
});
/**
 * Remove the style of `elem` using keywords:
 *   
 *    (remove-style! elem :display :color)
 */
dommy.core.remove_style_BANG_ = (function dommy$core$remove_style_BANG_(var_args){
var args__24208__auto__ = [];
var len__24201__auto___26181 = arguments.length;
var i__24202__auto___26182 = (0);
while(true){
if((i__24202__auto___26182 < len__24201__auto___26181)){
args__24208__auto__.push((arguments[i__24202__auto___26182]));

var G__26183 = (i__24202__auto___26182 + (1));
i__24202__auto___26182 = G__26183;
continue;
} else {
}
break;
}

var argseq__24209__auto__ = ((((1) < args__24208__auto__.length))?(new cljs.core.IndexedSeq(args__24208__auto__.slice((1)),(0))):null);
return dommy.core.remove_style_BANG_.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),argseq__24209__auto__);
});

dommy.core.remove_style_BANG_.cljs$core$IFn$_invoke$arity$variadic = (function (elem,keywords){
var style = elem.style;
var seq__26177_26184 = cljs.core.seq.call(null,keywords);
var chunk__26178_26185 = null;
var count__26179_26186 = (0);
var i__26180_26187 = (0);
while(true){
if((i__26180_26187 < count__26179_26186)){
var kw_26188 = cljs.core._nth.call(null,chunk__26178_26185,i__26180_26187);
style.removeProperty(dommy.utils.as_str.call(null,kw_26188));

var G__26189 = seq__26177_26184;
var G__26190 = chunk__26178_26185;
var G__26191 = count__26179_26186;
var G__26192 = (i__26180_26187 + (1));
seq__26177_26184 = G__26189;
chunk__26178_26185 = G__26190;
count__26179_26186 = G__26191;
i__26180_26187 = G__26192;
continue;
} else {
var temp__4657__auto___26193 = cljs.core.seq.call(null,seq__26177_26184);
if(temp__4657__auto___26193){
var seq__26177_26194__$1 = temp__4657__auto___26193;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__26177_26194__$1)){
var c__23946__auto___26195 = cljs.core.chunk_first.call(null,seq__26177_26194__$1);
var G__26196 = cljs.core.chunk_rest.call(null,seq__26177_26194__$1);
var G__26197 = c__23946__auto___26195;
var G__26198 = cljs.core.count.call(null,c__23946__auto___26195);
var G__26199 = (0);
seq__26177_26184 = G__26196;
chunk__26178_26185 = G__26197;
count__26179_26186 = G__26198;
i__26180_26187 = G__26199;
continue;
} else {
var kw_26200 = cljs.core.first.call(null,seq__26177_26194__$1);
style.removeProperty(dommy.utils.as_str.call(null,kw_26200));

var G__26201 = cljs.core.next.call(null,seq__26177_26194__$1);
var G__26202 = null;
var G__26203 = (0);
var G__26204 = (0);
seq__26177_26184 = G__26201;
chunk__26178_26185 = G__26202;
count__26179_26186 = G__26203;
i__26180_26187 = G__26204;
continue;
}
} else {
}
}
break;
}

return elem;
});

dommy.core.remove_style_BANG_.cljs$lang$maxFixedArity = (1);

dommy.core.remove_style_BANG_.cljs$lang$applyTo = (function (seq26175){
var G__26176 = cljs.core.first.call(null,seq26175);
var seq26175__$1 = cljs.core.next.call(null,seq26175);
return dommy.core.remove_style_BANG_.cljs$core$IFn$_invoke$arity$variadic(G__26176,seq26175__$1);
});
dommy.core.set_px_BANG_ = (function dommy$core$set_px_BANG_(var_args){
var args__24208__auto__ = [];
var len__24201__auto___26213 = arguments.length;
var i__24202__auto___26214 = (0);
while(true){
if((i__24202__auto___26214 < len__24201__auto___26213)){
args__24208__auto__.push((arguments[i__24202__auto___26214]));

var G__26215 = (i__24202__auto___26214 + (1));
i__24202__auto___26214 = G__26215;
continue;
} else {
}
break;
}

var argseq__24209__auto__ = ((((1) < args__24208__auto__.length))?(new cljs.core.IndexedSeq(args__24208__auto__.slice((1)),(0))):null);
return dommy.core.set_px_BANG_.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),argseq__24209__auto__);
});

dommy.core.set_px_BANG_.cljs$core$IFn$_invoke$arity$variadic = (function (elem,kvs){

if(cljs.core.even_QMARK_.call(null,cljs.core.count.call(null,kvs))){
} else {
throw (new Error([cljs.core.str("Assert failed: "),cljs.core.str(cljs.core.pr_str.call(null,cljs.core.list(new cljs.core.Symbol(null,"even?","even?",-1827825394,null),cljs.core.list(new cljs.core.Symbol(null,"count","count",-514511684,null),new cljs.core.Symbol(null,"kvs","kvs",-1695980277,null)))))].join('')));
}

var seq__26207_26216 = cljs.core.seq.call(null,cljs.core.partition.call(null,(2),kvs));
var chunk__26208_26217 = null;
var count__26209_26218 = (0);
var i__26210_26219 = (0);
while(true){
if((i__26210_26219 < count__26209_26218)){
var vec__26211_26220 = cljs.core._nth.call(null,chunk__26208_26217,i__26210_26219);
var k_26221 = cljs.core.nth.call(null,vec__26211_26220,(0),null);
var v_26222 = cljs.core.nth.call(null,vec__26211_26220,(1),null);
dommy.core.set_style_BANG_.call(null,elem,k_26221,[cljs.core.str(v_26222),cljs.core.str("px")].join(''));

var G__26223 = seq__26207_26216;
var G__26224 = chunk__26208_26217;
var G__26225 = count__26209_26218;
var G__26226 = (i__26210_26219 + (1));
seq__26207_26216 = G__26223;
chunk__26208_26217 = G__26224;
count__26209_26218 = G__26225;
i__26210_26219 = G__26226;
continue;
} else {
var temp__4657__auto___26227 = cljs.core.seq.call(null,seq__26207_26216);
if(temp__4657__auto___26227){
var seq__26207_26228__$1 = temp__4657__auto___26227;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__26207_26228__$1)){
var c__23946__auto___26229 = cljs.core.chunk_first.call(null,seq__26207_26228__$1);
var G__26230 = cljs.core.chunk_rest.call(null,seq__26207_26228__$1);
var G__26231 = c__23946__auto___26229;
var G__26232 = cljs.core.count.call(null,c__23946__auto___26229);
var G__26233 = (0);
seq__26207_26216 = G__26230;
chunk__26208_26217 = G__26231;
count__26209_26218 = G__26232;
i__26210_26219 = G__26233;
continue;
} else {
var vec__26212_26234 = cljs.core.first.call(null,seq__26207_26228__$1);
var k_26235 = cljs.core.nth.call(null,vec__26212_26234,(0),null);
var v_26236 = cljs.core.nth.call(null,vec__26212_26234,(1),null);
dommy.core.set_style_BANG_.call(null,elem,k_26235,[cljs.core.str(v_26236),cljs.core.str("px")].join(''));

var G__26237 = cljs.core.next.call(null,seq__26207_26228__$1);
var G__26238 = null;
var G__26239 = (0);
var G__26240 = (0);
seq__26207_26216 = G__26237;
chunk__26208_26217 = G__26238;
count__26209_26218 = G__26239;
i__26210_26219 = G__26240;
continue;
}
} else {
}
}
break;
}

return elem;
});

dommy.core.set_px_BANG_.cljs$lang$maxFixedArity = (1);

dommy.core.set_px_BANG_.cljs$lang$applyTo = (function (seq26205){
var G__26206 = cljs.core.first.call(null,seq26205);
var seq26205__$1 = cljs.core.next.call(null,seq26205);
return dommy.core.set_px_BANG_.cljs$core$IFn$_invoke$arity$variadic(G__26206,seq26205__$1);
});
/**
 * Sets dom attributes on and returns `elem`.
 * Attributes without values will be set to their name:
 * 
 *     (set-attr! elem :disabled)
 * 
 * With values, the function takes variadic kv pairs:
 * 
 *     (set-attr! elem :id "some-id"
 *                     :name "some-name")
 */
dommy.core.set_attr_BANG_ = (function dommy$core$set_attr_BANG_(var_args){
var args26241 = [];
var len__24201__auto___26256 = arguments.length;
var i__24202__auto___26257 = (0);
while(true){
if((i__24202__auto___26257 < len__24201__auto___26256)){
args26241.push((arguments[i__24202__auto___26257]));

var G__26258 = (i__24202__auto___26257 + (1));
i__24202__auto___26257 = G__26258;
continue;
} else {
}
break;
}

var G__26247 = args26241.length;
switch (G__26247) {
case 2:
return dommy.core.set_attr_BANG_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return dommy.core.set_attr_BANG_.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
var argseq__24220__auto__ = (new cljs.core.IndexedSeq(args26241.slice((3)),(0)));
return dommy.core.set_attr_BANG_.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),argseq__24220__auto__);

}
});

dommy.core.set_attr_BANG_.cljs$core$IFn$_invoke$arity$2 = (function (elem,k){
return dommy.core.set_attr_BANG_.call(null,elem,k,dommy.utils.as_str.call(null,k));
});

dommy.core.set_attr_BANG_.cljs$core$IFn$_invoke$arity$3 = (function (elem,k,v){
var k__$1 = dommy.utils.as_str.call(null,k);
if(cljs.core.truth_(v)){
if(cljs.core.fn_QMARK_.call(null,v)){
var G__26248 = elem;
(G__26248[k__$1] = v);

return G__26248;
} else {
var G__26249 = elem;
G__26249.setAttribute(k__$1,v);

return G__26249;
}
} else {
return null;
}
});

dommy.core.set_attr_BANG_.cljs$core$IFn$_invoke$arity$variadic = (function (elem,k,v,kvs){
if(cljs.core.even_QMARK_.call(null,cljs.core.count.call(null,kvs))){
} else {
throw (new Error([cljs.core.str("Assert failed: "),cljs.core.str(cljs.core.pr_str.call(null,cljs.core.list(new cljs.core.Symbol(null,"even?","even?",-1827825394,null),cljs.core.list(new cljs.core.Symbol(null,"count","count",-514511684,null),new cljs.core.Symbol(null,"kvs","kvs",-1695980277,null)))))].join('')));
}

var seq__26250_26260 = cljs.core.seq.call(null,cljs.core.cons.call(null,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [k,v], null),cljs.core.partition.call(null,(2),kvs)));
var chunk__26251_26261 = null;
var count__26252_26262 = (0);
var i__26253_26263 = (0);
while(true){
if((i__26253_26263 < count__26252_26262)){
var vec__26254_26264 = cljs.core._nth.call(null,chunk__26251_26261,i__26253_26263);
var k_26265__$1 = cljs.core.nth.call(null,vec__26254_26264,(0),null);
var v_26266__$1 = cljs.core.nth.call(null,vec__26254_26264,(1),null);
dommy.core.set_attr_BANG_.call(null,elem,k_26265__$1,v_26266__$1);

var G__26267 = seq__26250_26260;
var G__26268 = chunk__26251_26261;
var G__26269 = count__26252_26262;
var G__26270 = (i__26253_26263 + (1));
seq__26250_26260 = G__26267;
chunk__26251_26261 = G__26268;
count__26252_26262 = G__26269;
i__26253_26263 = G__26270;
continue;
} else {
var temp__4657__auto___26271 = cljs.core.seq.call(null,seq__26250_26260);
if(temp__4657__auto___26271){
var seq__26250_26272__$1 = temp__4657__auto___26271;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__26250_26272__$1)){
var c__23946__auto___26273 = cljs.core.chunk_first.call(null,seq__26250_26272__$1);
var G__26274 = cljs.core.chunk_rest.call(null,seq__26250_26272__$1);
var G__26275 = c__23946__auto___26273;
var G__26276 = cljs.core.count.call(null,c__23946__auto___26273);
var G__26277 = (0);
seq__26250_26260 = G__26274;
chunk__26251_26261 = G__26275;
count__26252_26262 = G__26276;
i__26253_26263 = G__26277;
continue;
} else {
var vec__26255_26278 = cljs.core.first.call(null,seq__26250_26272__$1);
var k_26279__$1 = cljs.core.nth.call(null,vec__26255_26278,(0),null);
var v_26280__$1 = cljs.core.nth.call(null,vec__26255_26278,(1),null);
dommy.core.set_attr_BANG_.call(null,elem,k_26279__$1,v_26280__$1);

var G__26281 = cljs.core.next.call(null,seq__26250_26272__$1);
var G__26282 = null;
var G__26283 = (0);
var G__26284 = (0);
seq__26250_26260 = G__26281;
chunk__26251_26261 = G__26282;
count__26252_26262 = G__26283;
i__26253_26263 = G__26284;
continue;
}
} else {
}
}
break;
}

return elem;
});

dommy.core.set_attr_BANG_.cljs$lang$applyTo = (function (seq26242){
var G__26243 = cljs.core.first.call(null,seq26242);
var seq26242__$1 = cljs.core.next.call(null,seq26242);
var G__26244 = cljs.core.first.call(null,seq26242__$1);
var seq26242__$2 = cljs.core.next.call(null,seq26242__$1);
var G__26245 = cljs.core.first.call(null,seq26242__$2);
var seq26242__$3 = cljs.core.next.call(null,seq26242__$2);
return dommy.core.set_attr_BANG_.cljs$core$IFn$_invoke$arity$variadic(G__26243,G__26244,G__26245,seq26242__$3);
});

dommy.core.set_attr_BANG_.cljs$lang$maxFixedArity = (3);
/**
 * Removes dom attributes on and returns `elem`.
 * `class` and `classes` are special cases which clear
 * out the class name on removal.
 */
dommy.core.remove_attr_BANG_ = (function dommy$core$remove_attr_BANG_(var_args){
var args26285 = [];
var len__24201__auto___26295 = arguments.length;
var i__24202__auto___26296 = (0);
while(true){
if((i__24202__auto___26296 < len__24201__auto___26295)){
args26285.push((arguments[i__24202__auto___26296]));

var G__26297 = (i__24202__auto___26296 + (1));
i__24202__auto___26296 = G__26297;
continue;
} else {
}
break;
}

var G__26290 = args26285.length;
switch (G__26290) {
case 2:
return dommy.core.remove_attr_BANG_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
var argseq__24220__auto__ = (new cljs.core.IndexedSeq(args26285.slice((2)),(0)));
return dommy.core.remove_attr_BANG_.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),(arguments[(1)]),argseq__24220__auto__);

}
});

dommy.core.remove_attr_BANG_.cljs$core$IFn$_invoke$arity$2 = (function (elem,k){
var k_26299__$1 = dommy.utils.as_str.call(null,k);
if(cljs.core.truth_(new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 2, ["class",null,"classes",null], null), null).call(null,k_26299__$1))){
dommy.core.set_class_BANG_.call(null,elem,"");
} else {
elem.removeAttribute(k_26299__$1);
}

return elem;
});

dommy.core.remove_attr_BANG_.cljs$core$IFn$_invoke$arity$variadic = (function (elem,k,ks){
var seq__26291_26300 = cljs.core.seq.call(null,cljs.core.cons.call(null,k,ks));
var chunk__26292_26301 = null;
var count__26293_26302 = (0);
var i__26294_26303 = (0);
while(true){
if((i__26294_26303 < count__26293_26302)){
var k_26304__$1 = cljs.core._nth.call(null,chunk__26292_26301,i__26294_26303);
dommy.core.remove_attr_BANG_.call(null,elem,k_26304__$1);

var G__26305 = seq__26291_26300;
var G__26306 = chunk__26292_26301;
var G__26307 = count__26293_26302;
var G__26308 = (i__26294_26303 + (1));
seq__26291_26300 = G__26305;
chunk__26292_26301 = G__26306;
count__26293_26302 = G__26307;
i__26294_26303 = G__26308;
continue;
} else {
var temp__4657__auto___26309 = cljs.core.seq.call(null,seq__26291_26300);
if(temp__4657__auto___26309){
var seq__26291_26310__$1 = temp__4657__auto___26309;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__26291_26310__$1)){
var c__23946__auto___26311 = cljs.core.chunk_first.call(null,seq__26291_26310__$1);
var G__26312 = cljs.core.chunk_rest.call(null,seq__26291_26310__$1);
var G__26313 = c__23946__auto___26311;
var G__26314 = cljs.core.count.call(null,c__23946__auto___26311);
var G__26315 = (0);
seq__26291_26300 = G__26312;
chunk__26292_26301 = G__26313;
count__26293_26302 = G__26314;
i__26294_26303 = G__26315;
continue;
} else {
var k_26316__$1 = cljs.core.first.call(null,seq__26291_26310__$1);
dommy.core.remove_attr_BANG_.call(null,elem,k_26316__$1);

var G__26317 = cljs.core.next.call(null,seq__26291_26310__$1);
var G__26318 = null;
var G__26319 = (0);
var G__26320 = (0);
seq__26291_26300 = G__26317;
chunk__26292_26301 = G__26318;
count__26293_26302 = G__26319;
i__26294_26303 = G__26320;
continue;
}
} else {
}
}
break;
}

return elem;
});

dommy.core.remove_attr_BANG_.cljs$lang$applyTo = (function (seq26286){
var G__26287 = cljs.core.first.call(null,seq26286);
var seq26286__$1 = cljs.core.next.call(null,seq26286);
var G__26288 = cljs.core.first.call(null,seq26286__$1);
var seq26286__$2 = cljs.core.next.call(null,seq26286__$1);
return dommy.core.remove_attr_BANG_.cljs$core$IFn$_invoke$arity$variadic(G__26287,G__26288,seq26286__$2);
});

dommy.core.remove_attr_BANG_.cljs$lang$maxFixedArity = (2);
/**
 * Toggles a dom attribute `k` on `elem`, optionally specifying
 * the boolean value with `add?`
 */
dommy.core.toggle_attr_BANG_ = (function dommy$core$toggle_attr_BANG_(var_args){
var args26321 = [];
var len__24201__auto___26324 = arguments.length;
var i__24202__auto___26325 = (0);
while(true){
if((i__24202__auto___26325 < len__24201__auto___26324)){
args26321.push((arguments[i__24202__auto___26325]));

var G__26326 = (i__24202__auto___26325 + (1));
i__24202__auto___26325 = G__26326;
continue;
} else {
}
break;
}

var G__26323 = args26321.length;
switch (G__26323) {
case 2:
return dommy.core.toggle_attr_BANG_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return dommy.core.toggle_attr_BANG_.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error([cljs.core.str("Invalid arity: "),cljs.core.str(args26321.length)].join('')));

}
});

dommy.core.toggle_attr_BANG_.cljs$core$IFn$_invoke$arity$2 = (function (elem,k){
return dommy.core.toggle_attr_BANG_.call(null,elem,k,cljs.core.boolean$.call(null,dommy.core.attr.call(null,elem,k)));
});

dommy.core.toggle_attr_BANG_.cljs$core$IFn$_invoke$arity$3 = (function (elem,k,add_QMARK_){
if(add_QMARK_){
return dommy.core.set_attr_BANG_.call(null,elem,k);
} else {
return dommy.core.remove_attr_BANG_.call(null,elem,k);
}
});

dommy.core.toggle_attr_BANG_.cljs$lang$maxFixedArity = 3;
/**
 * Add `classes` to `elem`, trying to use Element::classList, and
 * falling back to fast string parsing/manipulation
 */
dommy.core.add_class_BANG_ = (function dommy$core$add_class_BANG_(var_args){
var args26328 = [];
var len__24201__auto___26346 = arguments.length;
var i__24202__auto___26347 = (0);
while(true){
if((i__24202__auto___26347 < len__24201__auto___26346)){
args26328.push((arguments[i__24202__auto___26347]));

var G__26348 = (i__24202__auto___26347 + (1));
i__24202__auto___26347 = G__26348;
continue;
} else {
}
break;
}

var G__26333 = args26328.length;
switch (G__26333) {
case 2:
return dommy.core.add_class_BANG_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
var argseq__24220__auto__ = (new cljs.core.IndexedSeq(args26328.slice((2)),(0)));
return dommy.core.add_class_BANG_.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),(arguments[(1)]),argseq__24220__auto__);

}
});

dommy.core.add_class_BANG_.cljs$core$IFn$_invoke$arity$2 = (function (elem,classes){
var classes__$1 = clojure.string.trim.call(null,dommy.utils.as_str.call(null,classes)).split(/\s+/);
if(cljs.core.seq.call(null,classes__$1)){
var temp__4655__auto___26350 = elem.classList;
if(cljs.core.truth_(temp__4655__auto___26350)){
var class_list_26351 = temp__4655__auto___26350;
var seq__26334_26352 = cljs.core.seq.call(null,classes__$1);
var chunk__26335_26353 = null;
var count__26336_26354 = (0);
var i__26337_26355 = (0);
while(true){
if((i__26337_26355 < count__26336_26354)){
var c_26356 = cljs.core._nth.call(null,chunk__26335_26353,i__26337_26355);
class_list_26351.add(c_26356);

var G__26357 = seq__26334_26352;
var G__26358 = chunk__26335_26353;
var G__26359 = count__26336_26354;
var G__26360 = (i__26337_26355 + (1));
seq__26334_26352 = G__26357;
chunk__26335_26353 = G__26358;
count__26336_26354 = G__26359;
i__26337_26355 = G__26360;
continue;
} else {
var temp__4657__auto___26361 = cljs.core.seq.call(null,seq__26334_26352);
if(temp__4657__auto___26361){
var seq__26334_26362__$1 = temp__4657__auto___26361;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__26334_26362__$1)){
var c__23946__auto___26363 = cljs.core.chunk_first.call(null,seq__26334_26362__$1);
var G__26364 = cljs.core.chunk_rest.call(null,seq__26334_26362__$1);
var G__26365 = c__23946__auto___26363;
var G__26366 = cljs.core.count.call(null,c__23946__auto___26363);
var G__26367 = (0);
seq__26334_26352 = G__26364;
chunk__26335_26353 = G__26365;
count__26336_26354 = G__26366;
i__26337_26355 = G__26367;
continue;
} else {
var c_26368 = cljs.core.first.call(null,seq__26334_26362__$1);
class_list_26351.add(c_26368);

var G__26369 = cljs.core.next.call(null,seq__26334_26362__$1);
var G__26370 = null;
var G__26371 = (0);
var G__26372 = (0);
seq__26334_26352 = G__26369;
chunk__26335_26353 = G__26370;
count__26336_26354 = G__26371;
i__26337_26355 = G__26372;
continue;
}
} else {
}
}
break;
}
} else {
var seq__26338_26373 = cljs.core.seq.call(null,classes__$1);
var chunk__26339_26374 = null;
var count__26340_26375 = (0);
var i__26341_26376 = (0);
while(true){
if((i__26341_26376 < count__26340_26375)){
var c_26377 = cljs.core._nth.call(null,chunk__26339_26374,i__26341_26376);
var class_name_26378 = dommy.core.class$.call(null,elem);
if(cljs.core.truth_(dommy.utils.class_index.call(null,class_name_26378,c_26377))){
} else {
dommy.core.set_class_BANG_.call(null,elem,(((class_name_26378 === ""))?c_26377:[cljs.core.str(class_name_26378),cljs.core.str(" "),cljs.core.str(c_26377)].join('')));
}

var G__26379 = seq__26338_26373;
var G__26380 = chunk__26339_26374;
var G__26381 = count__26340_26375;
var G__26382 = (i__26341_26376 + (1));
seq__26338_26373 = G__26379;
chunk__26339_26374 = G__26380;
count__26340_26375 = G__26381;
i__26341_26376 = G__26382;
continue;
} else {
var temp__4657__auto___26383 = cljs.core.seq.call(null,seq__26338_26373);
if(temp__4657__auto___26383){
var seq__26338_26384__$1 = temp__4657__auto___26383;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__26338_26384__$1)){
var c__23946__auto___26385 = cljs.core.chunk_first.call(null,seq__26338_26384__$1);
var G__26386 = cljs.core.chunk_rest.call(null,seq__26338_26384__$1);
var G__26387 = c__23946__auto___26385;
var G__26388 = cljs.core.count.call(null,c__23946__auto___26385);
var G__26389 = (0);
seq__26338_26373 = G__26386;
chunk__26339_26374 = G__26387;
count__26340_26375 = G__26388;
i__26341_26376 = G__26389;
continue;
} else {
var c_26390 = cljs.core.first.call(null,seq__26338_26384__$1);
var class_name_26391 = dommy.core.class$.call(null,elem);
if(cljs.core.truth_(dommy.utils.class_index.call(null,class_name_26391,c_26390))){
} else {
dommy.core.set_class_BANG_.call(null,elem,(((class_name_26391 === ""))?c_26390:[cljs.core.str(class_name_26391),cljs.core.str(" "),cljs.core.str(c_26390)].join('')));
}

var G__26392 = cljs.core.next.call(null,seq__26338_26384__$1);
var G__26393 = null;
var G__26394 = (0);
var G__26395 = (0);
seq__26338_26373 = G__26392;
chunk__26339_26374 = G__26393;
count__26340_26375 = G__26394;
i__26341_26376 = G__26395;
continue;
}
} else {
}
}
break;
}
}
} else {
}

return elem;
});

dommy.core.add_class_BANG_.cljs$core$IFn$_invoke$arity$variadic = (function (elem,classes,more_classes){
var seq__26342_26396 = cljs.core.seq.call(null,cljs.core.conj.call(null,more_classes,classes));
var chunk__26343_26397 = null;
var count__26344_26398 = (0);
var i__26345_26399 = (0);
while(true){
if((i__26345_26399 < count__26344_26398)){
var c_26400 = cljs.core._nth.call(null,chunk__26343_26397,i__26345_26399);
dommy.core.add_class_BANG_.call(null,elem,c_26400);

var G__26401 = seq__26342_26396;
var G__26402 = chunk__26343_26397;
var G__26403 = count__26344_26398;
var G__26404 = (i__26345_26399 + (1));
seq__26342_26396 = G__26401;
chunk__26343_26397 = G__26402;
count__26344_26398 = G__26403;
i__26345_26399 = G__26404;
continue;
} else {
var temp__4657__auto___26405 = cljs.core.seq.call(null,seq__26342_26396);
if(temp__4657__auto___26405){
var seq__26342_26406__$1 = temp__4657__auto___26405;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__26342_26406__$1)){
var c__23946__auto___26407 = cljs.core.chunk_first.call(null,seq__26342_26406__$1);
var G__26408 = cljs.core.chunk_rest.call(null,seq__26342_26406__$1);
var G__26409 = c__23946__auto___26407;
var G__26410 = cljs.core.count.call(null,c__23946__auto___26407);
var G__26411 = (0);
seq__26342_26396 = G__26408;
chunk__26343_26397 = G__26409;
count__26344_26398 = G__26410;
i__26345_26399 = G__26411;
continue;
} else {
var c_26412 = cljs.core.first.call(null,seq__26342_26406__$1);
dommy.core.add_class_BANG_.call(null,elem,c_26412);

var G__26413 = cljs.core.next.call(null,seq__26342_26406__$1);
var G__26414 = null;
var G__26415 = (0);
var G__26416 = (0);
seq__26342_26396 = G__26413;
chunk__26343_26397 = G__26414;
count__26344_26398 = G__26415;
i__26345_26399 = G__26416;
continue;
}
} else {
}
}
break;
}

return elem;
});

dommy.core.add_class_BANG_.cljs$lang$applyTo = (function (seq26329){
var G__26330 = cljs.core.first.call(null,seq26329);
var seq26329__$1 = cljs.core.next.call(null,seq26329);
var G__26331 = cljs.core.first.call(null,seq26329__$1);
var seq26329__$2 = cljs.core.next.call(null,seq26329__$1);
return dommy.core.add_class_BANG_.cljs$core$IFn$_invoke$arity$variadic(G__26330,G__26331,seq26329__$2);
});

dommy.core.add_class_BANG_.cljs$lang$maxFixedArity = (2);
/**
 * Remove `c` from `elem` class list
 */
dommy.core.remove_class_BANG_ = (function dommy$core$remove_class_BANG_(var_args){
var args26417 = [];
var len__24201__auto___26427 = arguments.length;
var i__24202__auto___26428 = (0);
while(true){
if((i__24202__auto___26428 < len__24201__auto___26427)){
args26417.push((arguments[i__24202__auto___26428]));

var G__26429 = (i__24202__auto___26428 + (1));
i__24202__auto___26428 = G__26429;
continue;
} else {
}
break;
}

var G__26422 = args26417.length;
switch (G__26422) {
case 2:
return dommy.core.remove_class_BANG_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
var argseq__24220__auto__ = (new cljs.core.IndexedSeq(args26417.slice((2)),(0)));
return dommy.core.remove_class_BANG_.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),(arguments[(1)]),argseq__24220__auto__);

}
});

dommy.core.remove_class_BANG_.cljs$core$IFn$_invoke$arity$2 = (function (elem,c){
var c__$1 = dommy.utils.as_str.call(null,c);
var temp__4655__auto___26431 = elem.classList;
if(cljs.core.truth_(temp__4655__auto___26431)){
var class_list_26432 = temp__4655__auto___26431;
class_list_26432.remove(c__$1);
} else {
var class_name_26433 = dommy.core.class$.call(null,elem);
var new_class_name_26434 = dommy.utils.remove_class_str.call(null,class_name_26433,c__$1);
if((class_name_26433 === new_class_name_26434)){
} else {
dommy.core.set_class_BANG_.call(null,elem,new_class_name_26434);
}
}

return elem;
});

dommy.core.remove_class_BANG_.cljs$core$IFn$_invoke$arity$variadic = (function (elem,class$,classes){
var seq__26423 = cljs.core.seq.call(null,cljs.core.conj.call(null,classes,class$));
var chunk__26424 = null;
var count__26425 = (0);
var i__26426 = (0);
while(true){
if((i__26426 < count__26425)){
var c = cljs.core._nth.call(null,chunk__26424,i__26426);
dommy.core.remove_class_BANG_.call(null,elem,c);

var G__26435 = seq__26423;
var G__26436 = chunk__26424;
var G__26437 = count__26425;
var G__26438 = (i__26426 + (1));
seq__26423 = G__26435;
chunk__26424 = G__26436;
count__26425 = G__26437;
i__26426 = G__26438;
continue;
} else {
var temp__4657__auto__ = cljs.core.seq.call(null,seq__26423);
if(temp__4657__auto__){
var seq__26423__$1 = temp__4657__auto__;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__26423__$1)){
var c__23946__auto__ = cljs.core.chunk_first.call(null,seq__26423__$1);
var G__26439 = cljs.core.chunk_rest.call(null,seq__26423__$1);
var G__26440 = c__23946__auto__;
var G__26441 = cljs.core.count.call(null,c__23946__auto__);
var G__26442 = (0);
seq__26423 = G__26439;
chunk__26424 = G__26440;
count__26425 = G__26441;
i__26426 = G__26442;
continue;
} else {
var c = cljs.core.first.call(null,seq__26423__$1);
dommy.core.remove_class_BANG_.call(null,elem,c);

var G__26443 = cljs.core.next.call(null,seq__26423__$1);
var G__26444 = null;
var G__26445 = (0);
var G__26446 = (0);
seq__26423 = G__26443;
chunk__26424 = G__26444;
count__26425 = G__26445;
i__26426 = G__26446;
continue;
}
} else {
return null;
}
}
break;
}
});

dommy.core.remove_class_BANG_.cljs$lang$applyTo = (function (seq26418){
var G__26419 = cljs.core.first.call(null,seq26418);
var seq26418__$1 = cljs.core.next.call(null,seq26418);
var G__26420 = cljs.core.first.call(null,seq26418__$1);
var seq26418__$2 = cljs.core.next.call(null,seq26418__$1);
return dommy.core.remove_class_BANG_.cljs$core$IFn$_invoke$arity$variadic(G__26419,G__26420,seq26418__$2);
});

dommy.core.remove_class_BANG_.cljs$lang$maxFixedArity = (2);
/**
 * (toggle-class! elem class) will add-class! if elem does not have class
 * and remove-class! otherwise.
 * (toggle-class! elem class add?) will add-class! if add? is truthy,
 * otherwise it will remove-class!
 */
dommy.core.toggle_class_BANG_ = (function dommy$core$toggle_class_BANG_(var_args){
var args26447 = [];
var len__24201__auto___26450 = arguments.length;
var i__24202__auto___26451 = (0);
while(true){
if((i__24202__auto___26451 < len__24201__auto___26450)){
args26447.push((arguments[i__24202__auto___26451]));

var G__26452 = (i__24202__auto___26451 + (1));
i__24202__auto___26451 = G__26452;
continue;
} else {
}
break;
}

var G__26449 = args26447.length;
switch (G__26449) {
case 2:
return dommy.core.toggle_class_BANG_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return dommy.core.toggle_class_BANG_.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error([cljs.core.str("Invalid arity: "),cljs.core.str(args26447.length)].join('')));

}
});

dommy.core.toggle_class_BANG_.cljs$core$IFn$_invoke$arity$2 = (function (elem,c){
var c__$1 = dommy.utils.as_str.call(null,c);
var temp__4655__auto___26454 = elem.classList;
if(cljs.core.truth_(temp__4655__auto___26454)){
var class_list_26455 = temp__4655__auto___26454;
class_list_26455.toggle(c__$1);
} else {
dommy.core.toggle_class_BANG_.call(null,elem,c__$1,!(dommy.core.has_class_QMARK_.call(null,elem,c__$1)));
}

return elem;
});

dommy.core.toggle_class_BANG_.cljs$core$IFn$_invoke$arity$3 = (function (elem,class$,add_QMARK_){
if(add_QMARK_){
dommy.core.add_class_BANG_.call(null,elem,class$);
} else {
dommy.core.remove_class_BANG_.call(null,elem,class$);
}

return elem;
});

dommy.core.toggle_class_BANG_.cljs$lang$maxFixedArity = 3;
/**
 * Display or hide the given `elem` (using display: none).
 * Takes an optional boolean `show?`
 */
dommy.core.toggle_BANG_ = (function dommy$core$toggle_BANG_(var_args){
var args26456 = [];
var len__24201__auto___26459 = arguments.length;
var i__24202__auto___26460 = (0);
while(true){
if((i__24202__auto___26460 < len__24201__auto___26459)){
args26456.push((arguments[i__24202__auto___26460]));

var G__26461 = (i__24202__auto___26460 + (1));
i__24202__auto___26460 = G__26461;
continue;
} else {
}
break;
}

var G__26458 = args26456.length;
switch (G__26458) {
case 2:
return dommy.core.toggle_BANG_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 1:
return dommy.core.toggle_BANG_.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
default:
throw (new Error([cljs.core.str("Invalid arity: "),cljs.core.str(args26456.length)].join('')));

}
});

dommy.core.toggle_BANG_.cljs$core$IFn$_invoke$arity$2 = (function (elem,show_QMARK_){
return dommy.core.set_style_BANG_.call(null,elem,new cljs.core.Keyword(null,"display","display",242065432),((show_QMARK_)?"":"none"));
});

dommy.core.toggle_BANG_.cljs$core$IFn$_invoke$arity$1 = (function (elem){
return dommy.core.toggle_BANG_.call(null,elem,dommy.core.hidden_QMARK_.call(null,elem));
});

dommy.core.toggle_BANG_.cljs$lang$maxFixedArity = 2;
dommy.core.hide_BANG_ = (function dommy$core$hide_BANG_(elem){
return dommy.core.toggle_BANG_.call(null,elem,false);
});
dommy.core.show_BANG_ = (function dommy$core$show_BANG_(elem){
return dommy.core.toggle_BANG_.call(null,elem,true);
});
dommy.core.scroll_into_view = (function dommy$core$scroll_into_view(elem,align_with_top_QMARK_){
var top = new cljs.core.Keyword(null,"top","top",-1856271961).cljs$core$IFn$_invoke$arity$1(dommy.core.bounding_client_rect.call(null,elem));
if((window.innerHeight < (top + elem.offsetHeight))){
return elem.scrollIntoView(align_with_top_QMARK_);
} else {
return null;
}
});
dommy.core.create_element = (function dommy$core$create_element(var_args){
var args26463 = [];
var len__24201__auto___26466 = arguments.length;
var i__24202__auto___26467 = (0);
while(true){
if((i__24202__auto___26467 < len__24201__auto___26466)){
args26463.push((arguments[i__24202__auto___26467]));

var G__26468 = (i__24202__auto___26467 + (1));
i__24202__auto___26467 = G__26468;
continue;
} else {
}
break;
}

var G__26465 = args26463.length;
switch (G__26465) {
case 1:
return dommy.core.create_element.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return dommy.core.create_element.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error([cljs.core.str("Invalid arity: "),cljs.core.str(args26463.length)].join('')));

}
});

dommy.core.create_element.cljs$core$IFn$_invoke$arity$1 = (function (tag){
return document.createElement(dommy.utils.as_str.call(null,tag));
});

dommy.core.create_element.cljs$core$IFn$_invoke$arity$2 = (function (tag_ns,tag){
return document.createElementNS(dommy.utils.as_str.call(null,tag_ns),dommy.utils.as_str.call(null,tag));
});

dommy.core.create_element.cljs$lang$maxFixedArity = 2;
dommy.core.create_text_node = (function dommy$core$create_text_node(text){
return document.createTextNode(text);
});
/**
 * Clears all children from `elem`
 */
dommy.core.clear_BANG_ = (function dommy$core$clear_BANG_(elem){
return dommy.core.set_html_BANG_.call(null,elem,"");
});
/**
 * Append `child` to `parent`
 */
dommy.core.append_BANG_ = (function dommy$core$append_BANG_(var_args){
var args26470 = [];
var len__24201__auto___26481 = arguments.length;
var i__24202__auto___26482 = (0);
while(true){
if((i__24202__auto___26482 < len__24201__auto___26481)){
args26470.push((arguments[i__24202__auto___26482]));

var G__26483 = (i__24202__auto___26482 + (1));
i__24202__auto___26482 = G__26483;
continue;
} else {
}
break;
}

var G__26475 = args26470.length;
switch (G__26475) {
case 2:
return dommy.core.append_BANG_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
var argseq__24220__auto__ = (new cljs.core.IndexedSeq(args26470.slice((2)),(0)));
return dommy.core.append_BANG_.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),(arguments[(1)]),argseq__24220__auto__);

}
});

dommy.core.append_BANG_.cljs$core$IFn$_invoke$arity$2 = (function (parent,child){
var G__26476 = parent;
G__26476.appendChild(child);

return G__26476;
});

dommy.core.append_BANG_.cljs$core$IFn$_invoke$arity$variadic = (function (parent,child,more_children){
var seq__26477_26485 = cljs.core.seq.call(null,cljs.core.cons.call(null,child,more_children));
var chunk__26478_26486 = null;
var count__26479_26487 = (0);
var i__26480_26488 = (0);
while(true){
if((i__26480_26488 < count__26479_26487)){
var c_26489 = cljs.core._nth.call(null,chunk__26478_26486,i__26480_26488);
dommy.core.append_BANG_.call(null,parent,c_26489);

var G__26490 = seq__26477_26485;
var G__26491 = chunk__26478_26486;
var G__26492 = count__26479_26487;
var G__26493 = (i__26480_26488 + (1));
seq__26477_26485 = G__26490;
chunk__26478_26486 = G__26491;
count__26479_26487 = G__26492;
i__26480_26488 = G__26493;
continue;
} else {
var temp__4657__auto___26494 = cljs.core.seq.call(null,seq__26477_26485);
if(temp__4657__auto___26494){
var seq__26477_26495__$1 = temp__4657__auto___26494;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__26477_26495__$1)){
var c__23946__auto___26496 = cljs.core.chunk_first.call(null,seq__26477_26495__$1);
var G__26497 = cljs.core.chunk_rest.call(null,seq__26477_26495__$1);
var G__26498 = c__23946__auto___26496;
var G__26499 = cljs.core.count.call(null,c__23946__auto___26496);
var G__26500 = (0);
seq__26477_26485 = G__26497;
chunk__26478_26486 = G__26498;
count__26479_26487 = G__26499;
i__26480_26488 = G__26500;
continue;
} else {
var c_26501 = cljs.core.first.call(null,seq__26477_26495__$1);
dommy.core.append_BANG_.call(null,parent,c_26501);

var G__26502 = cljs.core.next.call(null,seq__26477_26495__$1);
var G__26503 = null;
var G__26504 = (0);
var G__26505 = (0);
seq__26477_26485 = G__26502;
chunk__26478_26486 = G__26503;
count__26479_26487 = G__26504;
i__26480_26488 = G__26505;
continue;
}
} else {
}
}
break;
}

return parent;
});

dommy.core.append_BANG_.cljs$lang$applyTo = (function (seq26471){
var G__26472 = cljs.core.first.call(null,seq26471);
var seq26471__$1 = cljs.core.next.call(null,seq26471);
var G__26473 = cljs.core.first.call(null,seq26471__$1);
var seq26471__$2 = cljs.core.next.call(null,seq26471__$1);
return dommy.core.append_BANG_.cljs$core$IFn$_invoke$arity$variadic(G__26472,G__26473,seq26471__$2);
});

dommy.core.append_BANG_.cljs$lang$maxFixedArity = (2);
/**
 * Prepend `child` to `parent`
 */
dommy.core.prepend_BANG_ = (function dommy$core$prepend_BANG_(var_args){
var args26506 = [];
var len__24201__auto___26517 = arguments.length;
var i__24202__auto___26518 = (0);
while(true){
if((i__24202__auto___26518 < len__24201__auto___26517)){
args26506.push((arguments[i__24202__auto___26518]));

var G__26519 = (i__24202__auto___26518 + (1));
i__24202__auto___26518 = G__26519;
continue;
} else {
}
break;
}

var G__26511 = args26506.length;
switch (G__26511) {
case 2:
return dommy.core.prepend_BANG_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
var argseq__24220__auto__ = (new cljs.core.IndexedSeq(args26506.slice((2)),(0)));
return dommy.core.prepend_BANG_.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),(arguments[(1)]),argseq__24220__auto__);

}
});

dommy.core.prepend_BANG_.cljs$core$IFn$_invoke$arity$2 = (function (parent,child){
var G__26512 = parent;
G__26512.insertBefore(child,parent.firstChild);

return G__26512;
});

dommy.core.prepend_BANG_.cljs$core$IFn$_invoke$arity$variadic = (function (parent,child,more_children){
var seq__26513_26521 = cljs.core.seq.call(null,cljs.core.cons.call(null,child,more_children));
var chunk__26514_26522 = null;
var count__26515_26523 = (0);
var i__26516_26524 = (0);
while(true){
if((i__26516_26524 < count__26515_26523)){
var c_26525 = cljs.core._nth.call(null,chunk__26514_26522,i__26516_26524);
dommy.core.prepend_BANG_.call(null,parent,c_26525);

var G__26526 = seq__26513_26521;
var G__26527 = chunk__26514_26522;
var G__26528 = count__26515_26523;
var G__26529 = (i__26516_26524 + (1));
seq__26513_26521 = G__26526;
chunk__26514_26522 = G__26527;
count__26515_26523 = G__26528;
i__26516_26524 = G__26529;
continue;
} else {
var temp__4657__auto___26530 = cljs.core.seq.call(null,seq__26513_26521);
if(temp__4657__auto___26530){
var seq__26513_26531__$1 = temp__4657__auto___26530;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__26513_26531__$1)){
var c__23946__auto___26532 = cljs.core.chunk_first.call(null,seq__26513_26531__$1);
var G__26533 = cljs.core.chunk_rest.call(null,seq__26513_26531__$1);
var G__26534 = c__23946__auto___26532;
var G__26535 = cljs.core.count.call(null,c__23946__auto___26532);
var G__26536 = (0);
seq__26513_26521 = G__26533;
chunk__26514_26522 = G__26534;
count__26515_26523 = G__26535;
i__26516_26524 = G__26536;
continue;
} else {
var c_26537 = cljs.core.first.call(null,seq__26513_26531__$1);
dommy.core.prepend_BANG_.call(null,parent,c_26537);

var G__26538 = cljs.core.next.call(null,seq__26513_26531__$1);
var G__26539 = null;
var G__26540 = (0);
var G__26541 = (0);
seq__26513_26521 = G__26538;
chunk__26514_26522 = G__26539;
count__26515_26523 = G__26540;
i__26516_26524 = G__26541;
continue;
}
} else {
}
}
break;
}

return parent;
});

dommy.core.prepend_BANG_.cljs$lang$applyTo = (function (seq26507){
var G__26508 = cljs.core.first.call(null,seq26507);
var seq26507__$1 = cljs.core.next.call(null,seq26507);
var G__26509 = cljs.core.first.call(null,seq26507__$1);
var seq26507__$2 = cljs.core.next.call(null,seq26507__$1);
return dommy.core.prepend_BANG_.cljs$core$IFn$_invoke$arity$variadic(G__26508,G__26509,seq26507__$2);
});

dommy.core.prepend_BANG_.cljs$lang$maxFixedArity = (2);
/**
 * Insert `elem` before `other`, `other` must have a parent
 */
dommy.core.insert_before_BANG_ = (function dommy$core$insert_before_BANG_(elem,other){
var p = dommy.core.parent.call(null,other);
if(cljs.core.truth_(p)){
} else {
throw (new Error([cljs.core.str("Assert failed: "),cljs.core.str("Target element must have a parent"),cljs.core.str("\n"),cljs.core.str(cljs.core.pr_str.call(null,new cljs.core.Symbol(null,"p","p",1791580836,null)))].join('')));
}

p.insertBefore(elem,other);

return elem;
});
/**
 * Insert `elem` after `other`, `other` must have a parent
 */
dommy.core.insert_after_BANG_ = (function dommy$core$insert_after_BANG_(elem,other){
var temp__4655__auto___26542 = other.nextSibling;
if(cljs.core.truth_(temp__4655__auto___26542)){
var next_26543 = temp__4655__auto___26542;
dommy.core.insert_before_BANG_.call(null,elem,next_26543);
} else {
dommy.core.append_BANG_.call(null,dommy.core.parent.call(null,other),elem);
}

return elem;
});
/**
 * Replace `elem` with `new`, return `new`
 */
dommy.core.replace_BANG_ = (function dommy$core$replace_BANG_(elem,new$){
var p = dommy.core.parent.call(null,elem);
if(cljs.core.truth_(p)){
} else {
throw (new Error([cljs.core.str("Assert failed: "),cljs.core.str("Target element must have a parent"),cljs.core.str("\n"),cljs.core.str(cljs.core.pr_str.call(null,new cljs.core.Symbol(null,"p","p",1791580836,null)))].join('')));
}

p.replaceChild(new$,elem);

return new$;
});
/**
 * Replace children of `elem` with `child`
 */
dommy.core.replace_contents_BANG_ = (function dommy$core$replace_contents_BANG_(p,child){
return dommy.core.append_BANG_.call(null,dommy.core.clear_BANG_.call(null,p),child);
});
/**
 * Remove `elem` from `parent`, return `parent`
 */
dommy.core.remove_BANG_ = (function dommy$core$remove_BANG_(var_args){
var args26544 = [];
var len__24201__auto___26548 = arguments.length;
var i__24202__auto___26549 = (0);
while(true){
if((i__24202__auto___26549 < len__24201__auto___26548)){
args26544.push((arguments[i__24202__auto___26549]));

var G__26550 = (i__24202__auto___26549 + (1));
i__24202__auto___26549 = G__26550;
continue;
} else {
}
break;
}

var G__26546 = args26544.length;
switch (G__26546) {
case 1:
return dommy.core.remove_BANG_.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return dommy.core.remove_BANG_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error([cljs.core.str("Invalid arity: "),cljs.core.str(args26544.length)].join('')));

}
});

dommy.core.remove_BANG_.cljs$core$IFn$_invoke$arity$1 = (function (elem){
var p = dommy.core.parent.call(null,elem);
if(cljs.core.truth_(p)){
} else {
throw (new Error([cljs.core.str("Assert failed: "),cljs.core.str("Target element must have a parent"),cljs.core.str("\n"),cljs.core.str(cljs.core.pr_str.call(null,new cljs.core.Symbol(null,"p","p",1791580836,null)))].join('')));
}

return dommy.core.remove_BANG_.call(null,p,elem);
});

dommy.core.remove_BANG_.cljs$core$IFn$_invoke$arity$2 = (function (p,elem){
var G__26547 = p;
G__26547.removeChild(elem);

return G__26547;
});

dommy.core.remove_BANG_.cljs$lang$maxFixedArity = 2;
dommy.core.special_listener_makers = cljs.core.into.call(null,cljs.core.PersistentArrayMap.EMPTY,cljs.core.map.call(null,(function (p__26552){
var vec__26553 = p__26552;
var special_mouse_event = cljs.core.nth.call(null,vec__26553,(0),null);
var real_mouse_event = cljs.core.nth.call(null,vec__26553,(1),null);
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [special_mouse_event,cljs.core.PersistentArrayMap.fromArray([real_mouse_event,((function (vec__26553,special_mouse_event,real_mouse_event){
return (function (f){
return ((function (vec__26553,special_mouse_event,real_mouse_event){
return (function (event){
var related_target = event.relatedTarget;
var listener_target = (function (){var or__23143__auto__ = event.selectedTarget;
if(cljs.core.truth_(or__23143__auto__)){
return or__23143__auto__;
} else {
return event.currentTarget;
}
})();
if(cljs.core.truth_((function (){var and__23131__auto__ = related_target;
if(cljs.core.truth_(and__23131__auto__)){
return dommy.core.descendant_QMARK_.call(null,related_target,listener_target);
} else {
return and__23131__auto__;
}
})())){
return null;
} else {
return f.call(null,event);
}
});
;})(vec__26553,special_mouse_event,real_mouse_event))
});})(vec__26553,special_mouse_event,real_mouse_event))
], true, false)], null);
}),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"mouseenter","mouseenter",-1792413560),new cljs.core.Keyword(null,"mouseover","mouseover",-484272303),new cljs.core.Keyword(null,"mouseleave","mouseleave",531566580),new cljs.core.Keyword(null,"mouseout","mouseout",2049446890)], null)));
/**
 * fires f if event.target is found with `selector`
 */
dommy.core.live_listener = (function dommy$core$live_listener(elem,selector,f){
return (function (event){
var selected_target = dommy.core.closest.call(null,elem,event.target,selector);
if(cljs.core.truth_((function (){var and__23131__auto__ = selected_target;
if(cljs.core.truth_(and__23131__auto__)){
return cljs.core.not.call(null,dommy.core.attr.call(null,selected_target,new cljs.core.Keyword(null,"disabled","disabled",-1529784218)));
} else {
return and__23131__auto__;
}
})())){
event.selectedTarget = selected_target;

return f.call(null,event);
} else {
return null;
}
});
});
/**
 * Returns a nested map of event listeners on `elem`
 */
dommy.core.event_listeners = (function dommy$core$event_listeners(elem){
var or__23143__auto__ = elem.dommyEventListeners;
if(cljs.core.truth_(or__23143__auto__)){
return or__23143__auto__;
} else {
return cljs.core.PersistentArrayMap.EMPTY;
}
});
dommy.core.update_event_listeners_BANG_ = (function dommy$core$update_event_listeners_BANG_(var_args){
var args__24208__auto__ = [];
var len__24201__auto___26557 = arguments.length;
var i__24202__auto___26558 = (0);
while(true){
if((i__24202__auto___26558 < len__24201__auto___26557)){
args__24208__auto__.push((arguments[i__24202__auto___26558]));

var G__26559 = (i__24202__auto___26558 + (1));
i__24202__auto___26558 = G__26559;
continue;
} else {
}
break;
}

var argseq__24209__auto__ = ((((2) < args__24208__auto__.length))?(new cljs.core.IndexedSeq(args__24208__auto__.slice((2)),(0))):null);
return dommy.core.update_event_listeners_BANG_.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),(arguments[(1)]),argseq__24209__auto__);
});

dommy.core.update_event_listeners_BANG_.cljs$core$IFn$_invoke$arity$variadic = (function (elem,f,args){
var elem__$1 = elem;
return elem__$1.dommyEventListeners = cljs.core.apply.call(null,f,dommy.core.event_listeners.call(null,elem__$1),args);
});

dommy.core.update_event_listeners_BANG_.cljs$lang$maxFixedArity = (2);

dommy.core.update_event_listeners_BANG_.cljs$lang$applyTo = (function (seq26554){
var G__26555 = cljs.core.first.call(null,seq26554);
var seq26554__$1 = cljs.core.next.call(null,seq26554);
var G__26556 = cljs.core.first.call(null,seq26554__$1);
var seq26554__$2 = cljs.core.next.call(null,seq26554__$1);
return dommy.core.update_event_listeners_BANG_.cljs$core$IFn$_invoke$arity$variadic(G__26555,G__26556,seq26554__$2);
});
dommy.core.elem_and_selector = (function dommy$core$elem_and_selector(elem_sel){
if(cljs.core.sequential_QMARK_.call(null,elem_sel)){
return cljs.core.juxt.call(null,cljs.core.first,cljs.core.rest).call(null,elem_sel);
} else {
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [elem_sel,null], null);
}
});
/**
 * Adds `f` as a listener for events of type `event-type` on
 * `elem-sel`, which must either be a DOM node, or a sequence
 * whose first item is a DOM node.
 * 
 * In other words, the call to `listen!` can take two forms:
 * 
 * If `elem-sel` is a DOM node, i.e., you're doing something like:
 * 
 *     (listen! elem :click click-handler)
 * 
 * then `click-handler` will be set as a listener for `click` events
 * on the `elem`.
 * 
 * If `elem-sel` is a sequence:
 * 
 *     (listen! [elem :.selector.for :.some.descendants] :click click-handler)
 * 
 * then `click-handler` will be set as a listener for `click` events
 * on descendants of `elem` that match the selector
 * 
 * Also accepts any number of event-type and handler pairs for setting
 * multiple listeners at once:
 * 
 *     (listen! some-elem :click click-handler :hover hover-handler)
 */
dommy.core.listen_BANG_ = (function dommy$core$listen_BANG_(var_args){
var args__24208__auto__ = [];
var len__24201__auto___26585 = arguments.length;
var i__24202__auto___26586 = (0);
while(true){
if((i__24202__auto___26586 < len__24201__auto___26585)){
args__24208__auto__.push((arguments[i__24202__auto___26586]));

var G__26587 = (i__24202__auto___26586 + (1));
i__24202__auto___26586 = G__26587;
continue;
} else {
}
break;
}

var argseq__24209__auto__ = ((((1) < args__24208__auto__.length))?(new cljs.core.IndexedSeq(args__24208__auto__.slice((1)),(0))):null);
return dommy.core.listen_BANG_.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),argseq__24209__auto__);
});

dommy.core.listen_BANG_.cljs$core$IFn$_invoke$arity$variadic = (function (elem_sel,type_fs){
if(cljs.core.even_QMARK_.call(null,cljs.core.count.call(null,type_fs))){
} else {
throw (new Error([cljs.core.str("Assert failed: "),cljs.core.str(cljs.core.pr_str.call(null,cljs.core.list(new cljs.core.Symbol(null,"even?","even?",-1827825394,null),cljs.core.list(new cljs.core.Symbol(null,"count","count",-514511684,null),new cljs.core.Symbol(null,"type-fs","type-fs",1567896074,null)))))].join('')));
}

var vec__26562_26588 = dommy.core.elem_and_selector.call(null,elem_sel);
var elem_26589 = cljs.core.nth.call(null,vec__26562_26588,(0),null);
var selector_26590 = cljs.core.nth.call(null,vec__26562_26588,(1),null);
var seq__26563_26591 = cljs.core.seq.call(null,cljs.core.partition.call(null,(2),type_fs));
var chunk__26570_26592 = null;
var count__26571_26593 = (0);
var i__26572_26594 = (0);
while(true){
if((i__26572_26594 < count__26571_26593)){
var vec__26579_26595 = cljs.core._nth.call(null,chunk__26570_26592,i__26572_26594);
var orig_type_26596 = cljs.core.nth.call(null,vec__26579_26595,(0),null);
var f_26597 = cljs.core.nth.call(null,vec__26579_26595,(1),null);
var seq__26573_26598 = cljs.core.seq.call(null,cljs.core.get.call(null,dommy.core.special_listener_makers,orig_type_26596,cljs.core.PersistentArrayMap.fromArray([orig_type_26596,cljs.core.identity], true, false)));
var chunk__26575_26599 = null;
var count__26576_26600 = (0);
var i__26577_26601 = (0);
while(true){
if((i__26577_26601 < count__26576_26600)){
var vec__26580_26602 = cljs.core._nth.call(null,chunk__26575_26599,i__26577_26601);
var actual_type_26603 = cljs.core.nth.call(null,vec__26580_26602,(0),null);
var factory_26604 = cljs.core.nth.call(null,vec__26580_26602,(1),null);
var canonical_f_26605 = (cljs.core.truth_(selector_26590)?cljs.core.partial.call(null,dommy.core.live_listener,elem_26589,selector_26590):cljs.core.identity).call(null,factory_26604.call(null,f_26597));
dommy.core.update_event_listeners_BANG_.call(null,elem_26589,cljs.core.assoc_in,new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [selector_26590,actual_type_26603,f_26597], null),canonical_f_26605);

if(cljs.core.truth_(elem_26589.addEventListener)){
elem_26589.addEventListener(cljs.core.name.call(null,actual_type_26603),canonical_f_26605);
} else {
elem_26589.attachEvent(cljs.core.name.call(null,actual_type_26603),canonical_f_26605);
}

var G__26606 = seq__26573_26598;
var G__26607 = chunk__26575_26599;
var G__26608 = count__26576_26600;
var G__26609 = (i__26577_26601 + (1));
seq__26573_26598 = G__26606;
chunk__26575_26599 = G__26607;
count__26576_26600 = G__26608;
i__26577_26601 = G__26609;
continue;
} else {
var temp__4657__auto___26610 = cljs.core.seq.call(null,seq__26573_26598);
if(temp__4657__auto___26610){
var seq__26573_26611__$1 = temp__4657__auto___26610;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__26573_26611__$1)){
var c__23946__auto___26612 = cljs.core.chunk_first.call(null,seq__26573_26611__$1);
var G__26613 = cljs.core.chunk_rest.call(null,seq__26573_26611__$1);
var G__26614 = c__23946__auto___26612;
var G__26615 = cljs.core.count.call(null,c__23946__auto___26612);
var G__26616 = (0);
seq__26573_26598 = G__26613;
chunk__26575_26599 = G__26614;
count__26576_26600 = G__26615;
i__26577_26601 = G__26616;
continue;
} else {
var vec__26581_26617 = cljs.core.first.call(null,seq__26573_26611__$1);
var actual_type_26618 = cljs.core.nth.call(null,vec__26581_26617,(0),null);
var factory_26619 = cljs.core.nth.call(null,vec__26581_26617,(1),null);
var canonical_f_26620 = (cljs.core.truth_(selector_26590)?cljs.core.partial.call(null,dommy.core.live_listener,elem_26589,selector_26590):cljs.core.identity).call(null,factory_26619.call(null,f_26597));
dommy.core.update_event_listeners_BANG_.call(null,elem_26589,cljs.core.assoc_in,new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [selector_26590,actual_type_26618,f_26597], null),canonical_f_26620);

if(cljs.core.truth_(elem_26589.addEventListener)){
elem_26589.addEventListener(cljs.core.name.call(null,actual_type_26618),canonical_f_26620);
} else {
elem_26589.attachEvent(cljs.core.name.call(null,actual_type_26618),canonical_f_26620);
}

var G__26621 = cljs.core.next.call(null,seq__26573_26611__$1);
var G__26622 = null;
var G__26623 = (0);
var G__26624 = (0);
seq__26573_26598 = G__26621;
chunk__26575_26599 = G__26622;
count__26576_26600 = G__26623;
i__26577_26601 = G__26624;
continue;
}
} else {
}
}
break;
}

var G__26625 = seq__26563_26591;
var G__26626 = chunk__26570_26592;
var G__26627 = count__26571_26593;
var G__26628 = (i__26572_26594 + (1));
seq__26563_26591 = G__26625;
chunk__26570_26592 = G__26626;
count__26571_26593 = G__26627;
i__26572_26594 = G__26628;
continue;
} else {
var temp__4657__auto___26629 = cljs.core.seq.call(null,seq__26563_26591);
if(temp__4657__auto___26629){
var seq__26563_26630__$1 = temp__4657__auto___26629;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__26563_26630__$1)){
var c__23946__auto___26631 = cljs.core.chunk_first.call(null,seq__26563_26630__$1);
var G__26632 = cljs.core.chunk_rest.call(null,seq__26563_26630__$1);
var G__26633 = c__23946__auto___26631;
var G__26634 = cljs.core.count.call(null,c__23946__auto___26631);
var G__26635 = (0);
seq__26563_26591 = G__26632;
chunk__26570_26592 = G__26633;
count__26571_26593 = G__26634;
i__26572_26594 = G__26635;
continue;
} else {
var vec__26582_26636 = cljs.core.first.call(null,seq__26563_26630__$1);
var orig_type_26637 = cljs.core.nth.call(null,vec__26582_26636,(0),null);
var f_26638 = cljs.core.nth.call(null,vec__26582_26636,(1),null);
var seq__26564_26639 = cljs.core.seq.call(null,cljs.core.get.call(null,dommy.core.special_listener_makers,orig_type_26637,cljs.core.PersistentArrayMap.fromArray([orig_type_26637,cljs.core.identity], true, false)));
var chunk__26566_26640 = null;
var count__26567_26641 = (0);
var i__26568_26642 = (0);
while(true){
if((i__26568_26642 < count__26567_26641)){
var vec__26583_26643 = cljs.core._nth.call(null,chunk__26566_26640,i__26568_26642);
var actual_type_26644 = cljs.core.nth.call(null,vec__26583_26643,(0),null);
var factory_26645 = cljs.core.nth.call(null,vec__26583_26643,(1),null);
var canonical_f_26646 = (cljs.core.truth_(selector_26590)?cljs.core.partial.call(null,dommy.core.live_listener,elem_26589,selector_26590):cljs.core.identity).call(null,factory_26645.call(null,f_26638));
dommy.core.update_event_listeners_BANG_.call(null,elem_26589,cljs.core.assoc_in,new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [selector_26590,actual_type_26644,f_26638], null),canonical_f_26646);

if(cljs.core.truth_(elem_26589.addEventListener)){
elem_26589.addEventListener(cljs.core.name.call(null,actual_type_26644),canonical_f_26646);
} else {
elem_26589.attachEvent(cljs.core.name.call(null,actual_type_26644),canonical_f_26646);
}

var G__26647 = seq__26564_26639;
var G__26648 = chunk__26566_26640;
var G__26649 = count__26567_26641;
var G__26650 = (i__26568_26642 + (1));
seq__26564_26639 = G__26647;
chunk__26566_26640 = G__26648;
count__26567_26641 = G__26649;
i__26568_26642 = G__26650;
continue;
} else {
var temp__4657__auto___26651__$1 = cljs.core.seq.call(null,seq__26564_26639);
if(temp__4657__auto___26651__$1){
var seq__26564_26652__$1 = temp__4657__auto___26651__$1;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__26564_26652__$1)){
var c__23946__auto___26653 = cljs.core.chunk_first.call(null,seq__26564_26652__$1);
var G__26654 = cljs.core.chunk_rest.call(null,seq__26564_26652__$1);
var G__26655 = c__23946__auto___26653;
var G__26656 = cljs.core.count.call(null,c__23946__auto___26653);
var G__26657 = (0);
seq__26564_26639 = G__26654;
chunk__26566_26640 = G__26655;
count__26567_26641 = G__26656;
i__26568_26642 = G__26657;
continue;
} else {
var vec__26584_26658 = cljs.core.first.call(null,seq__26564_26652__$1);
var actual_type_26659 = cljs.core.nth.call(null,vec__26584_26658,(0),null);
var factory_26660 = cljs.core.nth.call(null,vec__26584_26658,(1),null);
var canonical_f_26661 = (cljs.core.truth_(selector_26590)?cljs.core.partial.call(null,dommy.core.live_listener,elem_26589,selector_26590):cljs.core.identity).call(null,factory_26660.call(null,f_26638));
dommy.core.update_event_listeners_BANG_.call(null,elem_26589,cljs.core.assoc_in,new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [selector_26590,actual_type_26659,f_26638], null),canonical_f_26661);

if(cljs.core.truth_(elem_26589.addEventListener)){
elem_26589.addEventListener(cljs.core.name.call(null,actual_type_26659),canonical_f_26661);
} else {
elem_26589.attachEvent(cljs.core.name.call(null,actual_type_26659),canonical_f_26661);
}

var G__26662 = cljs.core.next.call(null,seq__26564_26652__$1);
var G__26663 = null;
var G__26664 = (0);
var G__26665 = (0);
seq__26564_26639 = G__26662;
chunk__26566_26640 = G__26663;
count__26567_26641 = G__26664;
i__26568_26642 = G__26665;
continue;
}
} else {
}
}
break;
}

var G__26666 = cljs.core.next.call(null,seq__26563_26630__$1);
var G__26667 = null;
var G__26668 = (0);
var G__26669 = (0);
seq__26563_26591 = G__26666;
chunk__26570_26592 = G__26667;
count__26571_26593 = G__26668;
i__26572_26594 = G__26669;
continue;
}
} else {
}
}
break;
}

return elem_sel;
});

dommy.core.listen_BANG_.cljs$lang$maxFixedArity = (1);

dommy.core.listen_BANG_.cljs$lang$applyTo = (function (seq26560){
var G__26561 = cljs.core.first.call(null,seq26560);
var seq26560__$1 = cljs.core.next.call(null,seq26560);
return dommy.core.listen_BANG_.cljs$core$IFn$_invoke$arity$variadic(G__26561,seq26560__$1);
});
/**
 * Removes event listener for the element defined in `elem-sel`,
 * which is the same format as listen!.
 * 
 *   The following forms are allowed, and will remove all handlers
 *   that match the parameters passed in:
 * 
 *    (unlisten! [elem :.selector] :click event-listener)
 * 
 *    (unlisten! [elem :.selector]
 *      :click event-listener
 *      :mouseover other-event-listener)
 */
dommy.core.unlisten_BANG_ = (function dommy$core$unlisten_BANG_(var_args){
var args__24208__auto__ = [];
var len__24201__auto___26695 = arguments.length;
var i__24202__auto___26696 = (0);
while(true){
if((i__24202__auto___26696 < len__24201__auto___26695)){
args__24208__auto__.push((arguments[i__24202__auto___26696]));

var G__26697 = (i__24202__auto___26696 + (1));
i__24202__auto___26696 = G__26697;
continue;
} else {
}
break;
}

var argseq__24209__auto__ = ((((1) < args__24208__auto__.length))?(new cljs.core.IndexedSeq(args__24208__auto__.slice((1)),(0))):null);
return dommy.core.unlisten_BANG_.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),argseq__24209__auto__);
});

dommy.core.unlisten_BANG_.cljs$core$IFn$_invoke$arity$variadic = (function (elem_sel,type_fs){
if(cljs.core.even_QMARK_.call(null,cljs.core.count.call(null,type_fs))){
} else {
throw (new Error([cljs.core.str("Assert failed: "),cljs.core.str(cljs.core.pr_str.call(null,cljs.core.list(new cljs.core.Symbol(null,"even?","even?",-1827825394,null),cljs.core.list(new cljs.core.Symbol(null,"count","count",-514511684,null),new cljs.core.Symbol(null,"type-fs","type-fs",1567896074,null)))))].join('')));
}

var vec__26672_26698 = dommy.core.elem_and_selector.call(null,elem_sel);
var elem_26699 = cljs.core.nth.call(null,vec__26672_26698,(0),null);
var selector_26700 = cljs.core.nth.call(null,vec__26672_26698,(1),null);
var seq__26673_26701 = cljs.core.seq.call(null,cljs.core.partition.call(null,(2),type_fs));
var chunk__26680_26702 = null;
var count__26681_26703 = (0);
var i__26682_26704 = (0);
while(true){
if((i__26682_26704 < count__26681_26703)){
var vec__26689_26705 = cljs.core._nth.call(null,chunk__26680_26702,i__26682_26704);
var orig_type_26706 = cljs.core.nth.call(null,vec__26689_26705,(0),null);
var f_26707 = cljs.core.nth.call(null,vec__26689_26705,(1),null);
var seq__26683_26708 = cljs.core.seq.call(null,cljs.core.get.call(null,dommy.core.special_listener_makers,orig_type_26706,cljs.core.PersistentArrayMap.fromArray([orig_type_26706,cljs.core.identity], true, false)));
var chunk__26685_26709 = null;
var count__26686_26710 = (0);
var i__26687_26711 = (0);
while(true){
if((i__26687_26711 < count__26686_26710)){
var vec__26690_26712 = cljs.core._nth.call(null,chunk__26685_26709,i__26687_26711);
var actual_type_26713 = cljs.core.nth.call(null,vec__26690_26712,(0),null);
var __26714 = cljs.core.nth.call(null,vec__26690_26712,(1),null);
var keys_26715 = new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [selector_26700,actual_type_26713,f_26707], null);
var canonical_f_26716 = cljs.core.get_in.call(null,dommy.core.event_listeners.call(null,elem_26699),keys_26715);
dommy.core.update_event_listeners_BANG_.call(null,elem_26699,dommy.utils.dissoc_in,keys_26715);

if(cljs.core.truth_(elem_26699.removeEventListener)){
elem_26699.removeEventListener(cljs.core.name.call(null,actual_type_26713),canonical_f_26716);
} else {
elem_26699.detachEvent(cljs.core.name.call(null,actual_type_26713),canonical_f_26716);
}

var G__26717 = seq__26683_26708;
var G__26718 = chunk__26685_26709;
var G__26719 = count__26686_26710;
var G__26720 = (i__26687_26711 + (1));
seq__26683_26708 = G__26717;
chunk__26685_26709 = G__26718;
count__26686_26710 = G__26719;
i__26687_26711 = G__26720;
continue;
} else {
var temp__4657__auto___26721 = cljs.core.seq.call(null,seq__26683_26708);
if(temp__4657__auto___26721){
var seq__26683_26722__$1 = temp__4657__auto___26721;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__26683_26722__$1)){
var c__23946__auto___26723 = cljs.core.chunk_first.call(null,seq__26683_26722__$1);
var G__26724 = cljs.core.chunk_rest.call(null,seq__26683_26722__$1);
var G__26725 = c__23946__auto___26723;
var G__26726 = cljs.core.count.call(null,c__23946__auto___26723);
var G__26727 = (0);
seq__26683_26708 = G__26724;
chunk__26685_26709 = G__26725;
count__26686_26710 = G__26726;
i__26687_26711 = G__26727;
continue;
} else {
var vec__26691_26728 = cljs.core.first.call(null,seq__26683_26722__$1);
var actual_type_26729 = cljs.core.nth.call(null,vec__26691_26728,(0),null);
var __26730 = cljs.core.nth.call(null,vec__26691_26728,(1),null);
var keys_26731 = new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [selector_26700,actual_type_26729,f_26707], null);
var canonical_f_26732 = cljs.core.get_in.call(null,dommy.core.event_listeners.call(null,elem_26699),keys_26731);
dommy.core.update_event_listeners_BANG_.call(null,elem_26699,dommy.utils.dissoc_in,keys_26731);

if(cljs.core.truth_(elem_26699.removeEventListener)){
elem_26699.removeEventListener(cljs.core.name.call(null,actual_type_26729),canonical_f_26732);
} else {
elem_26699.detachEvent(cljs.core.name.call(null,actual_type_26729),canonical_f_26732);
}

var G__26733 = cljs.core.next.call(null,seq__26683_26722__$1);
var G__26734 = null;
var G__26735 = (0);
var G__26736 = (0);
seq__26683_26708 = G__26733;
chunk__26685_26709 = G__26734;
count__26686_26710 = G__26735;
i__26687_26711 = G__26736;
continue;
}
} else {
}
}
break;
}

var G__26737 = seq__26673_26701;
var G__26738 = chunk__26680_26702;
var G__26739 = count__26681_26703;
var G__26740 = (i__26682_26704 + (1));
seq__26673_26701 = G__26737;
chunk__26680_26702 = G__26738;
count__26681_26703 = G__26739;
i__26682_26704 = G__26740;
continue;
} else {
var temp__4657__auto___26741 = cljs.core.seq.call(null,seq__26673_26701);
if(temp__4657__auto___26741){
var seq__26673_26742__$1 = temp__4657__auto___26741;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__26673_26742__$1)){
var c__23946__auto___26743 = cljs.core.chunk_first.call(null,seq__26673_26742__$1);
var G__26744 = cljs.core.chunk_rest.call(null,seq__26673_26742__$1);
var G__26745 = c__23946__auto___26743;
var G__26746 = cljs.core.count.call(null,c__23946__auto___26743);
var G__26747 = (0);
seq__26673_26701 = G__26744;
chunk__26680_26702 = G__26745;
count__26681_26703 = G__26746;
i__26682_26704 = G__26747;
continue;
} else {
var vec__26692_26748 = cljs.core.first.call(null,seq__26673_26742__$1);
var orig_type_26749 = cljs.core.nth.call(null,vec__26692_26748,(0),null);
var f_26750 = cljs.core.nth.call(null,vec__26692_26748,(1),null);
var seq__26674_26751 = cljs.core.seq.call(null,cljs.core.get.call(null,dommy.core.special_listener_makers,orig_type_26749,cljs.core.PersistentArrayMap.fromArray([orig_type_26749,cljs.core.identity], true, false)));
var chunk__26676_26752 = null;
var count__26677_26753 = (0);
var i__26678_26754 = (0);
while(true){
if((i__26678_26754 < count__26677_26753)){
var vec__26693_26755 = cljs.core._nth.call(null,chunk__26676_26752,i__26678_26754);
var actual_type_26756 = cljs.core.nth.call(null,vec__26693_26755,(0),null);
var __26757 = cljs.core.nth.call(null,vec__26693_26755,(1),null);
var keys_26758 = new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [selector_26700,actual_type_26756,f_26750], null);
var canonical_f_26759 = cljs.core.get_in.call(null,dommy.core.event_listeners.call(null,elem_26699),keys_26758);
dommy.core.update_event_listeners_BANG_.call(null,elem_26699,dommy.utils.dissoc_in,keys_26758);

if(cljs.core.truth_(elem_26699.removeEventListener)){
elem_26699.removeEventListener(cljs.core.name.call(null,actual_type_26756),canonical_f_26759);
} else {
elem_26699.detachEvent(cljs.core.name.call(null,actual_type_26756),canonical_f_26759);
}

var G__26760 = seq__26674_26751;
var G__26761 = chunk__26676_26752;
var G__26762 = count__26677_26753;
var G__26763 = (i__26678_26754 + (1));
seq__26674_26751 = G__26760;
chunk__26676_26752 = G__26761;
count__26677_26753 = G__26762;
i__26678_26754 = G__26763;
continue;
} else {
var temp__4657__auto___26764__$1 = cljs.core.seq.call(null,seq__26674_26751);
if(temp__4657__auto___26764__$1){
var seq__26674_26765__$1 = temp__4657__auto___26764__$1;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__26674_26765__$1)){
var c__23946__auto___26766 = cljs.core.chunk_first.call(null,seq__26674_26765__$1);
var G__26767 = cljs.core.chunk_rest.call(null,seq__26674_26765__$1);
var G__26768 = c__23946__auto___26766;
var G__26769 = cljs.core.count.call(null,c__23946__auto___26766);
var G__26770 = (0);
seq__26674_26751 = G__26767;
chunk__26676_26752 = G__26768;
count__26677_26753 = G__26769;
i__26678_26754 = G__26770;
continue;
} else {
var vec__26694_26771 = cljs.core.first.call(null,seq__26674_26765__$1);
var actual_type_26772 = cljs.core.nth.call(null,vec__26694_26771,(0),null);
var __26773 = cljs.core.nth.call(null,vec__26694_26771,(1),null);
var keys_26774 = new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [selector_26700,actual_type_26772,f_26750], null);
var canonical_f_26775 = cljs.core.get_in.call(null,dommy.core.event_listeners.call(null,elem_26699),keys_26774);
dommy.core.update_event_listeners_BANG_.call(null,elem_26699,dommy.utils.dissoc_in,keys_26774);

if(cljs.core.truth_(elem_26699.removeEventListener)){
elem_26699.removeEventListener(cljs.core.name.call(null,actual_type_26772),canonical_f_26775);
} else {
elem_26699.detachEvent(cljs.core.name.call(null,actual_type_26772),canonical_f_26775);
}

var G__26776 = cljs.core.next.call(null,seq__26674_26765__$1);
var G__26777 = null;
var G__26778 = (0);
var G__26779 = (0);
seq__26674_26751 = G__26776;
chunk__26676_26752 = G__26777;
count__26677_26753 = G__26778;
i__26678_26754 = G__26779;
continue;
}
} else {
}
}
break;
}

var G__26780 = cljs.core.next.call(null,seq__26673_26742__$1);
var G__26781 = null;
var G__26782 = (0);
var G__26783 = (0);
seq__26673_26701 = G__26780;
chunk__26680_26702 = G__26781;
count__26681_26703 = G__26782;
i__26682_26704 = G__26783;
continue;
}
} else {
}
}
break;
}

return elem_sel;
});

dommy.core.unlisten_BANG_.cljs$lang$maxFixedArity = (1);

dommy.core.unlisten_BANG_.cljs$lang$applyTo = (function (seq26670){
var G__26671 = cljs.core.first.call(null,seq26670);
var seq26670__$1 = cljs.core.next.call(null,seq26670);
return dommy.core.unlisten_BANG_.cljs$core$IFn$_invoke$arity$variadic(G__26671,seq26670__$1);
});
/**
 * Behaves like `listen!`, but removes the listener after the first event occurs.
 */
dommy.core.listen_once_BANG_ = (function dommy$core$listen_once_BANG_(var_args){
var args__24208__auto__ = [];
var len__24201__auto___26793 = arguments.length;
var i__24202__auto___26794 = (0);
while(true){
if((i__24202__auto___26794 < len__24201__auto___26793)){
args__24208__auto__.push((arguments[i__24202__auto___26794]));

var G__26795 = (i__24202__auto___26794 + (1));
i__24202__auto___26794 = G__26795;
continue;
} else {
}
break;
}

var argseq__24209__auto__ = ((((1) < args__24208__auto__.length))?(new cljs.core.IndexedSeq(args__24208__auto__.slice((1)),(0))):null);
return dommy.core.listen_once_BANG_.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),argseq__24209__auto__);
});

dommy.core.listen_once_BANG_.cljs$core$IFn$_invoke$arity$variadic = (function (elem_sel,type_fs){
if(cljs.core.even_QMARK_.call(null,cljs.core.count.call(null,type_fs))){
} else {
throw (new Error([cljs.core.str("Assert failed: "),cljs.core.str(cljs.core.pr_str.call(null,cljs.core.list(new cljs.core.Symbol(null,"even?","even?",-1827825394,null),cljs.core.list(new cljs.core.Symbol(null,"count","count",-514511684,null),new cljs.core.Symbol(null,"type-fs","type-fs",1567896074,null)))))].join('')));
}

var vec__26786_26796 = dommy.core.elem_and_selector.call(null,elem_sel);
var elem_26797 = cljs.core.nth.call(null,vec__26786_26796,(0),null);
var selector_26798 = cljs.core.nth.call(null,vec__26786_26796,(1),null);
var seq__26787_26799 = cljs.core.seq.call(null,cljs.core.partition.call(null,(2),type_fs));
var chunk__26788_26800 = null;
var count__26789_26801 = (0);
var i__26790_26802 = (0);
while(true){
if((i__26790_26802 < count__26789_26801)){
var vec__26791_26803 = cljs.core._nth.call(null,chunk__26788_26800,i__26790_26802);
var type_26804 = cljs.core.nth.call(null,vec__26791_26803,(0),null);
var f_26805 = cljs.core.nth.call(null,vec__26791_26803,(1),null);
dommy.core.listen_BANG_.call(null,elem_sel,type_26804,((function (seq__26787_26799,chunk__26788_26800,count__26789_26801,i__26790_26802,vec__26791_26803,type_26804,f_26805,vec__26786_26796,elem_26797,selector_26798){
return (function dommy$core$this_fn(e){
dommy.core.unlisten_BANG_.call(null,elem_sel,type_26804,dommy$core$this_fn);

return f_26805.call(null,e);
});})(seq__26787_26799,chunk__26788_26800,count__26789_26801,i__26790_26802,vec__26791_26803,type_26804,f_26805,vec__26786_26796,elem_26797,selector_26798))
);

var G__26806 = seq__26787_26799;
var G__26807 = chunk__26788_26800;
var G__26808 = count__26789_26801;
var G__26809 = (i__26790_26802 + (1));
seq__26787_26799 = G__26806;
chunk__26788_26800 = G__26807;
count__26789_26801 = G__26808;
i__26790_26802 = G__26809;
continue;
} else {
var temp__4657__auto___26810 = cljs.core.seq.call(null,seq__26787_26799);
if(temp__4657__auto___26810){
var seq__26787_26811__$1 = temp__4657__auto___26810;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__26787_26811__$1)){
var c__23946__auto___26812 = cljs.core.chunk_first.call(null,seq__26787_26811__$1);
var G__26813 = cljs.core.chunk_rest.call(null,seq__26787_26811__$1);
var G__26814 = c__23946__auto___26812;
var G__26815 = cljs.core.count.call(null,c__23946__auto___26812);
var G__26816 = (0);
seq__26787_26799 = G__26813;
chunk__26788_26800 = G__26814;
count__26789_26801 = G__26815;
i__26790_26802 = G__26816;
continue;
} else {
var vec__26792_26817 = cljs.core.first.call(null,seq__26787_26811__$1);
var type_26818 = cljs.core.nth.call(null,vec__26792_26817,(0),null);
var f_26819 = cljs.core.nth.call(null,vec__26792_26817,(1),null);
dommy.core.listen_BANG_.call(null,elem_sel,type_26818,((function (seq__26787_26799,chunk__26788_26800,count__26789_26801,i__26790_26802,vec__26792_26817,type_26818,f_26819,seq__26787_26811__$1,temp__4657__auto___26810,vec__26786_26796,elem_26797,selector_26798){
return (function dommy$core$this_fn(e){
dommy.core.unlisten_BANG_.call(null,elem_sel,type_26818,dommy$core$this_fn);

return f_26819.call(null,e);
});})(seq__26787_26799,chunk__26788_26800,count__26789_26801,i__26790_26802,vec__26792_26817,type_26818,f_26819,seq__26787_26811__$1,temp__4657__auto___26810,vec__26786_26796,elem_26797,selector_26798))
);

var G__26820 = cljs.core.next.call(null,seq__26787_26811__$1);
var G__26821 = null;
var G__26822 = (0);
var G__26823 = (0);
seq__26787_26799 = G__26820;
chunk__26788_26800 = G__26821;
count__26789_26801 = G__26822;
i__26790_26802 = G__26823;
continue;
}
} else {
}
}
break;
}

return elem_sel;
});

dommy.core.listen_once_BANG_.cljs$lang$maxFixedArity = (1);

dommy.core.listen_once_BANG_.cljs$lang$applyTo = (function (seq26784){
var G__26785 = cljs.core.first.call(null,seq26784);
var seq26784__$1 = cljs.core.next.call(null,seq26784);
return dommy.core.listen_once_BANG_.cljs$core$IFn$_invoke$arity$variadic(G__26785,seq26784__$1);
});

//# sourceMappingURL=core.js.map