// Compiled by ClojureScript 1.7.170 {}
goog.provide('figwheel.client');
goog.require('cljs.core');
goog.require('goog.userAgent.product');
goog.require('goog.Uri');
goog.require('cljs.core.async');
goog.require('figwheel.client.socket');
goog.require('figwheel.client.file_reloading');
goog.require('clojure.string');
goog.require('figwheel.client.utils');
goog.require('cljs.repl');
goog.require('figwheel.client.heads_up');
figwheel.client.figwheel_repl_print = (function figwheel$client$figwheel_repl_print(args){
figwheel.client.socket.send_BANG_.call(null,new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"figwheel-event","figwheel-event",519570592),"callback",new cljs.core.Keyword(null,"callback-name","callback-name",336964714),"figwheel-repl-print",new cljs.core.Keyword(null,"content","content",15833224),args], null));

return args;
});
figwheel.client.autoload_QMARK_ = (cljs.core.truth_(figwheel.client.utils.html_env_QMARK_.call(null))?(function (){
var pred__32890 = cljs.core._EQ_;
var expr__32891 = (function (){var or__23143__auto__ = localStorage.getItem("figwheel_autoload");
if(cljs.core.truth_(or__23143__auto__)){
return or__23143__auto__;
} else {
return "true";
}
})();
if(cljs.core.truth_(pred__32890.call(null,"true",expr__32891))){
return true;
} else {
if(cljs.core.truth_(pred__32890.call(null,"false",expr__32891))){
return false;
} else {
throw (new Error([cljs.core.str("No matching clause: "),cljs.core.str(expr__32891)].join('')));
}
}
}):(function (){
return true;
}));
figwheel.client.toggle_autoload = (function figwheel$client$toggle_autoload(){
if(cljs.core.truth_(figwheel.client.utils.html_env_QMARK_.call(null))){
localStorage.setItem("figwheel_autoload",cljs.core.not.call(null,figwheel.client.autoload_QMARK_.call(null)));

return figwheel.client.utils.log.call(null,new cljs.core.Keyword(null,"info","info",-317069002),[cljs.core.str("Figwheel autoloading "),cljs.core.str((cljs.core.truth_(figwheel.client.autoload_QMARK_.call(null))?"ON":"OFF"))].join(''));
} else {
return null;
}
});
goog.exportSymbol('figwheel.client.toggle_autoload', figwheel.client.toggle_autoload);
figwheel.client.console_print = (function figwheel$client$console_print(args){
console.log.apply(console,cljs.core.into_array.call(null,args));

return args;
});
figwheel.client.repl_print_fn = (function figwheel$client$repl_print_fn(var_args){
var args__24208__auto__ = [];
var len__24201__auto___32894 = arguments.length;
var i__24202__auto___32895 = (0);
while(true){
if((i__24202__auto___32895 < len__24201__auto___32894)){
args__24208__auto__.push((arguments[i__24202__auto___32895]));

var G__32896 = (i__24202__auto___32895 + (1));
i__24202__auto___32895 = G__32896;
continue;
} else {
}
break;
}

var argseq__24209__auto__ = ((((0) < args__24208__auto__.length))?(new cljs.core.IndexedSeq(args__24208__auto__.slice((0)),(0))):null);
return figwheel.client.repl_print_fn.cljs$core$IFn$_invoke$arity$variadic(argseq__24209__auto__);
});

figwheel.client.repl_print_fn.cljs$core$IFn$_invoke$arity$variadic = (function (args){
figwheel.client.figwheel_repl_print.call(null,figwheel.client.console_print.call(null,args));

return null;
});

figwheel.client.repl_print_fn.cljs$lang$maxFixedArity = (0);

figwheel.client.repl_print_fn.cljs$lang$applyTo = (function (seq32893){
return figwheel.client.repl_print_fn.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq.call(null,seq32893));
});
figwheel.client.enable_repl_print_BANG_ = (function figwheel$client$enable_repl_print_BANG_(){
cljs.core._STAR_print_newline_STAR_ = false;

return cljs.core._STAR_print_fn_STAR_ = figwheel.client.repl_print_fn;
});
figwheel.client.get_essential_messages = (function figwheel$client$get_essential_messages(ed){
if(cljs.core.truth_(ed)){
return cljs.core.cons.call(null,cljs.core.select_keys.call(null,ed,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"message","message",-406056002),new cljs.core.Keyword(null,"class","class",-2030961996)], null)),figwheel$client$get_essential_messages.call(null,new cljs.core.Keyword(null,"cause","cause",231901252).cljs$core$IFn$_invoke$arity$1(ed)));
} else {
return null;
}
});
figwheel.client.error_msg_format = (function figwheel$client$error_msg_format(p__32897){
var map__32900 = p__32897;
var map__32900__$1 = ((((!((map__32900 == null)))?((((map__32900.cljs$lang$protocol_mask$partition0$ & (64))) || (map__32900.cljs$core$ISeq$))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__32900):map__32900);
var message = cljs.core.get.call(null,map__32900__$1,new cljs.core.Keyword(null,"message","message",-406056002));
var class$ = cljs.core.get.call(null,map__32900__$1,new cljs.core.Keyword(null,"class","class",-2030961996));
return [cljs.core.str(class$),cljs.core.str(" : "),cljs.core.str(message)].join('');
});
figwheel.client.format_messages = cljs.core.comp.call(null,cljs.core.partial.call(null,cljs.core.map,figwheel.client.error_msg_format),figwheel.client.get_essential_messages);
figwheel.client.focus_msgs = (function figwheel$client$focus_msgs(name_set,msg_hist){
return cljs.core.cons.call(null,cljs.core.first.call(null,msg_hist),cljs.core.filter.call(null,cljs.core.comp.call(null,name_set,new cljs.core.Keyword(null,"msg-name","msg-name",-353709863)),cljs.core.rest.call(null,msg_hist)));
});
figwheel.client.reload_file_QMARK__STAR_ = (function figwheel$client$reload_file_QMARK__STAR_(msg_name,opts){
var or__23143__auto__ = new cljs.core.Keyword(null,"load-warninged-code","load-warninged-code",-2030345223).cljs$core$IFn$_invoke$arity$1(opts);
if(cljs.core.truth_(or__23143__auto__)){
return or__23143__auto__;
} else {
return cljs.core.not_EQ_.call(null,msg_name,new cljs.core.Keyword(null,"compile-warning","compile-warning",43425356));
}
});
figwheel.client.reload_file_state_QMARK_ = (function figwheel$client$reload_file_state_QMARK_(msg_names,opts){
var and__23131__auto__ = cljs.core._EQ_.call(null,cljs.core.first.call(null,msg_names),new cljs.core.Keyword(null,"files-changed","files-changed",-1418200563));
if(and__23131__auto__){
return figwheel.client.reload_file_QMARK__STAR_.call(null,cljs.core.second.call(null,msg_names),opts);
} else {
return and__23131__auto__;
}
});
figwheel.client.block_reload_file_state_QMARK_ = (function figwheel$client$block_reload_file_state_QMARK_(msg_names,opts){
return (cljs.core._EQ_.call(null,cljs.core.first.call(null,msg_names),new cljs.core.Keyword(null,"files-changed","files-changed",-1418200563))) && (cljs.core.not.call(null,figwheel.client.reload_file_QMARK__STAR_.call(null,cljs.core.second.call(null,msg_names),opts)));
});
figwheel.client.warning_append_state_QMARK_ = (function figwheel$client$warning_append_state_QMARK_(msg_names){
return cljs.core._EQ_.call(null,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"compile-warning","compile-warning",43425356),new cljs.core.Keyword(null,"compile-warning","compile-warning",43425356)], null),cljs.core.take.call(null,(2),msg_names));
});
figwheel.client.warning_state_QMARK_ = (function figwheel$client$warning_state_QMARK_(msg_names){
return cljs.core._EQ_.call(null,new cljs.core.Keyword(null,"compile-warning","compile-warning",43425356),cljs.core.first.call(null,msg_names));
});
figwheel.client.rewarning_state_QMARK_ = (function figwheel$client$rewarning_state_QMARK_(msg_names){
return cljs.core._EQ_.call(null,new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"compile-warning","compile-warning",43425356),new cljs.core.Keyword(null,"files-changed","files-changed",-1418200563),new cljs.core.Keyword(null,"compile-warning","compile-warning",43425356)], null),cljs.core.take.call(null,(3),msg_names));
});
figwheel.client.compile_fail_state_QMARK_ = (function figwheel$client$compile_fail_state_QMARK_(msg_names){
return cljs.core._EQ_.call(null,new cljs.core.Keyword(null,"compile-failed","compile-failed",-477639289),cljs.core.first.call(null,msg_names));
});
figwheel.client.compile_refail_state_QMARK_ = (function figwheel$client$compile_refail_state_QMARK_(msg_names){
return cljs.core._EQ_.call(null,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"compile-failed","compile-failed",-477639289),new cljs.core.Keyword(null,"compile-failed","compile-failed",-477639289)], null),cljs.core.take.call(null,(2),msg_names));
});
figwheel.client.css_loaded_state_QMARK_ = (function figwheel$client$css_loaded_state_QMARK_(msg_names){
return cljs.core._EQ_.call(null,new cljs.core.Keyword(null,"css-files-changed","css-files-changed",720773874),cljs.core.first.call(null,msg_names));
});
figwheel.client.file_reloader_plugin = (function figwheel$client$file_reloader_plugin(opts){
var ch = cljs.core.async.chan.call(null);
var c__28489__auto___33062 = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__28489__auto___33062,ch){
return (function (){
var f__28490__auto__ = (function (){var switch__28377__auto__ = ((function (c__28489__auto___33062,ch){
return (function (state_33031){
var state_val_33032 = (state_33031[(1)]);
if((state_val_33032 === (7))){
var inst_33027 = (state_33031[(2)]);
var state_33031__$1 = state_33031;
var statearr_33033_33063 = state_33031__$1;
(statearr_33033_33063[(2)] = inst_33027);

(statearr_33033_33063[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33032 === (1))){
var state_33031__$1 = state_33031;
var statearr_33034_33064 = state_33031__$1;
(statearr_33034_33064[(2)] = null);

(statearr_33034_33064[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33032 === (4))){
var inst_32984 = (state_33031[(7)]);
var inst_32984__$1 = (state_33031[(2)]);
var state_33031__$1 = (function (){var statearr_33035 = state_33031;
(statearr_33035[(7)] = inst_32984__$1);

return statearr_33035;
})();
if(cljs.core.truth_(inst_32984__$1)){
var statearr_33036_33065 = state_33031__$1;
(statearr_33036_33065[(1)] = (5));

} else {
var statearr_33037_33066 = state_33031__$1;
(statearr_33037_33066[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33032 === (15))){
var inst_32991 = (state_33031[(8)]);
var inst_33006 = new cljs.core.Keyword(null,"files","files",-472457450).cljs$core$IFn$_invoke$arity$1(inst_32991);
var inst_33007 = cljs.core.first.call(null,inst_33006);
var inst_33008 = new cljs.core.Keyword(null,"file","file",-1269645878).cljs$core$IFn$_invoke$arity$1(inst_33007);
var inst_33009 = [cljs.core.str("Figwheel: Not loading code with warnings - "),cljs.core.str(inst_33008)].join('');
var inst_33010 = figwheel.client.utils.log.call(null,new cljs.core.Keyword(null,"warn","warn",-436710552),inst_33009);
var state_33031__$1 = state_33031;
var statearr_33038_33067 = state_33031__$1;
(statearr_33038_33067[(2)] = inst_33010);

(statearr_33038_33067[(1)] = (17));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33032 === (13))){
var inst_33015 = (state_33031[(2)]);
var state_33031__$1 = state_33031;
var statearr_33039_33068 = state_33031__$1;
(statearr_33039_33068[(2)] = inst_33015);

(statearr_33039_33068[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33032 === (6))){
var state_33031__$1 = state_33031;
var statearr_33040_33069 = state_33031__$1;
(statearr_33040_33069[(2)] = null);

(statearr_33040_33069[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33032 === (17))){
var inst_33013 = (state_33031[(2)]);
var state_33031__$1 = state_33031;
var statearr_33041_33070 = state_33031__$1;
(statearr_33041_33070[(2)] = inst_33013);

(statearr_33041_33070[(1)] = (13));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33032 === (3))){
var inst_33029 = (state_33031[(2)]);
var state_33031__$1 = state_33031;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_33031__$1,inst_33029);
} else {
if((state_val_33032 === (12))){
var inst_32990 = (state_33031[(9)]);
var inst_33004 = figwheel.client.block_reload_file_state_QMARK_.call(null,inst_32990,opts);
var state_33031__$1 = state_33031;
if(cljs.core.truth_(inst_33004)){
var statearr_33042_33071 = state_33031__$1;
(statearr_33042_33071[(1)] = (15));

} else {
var statearr_33043_33072 = state_33031__$1;
(statearr_33043_33072[(1)] = (16));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33032 === (2))){
var state_33031__$1 = state_33031;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_33031__$1,(4),ch);
} else {
if((state_val_33032 === (11))){
var inst_32991 = (state_33031[(8)]);
var inst_32996 = cljs.core.PersistentVector.EMPTY_NODE;
var inst_32997 = figwheel.client.file_reloading.reload_js_files.call(null,opts,inst_32991);
var inst_32998 = cljs.core.async.timeout.call(null,(1000));
var inst_32999 = [inst_32997,inst_32998];
var inst_33000 = (new cljs.core.PersistentVector(null,2,(5),inst_32996,inst_32999,null));
var state_33031__$1 = state_33031;
return cljs.core.async.ioc_alts_BANG_.call(null,state_33031__$1,(14),inst_33000);
} else {
if((state_val_33032 === (9))){
var inst_32991 = (state_33031[(8)]);
var inst_33017 = figwheel.client.utils.log.call(null,new cljs.core.Keyword(null,"warn","warn",-436710552),"Figwheel: code autoloading is OFF");
var inst_33018 = new cljs.core.Keyword(null,"files","files",-472457450).cljs$core$IFn$_invoke$arity$1(inst_32991);
var inst_33019 = cljs.core.map.call(null,new cljs.core.Keyword(null,"file","file",-1269645878),inst_33018);
var inst_33020 = [cljs.core.str("Not loading: "),cljs.core.str(inst_33019)].join('');
var inst_33021 = figwheel.client.utils.log.call(null,new cljs.core.Keyword(null,"info","info",-317069002),inst_33020);
var state_33031__$1 = (function (){var statearr_33044 = state_33031;
(statearr_33044[(10)] = inst_33017);

return statearr_33044;
})();
var statearr_33045_33073 = state_33031__$1;
(statearr_33045_33073[(2)] = inst_33021);

(statearr_33045_33073[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33032 === (5))){
var inst_32984 = (state_33031[(7)]);
var inst_32986 = [new cljs.core.Keyword(null,"compile-warning","compile-warning",43425356),null,new cljs.core.Keyword(null,"files-changed","files-changed",-1418200563),null];
var inst_32987 = (new cljs.core.PersistentArrayMap(null,2,inst_32986,null));
var inst_32988 = (new cljs.core.PersistentHashSet(null,inst_32987,null));
var inst_32989 = figwheel.client.focus_msgs.call(null,inst_32988,inst_32984);
var inst_32990 = cljs.core.map.call(null,new cljs.core.Keyword(null,"msg-name","msg-name",-353709863),inst_32989);
var inst_32991 = cljs.core.first.call(null,inst_32989);
var inst_32992 = figwheel.client.autoload_QMARK_.call(null);
var state_33031__$1 = (function (){var statearr_33046 = state_33031;
(statearr_33046[(8)] = inst_32991);

(statearr_33046[(9)] = inst_32990);

return statearr_33046;
})();
if(cljs.core.truth_(inst_32992)){
var statearr_33047_33074 = state_33031__$1;
(statearr_33047_33074[(1)] = (8));

} else {
var statearr_33048_33075 = state_33031__$1;
(statearr_33048_33075[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33032 === (14))){
var inst_33002 = (state_33031[(2)]);
var state_33031__$1 = state_33031;
var statearr_33049_33076 = state_33031__$1;
(statearr_33049_33076[(2)] = inst_33002);

(statearr_33049_33076[(1)] = (13));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33032 === (16))){
var state_33031__$1 = state_33031;
var statearr_33050_33077 = state_33031__$1;
(statearr_33050_33077[(2)] = null);

(statearr_33050_33077[(1)] = (17));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33032 === (10))){
var inst_33023 = (state_33031[(2)]);
var state_33031__$1 = (function (){var statearr_33051 = state_33031;
(statearr_33051[(11)] = inst_33023);

return statearr_33051;
})();
var statearr_33052_33078 = state_33031__$1;
(statearr_33052_33078[(2)] = null);

(statearr_33052_33078[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33032 === (8))){
var inst_32990 = (state_33031[(9)]);
var inst_32994 = figwheel.client.reload_file_state_QMARK_.call(null,inst_32990,opts);
var state_33031__$1 = state_33031;
if(cljs.core.truth_(inst_32994)){
var statearr_33053_33079 = state_33031__$1;
(statearr_33053_33079[(1)] = (11));

} else {
var statearr_33054_33080 = state_33031__$1;
(statearr_33054_33080[(1)] = (12));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__28489__auto___33062,ch))
;
return ((function (switch__28377__auto__,c__28489__auto___33062,ch){
return (function() {
var figwheel$client$file_reloader_plugin_$_state_machine__28378__auto__ = null;
var figwheel$client$file_reloader_plugin_$_state_machine__28378__auto____0 = (function (){
var statearr_33058 = [null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_33058[(0)] = figwheel$client$file_reloader_plugin_$_state_machine__28378__auto__);

(statearr_33058[(1)] = (1));

return statearr_33058;
});
var figwheel$client$file_reloader_plugin_$_state_machine__28378__auto____1 = (function (state_33031){
while(true){
var ret_value__28379__auto__ = (function (){try{while(true){
var result__28380__auto__ = switch__28377__auto__.call(null,state_33031);
if(cljs.core.keyword_identical_QMARK_.call(null,result__28380__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__28380__auto__;
}
break;
}
}catch (e33059){if((e33059 instanceof Object)){
var ex__28381__auto__ = e33059;
var statearr_33060_33081 = state_33031;
(statearr_33060_33081[(5)] = ex__28381__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_33031);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e33059;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__28379__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__33082 = state_33031;
state_33031 = G__33082;
continue;
} else {
return ret_value__28379__auto__;
}
break;
}
});
figwheel$client$file_reloader_plugin_$_state_machine__28378__auto__ = function(state_33031){
switch(arguments.length){
case 0:
return figwheel$client$file_reloader_plugin_$_state_machine__28378__auto____0.call(this);
case 1:
return figwheel$client$file_reloader_plugin_$_state_machine__28378__auto____1.call(this,state_33031);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
figwheel$client$file_reloader_plugin_$_state_machine__28378__auto__.cljs$core$IFn$_invoke$arity$0 = figwheel$client$file_reloader_plugin_$_state_machine__28378__auto____0;
figwheel$client$file_reloader_plugin_$_state_machine__28378__auto__.cljs$core$IFn$_invoke$arity$1 = figwheel$client$file_reloader_plugin_$_state_machine__28378__auto____1;
return figwheel$client$file_reloader_plugin_$_state_machine__28378__auto__;
})()
;})(switch__28377__auto__,c__28489__auto___33062,ch))
})();
var state__28491__auto__ = (function (){var statearr_33061 = f__28490__auto__.call(null);
(statearr_33061[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__28489__auto___33062);

return statearr_33061;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__28491__auto__);
});})(c__28489__auto___33062,ch))
);


return ((function (ch){
return (function (msg_hist){
cljs.core.async.put_BANG_.call(null,ch,msg_hist);

return msg_hist;
});
;})(ch))
});
figwheel.client.truncate_stack_trace = (function figwheel$client$truncate_stack_trace(stack_str){
return cljs.core.take_while.call(null,(function (p1__33083_SHARP_){
return cljs.core.not.call(null,cljs.core.re_matches.call(null,/.*eval_javascript_STAR__STAR_.*/,p1__33083_SHARP_));
}),clojure.string.split_lines.call(null,stack_str));
});
figwheel.client.get_ua_product = (function figwheel$client$get_ua_product(){
if(cljs.core.truth_(figwheel.client.utils.node_env_QMARK_.call(null))){
return new cljs.core.Keyword(null,"chrome","chrome",1718738387);
} else {
if(cljs.core.truth_(goog.userAgent.product.SAFARI)){
return new cljs.core.Keyword(null,"safari","safari",497115653);
} else {
if(cljs.core.truth_(goog.userAgent.product.CHROME)){
return new cljs.core.Keyword(null,"chrome","chrome",1718738387);
} else {
if(cljs.core.truth_(goog.userAgent.product.FIREFOX)){
return new cljs.core.Keyword(null,"firefox","firefox",1283768880);
} else {
if(cljs.core.truth_(goog.userAgent.product.IE)){
return new cljs.core.Keyword(null,"ie","ie",2038473780);
} else {
return null;
}
}
}
}
}
});
var base_path_33090 = figwheel.client.utils.base_url_path.call(null);
figwheel.client.eval_javascript_STAR__STAR_ = ((function (base_path_33090){
return (function figwheel$client$eval_javascript_STAR__STAR_(code,opts,result_handler){
try{var _STAR_print_fn_STAR_33088 = cljs.core._STAR_print_fn_STAR_;
var _STAR_print_newline_STAR_33089 = cljs.core._STAR_print_newline_STAR_;
cljs.core._STAR_print_fn_STAR_ = figwheel.client.repl_print_fn;

cljs.core._STAR_print_newline_STAR_ = false;

try{return result_handler.call(null,new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"status","status",-1997798413),new cljs.core.Keyword(null,"success","success",1890645906),new cljs.core.Keyword(null,"ua-product","ua-product",938384227),figwheel.client.get_ua_product.call(null),new cljs.core.Keyword(null,"value","value",305978217),figwheel.client.utils.eval_helper.call(null,code,opts)], null));
}finally {cljs.core._STAR_print_newline_STAR_ = _STAR_print_newline_STAR_33089;

cljs.core._STAR_print_fn_STAR_ = _STAR_print_fn_STAR_33088;
}}catch (e33087){if((e33087 instanceof Error)){
var e = e33087;
return result_handler.call(null,new cljs.core.PersistentArrayMap(null, 5, [new cljs.core.Keyword(null,"status","status",-1997798413),new cljs.core.Keyword(null,"exception","exception",-335277064),new cljs.core.Keyword(null,"value","value",305978217),cljs.core.pr_str.call(null,e),new cljs.core.Keyword(null,"ua-product","ua-product",938384227),figwheel.client.get_ua_product.call(null),new cljs.core.Keyword(null,"stacktrace","stacktrace",-95588394),clojure.string.join.call(null,"\n",figwheel.client.truncate_stack_trace.call(null,e.stack)),new cljs.core.Keyword(null,"base-path","base-path",495760020),base_path_33090], null));
} else {
var e = e33087;
return result_handler.call(null,new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword(null,"status","status",-1997798413),new cljs.core.Keyword(null,"exception","exception",-335277064),new cljs.core.Keyword(null,"ua-product","ua-product",938384227),figwheel.client.get_ua_product.call(null),new cljs.core.Keyword(null,"value","value",305978217),cljs.core.pr_str.call(null,e),new cljs.core.Keyword(null,"stacktrace","stacktrace",-95588394),"No stacktrace available."], null));

}
}});})(base_path_33090))
;
/**
 * The REPL can disconnect and reconnect lets ensure cljs.user exists at least.
 */
figwheel.client.ensure_cljs_user = (function figwheel$client$ensure_cljs_user(){
if(cljs.core.truth_(cljs.user)){
return null;
} else {
return cljs.user = {};
}
});
figwheel.client.repl_plugin = (function figwheel$client$repl_plugin(p__33091){
var map__33098 = p__33091;
var map__33098__$1 = ((((!((map__33098 == null)))?((((map__33098.cljs$lang$protocol_mask$partition0$ & (64))) || (map__33098.cljs$core$ISeq$))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__33098):map__33098);
var opts = map__33098__$1;
var build_id = cljs.core.get.call(null,map__33098__$1,new cljs.core.Keyword(null,"build-id","build-id",1642831089));
return ((function (map__33098,map__33098__$1,opts,build_id){
return (function (p__33100){
var vec__33101 = p__33100;
var map__33102 = cljs.core.nth.call(null,vec__33101,(0),null);
var map__33102__$1 = ((((!((map__33102 == null)))?((((map__33102.cljs$lang$protocol_mask$partition0$ & (64))) || (map__33102.cljs$core$ISeq$))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__33102):map__33102);
var msg = map__33102__$1;
var msg_name = cljs.core.get.call(null,map__33102__$1,new cljs.core.Keyword(null,"msg-name","msg-name",-353709863));
var _ = cljs.core.nthnext.call(null,vec__33101,(1));
if(cljs.core._EQ_.call(null,new cljs.core.Keyword(null,"repl-eval","repl-eval",-1784727398),msg_name)){
figwheel.client.ensure_cljs_user.call(null);

return figwheel.client.eval_javascript_STAR__STAR_.call(null,new cljs.core.Keyword(null,"code","code",1586293142).cljs$core$IFn$_invoke$arity$1(msg),opts,((function (vec__33101,map__33102,map__33102__$1,msg,msg_name,_,map__33098,map__33098__$1,opts,build_id){
return (function (res){
return figwheel.client.socket.send_BANG_.call(null,new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"figwheel-event","figwheel-event",519570592),"callback",new cljs.core.Keyword(null,"callback-name","callback-name",336964714),new cljs.core.Keyword(null,"callback-name","callback-name",336964714).cljs$core$IFn$_invoke$arity$1(msg),new cljs.core.Keyword(null,"content","content",15833224),res], null));
});})(vec__33101,map__33102,map__33102__$1,msg,msg_name,_,map__33098,map__33098__$1,opts,build_id))
);
} else {
return null;
}
});
;})(map__33098,map__33098__$1,opts,build_id))
});
figwheel.client.css_reloader_plugin = (function figwheel$client$css_reloader_plugin(opts){
return (function (p__33108){
var vec__33109 = p__33108;
var map__33110 = cljs.core.nth.call(null,vec__33109,(0),null);
var map__33110__$1 = ((((!((map__33110 == null)))?((((map__33110.cljs$lang$protocol_mask$partition0$ & (64))) || (map__33110.cljs$core$ISeq$))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__33110):map__33110);
var msg = map__33110__$1;
var msg_name = cljs.core.get.call(null,map__33110__$1,new cljs.core.Keyword(null,"msg-name","msg-name",-353709863));
var _ = cljs.core.nthnext.call(null,vec__33109,(1));
if(cljs.core._EQ_.call(null,msg_name,new cljs.core.Keyword(null,"css-files-changed","css-files-changed",720773874))){
return figwheel.client.file_reloading.reload_css_files.call(null,opts,msg);
} else {
return null;
}
});
});
figwheel.client.compile_fail_warning_plugin = (function figwheel$client$compile_fail_warning_plugin(p__33112){
var map__33122 = p__33112;
var map__33122__$1 = ((((!((map__33122 == null)))?((((map__33122.cljs$lang$protocol_mask$partition0$ & (64))) || (map__33122.cljs$core$ISeq$))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__33122):map__33122);
var on_compile_warning = cljs.core.get.call(null,map__33122__$1,new cljs.core.Keyword(null,"on-compile-warning","on-compile-warning",-1195585947));
var on_compile_fail = cljs.core.get.call(null,map__33122__$1,new cljs.core.Keyword(null,"on-compile-fail","on-compile-fail",728013036));
return ((function (map__33122,map__33122__$1,on_compile_warning,on_compile_fail){
return (function (p__33124){
var vec__33125 = p__33124;
var map__33126 = cljs.core.nth.call(null,vec__33125,(0),null);
var map__33126__$1 = ((((!((map__33126 == null)))?((((map__33126.cljs$lang$protocol_mask$partition0$ & (64))) || (map__33126.cljs$core$ISeq$))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__33126):map__33126);
var msg = map__33126__$1;
var msg_name = cljs.core.get.call(null,map__33126__$1,new cljs.core.Keyword(null,"msg-name","msg-name",-353709863));
var _ = cljs.core.nthnext.call(null,vec__33125,(1));
var pred__33128 = cljs.core._EQ_;
var expr__33129 = msg_name;
if(cljs.core.truth_(pred__33128.call(null,new cljs.core.Keyword(null,"compile-warning","compile-warning",43425356),expr__33129))){
return on_compile_warning.call(null,msg);
} else {
if(cljs.core.truth_(pred__33128.call(null,new cljs.core.Keyword(null,"compile-failed","compile-failed",-477639289),expr__33129))){
return on_compile_fail.call(null,msg);
} else {
return null;
}
}
});
;})(map__33122,map__33122__$1,on_compile_warning,on_compile_fail))
});
figwheel.client.heads_up_plugin_msg_handler = (function figwheel$client$heads_up_plugin_msg_handler(opts,msg_hist_SINGLEQUOTE_){
var msg_hist = figwheel.client.focus_msgs.call(null,new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"compile-failed","compile-failed",-477639289),null,new cljs.core.Keyword(null,"compile-warning","compile-warning",43425356),null,new cljs.core.Keyword(null,"files-changed","files-changed",-1418200563),null], null), null),msg_hist_SINGLEQUOTE_);
var msg_names = cljs.core.map.call(null,new cljs.core.Keyword(null,"msg-name","msg-name",-353709863),msg_hist);
var msg = cljs.core.first.call(null,msg_hist);
var c__28489__auto__ = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__28489__auto__,msg_hist,msg_names,msg){
return (function (){
var f__28490__auto__ = (function (){var switch__28377__auto__ = ((function (c__28489__auto__,msg_hist,msg_names,msg){
return (function (state_33345){
var state_val_33346 = (state_33345[(1)]);
if((state_val_33346 === (7))){
var inst_33269 = (state_33345[(2)]);
var state_33345__$1 = state_33345;
if(cljs.core.truth_(inst_33269)){
var statearr_33347_33393 = state_33345__$1;
(statearr_33347_33393[(1)] = (8));

} else {
var statearr_33348_33394 = state_33345__$1;
(statearr_33348_33394[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33346 === (20))){
var inst_33339 = (state_33345[(2)]);
var state_33345__$1 = state_33345;
var statearr_33349_33395 = state_33345__$1;
(statearr_33349_33395[(2)] = inst_33339);

(statearr_33349_33395[(1)] = (15));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33346 === (27))){
var inst_33335 = (state_33345[(2)]);
var state_33345__$1 = state_33345;
var statearr_33350_33396 = state_33345__$1;
(statearr_33350_33396[(2)] = inst_33335);

(statearr_33350_33396[(1)] = (24));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33346 === (1))){
var inst_33262 = figwheel.client.reload_file_state_QMARK_.call(null,msg_names,opts);
var state_33345__$1 = state_33345;
if(cljs.core.truth_(inst_33262)){
var statearr_33351_33397 = state_33345__$1;
(statearr_33351_33397[(1)] = (2));

} else {
var statearr_33352_33398 = state_33345__$1;
(statearr_33352_33398[(1)] = (3));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33346 === (24))){
var inst_33337 = (state_33345[(2)]);
var state_33345__$1 = state_33345;
var statearr_33353_33399 = state_33345__$1;
(statearr_33353_33399[(2)] = inst_33337);

(statearr_33353_33399[(1)] = (20));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33346 === (4))){
var inst_33343 = (state_33345[(2)]);
var state_33345__$1 = state_33345;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_33345__$1,inst_33343);
} else {
if((state_val_33346 === (15))){
var inst_33341 = (state_33345[(2)]);
var state_33345__$1 = state_33345;
var statearr_33354_33400 = state_33345__$1;
(statearr_33354_33400[(2)] = inst_33341);

(statearr_33354_33400[(1)] = (4));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33346 === (21))){
var inst_33300 = (state_33345[(2)]);
var state_33345__$1 = state_33345;
var statearr_33355_33401 = state_33345__$1;
(statearr_33355_33401[(2)] = inst_33300);

(statearr_33355_33401[(1)] = (20));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33346 === (31))){
var inst_33324 = figwheel.client.css_loaded_state_QMARK_.call(null,msg_names);
var state_33345__$1 = state_33345;
if(cljs.core.truth_(inst_33324)){
var statearr_33356_33402 = state_33345__$1;
(statearr_33356_33402[(1)] = (34));

} else {
var statearr_33357_33403 = state_33345__$1;
(statearr_33357_33403[(1)] = (35));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33346 === (32))){
var inst_33333 = (state_33345[(2)]);
var state_33345__$1 = state_33345;
var statearr_33358_33404 = state_33345__$1;
(statearr_33358_33404[(2)] = inst_33333);

(statearr_33358_33404[(1)] = (27));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33346 === (33))){
var inst_33322 = (state_33345[(2)]);
var state_33345__$1 = state_33345;
var statearr_33359_33405 = state_33345__$1;
(statearr_33359_33405[(2)] = inst_33322);

(statearr_33359_33405[(1)] = (32));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33346 === (13))){
var inst_33283 = figwheel.client.heads_up.clear.call(null);
var state_33345__$1 = state_33345;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_33345__$1,(16),inst_33283);
} else {
if((state_val_33346 === (22))){
var inst_33304 = new cljs.core.Keyword(null,"message","message",-406056002).cljs$core$IFn$_invoke$arity$1(msg);
var inst_33305 = figwheel.client.heads_up.append_message.call(null,inst_33304);
var state_33345__$1 = state_33345;
var statearr_33360_33406 = state_33345__$1;
(statearr_33360_33406[(2)] = inst_33305);

(statearr_33360_33406[(1)] = (24));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33346 === (36))){
var inst_33331 = (state_33345[(2)]);
var state_33345__$1 = state_33345;
var statearr_33361_33407 = state_33345__$1;
(statearr_33361_33407[(2)] = inst_33331);

(statearr_33361_33407[(1)] = (32));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33346 === (29))){
var inst_33315 = (state_33345[(2)]);
var state_33345__$1 = state_33345;
var statearr_33362_33408 = state_33345__$1;
(statearr_33362_33408[(2)] = inst_33315);

(statearr_33362_33408[(1)] = (27));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33346 === (6))){
var inst_33264 = (state_33345[(7)]);
var state_33345__$1 = state_33345;
var statearr_33363_33409 = state_33345__$1;
(statearr_33363_33409[(2)] = inst_33264);

(statearr_33363_33409[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33346 === (28))){
var inst_33311 = (state_33345[(2)]);
var inst_33312 = new cljs.core.Keyword(null,"message","message",-406056002).cljs$core$IFn$_invoke$arity$1(msg);
var inst_33313 = figwheel.client.heads_up.display_warning.call(null,inst_33312);
var state_33345__$1 = (function (){var statearr_33364 = state_33345;
(statearr_33364[(8)] = inst_33311);

return statearr_33364;
})();
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_33345__$1,(29),inst_33313);
} else {
if((state_val_33346 === (25))){
var inst_33309 = figwheel.client.heads_up.clear.call(null);
var state_33345__$1 = state_33345;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_33345__$1,(28),inst_33309);
} else {
if((state_val_33346 === (34))){
var inst_33326 = figwheel.client.heads_up.flash_loaded.call(null);
var state_33345__$1 = state_33345;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_33345__$1,(37),inst_33326);
} else {
if((state_val_33346 === (17))){
var inst_33291 = (state_33345[(2)]);
var state_33345__$1 = state_33345;
var statearr_33365_33410 = state_33345__$1;
(statearr_33365_33410[(2)] = inst_33291);

(statearr_33365_33410[(1)] = (15));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33346 === (3))){
var inst_33281 = figwheel.client.compile_refail_state_QMARK_.call(null,msg_names);
var state_33345__$1 = state_33345;
if(cljs.core.truth_(inst_33281)){
var statearr_33366_33411 = state_33345__$1;
(statearr_33366_33411[(1)] = (13));

} else {
var statearr_33367_33412 = state_33345__$1;
(statearr_33367_33412[(1)] = (14));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33346 === (12))){
var inst_33277 = (state_33345[(2)]);
var state_33345__$1 = state_33345;
var statearr_33368_33413 = state_33345__$1;
(statearr_33368_33413[(2)] = inst_33277);

(statearr_33368_33413[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33346 === (2))){
var inst_33264 = (state_33345[(7)]);
var inst_33264__$1 = figwheel.client.autoload_QMARK_.call(null);
var state_33345__$1 = (function (){var statearr_33369 = state_33345;
(statearr_33369[(7)] = inst_33264__$1);

return statearr_33369;
})();
if(cljs.core.truth_(inst_33264__$1)){
var statearr_33370_33414 = state_33345__$1;
(statearr_33370_33414[(1)] = (5));

} else {
var statearr_33371_33415 = state_33345__$1;
(statearr_33371_33415[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33346 === (23))){
var inst_33307 = figwheel.client.rewarning_state_QMARK_.call(null,msg_names);
var state_33345__$1 = state_33345;
if(cljs.core.truth_(inst_33307)){
var statearr_33372_33416 = state_33345__$1;
(statearr_33372_33416[(1)] = (25));

} else {
var statearr_33373_33417 = state_33345__$1;
(statearr_33373_33417[(1)] = (26));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33346 === (35))){
var state_33345__$1 = state_33345;
var statearr_33374_33418 = state_33345__$1;
(statearr_33374_33418[(2)] = null);

(statearr_33374_33418[(1)] = (36));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33346 === (19))){
var inst_33302 = figwheel.client.warning_append_state_QMARK_.call(null,msg_names);
var state_33345__$1 = state_33345;
if(cljs.core.truth_(inst_33302)){
var statearr_33375_33419 = state_33345__$1;
(statearr_33375_33419[(1)] = (22));

} else {
var statearr_33376_33420 = state_33345__$1;
(statearr_33376_33420[(1)] = (23));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33346 === (11))){
var inst_33273 = (state_33345[(2)]);
var state_33345__$1 = state_33345;
var statearr_33377_33421 = state_33345__$1;
(statearr_33377_33421[(2)] = inst_33273);

(statearr_33377_33421[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33346 === (9))){
var inst_33275 = figwheel.client.heads_up.clear.call(null);
var state_33345__$1 = state_33345;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_33345__$1,(12),inst_33275);
} else {
if((state_val_33346 === (5))){
var inst_33266 = new cljs.core.Keyword(null,"autoload","autoload",-354122500).cljs$core$IFn$_invoke$arity$1(opts);
var state_33345__$1 = state_33345;
var statearr_33378_33422 = state_33345__$1;
(statearr_33378_33422[(2)] = inst_33266);

(statearr_33378_33422[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33346 === (14))){
var inst_33293 = figwheel.client.compile_fail_state_QMARK_.call(null,msg_names);
var state_33345__$1 = state_33345;
if(cljs.core.truth_(inst_33293)){
var statearr_33379_33423 = state_33345__$1;
(statearr_33379_33423[(1)] = (18));

} else {
var statearr_33380_33424 = state_33345__$1;
(statearr_33380_33424[(1)] = (19));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33346 === (26))){
var inst_33317 = figwheel.client.warning_state_QMARK_.call(null,msg_names);
var state_33345__$1 = state_33345;
if(cljs.core.truth_(inst_33317)){
var statearr_33381_33425 = state_33345__$1;
(statearr_33381_33425[(1)] = (30));

} else {
var statearr_33382_33426 = state_33345__$1;
(statearr_33382_33426[(1)] = (31));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33346 === (16))){
var inst_33285 = (state_33345[(2)]);
var inst_33286 = new cljs.core.Keyword(null,"exception-data","exception-data",-512474886).cljs$core$IFn$_invoke$arity$1(msg);
var inst_33287 = figwheel.client.format_messages.call(null,inst_33286);
var inst_33288 = new cljs.core.Keyword(null,"cause","cause",231901252).cljs$core$IFn$_invoke$arity$1(msg);
var inst_33289 = figwheel.client.heads_up.display_error.call(null,inst_33287,inst_33288);
var state_33345__$1 = (function (){var statearr_33383 = state_33345;
(statearr_33383[(9)] = inst_33285);

return statearr_33383;
})();
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_33345__$1,(17),inst_33289);
} else {
if((state_val_33346 === (30))){
var inst_33319 = new cljs.core.Keyword(null,"message","message",-406056002).cljs$core$IFn$_invoke$arity$1(msg);
var inst_33320 = figwheel.client.heads_up.display_warning.call(null,inst_33319);
var state_33345__$1 = state_33345;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_33345__$1,(33),inst_33320);
} else {
if((state_val_33346 === (10))){
var inst_33279 = (state_33345[(2)]);
var state_33345__$1 = state_33345;
var statearr_33384_33427 = state_33345__$1;
(statearr_33384_33427[(2)] = inst_33279);

(statearr_33384_33427[(1)] = (4));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33346 === (18))){
var inst_33295 = new cljs.core.Keyword(null,"exception-data","exception-data",-512474886).cljs$core$IFn$_invoke$arity$1(msg);
var inst_33296 = figwheel.client.format_messages.call(null,inst_33295);
var inst_33297 = new cljs.core.Keyword(null,"cause","cause",231901252).cljs$core$IFn$_invoke$arity$1(msg);
var inst_33298 = figwheel.client.heads_up.display_error.call(null,inst_33296,inst_33297);
var state_33345__$1 = state_33345;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_33345__$1,(21),inst_33298);
} else {
if((state_val_33346 === (37))){
var inst_33328 = (state_33345[(2)]);
var state_33345__$1 = state_33345;
var statearr_33385_33428 = state_33345__$1;
(statearr_33385_33428[(2)] = inst_33328);

(statearr_33385_33428[(1)] = (36));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33346 === (8))){
var inst_33271 = figwheel.client.heads_up.flash_loaded.call(null);
var state_33345__$1 = state_33345;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_33345__$1,(11),inst_33271);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__28489__auto__,msg_hist,msg_names,msg))
;
return ((function (switch__28377__auto__,c__28489__auto__,msg_hist,msg_names,msg){
return (function() {
var figwheel$client$heads_up_plugin_msg_handler_$_state_machine__28378__auto__ = null;
var figwheel$client$heads_up_plugin_msg_handler_$_state_machine__28378__auto____0 = (function (){
var statearr_33389 = [null,null,null,null,null,null,null,null,null,null];
(statearr_33389[(0)] = figwheel$client$heads_up_plugin_msg_handler_$_state_machine__28378__auto__);

(statearr_33389[(1)] = (1));

return statearr_33389;
});
var figwheel$client$heads_up_plugin_msg_handler_$_state_machine__28378__auto____1 = (function (state_33345){
while(true){
var ret_value__28379__auto__ = (function (){try{while(true){
var result__28380__auto__ = switch__28377__auto__.call(null,state_33345);
if(cljs.core.keyword_identical_QMARK_.call(null,result__28380__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__28380__auto__;
}
break;
}
}catch (e33390){if((e33390 instanceof Object)){
var ex__28381__auto__ = e33390;
var statearr_33391_33429 = state_33345;
(statearr_33391_33429[(5)] = ex__28381__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_33345);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e33390;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__28379__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__33430 = state_33345;
state_33345 = G__33430;
continue;
} else {
return ret_value__28379__auto__;
}
break;
}
});
figwheel$client$heads_up_plugin_msg_handler_$_state_machine__28378__auto__ = function(state_33345){
switch(arguments.length){
case 0:
return figwheel$client$heads_up_plugin_msg_handler_$_state_machine__28378__auto____0.call(this);
case 1:
return figwheel$client$heads_up_plugin_msg_handler_$_state_machine__28378__auto____1.call(this,state_33345);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
figwheel$client$heads_up_plugin_msg_handler_$_state_machine__28378__auto__.cljs$core$IFn$_invoke$arity$0 = figwheel$client$heads_up_plugin_msg_handler_$_state_machine__28378__auto____0;
figwheel$client$heads_up_plugin_msg_handler_$_state_machine__28378__auto__.cljs$core$IFn$_invoke$arity$1 = figwheel$client$heads_up_plugin_msg_handler_$_state_machine__28378__auto____1;
return figwheel$client$heads_up_plugin_msg_handler_$_state_machine__28378__auto__;
})()
;})(switch__28377__auto__,c__28489__auto__,msg_hist,msg_names,msg))
})();
var state__28491__auto__ = (function (){var statearr_33392 = f__28490__auto__.call(null);
(statearr_33392[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__28489__auto__);

return statearr_33392;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__28491__auto__);
});})(c__28489__auto__,msg_hist,msg_names,msg))
);

return c__28489__auto__;
});
figwheel.client.heads_up_plugin = (function figwheel$client$heads_up_plugin(opts){
var ch = cljs.core.async.chan.call(null);
figwheel.client.heads_up_config_options_STAR__STAR_ = opts;

var c__28489__auto___33493 = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__28489__auto___33493,ch){
return (function (){
var f__28490__auto__ = (function (){var switch__28377__auto__ = ((function (c__28489__auto___33493,ch){
return (function (state_33476){
var state_val_33477 = (state_33476[(1)]);
if((state_val_33477 === (1))){
var state_33476__$1 = state_33476;
var statearr_33478_33494 = state_33476__$1;
(statearr_33478_33494[(2)] = null);

(statearr_33478_33494[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33477 === (2))){
var state_33476__$1 = state_33476;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_33476__$1,(4),ch);
} else {
if((state_val_33477 === (3))){
var inst_33474 = (state_33476[(2)]);
var state_33476__$1 = state_33476;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_33476__$1,inst_33474);
} else {
if((state_val_33477 === (4))){
var inst_33464 = (state_33476[(7)]);
var inst_33464__$1 = (state_33476[(2)]);
var state_33476__$1 = (function (){var statearr_33479 = state_33476;
(statearr_33479[(7)] = inst_33464__$1);

return statearr_33479;
})();
if(cljs.core.truth_(inst_33464__$1)){
var statearr_33480_33495 = state_33476__$1;
(statearr_33480_33495[(1)] = (5));

} else {
var statearr_33481_33496 = state_33476__$1;
(statearr_33481_33496[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33477 === (5))){
var inst_33464 = (state_33476[(7)]);
var inst_33466 = figwheel.client.heads_up_plugin_msg_handler.call(null,opts,inst_33464);
var state_33476__$1 = state_33476;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_33476__$1,(8),inst_33466);
} else {
if((state_val_33477 === (6))){
var state_33476__$1 = state_33476;
var statearr_33482_33497 = state_33476__$1;
(statearr_33482_33497[(2)] = null);

(statearr_33482_33497[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33477 === (7))){
var inst_33472 = (state_33476[(2)]);
var state_33476__$1 = state_33476;
var statearr_33483_33498 = state_33476__$1;
(statearr_33483_33498[(2)] = inst_33472);

(statearr_33483_33498[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33477 === (8))){
var inst_33468 = (state_33476[(2)]);
var state_33476__$1 = (function (){var statearr_33484 = state_33476;
(statearr_33484[(8)] = inst_33468);

return statearr_33484;
})();
var statearr_33485_33499 = state_33476__$1;
(statearr_33485_33499[(2)] = null);

(statearr_33485_33499[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
});})(c__28489__auto___33493,ch))
;
return ((function (switch__28377__auto__,c__28489__auto___33493,ch){
return (function() {
var figwheel$client$heads_up_plugin_$_state_machine__28378__auto__ = null;
var figwheel$client$heads_up_plugin_$_state_machine__28378__auto____0 = (function (){
var statearr_33489 = [null,null,null,null,null,null,null,null,null];
(statearr_33489[(0)] = figwheel$client$heads_up_plugin_$_state_machine__28378__auto__);

(statearr_33489[(1)] = (1));

return statearr_33489;
});
var figwheel$client$heads_up_plugin_$_state_machine__28378__auto____1 = (function (state_33476){
while(true){
var ret_value__28379__auto__ = (function (){try{while(true){
var result__28380__auto__ = switch__28377__auto__.call(null,state_33476);
if(cljs.core.keyword_identical_QMARK_.call(null,result__28380__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__28380__auto__;
}
break;
}
}catch (e33490){if((e33490 instanceof Object)){
var ex__28381__auto__ = e33490;
var statearr_33491_33500 = state_33476;
(statearr_33491_33500[(5)] = ex__28381__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_33476);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e33490;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__28379__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__33501 = state_33476;
state_33476 = G__33501;
continue;
} else {
return ret_value__28379__auto__;
}
break;
}
});
figwheel$client$heads_up_plugin_$_state_machine__28378__auto__ = function(state_33476){
switch(arguments.length){
case 0:
return figwheel$client$heads_up_plugin_$_state_machine__28378__auto____0.call(this);
case 1:
return figwheel$client$heads_up_plugin_$_state_machine__28378__auto____1.call(this,state_33476);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
figwheel$client$heads_up_plugin_$_state_machine__28378__auto__.cljs$core$IFn$_invoke$arity$0 = figwheel$client$heads_up_plugin_$_state_machine__28378__auto____0;
figwheel$client$heads_up_plugin_$_state_machine__28378__auto__.cljs$core$IFn$_invoke$arity$1 = figwheel$client$heads_up_plugin_$_state_machine__28378__auto____1;
return figwheel$client$heads_up_plugin_$_state_machine__28378__auto__;
})()
;})(switch__28377__auto__,c__28489__auto___33493,ch))
})();
var state__28491__auto__ = (function (){var statearr_33492 = f__28490__auto__.call(null);
(statearr_33492[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__28489__auto___33493);

return statearr_33492;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__28491__auto__);
});})(c__28489__auto___33493,ch))
);


figwheel.client.heads_up.ensure_container.call(null);

return ((function (ch){
return (function (msg_hist){
cljs.core.async.put_BANG_.call(null,ch,msg_hist);

return msg_hist;
});
;})(ch))
});
figwheel.client.enforce_project_plugin = (function figwheel$client$enforce_project_plugin(opts){
return (function (msg_hist){
if(((1) < cljs.core.count.call(null,cljs.core.set.call(null,cljs.core.keep.call(null,new cljs.core.Keyword(null,"project-id","project-id",206449307),cljs.core.take.call(null,(5),msg_hist)))))){
figwheel.client.socket.close_BANG_.call(null);

console.error("Figwheel: message received from different project. Shutting socket down.");

if(cljs.core.truth_(new cljs.core.Keyword(null,"heads-up-display","heads-up-display",-896577202).cljs$core$IFn$_invoke$arity$1(opts))){
var c__28489__auto__ = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__28489__auto__){
return (function (){
var f__28490__auto__ = (function (){var switch__28377__auto__ = ((function (c__28489__auto__){
return (function (state_33522){
var state_val_33523 = (state_33522[(1)]);
if((state_val_33523 === (1))){
var inst_33517 = cljs.core.async.timeout.call(null,(3000));
var state_33522__$1 = state_33522;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_33522__$1,(2),inst_33517);
} else {
if((state_val_33523 === (2))){
var inst_33519 = (state_33522[(2)]);
var inst_33520 = figwheel.client.heads_up.display_system_warning.call(null,"Connection from different project","Shutting connection down!!!!!");
var state_33522__$1 = (function (){var statearr_33524 = state_33522;
(statearr_33524[(7)] = inst_33519);

return statearr_33524;
})();
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_33522__$1,inst_33520);
} else {
return null;
}
}
});})(c__28489__auto__))
;
return ((function (switch__28377__auto__,c__28489__auto__){
return (function() {
var figwheel$client$enforce_project_plugin_$_state_machine__28378__auto__ = null;
var figwheel$client$enforce_project_plugin_$_state_machine__28378__auto____0 = (function (){
var statearr_33528 = [null,null,null,null,null,null,null,null];
(statearr_33528[(0)] = figwheel$client$enforce_project_plugin_$_state_machine__28378__auto__);

(statearr_33528[(1)] = (1));

return statearr_33528;
});
var figwheel$client$enforce_project_plugin_$_state_machine__28378__auto____1 = (function (state_33522){
while(true){
var ret_value__28379__auto__ = (function (){try{while(true){
var result__28380__auto__ = switch__28377__auto__.call(null,state_33522);
if(cljs.core.keyword_identical_QMARK_.call(null,result__28380__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__28380__auto__;
}
break;
}
}catch (e33529){if((e33529 instanceof Object)){
var ex__28381__auto__ = e33529;
var statearr_33530_33532 = state_33522;
(statearr_33530_33532[(5)] = ex__28381__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_33522);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e33529;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__28379__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__33533 = state_33522;
state_33522 = G__33533;
continue;
} else {
return ret_value__28379__auto__;
}
break;
}
});
figwheel$client$enforce_project_plugin_$_state_machine__28378__auto__ = function(state_33522){
switch(arguments.length){
case 0:
return figwheel$client$enforce_project_plugin_$_state_machine__28378__auto____0.call(this);
case 1:
return figwheel$client$enforce_project_plugin_$_state_machine__28378__auto____1.call(this,state_33522);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
figwheel$client$enforce_project_plugin_$_state_machine__28378__auto__.cljs$core$IFn$_invoke$arity$0 = figwheel$client$enforce_project_plugin_$_state_machine__28378__auto____0;
figwheel$client$enforce_project_plugin_$_state_machine__28378__auto__.cljs$core$IFn$_invoke$arity$1 = figwheel$client$enforce_project_plugin_$_state_machine__28378__auto____1;
return figwheel$client$enforce_project_plugin_$_state_machine__28378__auto__;
})()
;})(switch__28377__auto__,c__28489__auto__))
})();
var state__28491__auto__ = (function (){var statearr_33531 = f__28490__auto__.call(null);
(statearr_33531[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__28489__auto__);

return statearr_33531;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__28491__auto__);
});})(c__28489__auto__))
);

return c__28489__auto__;
} else {
return null;
}
} else {
return null;
}
});
});
figwheel.client.default_on_jsload = cljs.core.identity;
figwheel.client.default_on_compile_fail = (function figwheel$client$default_on_compile_fail(p__33534){
var map__33541 = p__33534;
var map__33541__$1 = ((((!((map__33541 == null)))?((((map__33541.cljs$lang$protocol_mask$partition0$ & (64))) || (map__33541.cljs$core$ISeq$))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__33541):map__33541);
var ed = map__33541__$1;
var formatted_exception = cljs.core.get.call(null,map__33541__$1,new cljs.core.Keyword(null,"formatted-exception","formatted-exception",-116489026));
var exception_data = cljs.core.get.call(null,map__33541__$1,new cljs.core.Keyword(null,"exception-data","exception-data",-512474886));
var cause = cljs.core.get.call(null,map__33541__$1,new cljs.core.Keyword(null,"cause","cause",231901252));
figwheel.client.utils.log.call(null,new cljs.core.Keyword(null,"debug","debug",-1608172596),"Figwheel: Compile Exception");

var seq__33543_33547 = cljs.core.seq.call(null,figwheel.client.format_messages.call(null,exception_data));
var chunk__33544_33548 = null;
var count__33545_33549 = (0);
var i__33546_33550 = (0);
while(true){
if((i__33546_33550 < count__33545_33549)){
var msg_33551 = cljs.core._nth.call(null,chunk__33544_33548,i__33546_33550);
figwheel.client.utils.log.call(null,new cljs.core.Keyword(null,"info","info",-317069002),msg_33551);

var G__33552 = seq__33543_33547;
var G__33553 = chunk__33544_33548;
var G__33554 = count__33545_33549;
var G__33555 = (i__33546_33550 + (1));
seq__33543_33547 = G__33552;
chunk__33544_33548 = G__33553;
count__33545_33549 = G__33554;
i__33546_33550 = G__33555;
continue;
} else {
var temp__4657__auto___33556 = cljs.core.seq.call(null,seq__33543_33547);
if(temp__4657__auto___33556){
var seq__33543_33557__$1 = temp__4657__auto___33556;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__33543_33557__$1)){
var c__23946__auto___33558 = cljs.core.chunk_first.call(null,seq__33543_33557__$1);
var G__33559 = cljs.core.chunk_rest.call(null,seq__33543_33557__$1);
var G__33560 = c__23946__auto___33558;
var G__33561 = cljs.core.count.call(null,c__23946__auto___33558);
var G__33562 = (0);
seq__33543_33547 = G__33559;
chunk__33544_33548 = G__33560;
count__33545_33549 = G__33561;
i__33546_33550 = G__33562;
continue;
} else {
var msg_33563 = cljs.core.first.call(null,seq__33543_33557__$1);
figwheel.client.utils.log.call(null,new cljs.core.Keyword(null,"info","info",-317069002),msg_33563);

var G__33564 = cljs.core.next.call(null,seq__33543_33557__$1);
var G__33565 = null;
var G__33566 = (0);
var G__33567 = (0);
seq__33543_33547 = G__33564;
chunk__33544_33548 = G__33565;
count__33545_33549 = G__33566;
i__33546_33550 = G__33567;
continue;
}
} else {
}
}
break;
}

if(cljs.core.truth_(cause)){
figwheel.client.utils.log.call(null,new cljs.core.Keyword(null,"info","info",-317069002),[cljs.core.str("Error on file "),cljs.core.str(new cljs.core.Keyword(null,"file","file",-1269645878).cljs$core$IFn$_invoke$arity$1(cause)),cljs.core.str(", line "),cljs.core.str(new cljs.core.Keyword(null,"line","line",212345235).cljs$core$IFn$_invoke$arity$1(cause)),cljs.core.str(", column "),cljs.core.str(new cljs.core.Keyword(null,"column","column",2078222095).cljs$core$IFn$_invoke$arity$1(cause))].join(''));
} else {
}

return ed;
});
figwheel.client.default_on_compile_warning = (function figwheel$client$default_on_compile_warning(p__33568){
var map__33571 = p__33568;
var map__33571__$1 = ((((!((map__33571 == null)))?((((map__33571.cljs$lang$protocol_mask$partition0$ & (64))) || (map__33571.cljs$core$ISeq$))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__33571):map__33571);
var w = map__33571__$1;
var message = cljs.core.get.call(null,map__33571__$1,new cljs.core.Keyword(null,"message","message",-406056002));
figwheel.client.utils.log.call(null,new cljs.core.Keyword(null,"warn","warn",-436710552),[cljs.core.str("Figwheel: Compile Warning - "),cljs.core.str(message)].join(''));

return w;
});
figwheel.client.default_before_load = (function figwheel$client$default_before_load(files){
figwheel.client.utils.log.call(null,new cljs.core.Keyword(null,"debug","debug",-1608172596),"Figwheel: notified of file changes");

return files;
});
figwheel.client.default_on_cssload = (function figwheel$client$default_on_cssload(files){
figwheel.client.utils.log.call(null,new cljs.core.Keyword(null,"debug","debug",-1608172596),"Figwheel: loaded CSS files");

figwheel.client.utils.log.call(null,new cljs.core.Keyword(null,"info","info",-317069002),cljs.core.pr_str.call(null,cljs.core.map.call(null,new cljs.core.Keyword(null,"file","file",-1269645878),files)));

return files;
});
if(typeof figwheel.client.config_defaults !== 'undefined'){
} else {
figwheel.client.config_defaults = cljs.core.PersistentHashMap.fromArrays([new cljs.core.Keyword(null,"on-compile-warning","on-compile-warning",-1195585947),new cljs.core.Keyword(null,"on-jsload","on-jsload",-395756602),new cljs.core.Keyword(null,"reload-dependents","reload-dependents",-956865430),new cljs.core.Keyword(null,"on-compile-fail","on-compile-fail",728013036),new cljs.core.Keyword(null,"debug","debug",-1608172596),new cljs.core.Keyword(null,"heads-up-display","heads-up-display",-896577202),new cljs.core.Keyword(null,"websocket-url","websocket-url",-490444938),new cljs.core.Keyword(null,"before-jsload","before-jsload",-847513128),new cljs.core.Keyword(null,"load-warninged-code","load-warninged-code",-2030345223),new cljs.core.Keyword(null,"eval-fn","eval-fn",-1111644294),new cljs.core.Keyword(null,"retry-count","retry-count",1936122875),new cljs.core.Keyword(null,"autoload","autoload",-354122500),new cljs.core.Keyword(null,"on-cssload","on-cssload",1825432318)],[figwheel.client.default_on_compile_warning,figwheel.client.default_on_jsload,true,figwheel.client.default_on_compile_fail,false,true,[cljs.core.str("ws://"),cljs.core.str((cljs.core.truth_(figwheel.client.utils.html_env_QMARK_.call(null))?location.host:"localhost:3449")),cljs.core.str("/figwheel-ws")].join(''),figwheel.client.default_before_load,false,false,(100),true,figwheel.client.default_on_cssload]);
}
figwheel.client.handle_deprecated_jsload_callback = (function figwheel$client$handle_deprecated_jsload_callback(config){
if(cljs.core.truth_(new cljs.core.Keyword(null,"jsload-callback","jsload-callback",-1949628369).cljs$core$IFn$_invoke$arity$1(config))){
return cljs.core.dissoc.call(null,cljs.core.assoc.call(null,config,new cljs.core.Keyword(null,"on-jsload","on-jsload",-395756602),new cljs.core.Keyword(null,"jsload-callback","jsload-callback",-1949628369).cljs$core$IFn$_invoke$arity$1(config)),new cljs.core.Keyword(null,"jsload-callback","jsload-callback",-1949628369));
} else {
return config;
}
});
figwheel.client.base_plugins = (function figwheel$client$base_plugins(system_options){
var base = new cljs.core.PersistentArrayMap(null, 5, [new cljs.core.Keyword(null,"enforce-project-plugin","enforce-project-plugin",959402899),figwheel.client.enforce_project_plugin,new cljs.core.Keyword(null,"file-reloader-plugin","file-reloader-plugin",-1792964733),figwheel.client.file_reloader_plugin,new cljs.core.Keyword(null,"comp-fail-warning-plugin","comp-fail-warning-plugin",634311),figwheel.client.compile_fail_warning_plugin,new cljs.core.Keyword(null,"css-reloader-plugin","css-reloader-plugin",2002032904),figwheel.client.css_reloader_plugin,new cljs.core.Keyword(null,"repl-plugin","repl-plugin",-1138952371),figwheel.client.repl_plugin], null);
var base__$1 = ((cljs.core.not.call(null,figwheel.client.utils.html_env_QMARK_.call(null)))?cljs.core.select_keys.call(null,base,new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"file-reloader-plugin","file-reloader-plugin",-1792964733),new cljs.core.Keyword(null,"comp-fail-warning-plugin","comp-fail-warning-plugin",634311),new cljs.core.Keyword(null,"repl-plugin","repl-plugin",-1138952371)], null)):base);
var base__$2 = ((new cljs.core.Keyword(null,"autoload","autoload",-354122500).cljs$core$IFn$_invoke$arity$1(system_options) === false)?cljs.core.dissoc.call(null,base__$1,new cljs.core.Keyword(null,"file-reloader-plugin","file-reloader-plugin",-1792964733)):base__$1);
if(cljs.core.truth_((function (){var and__23131__auto__ = new cljs.core.Keyword(null,"heads-up-display","heads-up-display",-896577202).cljs$core$IFn$_invoke$arity$1(system_options);
if(cljs.core.truth_(and__23131__auto__)){
return figwheel.client.utils.html_env_QMARK_.call(null);
} else {
return and__23131__auto__;
}
})())){
return cljs.core.assoc.call(null,base__$2,new cljs.core.Keyword(null,"heads-up-display-plugin","heads-up-display-plugin",1745207501),figwheel.client.heads_up_plugin);
} else {
return base__$2;
}
});
figwheel.client.add_message_watch = (function figwheel$client$add_message_watch(key,callback){
return cljs.core.add_watch.call(null,figwheel.client.socket.message_history_atom,key,(function (_,___$1,___$2,msg_hist){
return callback.call(null,cljs.core.first.call(null,msg_hist));
}));
});
figwheel.client.add_plugins = (function figwheel$client$add_plugins(plugins,system_options){
var seq__33579 = cljs.core.seq.call(null,plugins);
var chunk__33580 = null;
var count__33581 = (0);
var i__33582 = (0);
while(true){
if((i__33582 < count__33581)){
var vec__33583 = cljs.core._nth.call(null,chunk__33580,i__33582);
var k = cljs.core.nth.call(null,vec__33583,(0),null);
var plugin = cljs.core.nth.call(null,vec__33583,(1),null);
if(cljs.core.truth_(plugin)){
var pl_33585 = plugin.call(null,system_options);
cljs.core.add_watch.call(null,figwheel.client.socket.message_history_atom,k,((function (seq__33579,chunk__33580,count__33581,i__33582,pl_33585,vec__33583,k,plugin){
return (function (_,___$1,___$2,msg_hist){
return pl_33585.call(null,msg_hist);
});})(seq__33579,chunk__33580,count__33581,i__33582,pl_33585,vec__33583,k,plugin))
);
} else {
}

var G__33586 = seq__33579;
var G__33587 = chunk__33580;
var G__33588 = count__33581;
var G__33589 = (i__33582 + (1));
seq__33579 = G__33586;
chunk__33580 = G__33587;
count__33581 = G__33588;
i__33582 = G__33589;
continue;
} else {
var temp__4657__auto__ = cljs.core.seq.call(null,seq__33579);
if(temp__4657__auto__){
var seq__33579__$1 = temp__4657__auto__;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__33579__$1)){
var c__23946__auto__ = cljs.core.chunk_first.call(null,seq__33579__$1);
var G__33590 = cljs.core.chunk_rest.call(null,seq__33579__$1);
var G__33591 = c__23946__auto__;
var G__33592 = cljs.core.count.call(null,c__23946__auto__);
var G__33593 = (0);
seq__33579 = G__33590;
chunk__33580 = G__33591;
count__33581 = G__33592;
i__33582 = G__33593;
continue;
} else {
var vec__33584 = cljs.core.first.call(null,seq__33579__$1);
var k = cljs.core.nth.call(null,vec__33584,(0),null);
var plugin = cljs.core.nth.call(null,vec__33584,(1),null);
if(cljs.core.truth_(plugin)){
var pl_33594 = plugin.call(null,system_options);
cljs.core.add_watch.call(null,figwheel.client.socket.message_history_atom,k,((function (seq__33579,chunk__33580,count__33581,i__33582,pl_33594,vec__33584,k,plugin,seq__33579__$1,temp__4657__auto__){
return (function (_,___$1,___$2,msg_hist){
return pl_33594.call(null,msg_hist);
});})(seq__33579,chunk__33580,count__33581,i__33582,pl_33594,vec__33584,k,plugin,seq__33579__$1,temp__4657__auto__))
);
} else {
}

var G__33595 = cljs.core.next.call(null,seq__33579__$1);
var G__33596 = null;
var G__33597 = (0);
var G__33598 = (0);
seq__33579 = G__33595;
chunk__33580 = G__33596;
count__33581 = G__33597;
i__33582 = G__33598;
continue;
}
} else {
return null;
}
}
break;
}
});
figwheel.client.start = (function figwheel$client$start(var_args){
var args33599 = [];
var len__24201__auto___33602 = arguments.length;
var i__24202__auto___33603 = (0);
while(true){
if((i__24202__auto___33603 < len__24201__auto___33602)){
args33599.push((arguments[i__24202__auto___33603]));

var G__33604 = (i__24202__auto___33603 + (1));
i__24202__auto___33603 = G__33604;
continue;
} else {
}
break;
}

var G__33601 = args33599.length;
switch (G__33601) {
case 1:
return figwheel.client.start.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 0:
return figwheel.client.start.cljs$core$IFn$_invoke$arity$0();

break;
default:
throw (new Error([cljs.core.str("Invalid arity: "),cljs.core.str(args33599.length)].join('')));

}
});

figwheel.client.start.cljs$core$IFn$_invoke$arity$1 = (function (opts){
if((goog.dependencies_ == null)){
return null;
} else {
if(typeof figwheel.client.__figwheel_start_once__ !== 'undefined'){
return null;
} else {
figwheel.client.__figwheel_start_once__ = setTimeout((function (){
var plugins_SINGLEQUOTE_ = new cljs.core.Keyword(null,"plugins","plugins",1900073717).cljs$core$IFn$_invoke$arity$1(opts);
var merge_plugins = new cljs.core.Keyword(null,"merge-plugins","merge-plugins",-1193912370).cljs$core$IFn$_invoke$arity$1(opts);
var system_options = figwheel.client.handle_deprecated_jsload_callback.call(null,cljs.core.merge.call(null,figwheel.client.config_defaults,cljs.core.dissoc.call(null,opts,new cljs.core.Keyword(null,"plugins","plugins",1900073717),new cljs.core.Keyword(null,"merge-plugins","merge-plugins",-1193912370))));
var plugins = (cljs.core.truth_(plugins_SINGLEQUOTE_)?plugins_SINGLEQUOTE_:cljs.core.merge.call(null,figwheel.client.base_plugins.call(null,system_options),merge_plugins));
figwheel.client.utils._STAR_print_debug_STAR_ = new cljs.core.Keyword(null,"debug","debug",-1608172596).cljs$core$IFn$_invoke$arity$1(opts);

figwheel.client.add_plugins.call(null,plugins,system_options);

figwheel.client.file_reloading.patch_goog_base.call(null);

return figwheel.client.socket.open.call(null,system_options);
}));
}
}
});

figwheel.client.start.cljs$core$IFn$_invoke$arity$0 = (function (){
return figwheel.client.start.call(null,cljs.core.PersistentArrayMap.EMPTY);
});

figwheel.client.start.cljs$lang$maxFixedArity = 1;
figwheel.client.watch_and_reload_with_opts = figwheel.client.start;
figwheel.client.watch_and_reload = (function figwheel$client$watch_and_reload(var_args){
var args__24208__auto__ = [];
var len__24201__auto___33610 = arguments.length;
var i__24202__auto___33611 = (0);
while(true){
if((i__24202__auto___33611 < len__24201__auto___33610)){
args__24208__auto__.push((arguments[i__24202__auto___33611]));

var G__33612 = (i__24202__auto___33611 + (1));
i__24202__auto___33611 = G__33612;
continue;
} else {
}
break;
}

var argseq__24209__auto__ = ((((0) < args__24208__auto__.length))?(new cljs.core.IndexedSeq(args__24208__auto__.slice((0)),(0))):null);
return figwheel.client.watch_and_reload.cljs$core$IFn$_invoke$arity$variadic(argseq__24209__auto__);
});

figwheel.client.watch_and_reload.cljs$core$IFn$_invoke$arity$variadic = (function (p__33607){
var map__33608 = p__33607;
var map__33608__$1 = ((((!((map__33608 == null)))?((((map__33608.cljs$lang$protocol_mask$partition0$ & (64))) || (map__33608.cljs$core$ISeq$))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__33608):map__33608);
var opts = map__33608__$1;
return figwheel.client.start.call(null,opts);
});

figwheel.client.watch_and_reload.cljs$lang$maxFixedArity = (0);

figwheel.client.watch_and_reload.cljs$lang$applyTo = (function (seq33606){
return figwheel.client.watch_and_reload.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq.call(null,seq33606));
});

//# sourceMappingURL=client.js.map