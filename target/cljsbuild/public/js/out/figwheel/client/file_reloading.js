// Compiled by ClojureScript 1.7.170 {}
goog.provide('figwheel.client.file_reloading');
goog.require('cljs.core');
goog.require('goog.string');
goog.require('goog.Uri');
goog.require('goog.net.jsloader');
goog.require('cljs.core.async');
goog.require('goog.object');
goog.require('clojure.set');
goog.require('clojure.string');
goog.require('figwheel.client.utils');
figwheel.client.file_reloading.queued_file_reload;
if(typeof figwheel.client.file_reloading.figwheel_meta_pragmas !== 'undefined'){
} else {
figwheel.client.file_reloading.figwheel_meta_pragmas = cljs.core.atom.call(null,cljs.core.PersistentArrayMap.EMPTY);
}
figwheel.client.file_reloading.on_jsload_custom_event = (function figwheel$client$file_reloading$on_jsload_custom_event(url){
return figwheel.client.utils.dispatch_custom_event.call(null,"figwheel.js-reload",url);
});
figwheel.client.file_reloading.before_jsload_custom_event = (function figwheel$client$file_reloading$before_jsload_custom_event(files){
return figwheel.client.utils.dispatch_custom_event.call(null,"figwheel.before-js-reload",files);
});
figwheel.client.file_reloading.namespace_file_map_QMARK_ = (function figwheel$client$file_reloading$namespace_file_map_QMARK_(m){
var or__23143__auto__ = (cljs.core.map_QMARK_.call(null,m)) && (typeof new cljs.core.Keyword(null,"namespace","namespace",-377510372).cljs$core$IFn$_invoke$arity$1(m) === 'string') && (((new cljs.core.Keyword(null,"file","file",-1269645878).cljs$core$IFn$_invoke$arity$1(m) == null)) || (typeof new cljs.core.Keyword(null,"file","file",-1269645878).cljs$core$IFn$_invoke$arity$1(m) === 'string')) && (cljs.core._EQ_.call(null,new cljs.core.Keyword(null,"type","type",1174270348).cljs$core$IFn$_invoke$arity$1(m),new cljs.core.Keyword(null,"namespace","namespace",-377510372)));
if(or__23143__auto__){
return or__23143__auto__;
} else {
cljs.core.println.call(null,"Error not namespace-file-map",cljs.core.pr_str.call(null,m));

return false;
}
});
figwheel.client.file_reloading.add_cache_buster = (function figwheel$client$file_reloading$add_cache_buster(url){

return goog.Uri.parse(url).makeUnique();
});
figwheel.client.file_reloading.name__GT_path = (function figwheel$client$file_reloading$name__GT_path(ns){

return (goog.dependencies_.nameToPath[ns]);
});
figwheel.client.file_reloading.provided_QMARK_ = (function figwheel$client$file_reloading$provided_QMARK_(ns){
return (goog.dependencies_.written[figwheel.client.file_reloading.name__GT_path.call(null,ns)]);
});
figwheel.client.file_reloading.fix_node_request_url = (function figwheel$client$file_reloading$fix_node_request_url(url){

if(cljs.core.truth_(goog.string.startsWith(url,"../"))){
return clojure.string.replace.call(null,url,"../","");
} else {
return [cljs.core.str("goog/"),cljs.core.str(url)].join('');
}
});
figwheel.client.file_reloading.immutable_ns_QMARK_ = (function figwheel$client$file_reloading$immutable_ns_QMARK_(name){
var or__23143__auto__ = new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 9, ["svgpan.SvgPan",null,"far.out",null,"testDep.bar",null,"someprotopackage.TestPackageTypes",null,"goog",null,"an.existing.path",null,"cljs.core",null,"ns",null,"dup.base",null], null), null).call(null,name);
if(cljs.core.truth_(or__23143__auto__)){
return or__23143__auto__;
} else {
return cljs.core.some.call(null,cljs.core.partial.call(null,goog.string.startsWith,name),new cljs.core.PersistentVector(null, 5, 5, cljs.core.PersistentVector.EMPTY_NODE, ["goog.","cljs.","clojure.","fake.","proto2."], null));
}
});
figwheel.client.file_reloading.get_requires = (function figwheel$client$file_reloading$get_requires(ns){
return cljs.core.set.call(null,cljs.core.filter.call(null,(function (p1__31322_SHARP_){
return cljs.core.not.call(null,figwheel.client.file_reloading.immutable_ns_QMARK_.call(null,p1__31322_SHARP_));
}),goog.object.getKeys((goog.dependencies_.requires[figwheel.client.file_reloading.name__GT_path.call(null,ns)]))));
});
if(typeof figwheel.client.file_reloading.dependency_data !== 'undefined'){
} else {
figwheel.client.file_reloading.dependency_data = cljs.core.atom.call(null,new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"pathToName","pathToName",-1236616181),cljs.core.PersistentArrayMap.EMPTY,new cljs.core.Keyword(null,"dependents","dependents",136812837),cljs.core.PersistentArrayMap.EMPTY], null));
}
figwheel.client.file_reloading.path_to_name_BANG_ = (function figwheel$client$file_reloading$path_to_name_BANG_(path,name){
return cljs.core.swap_BANG_.call(null,figwheel.client.file_reloading.dependency_data,cljs.core.update_in,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"pathToName","pathToName",-1236616181),path], null),cljs.core.fnil.call(null,clojure.set.union,cljs.core.PersistentHashSet.EMPTY),cljs.core.PersistentHashSet.fromArray([name], true));
});
/**
 * Setup a path to name dependencies map.
 * That goes from path -> #{ ns-names }
 */
figwheel.client.file_reloading.setup_path__GT_name_BANG_ = (function figwheel$client$file_reloading$setup_path__GT_name_BANG_(){
var nameToPath = goog.object.filter(goog.dependencies_.nameToPath,(function (v,k,o){
return goog.string.startsWith(v,"../");
}));
return goog.object.forEach(nameToPath,((function (nameToPath){
return (function (v,k,o){
return figwheel.client.file_reloading.path_to_name_BANG_.call(null,v,k);
});})(nameToPath))
);
});
/**
 * returns a set of namespaces defined by a path
 */
figwheel.client.file_reloading.path__GT_name = (function figwheel$client$file_reloading$path__GT_name(path){
return cljs.core.get_in.call(null,cljs.core.deref.call(null,figwheel.client.file_reloading.dependency_data),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"pathToName","pathToName",-1236616181),path], null));
});
figwheel.client.file_reloading.name_to_parent_BANG_ = (function figwheel$client$file_reloading$name_to_parent_BANG_(ns,parent_ns){
return cljs.core.swap_BANG_.call(null,figwheel.client.file_reloading.dependency_data,cljs.core.update_in,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"dependents","dependents",136812837),ns], null),cljs.core.fnil.call(null,clojure.set.union,cljs.core.PersistentHashSet.EMPTY),cljs.core.PersistentHashSet.fromArray([parent_ns], true));
});
/**
 * This reverses the goog.dependencies_.requires for looking up ns-dependents.
 */
figwheel.client.file_reloading.setup_ns__GT_dependents_BANG_ = (function figwheel$client$file_reloading$setup_ns__GT_dependents_BANG_(){
var requires = goog.object.filter(goog.dependencies_.requires,(function (v,k,o){
return goog.string.startsWith(k,"../");
}));
return goog.object.forEach(requires,((function (requires){
return (function (v,k,_){
return goog.object.forEach(v,((function (requires){
return (function (v_SINGLEQUOTE_,k_SINGLEQUOTE_,___$1){
var seq__31327 = cljs.core.seq.call(null,figwheel.client.file_reloading.path__GT_name.call(null,k));
var chunk__31328 = null;
var count__31329 = (0);
var i__31330 = (0);
while(true){
if((i__31330 < count__31329)){
var n = cljs.core._nth.call(null,chunk__31328,i__31330);
figwheel.client.file_reloading.name_to_parent_BANG_.call(null,k_SINGLEQUOTE_,n);

var G__31331 = seq__31327;
var G__31332 = chunk__31328;
var G__31333 = count__31329;
var G__31334 = (i__31330 + (1));
seq__31327 = G__31331;
chunk__31328 = G__31332;
count__31329 = G__31333;
i__31330 = G__31334;
continue;
} else {
var temp__4657__auto__ = cljs.core.seq.call(null,seq__31327);
if(temp__4657__auto__){
var seq__31327__$1 = temp__4657__auto__;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__31327__$1)){
var c__23946__auto__ = cljs.core.chunk_first.call(null,seq__31327__$1);
var G__31335 = cljs.core.chunk_rest.call(null,seq__31327__$1);
var G__31336 = c__23946__auto__;
var G__31337 = cljs.core.count.call(null,c__23946__auto__);
var G__31338 = (0);
seq__31327 = G__31335;
chunk__31328 = G__31336;
count__31329 = G__31337;
i__31330 = G__31338;
continue;
} else {
var n = cljs.core.first.call(null,seq__31327__$1);
figwheel.client.file_reloading.name_to_parent_BANG_.call(null,k_SINGLEQUOTE_,n);

var G__31339 = cljs.core.next.call(null,seq__31327__$1);
var G__31340 = null;
var G__31341 = (0);
var G__31342 = (0);
seq__31327 = G__31339;
chunk__31328 = G__31340;
count__31329 = G__31341;
i__31330 = G__31342;
continue;
}
} else {
return null;
}
}
break;
}
});})(requires))
);
});})(requires))
);
});
figwheel.client.file_reloading.ns__GT_dependents = (function figwheel$client$file_reloading$ns__GT_dependents(ns){
return cljs.core.get_in.call(null,cljs.core.deref.call(null,figwheel.client.file_reloading.dependency_data),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"dependents","dependents",136812837),ns], null));
});
figwheel.client.file_reloading.build_topo_sort = (function figwheel$client$file_reloading$build_topo_sort(get_deps){
var get_deps__$1 = cljs.core.memoize.call(null,get_deps);
var topo_sort_helper_STAR_ = ((function (get_deps__$1){
return (function figwheel$client$file_reloading$build_topo_sort_$_topo_sort_helper_STAR_(x,depth,state){
var deps = get_deps__$1.call(null,x);
if(cljs.core.empty_QMARK_.call(null,deps)){
return null;
} else {
return topo_sort_STAR_.call(null,deps,depth,state);
}
});})(get_deps__$1))
;
var topo_sort_STAR_ = ((function (get_deps__$1){
return (function() {
var figwheel$client$file_reloading$build_topo_sort_$_topo_sort_STAR_ = null;
var figwheel$client$file_reloading$build_topo_sort_$_topo_sort_STAR___1 = (function (deps){
return figwheel$client$file_reloading$build_topo_sort_$_topo_sort_STAR_.call(null,deps,(0),cljs.core.atom.call(null,cljs.core.sorted_map.call(null)));
});
var figwheel$client$file_reloading$build_topo_sort_$_topo_sort_STAR___3 = (function (deps,depth,state){
cljs.core.swap_BANG_.call(null,state,cljs.core.update_in,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [depth], null),cljs.core.fnil.call(null,cljs.core.into,cljs.core.PersistentHashSet.EMPTY),deps);

var seq__31381_31388 = cljs.core.seq.call(null,deps);
var chunk__31382_31389 = null;
var count__31383_31390 = (0);
var i__31384_31391 = (0);
while(true){
if((i__31384_31391 < count__31383_31390)){
var dep_31392 = cljs.core._nth.call(null,chunk__31382_31389,i__31384_31391);
topo_sort_helper_STAR_.call(null,dep_31392,(depth + (1)),state);

var G__31393 = seq__31381_31388;
var G__31394 = chunk__31382_31389;
var G__31395 = count__31383_31390;
var G__31396 = (i__31384_31391 + (1));
seq__31381_31388 = G__31393;
chunk__31382_31389 = G__31394;
count__31383_31390 = G__31395;
i__31384_31391 = G__31396;
continue;
} else {
var temp__4657__auto___31397 = cljs.core.seq.call(null,seq__31381_31388);
if(temp__4657__auto___31397){
var seq__31381_31398__$1 = temp__4657__auto___31397;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__31381_31398__$1)){
var c__23946__auto___31399 = cljs.core.chunk_first.call(null,seq__31381_31398__$1);
var G__31400 = cljs.core.chunk_rest.call(null,seq__31381_31398__$1);
var G__31401 = c__23946__auto___31399;
var G__31402 = cljs.core.count.call(null,c__23946__auto___31399);
var G__31403 = (0);
seq__31381_31388 = G__31400;
chunk__31382_31389 = G__31401;
count__31383_31390 = G__31402;
i__31384_31391 = G__31403;
continue;
} else {
var dep_31404 = cljs.core.first.call(null,seq__31381_31398__$1);
topo_sort_helper_STAR_.call(null,dep_31404,(depth + (1)),state);

var G__31405 = cljs.core.next.call(null,seq__31381_31398__$1);
var G__31406 = null;
var G__31407 = (0);
var G__31408 = (0);
seq__31381_31388 = G__31405;
chunk__31382_31389 = G__31406;
count__31383_31390 = G__31407;
i__31384_31391 = G__31408;
continue;
}
} else {
}
}
break;
}

if(cljs.core._EQ_.call(null,depth,(0))){
return elim_dups_STAR_.call(null,cljs.core.reverse.call(null,cljs.core.vals.call(null,cljs.core.deref.call(null,state))));
} else {
return null;
}
});
figwheel$client$file_reloading$build_topo_sort_$_topo_sort_STAR_ = function(deps,depth,state){
switch(arguments.length){
case 1:
return figwheel$client$file_reloading$build_topo_sort_$_topo_sort_STAR___1.call(this,deps);
case 3:
return figwheel$client$file_reloading$build_topo_sort_$_topo_sort_STAR___3.call(this,deps,depth,state);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
figwheel$client$file_reloading$build_topo_sort_$_topo_sort_STAR_.cljs$core$IFn$_invoke$arity$1 = figwheel$client$file_reloading$build_topo_sort_$_topo_sort_STAR___1;
figwheel$client$file_reloading$build_topo_sort_$_topo_sort_STAR_.cljs$core$IFn$_invoke$arity$3 = figwheel$client$file_reloading$build_topo_sort_$_topo_sort_STAR___3;
return figwheel$client$file_reloading$build_topo_sort_$_topo_sort_STAR_;
})()
;})(get_deps__$1))
;
var elim_dups_STAR_ = ((function (get_deps__$1){
return (function figwheel$client$file_reloading$build_topo_sort_$_elim_dups_STAR_(p__31385){
var vec__31387 = p__31385;
var x = cljs.core.nth.call(null,vec__31387,(0),null);
var xs = cljs.core.nthnext.call(null,vec__31387,(1));
if((x == null)){
return cljs.core.List.EMPTY;
} else {
return cljs.core.cons.call(null,x,figwheel$client$file_reloading$build_topo_sort_$_elim_dups_STAR_.call(null,cljs.core.map.call(null,((function (vec__31387,x,xs,get_deps__$1){
return (function (p1__31343_SHARP_){
return clojure.set.difference.call(null,p1__31343_SHARP_,x);
});})(vec__31387,x,xs,get_deps__$1))
,xs)));
}
});})(get_deps__$1))
;
return topo_sort_STAR_;
});
figwheel.client.file_reloading.get_all_dependencies = (function figwheel$client$file_reloading$get_all_dependencies(ns){
var topo_sort_SINGLEQUOTE_ = figwheel.client.file_reloading.build_topo_sort.call(null,figwheel.client.file_reloading.get_requires);
return cljs.core.apply.call(null,cljs.core.concat,topo_sort_SINGLEQUOTE_.call(null,cljs.core.set.call(null,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [ns], null))));
});
figwheel.client.file_reloading.get_all_dependents = (function figwheel$client$file_reloading$get_all_dependents(nss){
var topo_sort_SINGLEQUOTE_ = figwheel.client.file_reloading.build_topo_sort.call(null,figwheel.client.file_reloading.ns__GT_dependents);
return cljs.core.reverse.call(null,cljs.core.apply.call(null,cljs.core.concat,topo_sort_SINGLEQUOTE_.call(null,cljs.core.set.call(null,nss))));
});
figwheel.client.file_reloading.unprovide_BANG_ = (function figwheel$client$file_reloading$unprovide_BANG_(ns){
var path = figwheel.client.file_reloading.name__GT_path.call(null,ns);
goog.object.remove(goog.dependencies_.visited,path);

goog.object.remove(goog.dependencies_.written,path);

return goog.object.remove(goog.dependencies_.written,[cljs.core.str(goog.basePath),cljs.core.str(path)].join(''));
});
figwheel.client.file_reloading.resolve_ns = (function figwheel$client$file_reloading$resolve_ns(ns){
return [cljs.core.str(goog.basePath),cljs.core.str(figwheel.client.file_reloading.name__GT_path.call(null,ns))].join('');
});
figwheel.client.file_reloading.addDependency = (function figwheel$client$file_reloading$addDependency(path,provides,requires){
var seq__31421 = cljs.core.seq.call(null,provides);
var chunk__31422 = null;
var count__31423 = (0);
var i__31424 = (0);
while(true){
if((i__31424 < count__31423)){
var prov = cljs.core._nth.call(null,chunk__31422,i__31424);
figwheel.client.file_reloading.path_to_name_BANG_.call(null,path,prov);

var seq__31425_31433 = cljs.core.seq.call(null,requires);
var chunk__31426_31434 = null;
var count__31427_31435 = (0);
var i__31428_31436 = (0);
while(true){
if((i__31428_31436 < count__31427_31435)){
var req_31437 = cljs.core._nth.call(null,chunk__31426_31434,i__31428_31436);
figwheel.client.file_reloading.name_to_parent_BANG_.call(null,req_31437,prov);

var G__31438 = seq__31425_31433;
var G__31439 = chunk__31426_31434;
var G__31440 = count__31427_31435;
var G__31441 = (i__31428_31436 + (1));
seq__31425_31433 = G__31438;
chunk__31426_31434 = G__31439;
count__31427_31435 = G__31440;
i__31428_31436 = G__31441;
continue;
} else {
var temp__4657__auto___31442 = cljs.core.seq.call(null,seq__31425_31433);
if(temp__4657__auto___31442){
var seq__31425_31443__$1 = temp__4657__auto___31442;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__31425_31443__$1)){
var c__23946__auto___31444 = cljs.core.chunk_first.call(null,seq__31425_31443__$1);
var G__31445 = cljs.core.chunk_rest.call(null,seq__31425_31443__$1);
var G__31446 = c__23946__auto___31444;
var G__31447 = cljs.core.count.call(null,c__23946__auto___31444);
var G__31448 = (0);
seq__31425_31433 = G__31445;
chunk__31426_31434 = G__31446;
count__31427_31435 = G__31447;
i__31428_31436 = G__31448;
continue;
} else {
var req_31449 = cljs.core.first.call(null,seq__31425_31443__$1);
figwheel.client.file_reloading.name_to_parent_BANG_.call(null,req_31449,prov);

var G__31450 = cljs.core.next.call(null,seq__31425_31443__$1);
var G__31451 = null;
var G__31452 = (0);
var G__31453 = (0);
seq__31425_31433 = G__31450;
chunk__31426_31434 = G__31451;
count__31427_31435 = G__31452;
i__31428_31436 = G__31453;
continue;
}
} else {
}
}
break;
}

var G__31454 = seq__31421;
var G__31455 = chunk__31422;
var G__31456 = count__31423;
var G__31457 = (i__31424 + (1));
seq__31421 = G__31454;
chunk__31422 = G__31455;
count__31423 = G__31456;
i__31424 = G__31457;
continue;
} else {
var temp__4657__auto__ = cljs.core.seq.call(null,seq__31421);
if(temp__4657__auto__){
var seq__31421__$1 = temp__4657__auto__;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__31421__$1)){
var c__23946__auto__ = cljs.core.chunk_first.call(null,seq__31421__$1);
var G__31458 = cljs.core.chunk_rest.call(null,seq__31421__$1);
var G__31459 = c__23946__auto__;
var G__31460 = cljs.core.count.call(null,c__23946__auto__);
var G__31461 = (0);
seq__31421 = G__31458;
chunk__31422 = G__31459;
count__31423 = G__31460;
i__31424 = G__31461;
continue;
} else {
var prov = cljs.core.first.call(null,seq__31421__$1);
figwheel.client.file_reloading.path_to_name_BANG_.call(null,path,prov);

var seq__31429_31462 = cljs.core.seq.call(null,requires);
var chunk__31430_31463 = null;
var count__31431_31464 = (0);
var i__31432_31465 = (0);
while(true){
if((i__31432_31465 < count__31431_31464)){
var req_31466 = cljs.core._nth.call(null,chunk__31430_31463,i__31432_31465);
figwheel.client.file_reloading.name_to_parent_BANG_.call(null,req_31466,prov);

var G__31467 = seq__31429_31462;
var G__31468 = chunk__31430_31463;
var G__31469 = count__31431_31464;
var G__31470 = (i__31432_31465 + (1));
seq__31429_31462 = G__31467;
chunk__31430_31463 = G__31468;
count__31431_31464 = G__31469;
i__31432_31465 = G__31470;
continue;
} else {
var temp__4657__auto___31471__$1 = cljs.core.seq.call(null,seq__31429_31462);
if(temp__4657__auto___31471__$1){
var seq__31429_31472__$1 = temp__4657__auto___31471__$1;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__31429_31472__$1)){
var c__23946__auto___31473 = cljs.core.chunk_first.call(null,seq__31429_31472__$1);
var G__31474 = cljs.core.chunk_rest.call(null,seq__31429_31472__$1);
var G__31475 = c__23946__auto___31473;
var G__31476 = cljs.core.count.call(null,c__23946__auto___31473);
var G__31477 = (0);
seq__31429_31462 = G__31474;
chunk__31430_31463 = G__31475;
count__31431_31464 = G__31476;
i__31432_31465 = G__31477;
continue;
} else {
var req_31478 = cljs.core.first.call(null,seq__31429_31472__$1);
figwheel.client.file_reloading.name_to_parent_BANG_.call(null,req_31478,prov);

var G__31479 = cljs.core.next.call(null,seq__31429_31472__$1);
var G__31480 = null;
var G__31481 = (0);
var G__31482 = (0);
seq__31429_31462 = G__31479;
chunk__31430_31463 = G__31480;
count__31431_31464 = G__31481;
i__31432_31465 = G__31482;
continue;
}
} else {
}
}
break;
}

var G__31483 = cljs.core.next.call(null,seq__31421__$1);
var G__31484 = null;
var G__31485 = (0);
var G__31486 = (0);
seq__31421 = G__31483;
chunk__31422 = G__31484;
count__31423 = G__31485;
i__31424 = G__31486;
continue;
}
} else {
return null;
}
}
break;
}
});
figwheel.client.file_reloading.figwheel_require = (function figwheel$client$file_reloading$figwheel_require(src,reload){
goog.require = figwheel$client$file_reloading$figwheel_require;

if(cljs.core._EQ_.call(null,reload,"reload-all")){
var seq__31491_31495 = cljs.core.seq.call(null,figwheel.client.file_reloading.get_all_dependencies.call(null,src));
var chunk__31492_31496 = null;
var count__31493_31497 = (0);
var i__31494_31498 = (0);
while(true){
if((i__31494_31498 < count__31493_31497)){
var ns_31499 = cljs.core._nth.call(null,chunk__31492_31496,i__31494_31498);
figwheel.client.file_reloading.unprovide_BANG_.call(null,ns_31499);

var G__31500 = seq__31491_31495;
var G__31501 = chunk__31492_31496;
var G__31502 = count__31493_31497;
var G__31503 = (i__31494_31498 + (1));
seq__31491_31495 = G__31500;
chunk__31492_31496 = G__31501;
count__31493_31497 = G__31502;
i__31494_31498 = G__31503;
continue;
} else {
var temp__4657__auto___31504 = cljs.core.seq.call(null,seq__31491_31495);
if(temp__4657__auto___31504){
var seq__31491_31505__$1 = temp__4657__auto___31504;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__31491_31505__$1)){
var c__23946__auto___31506 = cljs.core.chunk_first.call(null,seq__31491_31505__$1);
var G__31507 = cljs.core.chunk_rest.call(null,seq__31491_31505__$1);
var G__31508 = c__23946__auto___31506;
var G__31509 = cljs.core.count.call(null,c__23946__auto___31506);
var G__31510 = (0);
seq__31491_31495 = G__31507;
chunk__31492_31496 = G__31508;
count__31493_31497 = G__31509;
i__31494_31498 = G__31510;
continue;
} else {
var ns_31511 = cljs.core.first.call(null,seq__31491_31505__$1);
figwheel.client.file_reloading.unprovide_BANG_.call(null,ns_31511);

var G__31512 = cljs.core.next.call(null,seq__31491_31505__$1);
var G__31513 = null;
var G__31514 = (0);
var G__31515 = (0);
seq__31491_31495 = G__31512;
chunk__31492_31496 = G__31513;
count__31493_31497 = G__31514;
i__31494_31498 = G__31515;
continue;
}
} else {
}
}
break;
}
} else {
}

if(cljs.core.truth_(reload)){
figwheel.client.file_reloading.unprovide_BANG_.call(null,src);
} else {
}

return goog.require_figwheel_backup_(src);
});
/**
 * Reusable browser REPL bootstrapping. Patches the essential functions
 *   in goog.base to support re-loading of namespaces after page load.
 */
figwheel.client.file_reloading.bootstrap_goog_base = (function figwheel$client$file_reloading$bootstrap_goog_base(){
if(cljs.core.truth_(COMPILED)){
return null;
} else {
goog.require_figwheel_backup_ = (function (){var or__23143__auto__ = goog.require__;
if(cljs.core.truth_(or__23143__auto__)){
return or__23143__auto__;
} else {
return goog.require;
}
})();

goog.isProvided_ = (function (name){
return false;
});

figwheel.client.file_reloading.setup_path__GT_name_BANG_.call(null);

figwheel.client.file_reloading.setup_ns__GT_dependents_BANG_.call(null);

goog.addDependency_figwheel_backup_ = goog.addDependency;

goog.addDependency = (function() { 
var G__31516__delegate = function (args){
cljs.core.apply.call(null,figwheel.client.file_reloading.addDependency,args);

return cljs.core.apply.call(null,goog.addDependency_figwheel_backup_,args);
};
var G__31516 = function (var_args){
var args = null;
if (arguments.length > 0) {
var G__31517__i = 0, G__31517__a = new Array(arguments.length -  0);
while (G__31517__i < G__31517__a.length) {G__31517__a[G__31517__i] = arguments[G__31517__i + 0]; ++G__31517__i;}
  args = new cljs.core.IndexedSeq(G__31517__a,0);
} 
return G__31516__delegate.call(this,args);};
G__31516.cljs$lang$maxFixedArity = 0;
G__31516.cljs$lang$applyTo = (function (arglist__31518){
var args = cljs.core.seq(arglist__31518);
return G__31516__delegate(args);
});
G__31516.cljs$core$IFn$_invoke$arity$variadic = G__31516__delegate;
return G__31516;
})()
;

goog.constructNamespace_("cljs.user");

goog.global.CLOSURE_IMPORT_SCRIPT = figwheel.client.file_reloading.queued_file_reload;

return goog.require = figwheel.client.file_reloading.figwheel_require;
}
});
figwheel.client.file_reloading.patch_goog_base = (function figwheel$client$file_reloading$patch_goog_base(){
if(typeof figwheel.client.file_reloading.bootstrapped_cljs !== 'undefined'){
return null;
} else {
figwheel.client.file_reloading.bootstrapped_cljs = (function (){
figwheel.client.file_reloading.bootstrap_goog_base.call(null);

return true;
})()
;
}
});
figwheel.client.file_reloading.reload_file_STAR_ = (function (){var pred__31520 = cljs.core._EQ_;
var expr__31521 = figwheel.client.utils.host_env_QMARK_.call(null);
if(cljs.core.truth_(pred__31520.call(null,new cljs.core.Keyword(null,"node","node",581201198),expr__31521))){
var path_parts = ((function (pred__31520,expr__31521){
return (function (p1__31519_SHARP_){
return clojure.string.split.call(null,p1__31519_SHARP_,/[\/\\]/);
});})(pred__31520,expr__31521))
;
var sep = (cljs.core.truth_(cljs.core.re_matches.call(null,/win.*/,process.platform))?"\\":"/");
var root = clojure.string.join.call(null,sep,cljs.core.pop.call(null,cljs.core.pop.call(null,path_parts.call(null,__dirname))));
return ((function (path_parts,sep,root,pred__31520,expr__31521){
return (function (request_url,callback){

var cache_path = clojure.string.join.call(null,sep,cljs.core.cons.call(null,root,path_parts.call(null,figwheel.client.file_reloading.fix_node_request_url.call(null,request_url))));
(require.cache[cache_path] = null);

return callback.call(null,(function (){try{return require(cache_path);
}catch (e31523){if((e31523 instanceof Error)){
var e = e31523;
figwheel.client.utils.log.call(null,new cljs.core.Keyword(null,"error","error",-978969032),[cljs.core.str("Figwheel: Error loading file "),cljs.core.str(cache_path)].join(''));

figwheel.client.utils.log.call(null,new cljs.core.Keyword(null,"error","error",-978969032),e.stack);

return false;
} else {
throw e31523;

}
}})());
});
;})(path_parts,sep,root,pred__31520,expr__31521))
} else {
if(cljs.core.truth_(pred__31520.call(null,new cljs.core.Keyword(null,"html","html",-998796897),expr__31521))){
return ((function (pred__31520,expr__31521){
return (function (request_url,callback){

var deferred = goog.net.jsloader.load(figwheel.client.file_reloading.add_cache_buster.call(null,request_url),{"cleanupWhenDone": true});
deferred.addCallback(((function (deferred,pred__31520,expr__31521){
return (function (){
return cljs.core.apply.call(null,callback,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [true], null));
});})(deferred,pred__31520,expr__31521))
);

return deferred.addErrback(((function (deferred,pred__31520,expr__31521){
return (function (){
return cljs.core.apply.call(null,callback,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [false], null));
});})(deferred,pred__31520,expr__31521))
);
});
;})(pred__31520,expr__31521))
} else {
return ((function (pred__31520,expr__31521){
return (function (a,b){
throw "Reload not defined for this platform";
});
;})(pred__31520,expr__31521))
}
}
})();
figwheel.client.file_reloading.reload_file = (function figwheel$client$file_reloading$reload_file(p__31524,callback){
var map__31527 = p__31524;
var map__31527__$1 = ((((!((map__31527 == null)))?((((map__31527.cljs$lang$protocol_mask$partition0$ & (64))) || (map__31527.cljs$core$ISeq$))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__31527):map__31527);
var file_msg = map__31527__$1;
var request_url = cljs.core.get.call(null,map__31527__$1,new cljs.core.Keyword(null,"request-url","request-url",2100346596));

figwheel.client.utils.debug_prn.call(null,[cljs.core.str("FigWheel: Attempting to load "),cljs.core.str(request_url)].join(''));

return figwheel.client.file_reloading.reload_file_STAR_.call(null,request_url,((function (map__31527,map__31527__$1,file_msg,request_url){
return (function (success_QMARK_){
if(cljs.core.truth_(success_QMARK_)){
figwheel.client.utils.debug_prn.call(null,[cljs.core.str("FigWheel: Successfully loaded "),cljs.core.str(request_url)].join(''));

return cljs.core.apply.call(null,callback,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.assoc.call(null,file_msg,new cljs.core.Keyword(null,"loaded-file","loaded-file",-168399375),true)], null));
} else {
figwheel.client.utils.log.call(null,new cljs.core.Keyword(null,"error","error",-978969032),[cljs.core.str("Figwheel: Error loading file "),cljs.core.str(request_url)].join(''));

return cljs.core.apply.call(null,callback,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [file_msg], null));
}
});})(map__31527,map__31527__$1,file_msg,request_url))
);
});
if(typeof figwheel.client.file_reloading.reload_chan !== 'undefined'){
} else {
figwheel.client.file_reloading.reload_chan = cljs.core.async.chan.call(null);
}
if(typeof figwheel.client.file_reloading.on_load_callbacks !== 'undefined'){
} else {
figwheel.client.file_reloading.on_load_callbacks = cljs.core.atom.call(null,cljs.core.PersistentArrayMap.EMPTY);
}
if(typeof figwheel.client.file_reloading.dependencies_loaded !== 'undefined'){
} else {
figwheel.client.file_reloading.dependencies_loaded = cljs.core.atom.call(null,cljs.core.PersistentVector.EMPTY);
}
figwheel.client.file_reloading.blocking_load = (function figwheel$client$file_reloading$blocking_load(url){
var out = cljs.core.async.chan.call(null);
figwheel.client.file_reloading.reload_file.call(null,new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"request-url","request-url",2100346596),url], null),((function (out){
return (function (file_msg){
cljs.core.async.put_BANG_.call(null,out,file_msg);

return cljs.core.async.close_BANG_.call(null,out);
});})(out))
);

return out;
});
if(typeof figwheel.client.file_reloading.reloader_loop !== 'undefined'){
} else {
figwheel.client.file_reloading.reloader_loop = (function (){var c__28489__auto__ = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__28489__auto__){
return (function (){
var f__28490__auto__ = (function (){var switch__28377__auto__ = ((function (c__28489__auto__){
return (function (state_31551){
var state_val_31552 = (state_31551[(1)]);
if((state_val_31552 === (7))){
var inst_31547 = (state_31551[(2)]);
var state_31551__$1 = state_31551;
var statearr_31553_31573 = state_31551__$1;
(statearr_31553_31573[(2)] = inst_31547);

(statearr_31553_31573[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31552 === (1))){
var state_31551__$1 = state_31551;
var statearr_31554_31574 = state_31551__$1;
(statearr_31554_31574[(2)] = null);

(statearr_31554_31574[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31552 === (4))){
var inst_31531 = (state_31551[(7)]);
var inst_31531__$1 = (state_31551[(2)]);
var state_31551__$1 = (function (){var statearr_31555 = state_31551;
(statearr_31555[(7)] = inst_31531__$1);

return statearr_31555;
})();
if(cljs.core.truth_(inst_31531__$1)){
var statearr_31556_31575 = state_31551__$1;
(statearr_31556_31575[(1)] = (5));

} else {
var statearr_31557_31576 = state_31551__$1;
(statearr_31557_31576[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31552 === (6))){
var state_31551__$1 = state_31551;
var statearr_31558_31577 = state_31551__$1;
(statearr_31558_31577[(2)] = null);

(statearr_31558_31577[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31552 === (3))){
var inst_31549 = (state_31551[(2)]);
var state_31551__$1 = state_31551;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_31551__$1,inst_31549);
} else {
if((state_val_31552 === (2))){
var state_31551__$1 = state_31551;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_31551__$1,(4),figwheel.client.file_reloading.reload_chan);
} else {
if((state_val_31552 === (11))){
var inst_31543 = (state_31551[(2)]);
var state_31551__$1 = (function (){var statearr_31559 = state_31551;
(statearr_31559[(8)] = inst_31543);

return statearr_31559;
})();
var statearr_31560_31578 = state_31551__$1;
(statearr_31560_31578[(2)] = null);

(statearr_31560_31578[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31552 === (9))){
var inst_31535 = (state_31551[(9)]);
var inst_31537 = (state_31551[(10)]);
var inst_31539 = inst_31537.call(null,inst_31535);
var state_31551__$1 = state_31551;
var statearr_31561_31579 = state_31551__$1;
(statearr_31561_31579[(2)] = inst_31539);

(statearr_31561_31579[(1)] = (11));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31552 === (5))){
var inst_31531 = (state_31551[(7)]);
var inst_31533 = figwheel.client.file_reloading.blocking_load.call(null,inst_31531);
var state_31551__$1 = state_31551;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_31551__$1,(8),inst_31533);
} else {
if((state_val_31552 === (10))){
var inst_31535 = (state_31551[(9)]);
var inst_31541 = cljs.core.swap_BANG_.call(null,figwheel.client.file_reloading.dependencies_loaded,cljs.core.conj,inst_31535);
var state_31551__$1 = state_31551;
var statearr_31562_31580 = state_31551__$1;
(statearr_31562_31580[(2)] = inst_31541);

(statearr_31562_31580[(1)] = (11));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31552 === (8))){
var inst_31531 = (state_31551[(7)]);
var inst_31537 = (state_31551[(10)]);
var inst_31535 = (state_31551[(2)]);
var inst_31536 = cljs.core.deref.call(null,figwheel.client.file_reloading.on_load_callbacks);
var inst_31537__$1 = cljs.core.get.call(null,inst_31536,inst_31531);
var state_31551__$1 = (function (){var statearr_31563 = state_31551;
(statearr_31563[(9)] = inst_31535);

(statearr_31563[(10)] = inst_31537__$1);

return statearr_31563;
})();
if(cljs.core.truth_(inst_31537__$1)){
var statearr_31564_31581 = state_31551__$1;
(statearr_31564_31581[(1)] = (9));

} else {
var statearr_31565_31582 = state_31551__$1;
(statearr_31565_31582[(1)] = (10));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
});})(c__28489__auto__))
;
return ((function (switch__28377__auto__,c__28489__auto__){
return (function() {
var figwheel$client$file_reloading$state_machine__28378__auto__ = null;
var figwheel$client$file_reloading$state_machine__28378__auto____0 = (function (){
var statearr_31569 = [null,null,null,null,null,null,null,null,null,null,null];
(statearr_31569[(0)] = figwheel$client$file_reloading$state_machine__28378__auto__);

(statearr_31569[(1)] = (1));

return statearr_31569;
});
var figwheel$client$file_reloading$state_machine__28378__auto____1 = (function (state_31551){
while(true){
var ret_value__28379__auto__ = (function (){try{while(true){
var result__28380__auto__ = switch__28377__auto__.call(null,state_31551);
if(cljs.core.keyword_identical_QMARK_.call(null,result__28380__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__28380__auto__;
}
break;
}
}catch (e31570){if((e31570 instanceof Object)){
var ex__28381__auto__ = e31570;
var statearr_31571_31583 = state_31551;
(statearr_31571_31583[(5)] = ex__28381__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_31551);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e31570;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__28379__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__31584 = state_31551;
state_31551 = G__31584;
continue;
} else {
return ret_value__28379__auto__;
}
break;
}
});
figwheel$client$file_reloading$state_machine__28378__auto__ = function(state_31551){
switch(arguments.length){
case 0:
return figwheel$client$file_reloading$state_machine__28378__auto____0.call(this);
case 1:
return figwheel$client$file_reloading$state_machine__28378__auto____1.call(this,state_31551);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
figwheel$client$file_reloading$state_machine__28378__auto__.cljs$core$IFn$_invoke$arity$0 = figwheel$client$file_reloading$state_machine__28378__auto____0;
figwheel$client$file_reloading$state_machine__28378__auto__.cljs$core$IFn$_invoke$arity$1 = figwheel$client$file_reloading$state_machine__28378__auto____1;
return figwheel$client$file_reloading$state_machine__28378__auto__;
})()
;})(switch__28377__auto__,c__28489__auto__))
})();
var state__28491__auto__ = (function (){var statearr_31572 = f__28490__auto__.call(null);
(statearr_31572[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__28489__auto__);

return statearr_31572;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__28491__auto__);
});})(c__28489__auto__))
);

return c__28489__auto__;
})();
}
figwheel.client.file_reloading.queued_file_reload = (function figwheel$client$file_reloading$queued_file_reload(url){
return cljs.core.async.put_BANG_.call(null,figwheel.client.file_reloading.reload_chan,url);
});
figwheel.client.file_reloading.require_with_callback = (function figwheel$client$file_reloading$require_with_callback(p__31585,callback){
var map__31588 = p__31585;
var map__31588__$1 = ((((!((map__31588 == null)))?((((map__31588.cljs$lang$protocol_mask$partition0$ & (64))) || (map__31588.cljs$core$ISeq$))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__31588):map__31588);
var file_msg = map__31588__$1;
var namespace = cljs.core.get.call(null,map__31588__$1,new cljs.core.Keyword(null,"namespace","namespace",-377510372));
var request_url = figwheel.client.file_reloading.resolve_ns.call(null,namespace);
cljs.core.swap_BANG_.call(null,figwheel.client.file_reloading.on_load_callbacks,cljs.core.assoc,request_url,((function (request_url,map__31588,map__31588__$1,file_msg,namespace){
return (function (file_msg_SINGLEQUOTE_){
cljs.core.swap_BANG_.call(null,figwheel.client.file_reloading.on_load_callbacks,cljs.core.dissoc,request_url);

return cljs.core.apply.call(null,callback,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.merge.call(null,file_msg,cljs.core.select_keys.call(null,file_msg_SINGLEQUOTE_,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"loaded-file","loaded-file",-168399375)], null)))], null));
});})(request_url,map__31588,map__31588__$1,file_msg,namespace))
);

return figwheel.client.file_reloading.figwheel_require.call(null,cljs.core.name.call(null,namespace),true);
});
figwheel.client.file_reloading.reload_file_QMARK_ = (function figwheel$client$file_reloading$reload_file_QMARK_(p__31590){
var map__31593 = p__31590;
var map__31593__$1 = ((((!((map__31593 == null)))?((((map__31593.cljs$lang$protocol_mask$partition0$ & (64))) || (map__31593.cljs$core$ISeq$))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__31593):map__31593);
var file_msg = map__31593__$1;
var namespace = cljs.core.get.call(null,map__31593__$1,new cljs.core.Keyword(null,"namespace","namespace",-377510372));

var meta_pragmas = cljs.core.get.call(null,cljs.core.deref.call(null,figwheel.client.file_reloading.figwheel_meta_pragmas),cljs.core.name.call(null,namespace));
var and__23131__auto__ = cljs.core.not.call(null,new cljs.core.Keyword(null,"figwheel-no-load","figwheel-no-load",-555840179).cljs$core$IFn$_invoke$arity$1(meta_pragmas));
if(and__23131__auto__){
var or__23143__auto__ = new cljs.core.Keyword(null,"figwheel-always","figwheel-always",799819691).cljs$core$IFn$_invoke$arity$1(meta_pragmas);
if(cljs.core.truth_(or__23143__auto__)){
return or__23143__auto__;
} else {
var or__23143__auto____$1 = new cljs.core.Keyword(null,"figwheel-load","figwheel-load",1316089175).cljs$core$IFn$_invoke$arity$1(meta_pragmas);
if(cljs.core.truth_(or__23143__auto____$1)){
return or__23143__auto____$1;
} else {
return figwheel.client.file_reloading.provided_QMARK_.call(null,cljs.core.name.call(null,namespace));
}
}
} else {
return and__23131__auto__;
}
});
figwheel.client.file_reloading.js_reload = (function figwheel$client$file_reloading$js_reload(p__31595,callback){
var map__31598 = p__31595;
var map__31598__$1 = ((((!((map__31598 == null)))?((((map__31598.cljs$lang$protocol_mask$partition0$ & (64))) || (map__31598.cljs$core$ISeq$))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__31598):map__31598);
var file_msg = map__31598__$1;
var request_url = cljs.core.get.call(null,map__31598__$1,new cljs.core.Keyword(null,"request-url","request-url",2100346596));
var namespace = cljs.core.get.call(null,map__31598__$1,new cljs.core.Keyword(null,"namespace","namespace",-377510372));

if(cljs.core.truth_(figwheel.client.file_reloading.reload_file_QMARK_.call(null,file_msg))){
return figwheel.client.file_reloading.require_with_callback.call(null,file_msg,callback);
} else {
figwheel.client.utils.debug_prn.call(null,[cljs.core.str("Figwheel: Not trying to load file "),cljs.core.str(request_url)].join(''));

return cljs.core.apply.call(null,callback,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [file_msg], null));
}
});
figwheel.client.file_reloading.reload_js_file = (function figwheel$client$file_reloading$reload_js_file(file_msg){
var out = cljs.core.async.chan.call(null);
figwheel.client.file_reloading.js_reload.call(null,file_msg,((function (out){
return (function (url){
cljs.core.async.put_BANG_.call(null,out,url);

return cljs.core.async.close_BANG_.call(null,out);
});})(out))
);

return out;
});
/**
 * Returns a chanel with one collection of loaded filenames on it.
 */
figwheel.client.file_reloading.load_all_js_files = (function figwheel$client$file_reloading$load_all_js_files(files){
var out = cljs.core.async.chan.call(null);
var c__28489__auto___31686 = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__28489__auto___31686,out){
return (function (){
var f__28490__auto__ = (function (){var switch__28377__auto__ = ((function (c__28489__auto___31686,out){
return (function (state_31668){
var state_val_31669 = (state_31668[(1)]);
if((state_val_31669 === (1))){
var inst_31646 = cljs.core.nth.call(null,files,(0),null);
var inst_31647 = cljs.core.nthnext.call(null,files,(1));
var inst_31648 = files;
var state_31668__$1 = (function (){var statearr_31670 = state_31668;
(statearr_31670[(7)] = inst_31647);

(statearr_31670[(8)] = inst_31648);

(statearr_31670[(9)] = inst_31646);

return statearr_31670;
})();
var statearr_31671_31687 = state_31668__$1;
(statearr_31671_31687[(2)] = null);

(statearr_31671_31687[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31669 === (2))){
var inst_31648 = (state_31668[(8)]);
var inst_31651 = (state_31668[(10)]);
var inst_31651__$1 = cljs.core.nth.call(null,inst_31648,(0),null);
var inst_31652 = cljs.core.nthnext.call(null,inst_31648,(1));
var inst_31653 = (inst_31651__$1 == null);
var inst_31654 = cljs.core.not.call(null,inst_31653);
var state_31668__$1 = (function (){var statearr_31672 = state_31668;
(statearr_31672[(11)] = inst_31652);

(statearr_31672[(10)] = inst_31651__$1);

return statearr_31672;
})();
if(inst_31654){
var statearr_31673_31688 = state_31668__$1;
(statearr_31673_31688[(1)] = (4));

} else {
var statearr_31674_31689 = state_31668__$1;
(statearr_31674_31689[(1)] = (5));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31669 === (3))){
var inst_31666 = (state_31668[(2)]);
var state_31668__$1 = state_31668;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_31668__$1,inst_31666);
} else {
if((state_val_31669 === (4))){
var inst_31651 = (state_31668[(10)]);
var inst_31656 = figwheel.client.file_reloading.reload_js_file.call(null,inst_31651);
var state_31668__$1 = state_31668;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_31668__$1,(7),inst_31656);
} else {
if((state_val_31669 === (5))){
var inst_31662 = cljs.core.async.close_BANG_.call(null,out);
var state_31668__$1 = state_31668;
var statearr_31675_31690 = state_31668__$1;
(statearr_31675_31690[(2)] = inst_31662);

(statearr_31675_31690[(1)] = (6));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31669 === (6))){
var inst_31664 = (state_31668[(2)]);
var state_31668__$1 = state_31668;
var statearr_31676_31691 = state_31668__$1;
(statearr_31676_31691[(2)] = inst_31664);

(statearr_31676_31691[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31669 === (7))){
var inst_31652 = (state_31668[(11)]);
var inst_31658 = (state_31668[(2)]);
var inst_31659 = cljs.core.async.put_BANG_.call(null,out,inst_31658);
var inst_31648 = inst_31652;
var state_31668__$1 = (function (){var statearr_31677 = state_31668;
(statearr_31677[(12)] = inst_31659);

(statearr_31677[(8)] = inst_31648);

return statearr_31677;
})();
var statearr_31678_31692 = state_31668__$1;
(statearr_31678_31692[(2)] = null);

(statearr_31678_31692[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
});})(c__28489__auto___31686,out))
;
return ((function (switch__28377__auto__,c__28489__auto___31686,out){
return (function() {
var figwheel$client$file_reloading$load_all_js_files_$_state_machine__28378__auto__ = null;
var figwheel$client$file_reloading$load_all_js_files_$_state_machine__28378__auto____0 = (function (){
var statearr_31682 = [null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_31682[(0)] = figwheel$client$file_reloading$load_all_js_files_$_state_machine__28378__auto__);

(statearr_31682[(1)] = (1));

return statearr_31682;
});
var figwheel$client$file_reloading$load_all_js_files_$_state_machine__28378__auto____1 = (function (state_31668){
while(true){
var ret_value__28379__auto__ = (function (){try{while(true){
var result__28380__auto__ = switch__28377__auto__.call(null,state_31668);
if(cljs.core.keyword_identical_QMARK_.call(null,result__28380__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__28380__auto__;
}
break;
}
}catch (e31683){if((e31683 instanceof Object)){
var ex__28381__auto__ = e31683;
var statearr_31684_31693 = state_31668;
(statearr_31684_31693[(5)] = ex__28381__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_31668);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e31683;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__28379__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__31694 = state_31668;
state_31668 = G__31694;
continue;
} else {
return ret_value__28379__auto__;
}
break;
}
});
figwheel$client$file_reloading$load_all_js_files_$_state_machine__28378__auto__ = function(state_31668){
switch(arguments.length){
case 0:
return figwheel$client$file_reloading$load_all_js_files_$_state_machine__28378__auto____0.call(this);
case 1:
return figwheel$client$file_reloading$load_all_js_files_$_state_machine__28378__auto____1.call(this,state_31668);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
figwheel$client$file_reloading$load_all_js_files_$_state_machine__28378__auto__.cljs$core$IFn$_invoke$arity$0 = figwheel$client$file_reloading$load_all_js_files_$_state_machine__28378__auto____0;
figwheel$client$file_reloading$load_all_js_files_$_state_machine__28378__auto__.cljs$core$IFn$_invoke$arity$1 = figwheel$client$file_reloading$load_all_js_files_$_state_machine__28378__auto____1;
return figwheel$client$file_reloading$load_all_js_files_$_state_machine__28378__auto__;
})()
;})(switch__28377__auto__,c__28489__auto___31686,out))
})();
var state__28491__auto__ = (function (){var statearr_31685 = f__28490__auto__.call(null);
(statearr_31685[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__28489__auto___31686);

return statearr_31685;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__28491__auto__);
});})(c__28489__auto___31686,out))
);


return cljs.core.async.into.call(null,cljs.core.PersistentVector.EMPTY,out);
});
figwheel.client.file_reloading.eval_body = (function figwheel$client$file_reloading$eval_body(p__31695,opts){
var map__31699 = p__31695;
var map__31699__$1 = ((((!((map__31699 == null)))?((((map__31699.cljs$lang$protocol_mask$partition0$ & (64))) || (map__31699.cljs$core$ISeq$))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__31699):map__31699);
var eval_body__$1 = cljs.core.get.call(null,map__31699__$1,new cljs.core.Keyword(null,"eval-body","eval-body",-907279883));
var file = cljs.core.get.call(null,map__31699__$1,new cljs.core.Keyword(null,"file","file",-1269645878));
if(cljs.core.truth_((function (){var and__23131__auto__ = eval_body__$1;
if(cljs.core.truth_(and__23131__auto__)){
return typeof eval_body__$1 === 'string';
} else {
return and__23131__auto__;
}
})())){
var code = eval_body__$1;
try{figwheel.client.utils.debug_prn.call(null,[cljs.core.str("Evaling file "),cljs.core.str(file)].join(''));

return figwheel.client.utils.eval_helper.call(null,code,opts);
}catch (e31701){var e = e31701;
return figwheel.client.utils.log.call(null,new cljs.core.Keyword(null,"error","error",-978969032),[cljs.core.str("Unable to evaluate "),cljs.core.str(file)].join(''));
}} else {
return null;
}
});
figwheel.client.file_reloading.expand_files = (function figwheel$client$file_reloading$expand_files(files){
var deps = figwheel.client.file_reloading.get_all_dependents.call(null,cljs.core.map.call(null,new cljs.core.Keyword(null,"namespace","namespace",-377510372),files));
return cljs.core.filter.call(null,cljs.core.comp.call(null,cljs.core.not,new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 1, ["figwheel.connect",null], null), null),new cljs.core.Keyword(null,"namespace","namespace",-377510372)),cljs.core.map.call(null,((function (deps){
return (function (n){
var temp__4655__auto__ = cljs.core.first.call(null,cljs.core.filter.call(null,((function (deps){
return (function (p1__31702_SHARP_){
return cljs.core._EQ_.call(null,new cljs.core.Keyword(null,"namespace","namespace",-377510372).cljs$core$IFn$_invoke$arity$1(p1__31702_SHARP_),n);
});})(deps))
,files));
if(cljs.core.truth_(temp__4655__auto__)){
var file_msg = temp__4655__auto__;
return file_msg;
} else {
return new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"type","type",1174270348),new cljs.core.Keyword(null,"namespace","namespace",-377510372),new cljs.core.Keyword(null,"namespace","namespace",-377510372),n], null);
}
});})(deps))
,deps));
});
figwheel.client.file_reloading.sort_files = (function figwheel$client$file_reloading$sort_files(files){
if((cljs.core.count.call(null,files) <= (1))){
return files;
} else {
var keep_files = cljs.core.set.call(null,cljs.core.keep.call(null,new cljs.core.Keyword(null,"namespace","namespace",-377510372),files));
return cljs.core.filter.call(null,cljs.core.comp.call(null,keep_files,new cljs.core.Keyword(null,"namespace","namespace",-377510372)),figwheel.client.file_reloading.expand_files.call(null,files));
}
});
figwheel.client.file_reloading.get_figwheel_always = (function figwheel$client$file_reloading$get_figwheel_always(){
return cljs.core.map.call(null,(function (p__31707){
var vec__31708 = p__31707;
var k = cljs.core.nth.call(null,vec__31708,(0),null);
var v = cljs.core.nth.call(null,vec__31708,(1),null);
return new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"namespace","namespace",-377510372),k,new cljs.core.Keyword(null,"type","type",1174270348),new cljs.core.Keyword(null,"namespace","namespace",-377510372)], null);
}),cljs.core.filter.call(null,(function (p__31709){
var vec__31710 = p__31709;
var k = cljs.core.nth.call(null,vec__31710,(0),null);
var v = cljs.core.nth.call(null,vec__31710,(1),null);
return new cljs.core.Keyword(null,"figwheel-always","figwheel-always",799819691).cljs$core$IFn$_invoke$arity$1(v);
}),cljs.core.deref.call(null,figwheel.client.file_reloading.figwheel_meta_pragmas)));
});
figwheel.client.file_reloading.reload_js_files = (function figwheel$client$file_reloading$reload_js_files(p__31714,p__31715){
var map__31962 = p__31714;
var map__31962__$1 = ((((!((map__31962 == null)))?((((map__31962.cljs$lang$protocol_mask$partition0$ & (64))) || (map__31962.cljs$core$ISeq$))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__31962):map__31962);
var opts = map__31962__$1;
var before_jsload = cljs.core.get.call(null,map__31962__$1,new cljs.core.Keyword(null,"before-jsload","before-jsload",-847513128));
var on_jsload = cljs.core.get.call(null,map__31962__$1,new cljs.core.Keyword(null,"on-jsload","on-jsload",-395756602));
var reload_dependents = cljs.core.get.call(null,map__31962__$1,new cljs.core.Keyword(null,"reload-dependents","reload-dependents",-956865430));
var map__31963 = p__31715;
var map__31963__$1 = ((((!((map__31963 == null)))?((((map__31963.cljs$lang$protocol_mask$partition0$ & (64))) || (map__31963.cljs$core$ISeq$))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__31963):map__31963);
var msg = map__31963__$1;
var files = cljs.core.get.call(null,map__31963__$1,new cljs.core.Keyword(null,"files","files",-472457450));
var figwheel_meta = cljs.core.get.call(null,map__31963__$1,new cljs.core.Keyword(null,"figwheel-meta","figwheel-meta",-225970237));
var recompile_dependents = cljs.core.get.call(null,map__31963__$1,new cljs.core.Keyword(null,"recompile-dependents","recompile-dependents",523804171));
if(cljs.core.empty_QMARK_.call(null,figwheel_meta)){
} else {
cljs.core.reset_BANG_.call(null,figwheel.client.file_reloading.figwheel_meta_pragmas,figwheel_meta);
}

var c__28489__auto__ = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__28489__auto__,map__31962,map__31962__$1,opts,before_jsload,on_jsload,reload_dependents,map__31963,map__31963__$1,msg,files,figwheel_meta,recompile_dependents){
return (function (){
var f__28490__auto__ = (function (){var switch__28377__auto__ = ((function (c__28489__auto__,map__31962,map__31962__$1,opts,before_jsload,on_jsload,reload_dependents,map__31963,map__31963__$1,msg,files,figwheel_meta,recompile_dependents){
return (function (state_32116){
var state_val_32117 = (state_32116[(1)]);
if((state_val_32117 === (7))){
var inst_31977 = (state_32116[(7)]);
var inst_31979 = (state_32116[(8)]);
var inst_31980 = (state_32116[(9)]);
var inst_31978 = (state_32116[(10)]);
var inst_31985 = cljs.core._nth.call(null,inst_31978,inst_31980);
var inst_31986 = figwheel.client.file_reloading.eval_body.call(null,inst_31985,opts);
var inst_31987 = (inst_31980 + (1));
var tmp32118 = inst_31977;
var tmp32119 = inst_31979;
var tmp32120 = inst_31978;
var inst_31977__$1 = tmp32118;
var inst_31978__$1 = tmp32120;
var inst_31979__$1 = tmp32119;
var inst_31980__$1 = inst_31987;
var state_32116__$1 = (function (){var statearr_32121 = state_32116;
(statearr_32121[(11)] = inst_31986);

(statearr_32121[(7)] = inst_31977__$1);

(statearr_32121[(8)] = inst_31979__$1);

(statearr_32121[(9)] = inst_31980__$1);

(statearr_32121[(10)] = inst_31978__$1);

return statearr_32121;
})();
var statearr_32122_32208 = state_32116__$1;
(statearr_32122_32208[(2)] = null);

(statearr_32122_32208[(1)] = (5));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32117 === (20))){
var inst_32020 = (state_32116[(12)]);
var inst_32028 = figwheel.client.file_reloading.sort_files.call(null,inst_32020);
var state_32116__$1 = state_32116;
var statearr_32123_32209 = state_32116__$1;
(statearr_32123_32209[(2)] = inst_32028);

(statearr_32123_32209[(1)] = (21));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32117 === (27))){
var state_32116__$1 = state_32116;
var statearr_32124_32210 = state_32116__$1;
(statearr_32124_32210[(2)] = null);

(statearr_32124_32210[(1)] = (28));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32117 === (1))){
var inst_31969 = (state_32116[(13)]);
var inst_31966 = before_jsload.call(null,files);
var inst_31967 = figwheel.client.file_reloading.before_jsload_custom_event.call(null,files);
var inst_31968 = (function (){return ((function (inst_31969,inst_31966,inst_31967,state_val_32117,c__28489__auto__,map__31962,map__31962__$1,opts,before_jsload,on_jsload,reload_dependents,map__31963,map__31963__$1,msg,files,figwheel_meta,recompile_dependents){
return (function (p1__31711_SHARP_){
return new cljs.core.Keyword(null,"eval-body","eval-body",-907279883).cljs$core$IFn$_invoke$arity$1(p1__31711_SHARP_);
});
;})(inst_31969,inst_31966,inst_31967,state_val_32117,c__28489__auto__,map__31962,map__31962__$1,opts,before_jsload,on_jsload,reload_dependents,map__31963,map__31963__$1,msg,files,figwheel_meta,recompile_dependents))
})();
var inst_31969__$1 = cljs.core.filter.call(null,inst_31968,files);
var inst_31970 = cljs.core.not_empty.call(null,inst_31969__$1);
var state_32116__$1 = (function (){var statearr_32125 = state_32116;
(statearr_32125[(14)] = inst_31967);

(statearr_32125[(15)] = inst_31966);

(statearr_32125[(13)] = inst_31969__$1);

return statearr_32125;
})();
if(cljs.core.truth_(inst_31970)){
var statearr_32126_32211 = state_32116__$1;
(statearr_32126_32211[(1)] = (2));

} else {
var statearr_32127_32212 = state_32116__$1;
(statearr_32127_32212[(1)] = (3));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32117 === (24))){
var state_32116__$1 = state_32116;
var statearr_32128_32213 = state_32116__$1;
(statearr_32128_32213[(2)] = null);

(statearr_32128_32213[(1)] = (25));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32117 === (39))){
var inst_32070 = (state_32116[(16)]);
var state_32116__$1 = state_32116;
var statearr_32129_32214 = state_32116__$1;
(statearr_32129_32214[(2)] = inst_32070);

(statearr_32129_32214[(1)] = (40));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32117 === (46))){
var inst_32111 = (state_32116[(2)]);
var state_32116__$1 = state_32116;
var statearr_32130_32215 = state_32116__$1;
(statearr_32130_32215[(2)] = inst_32111);

(statearr_32130_32215[(1)] = (31));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32117 === (4))){
var inst_32014 = (state_32116[(2)]);
var inst_32015 = cljs.core.List.EMPTY;
var inst_32016 = cljs.core.reset_BANG_.call(null,figwheel.client.file_reloading.dependencies_loaded,inst_32015);
var inst_32017 = (function (){return ((function (inst_32014,inst_32015,inst_32016,state_val_32117,c__28489__auto__,map__31962,map__31962__$1,opts,before_jsload,on_jsload,reload_dependents,map__31963,map__31963__$1,msg,files,figwheel_meta,recompile_dependents){
return (function (p1__31712_SHARP_){
var and__23131__auto__ = new cljs.core.Keyword(null,"namespace","namespace",-377510372).cljs$core$IFn$_invoke$arity$1(p1__31712_SHARP_);
if(cljs.core.truth_(and__23131__auto__)){
return cljs.core.not.call(null,new cljs.core.Keyword(null,"eval-body","eval-body",-907279883).cljs$core$IFn$_invoke$arity$1(p1__31712_SHARP_));
} else {
return and__23131__auto__;
}
});
;})(inst_32014,inst_32015,inst_32016,state_val_32117,c__28489__auto__,map__31962,map__31962__$1,opts,before_jsload,on_jsload,reload_dependents,map__31963,map__31963__$1,msg,files,figwheel_meta,recompile_dependents))
})();
var inst_32018 = cljs.core.filter.call(null,inst_32017,files);
var inst_32019 = figwheel.client.file_reloading.get_figwheel_always.call(null);
var inst_32020 = cljs.core.concat.call(null,inst_32018,inst_32019);
var state_32116__$1 = (function (){var statearr_32131 = state_32116;
(statearr_32131[(17)] = inst_32014);

(statearr_32131[(12)] = inst_32020);

(statearr_32131[(18)] = inst_32016);

return statearr_32131;
})();
if(cljs.core.truth_(reload_dependents)){
var statearr_32132_32216 = state_32116__$1;
(statearr_32132_32216[(1)] = (16));

} else {
var statearr_32133_32217 = state_32116__$1;
(statearr_32133_32217[(1)] = (17));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32117 === (15))){
var inst_32004 = (state_32116[(2)]);
var state_32116__$1 = state_32116;
var statearr_32134_32218 = state_32116__$1;
(statearr_32134_32218[(2)] = inst_32004);

(statearr_32134_32218[(1)] = (12));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32117 === (21))){
var inst_32030 = (state_32116[(19)]);
var inst_32030__$1 = (state_32116[(2)]);
var inst_32031 = figwheel.client.file_reloading.load_all_js_files.call(null,inst_32030__$1);
var state_32116__$1 = (function (){var statearr_32135 = state_32116;
(statearr_32135[(19)] = inst_32030__$1);

return statearr_32135;
})();
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_32116__$1,(22),inst_32031);
} else {
if((state_val_32117 === (31))){
var inst_32114 = (state_32116[(2)]);
var state_32116__$1 = state_32116;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_32116__$1,inst_32114);
} else {
if((state_val_32117 === (32))){
var inst_32070 = (state_32116[(16)]);
var inst_32075 = inst_32070.cljs$lang$protocol_mask$partition0$;
var inst_32076 = (inst_32075 & (64));
var inst_32077 = inst_32070.cljs$core$ISeq$;
var inst_32078 = (inst_32076) || (inst_32077);
var state_32116__$1 = state_32116;
if(cljs.core.truth_(inst_32078)){
var statearr_32136_32219 = state_32116__$1;
(statearr_32136_32219[(1)] = (35));

} else {
var statearr_32137_32220 = state_32116__$1;
(statearr_32137_32220[(1)] = (36));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32117 === (40))){
var inst_32091 = (state_32116[(20)]);
var inst_32090 = (state_32116[(2)]);
var inst_32091__$1 = cljs.core.get.call(null,inst_32090,new cljs.core.Keyword(null,"figwheel-no-load","figwheel-no-load",-555840179));
var inst_32092 = cljs.core.get.call(null,inst_32090,new cljs.core.Keyword(null,"not-required","not-required",-950359114));
var inst_32093 = cljs.core.not_empty.call(null,inst_32091__$1);
var state_32116__$1 = (function (){var statearr_32138 = state_32116;
(statearr_32138[(20)] = inst_32091__$1);

(statearr_32138[(21)] = inst_32092);

return statearr_32138;
})();
if(cljs.core.truth_(inst_32093)){
var statearr_32139_32221 = state_32116__$1;
(statearr_32139_32221[(1)] = (41));

} else {
var statearr_32140_32222 = state_32116__$1;
(statearr_32140_32222[(1)] = (42));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32117 === (33))){
var state_32116__$1 = state_32116;
var statearr_32141_32223 = state_32116__$1;
(statearr_32141_32223[(2)] = false);

(statearr_32141_32223[(1)] = (34));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32117 === (13))){
var inst_31990 = (state_32116[(22)]);
var inst_31994 = cljs.core.chunk_first.call(null,inst_31990);
var inst_31995 = cljs.core.chunk_rest.call(null,inst_31990);
var inst_31996 = cljs.core.count.call(null,inst_31994);
var inst_31977 = inst_31995;
var inst_31978 = inst_31994;
var inst_31979 = inst_31996;
var inst_31980 = (0);
var state_32116__$1 = (function (){var statearr_32142 = state_32116;
(statearr_32142[(7)] = inst_31977);

(statearr_32142[(8)] = inst_31979);

(statearr_32142[(9)] = inst_31980);

(statearr_32142[(10)] = inst_31978);

return statearr_32142;
})();
var statearr_32143_32224 = state_32116__$1;
(statearr_32143_32224[(2)] = null);

(statearr_32143_32224[(1)] = (5));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32117 === (22))){
var inst_32034 = (state_32116[(23)]);
var inst_32033 = (state_32116[(24)]);
var inst_32038 = (state_32116[(25)]);
var inst_32030 = (state_32116[(19)]);
var inst_32033__$1 = (state_32116[(2)]);
var inst_32034__$1 = cljs.core.filter.call(null,new cljs.core.Keyword(null,"loaded-file","loaded-file",-168399375),inst_32033__$1);
var inst_32035 = (function (){var all_files = inst_32030;
var res_SINGLEQUOTE_ = inst_32033__$1;
var res = inst_32034__$1;
return ((function (all_files,res_SINGLEQUOTE_,res,inst_32034,inst_32033,inst_32038,inst_32030,inst_32033__$1,inst_32034__$1,state_val_32117,c__28489__auto__,map__31962,map__31962__$1,opts,before_jsload,on_jsload,reload_dependents,map__31963,map__31963__$1,msg,files,figwheel_meta,recompile_dependents){
return (function (p1__31713_SHARP_){
return cljs.core.not.call(null,new cljs.core.Keyword(null,"loaded-file","loaded-file",-168399375).cljs$core$IFn$_invoke$arity$1(p1__31713_SHARP_));
});
;})(all_files,res_SINGLEQUOTE_,res,inst_32034,inst_32033,inst_32038,inst_32030,inst_32033__$1,inst_32034__$1,state_val_32117,c__28489__auto__,map__31962,map__31962__$1,opts,before_jsload,on_jsload,reload_dependents,map__31963,map__31963__$1,msg,files,figwheel_meta,recompile_dependents))
})();
var inst_32036 = cljs.core.filter.call(null,inst_32035,inst_32033__$1);
var inst_32037 = cljs.core.deref.call(null,figwheel.client.file_reloading.dependencies_loaded);
var inst_32038__$1 = cljs.core.filter.call(null,new cljs.core.Keyword(null,"loaded-file","loaded-file",-168399375),inst_32037);
var inst_32039 = cljs.core.not_empty.call(null,inst_32038__$1);
var state_32116__$1 = (function (){var statearr_32144 = state_32116;
(statearr_32144[(26)] = inst_32036);

(statearr_32144[(23)] = inst_32034__$1);

(statearr_32144[(24)] = inst_32033__$1);

(statearr_32144[(25)] = inst_32038__$1);

return statearr_32144;
})();
if(cljs.core.truth_(inst_32039)){
var statearr_32145_32225 = state_32116__$1;
(statearr_32145_32225[(1)] = (23));

} else {
var statearr_32146_32226 = state_32116__$1;
(statearr_32146_32226[(1)] = (24));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32117 === (36))){
var state_32116__$1 = state_32116;
var statearr_32147_32227 = state_32116__$1;
(statearr_32147_32227[(2)] = false);

(statearr_32147_32227[(1)] = (37));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32117 === (41))){
var inst_32091 = (state_32116[(20)]);
var inst_32095 = cljs.core.comp.call(null,figwheel.client.file_reloading.name__GT_path,new cljs.core.Keyword(null,"namespace","namespace",-377510372));
var inst_32096 = cljs.core.map.call(null,inst_32095,inst_32091);
var inst_32097 = cljs.core.pr_str.call(null,inst_32096);
var inst_32098 = [cljs.core.str("figwheel-no-load meta-data: "),cljs.core.str(inst_32097)].join('');
var inst_32099 = figwheel.client.utils.log.call(null,inst_32098);
var state_32116__$1 = state_32116;
var statearr_32148_32228 = state_32116__$1;
(statearr_32148_32228[(2)] = inst_32099);

(statearr_32148_32228[(1)] = (43));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32117 === (43))){
var inst_32092 = (state_32116[(21)]);
var inst_32102 = (state_32116[(2)]);
var inst_32103 = cljs.core.not_empty.call(null,inst_32092);
var state_32116__$1 = (function (){var statearr_32149 = state_32116;
(statearr_32149[(27)] = inst_32102);

return statearr_32149;
})();
if(cljs.core.truth_(inst_32103)){
var statearr_32150_32229 = state_32116__$1;
(statearr_32150_32229[(1)] = (44));

} else {
var statearr_32151_32230 = state_32116__$1;
(statearr_32151_32230[(1)] = (45));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32117 === (29))){
var inst_32036 = (state_32116[(26)]);
var inst_32070 = (state_32116[(16)]);
var inst_32034 = (state_32116[(23)]);
var inst_32033 = (state_32116[(24)]);
var inst_32038 = (state_32116[(25)]);
var inst_32030 = (state_32116[(19)]);
var inst_32066 = figwheel.client.utils.log.call(null,new cljs.core.Keyword(null,"debug","debug",-1608172596),"Figwheel: NOT loading these files ");
var inst_32069 = (function (){var all_files = inst_32030;
var res_SINGLEQUOTE_ = inst_32033;
var res = inst_32034;
var files_not_loaded = inst_32036;
var dependencies_that_loaded = inst_32038;
return ((function (all_files,res_SINGLEQUOTE_,res,files_not_loaded,dependencies_that_loaded,inst_32036,inst_32070,inst_32034,inst_32033,inst_32038,inst_32030,inst_32066,state_val_32117,c__28489__auto__,map__31962,map__31962__$1,opts,before_jsload,on_jsload,reload_dependents,map__31963,map__31963__$1,msg,files,figwheel_meta,recompile_dependents){
return (function (p__32068){
var map__32152 = p__32068;
var map__32152__$1 = ((((!((map__32152 == null)))?((((map__32152.cljs$lang$protocol_mask$partition0$ & (64))) || (map__32152.cljs$core$ISeq$))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__32152):map__32152);
var namespace = cljs.core.get.call(null,map__32152__$1,new cljs.core.Keyword(null,"namespace","namespace",-377510372));
var meta_data = cljs.core.get.call(null,cljs.core.deref.call(null,figwheel.client.file_reloading.figwheel_meta_pragmas),cljs.core.name.call(null,namespace));
if((meta_data == null)){
return new cljs.core.Keyword(null,"not-required","not-required",-950359114);
} else {
if(cljs.core.truth_(meta_data.call(null,new cljs.core.Keyword(null,"figwheel-no-load","figwheel-no-load",-555840179)))){
return new cljs.core.Keyword(null,"figwheel-no-load","figwheel-no-load",-555840179);
} else {
return new cljs.core.Keyword(null,"not-required","not-required",-950359114);

}
}
});
;})(all_files,res_SINGLEQUOTE_,res,files_not_loaded,dependencies_that_loaded,inst_32036,inst_32070,inst_32034,inst_32033,inst_32038,inst_32030,inst_32066,state_val_32117,c__28489__auto__,map__31962,map__31962__$1,opts,before_jsload,on_jsload,reload_dependents,map__31963,map__31963__$1,msg,files,figwheel_meta,recompile_dependents))
})();
var inst_32070__$1 = cljs.core.group_by.call(null,inst_32069,inst_32036);
var inst_32072 = (inst_32070__$1 == null);
var inst_32073 = cljs.core.not.call(null,inst_32072);
var state_32116__$1 = (function (){var statearr_32154 = state_32116;
(statearr_32154[(16)] = inst_32070__$1);

(statearr_32154[(28)] = inst_32066);

return statearr_32154;
})();
if(inst_32073){
var statearr_32155_32231 = state_32116__$1;
(statearr_32155_32231[(1)] = (32));

} else {
var statearr_32156_32232 = state_32116__$1;
(statearr_32156_32232[(1)] = (33));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32117 === (44))){
var inst_32092 = (state_32116[(21)]);
var inst_32105 = cljs.core.map.call(null,new cljs.core.Keyword(null,"file","file",-1269645878),inst_32092);
var inst_32106 = cljs.core.pr_str.call(null,inst_32105);
var inst_32107 = [cljs.core.str("not required: "),cljs.core.str(inst_32106)].join('');
var inst_32108 = figwheel.client.utils.log.call(null,inst_32107);
var state_32116__$1 = state_32116;
var statearr_32157_32233 = state_32116__$1;
(statearr_32157_32233[(2)] = inst_32108);

(statearr_32157_32233[(1)] = (46));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32117 === (6))){
var inst_32011 = (state_32116[(2)]);
var state_32116__$1 = state_32116;
var statearr_32158_32234 = state_32116__$1;
(statearr_32158_32234[(2)] = inst_32011);

(statearr_32158_32234[(1)] = (4));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32117 === (28))){
var inst_32036 = (state_32116[(26)]);
var inst_32063 = (state_32116[(2)]);
var inst_32064 = cljs.core.not_empty.call(null,inst_32036);
var state_32116__$1 = (function (){var statearr_32159 = state_32116;
(statearr_32159[(29)] = inst_32063);

return statearr_32159;
})();
if(cljs.core.truth_(inst_32064)){
var statearr_32160_32235 = state_32116__$1;
(statearr_32160_32235[(1)] = (29));

} else {
var statearr_32161_32236 = state_32116__$1;
(statearr_32161_32236[(1)] = (30));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32117 === (25))){
var inst_32034 = (state_32116[(23)]);
var inst_32050 = (state_32116[(2)]);
var inst_32051 = cljs.core.not_empty.call(null,inst_32034);
var state_32116__$1 = (function (){var statearr_32162 = state_32116;
(statearr_32162[(30)] = inst_32050);

return statearr_32162;
})();
if(cljs.core.truth_(inst_32051)){
var statearr_32163_32237 = state_32116__$1;
(statearr_32163_32237[(1)] = (26));

} else {
var statearr_32164_32238 = state_32116__$1;
(statearr_32164_32238[(1)] = (27));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32117 === (34))){
var inst_32085 = (state_32116[(2)]);
var state_32116__$1 = state_32116;
if(cljs.core.truth_(inst_32085)){
var statearr_32165_32239 = state_32116__$1;
(statearr_32165_32239[(1)] = (38));

} else {
var statearr_32166_32240 = state_32116__$1;
(statearr_32166_32240[(1)] = (39));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32117 === (17))){
var state_32116__$1 = state_32116;
var statearr_32167_32241 = state_32116__$1;
(statearr_32167_32241[(2)] = recompile_dependents);

(statearr_32167_32241[(1)] = (18));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32117 === (3))){
var state_32116__$1 = state_32116;
var statearr_32168_32242 = state_32116__$1;
(statearr_32168_32242[(2)] = null);

(statearr_32168_32242[(1)] = (4));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32117 === (12))){
var inst_32007 = (state_32116[(2)]);
var state_32116__$1 = state_32116;
var statearr_32169_32243 = state_32116__$1;
(statearr_32169_32243[(2)] = inst_32007);

(statearr_32169_32243[(1)] = (9));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32117 === (2))){
var inst_31969 = (state_32116[(13)]);
var inst_31976 = cljs.core.seq.call(null,inst_31969);
var inst_31977 = inst_31976;
var inst_31978 = null;
var inst_31979 = (0);
var inst_31980 = (0);
var state_32116__$1 = (function (){var statearr_32170 = state_32116;
(statearr_32170[(7)] = inst_31977);

(statearr_32170[(8)] = inst_31979);

(statearr_32170[(9)] = inst_31980);

(statearr_32170[(10)] = inst_31978);

return statearr_32170;
})();
var statearr_32171_32244 = state_32116__$1;
(statearr_32171_32244[(2)] = null);

(statearr_32171_32244[(1)] = (5));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32117 === (23))){
var inst_32036 = (state_32116[(26)]);
var inst_32034 = (state_32116[(23)]);
var inst_32033 = (state_32116[(24)]);
var inst_32038 = (state_32116[(25)]);
var inst_32030 = (state_32116[(19)]);
var inst_32041 = figwheel.client.utils.log.call(null,new cljs.core.Keyword(null,"debug","debug",-1608172596),"Figwheel: loaded these dependencies");
var inst_32043 = (function (){var all_files = inst_32030;
var res_SINGLEQUOTE_ = inst_32033;
var res = inst_32034;
var files_not_loaded = inst_32036;
var dependencies_that_loaded = inst_32038;
return ((function (all_files,res_SINGLEQUOTE_,res,files_not_loaded,dependencies_that_loaded,inst_32036,inst_32034,inst_32033,inst_32038,inst_32030,inst_32041,state_val_32117,c__28489__auto__,map__31962,map__31962__$1,opts,before_jsload,on_jsload,reload_dependents,map__31963,map__31963__$1,msg,files,figwheel_meta,recompile_dependents){
return (function (p__32042){
var map__32172 = p__32042;
var map__32172__$1 = ((((!((map__32172 == null)))?((((map__32172.cljs$lang$protocol_mask$partition0$ & (64))) || (map__32172.cljs$core$ISeq$))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__32172):map__32172);
var request_url = cljs.core.get.call(null,map__32172__$1,new cljs.core.Keyword(null,"request-url","request-url",2100346596));
return clojure.string.replace.call(null,request_url,goog.basePath,"");
});
;})(all_files,res_SINGLEQUOTE_,res,files_not_loaded,dependencies_that_loaded,inst_32036,inst_32034,inst_32033,inst_32038,inst_32030,inst_32041,state_val_32117,c__28489__auto__,map__31962,map__31962__$1,opts,before_jsload,on_jsload,reload_dependents,map__31963,map__31963__$1,msg,files,figwheel_meta,recompile_dependents))
})();
var inst_32044 = cljs.core.reverse.call(null,inst_32038);
var inst_32045 = cljs.core.map.call(null,inst_32043,inst_32044);
var inst_32046 = cljs.core.pr_str.call(null,inst_32045);
var inst_32047 = figwheel.client.utils.log.call(null,inst_32046);
var state_32116__$1 = (function (){var statearr_32174 = state_32116;
(statearr_32174[(31)] = inst_32041);

return statearr_32174;
})();
var statearr_32175_32245 = state_32116__$1;
(statearr_32175_32245[(2)] = inst_32047);

(statearr_32175_32245[(1)] = (25));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32117 === (35))){
var state_32116__$1 = state_32116;
var statearr_32176_32246 = state_32116__$1;
(statearr_32176_32246[(2)] = true);

(statearr_32176_32246[(1)] = (37));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32117 === (19))){
var inst_32020 = (state_32116[(12)]);
var inst_32026 = figwheel.client.file_reloading.expand_files.call(null,inst_32020);
var state_32116__$1 = state_32116;
var statearr_32177_32247 = state_32116__$1;
(statearr_32177_32247[(2)] = inst_32026);

(statearr_32177_32247[(1)] = (21));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32117 === (11))){
var state_32116__$1 = state_32116;
var statearr_32178_32248 = state_32116__$1;
(statearr_32178_32248[(2)] = null);

(statearr_32178_32248[(1)] = (12));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32117 === (9))){
var inst_32009 = (state_32116[(2)]);
var state_32116__$1 = state_32116;
var statearr_32179_32249 = state_32116__$1;
(statearr_32179_32249[(2)] = inst_32009);

(statearr_32179_32249[(1)] = (6));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32117 === (5))){
var inst_31979 = (state_32116[(8)]);
var inst_31980 = (state_32116[(9)]);
var inst_31982 = (inst_31980 < inst_31979);
var inst_31983 = inst_31982;
var state_32116__$1 = state_32116;
if(cljs.core.truth_(inst_31983)){
var statearr_32180_32250 = state_32116__$1;
(statearr_32180_32250[(1)] = (7));

} else {
var statearr_32181_32251 = state_32116__$1;
(statearr_32181_32251[(1)] = (8));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32117 === (14))){
var inst_31990 = (state_32116[(22)]);
var inst_31999 = cljs.core.first.call(null,inst_31990);
var inst_32000 = figwheel.client.file_reloading.eval_body.call(null,inst_31999,opts);
var inst_32001 = cljs.core.next.call(null,inst_31990);
var inst_31977 = inst_32001;
var inst_31978 = null;
var inst_31979 = (0);
var inst_31980 = (0);
var state_32116__$1 = (function (){var statearr_32182 = state_32116;
(statearr_32182[(32)] = inst_32000);

(statearr_32182[(7)] = inst_31977);

(statearr_32182[(8)] = inst_31979);

(statearr_32182[(9)] = inst_31980);

(statearr_32182[(10)] = inst_31978);

return statearr_32182;
})();
var statearr_32183_32252 = state_32116__$1;
(statearr_32183_32252[(2)] = null);

(statearr_32183_32252[(1)] = (5));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32117 === (45))){
var state_32116__$1 = state_32116;
var statearr_32184_32253 = state_32116__$1;
(statearr_32184_32253[(2)] = null);

(statearr_32184_32253[(1)] = (46));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32117 === (26))){
var inst_32036 = (state_32116[(26)]);
var inst_32034 = (state_32116[(23)]);
var inst_32033 = (state_32116[(24)]);
var inst_32038 = (state_32116[(25)]);
var inst_32030 = (state_32116[(19)]);
var inst_32053 = figwheel.client.utils.log.call(null,new cljs.core.Keyword(null,"debug","debug",-1608172596),"Figwheel: loaded these files");
var inst_32055 = (function (){var all_files = inst_32030;
var res_SINGLEQUOTE_ = inst_32033;
var res = inst_32034;
var files_not_loaded = inst_32036;
var dependencies_that_loaded = inst_32038;
return ((function (all_files,res_SINGLEQUOTE_,res,files_not_loaded,dependencies_that_loaded,inst_32036,inst_32034,inst_32033,inst_32038,inst_32030,inst_32053,state_val_32117,c__28489__auto__,map__31962,map__31962__$1,opts,before_jsload,on_jsload,reload_dependents,map__31963,map__31963__$1,msg,files,figwheel_meta,recompile_dependents){
return (function (p__32054){
var map__32185 = p__32054;
var map__32185__$1 = ((((!((map__32185 == null)))?((((map__32185.cljs$lang$protocol_mask$partition0$ & (64))) || (map__32185.cljs$core$ISeq$))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__32185):map__32185);
var namespace = cljs.core.get.call(null,map__32185__$1,new cljs.core.Keyword(null,"namespace","namespace",-377510372));
var file = cljs.core.get.call(null,map__32185__$1,new cljs.core.Keyword(null,"file","file",-1269645878));
if(cljs.core.truth_(namespace)){
return figwheel.client.file_reloading.name__GT_path.call(null,cljs.core.name.call(null,namespace));
} else {
return file;
}
});
;})(all_files,res_SINGLEQUOTE_,res,files_not_loaded,dependencies_that_loaded,inst_32036,inst_32034,inst_32033,inst_32038,inst_32030,inst_32053,state_val_32117,c__28489__auto__,map__31962,map__31962__$1,opts,before_jsload,on_jsload,reload_dependents,map__31963,map__31963__$1,msg,files,figwheel_meta,recompile_dependents))
})();
var inst_32056 = cljs.core.map.call(null,inst_32055,inst_32034);
var inst_32057 = cljs.core.pr_str.call(null,inst_32056);
var inst_32058 = figwheel.client.utils.log.call(null,inst_32057);
var inst_32059 = (function (){var all_files = inst_32030;
var res_SINGLEQUOTE_ = inst_32033;
var res = inst_32034;
var files_not_loaded = inst_32036;
var dependencies_that_loaded = inst_32038;
return ((function (all_files,res_SINGLEQUOTE_,res,files_not_loaded,dependencies_that_loaded,inst_32036,inst_32034,inst_32033,inst_32038,inst_32030,inst_32053,inst_32055,inst_32056,inst_32057,inst_32058,state_val_32117,c__28489__auto__,map__31962,map__31962__$1,opts,before_jsload,on_jsload,reload_dependents,map__31963,map__31963__$1,msg,files,figwheel_meta,recompile_dependents){
return (function (){
figwheel.client.file_reloading.on_jsload_custom_event.call(null,res);

return cljs.core.apply.call(null,on_jsload,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [res], null));
});
;})(all_files,res_SINGLEQUOTE_,res,files_not_loaded,dependencies_that_loaded,inst_32036,inst_32034,inst_32033,inst_32038,inst_32030,inst_32053,inst_32055,inst_32056,inst_32057,inst_32058,state_val_32117,c__28489__auto__,map__31962,map__31962__$1,opts,before_jsload,on_jsload,reload_dependents,map__31963,map__31963__$1,msg,files,figwheel_meta,recompile_dependents))
})();
var inst_32060 = setTimeout(inst_32059,(10));
var state_32116__$1 = (function (){var statearr_32187 = state_32116;
(statearr_32187[(33)] = inst_32058);

(statearr_32187[(34)] = inst_32053);

return statearr_32187;
})();
var statearr_32188_32254 = state_32116__$1;
(statearr_32188_32254[(2)] = inst_32060);

(statearr_32188_32254[(1)] = (28));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32117 === (16))){
var state_32116__$1 = state_32116;
var statearr_32189_32255 = state_32116__$1;
(statearr_32189_32255[(2)] = reload_dependents);

(statearr_32189_32255[(1)] = (18));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32117 === (38))){
var inst_32070 = (state_32116[(16)]);
var inst_32087 = cljs.core.apply.call(null,cljs.core.hash_map,inst_32070);
var state_32116__$1 = state_32116;
var statearr_32190_32256 = state_32116__$1;
(statearr_32190_32256[(2)] = inst_32087);

(statearr_32190_32256[(1)] = (40));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32117 === (30))){
var state_32116__$1 = state_32116;
var statearr_32191_32257 = state_32116__$1;
(statearr_32191_32257[(2)] = null);

(statearr_32191_32257[(1)] = (31));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32117 === (10))){
var inst_31990 = (state_32116[(22)]);
var inst_31992 = cljs.core.chunked_seq_QMARK_.call(null,inst_31990);
var state_32116__$1 = state_32116;
if(inst_31992){
var statearr_32192_32258 = state_32116__$1;
(statearr_32192_32258[(1)] = (13));

} else {
var statearr_32193_32259 = state_32116__$1;
(statearr_32193_32259[(1)] = (14));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32117 === (18))){
var inst_32024 = (state_32116[(2)]);
var state_32116__$1 = state_32116;
if(cljs.core.truth_(inst_32024)){
var statearr_32194_32260 = state_32116__$1;
(statearr_32194_32260[(1)] = (19));

} else {
var statearr_32195_32261 = state_32116__$1;
(statearr_32195_32261[(1)] = (20));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32117 === (42))){
var state_32116__$1 = state_32116;
var statearr_32196_32262 = state_32116__$1;
(statearr_32196_32262[(2)] = null);

(statearr_32196_32262[(1)] = (43));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32117 === (37))){
var inst_32082 = (state_32116[(2)]);
var state_32116__$1 = state_32116;
var statearr_32197_32263 = state_32116__$1;
(statearr_32197_32263[(2)] = inst_32082);

(statearr_32197_32263[(1)] = (34));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32117 === (8))){
var inst_31977 = (state_32116[(7)]);
var inst_31990 = (state_32116[(22)]);
var inst_31990__$1 = cljs.core.seq.call(null,inst_31977);
var state_32116__$1 = (function (){var statearr_32198 = state_32116;
(statearr_32198[(22)] = inst_31990__$1);

return statearr_32198;
})();
if(inst_31990__$1){
var statearr_32199_32264 = state_32116__$1;
(statearr_32199_32264[(1)] = (10));

} else {
var statearr_32200_32265 = state_32116__$1;
(statearr_32200_32265[(1)] = (11));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__28489__auto__,map__31962,map__31962__$1,opts,before_jsload,on_jsload,reload_dependents,map__31963,map__31963__$1,msg,files,figwheel_meta,recompile_dependents))
;
return ((function (switch__28377__auto__,c__28489__auto__,map__31962,map__31962__$1,opts,before_jsload,on_jsload,reload_dependents,map__31963,map__31963__$1,msg,files,figwheel_meta,recompile_dependents){
return (function() {
var figwheel$client$file_reloading$reload_js_files_$_state_machine__28378__auto__ = null;
var figwheel$client$file_reloading$reload_js_files_$_state_machine__28378__auto____0 = (function (){
var statearr_32204 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_32204[(0)] = figwheel$client$file_reloading$reload_js_files_$_state_machine__28378__auto__);

(statearr_32204[(1)] = (1));

return statearr_32204;
});
var figwheel$client$file_reloading$reload_js_files_$_state_machine__28378__auto____1 = (function (state_32116){
while(true){
var ret_value__28379__auto__ = (function (){try{while(true){
var result__28380__auto__ = switch__28377__auto__.call(null,state_32116);
if(cljs.core.keyword_identical_QMARK_.call(null,result__28380__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__28380__auto__;
}
break;
}
}catch (e32205){if((e32205 instanceof Object)){
var ex__28381__auto__ = e32205;
var statearr_32206_32266 = state_32116;
(statearr_32206_32266[(5)] = ex__28381__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_32116);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e32205;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__28379__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__32267 = state_32116;
state_32116 = G__32267;
continue;
} else {
return ret_value__28379__auto__;
}
break;
}
});
figwheel$client$file_reloading$reload_js_files_$_state_machine__28378__auto__ = function(state_32116){
switch(arguments.length){
case 0:
return figwheel$client$file_reloading$reload_js_files_$_state_machine__28378__auto____0.call(this);
case 1:
return figwheel$client$file_reloading$reload_js_files_$_state_machine__28378__auto____1.call(this,state_32116);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
figwheel$client$file_reloading$reload_js_files_$_state_machine__28378__auto__.cljs$core$IFn$_invoke$arity$0 = figwheel$client$file_reloading$reload_js_files_$_state_machine__28378__auto____0;
figwheel$client$file_reloading$reload_js_files_$_state_machine__28378__auto__.cljs$core$IFn$_invoke$arity$1 = figwheel$client$file_reloading$reload_js_files_$_state_machine__28378__auto____1;
return figwheel$client$file_reloading$reload_js_files_$_state_machine__28378__auto__;
})()
;})(switch__28377__auto__,c__28489__auto__,map__31962,map__31962__$1,opts,before_jsload,on_jsload,reload_dependents,map__31963,map__31963__$1,msg,files,figwheel_meta,recompile_dependents))
})();
var state__28491__auto__ = (function (){var statearr_32207 = f__28490__auto__.call(null);
(statearr_32207[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__28489__auto__);

return statearr_32207;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__28491__auto__);
});})(c__28489__auto__,map__31962,map__31962__$1,opts,before_jsload,on_jsload,reload_dependents,map__31963,map__31963__$1,msg,files,figwheel_meta,recompile_dependents))
);

return c__28489__auto__;
});
figwheel.client.file_reloading.current_links = (function figwheel$client$file_reloading$current_links(){
return Array.prototype.slice.call(document.getElementsByTagName("link"));
});
figwheel.client.file_reloading.truncate_url = (function figwheel$client$file_reloading$truncate_url(url){
return clojure.string.replace_first.call(null,clojure.string.replace_first.call(null,clojure.string.replace_first.call(null,clojure.string.replace_first.call(null,cljs.core.first.call(null,clojure.string.split.call(null,url,/\?/)),[cljs.core.str(location.protocol),cljs.core.str("//")].join(''),""),".*://",""),/^\/\//,""),/[^\\/]*/,"");
});
figwheel.client.file_reloading.matches_file_QMARK_ = (function figwheel$client$file_reloading$matches_file_QMARK_(p__32270,link){
var map__32273 = p__32270;
var map__32273__$1 = ((((!((map__32273 == null)))?((((map__32273.cljs$lang$protocol_mask$partition0$ & (64))) || (map__32273.cljs$core$ISeq$))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__32273):map__32273);
var file = cljs.core.get.call(null,map__32273__$1,new cljs.core.Keyword(null,"file","file",-1269645878));
var temp__4657__auto__ = link.href;
if(cljs.core.truth_(temp__4657__auto__)){
var link_href = temp__4657__auto__;
var match = clojure.string.join.call(null,"/",cljs.core.take_while.call(null,cljs.core.identity,cljs.core.map.call(null,((function (link_href,temp__4657__auto__,map__32273,map__32273__$1,file){
return (function (p1__32268_SHARP_,p2__32269_SHARP_){
if(cljs.core._EQ_.call(null,p1__32268_SHARP_,p2__32269_SHARP_)){
return p1__32268_SHARP_;
} else {
return false;
}
});})(link_href,temp__4657__auto__,map__32273,map__32273__$1,file))
,cljs.core.reverse.call(null,clojure.string.split.call(null,file,"/")),cljs.core.reverse.call(null,clojure.string.split.call(null,figwheel.client.file_reloading.truncate_url.call(null,link_href),"/")))));
var match_length = cljs.core.count.call(null,match);
var file_name_length = cljs.core.count.call(null,cljs.core.last.call(null,clojure.string.split.call(null,file,"/")));
if((match_length >= file_name_length)){
return new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword(null,"link","link",-1769163468),link,new cljs.core.Keyword(null,"link-href","link-href",-250644450),link_href,new cljs.core.Keyword(null,"match-length","match-length",1101537310),match_length,new cljs.core.Keyword(null,"current-url-length","current-url-length",380404083),cljs.core.count.call(null,figwheel.client.file_reloading.truncate_url.call(null,link_href))], null);
} else {
return null;
}
} else {
return null;
}
});
figwheel.client.file_reloading.get_correct_link = (function figwheel$client$file_reloading$get_correct_link(f_data){
var temp__4657__auto__ = cljs.core.first.call(null,cljs.core.sort_by.call(null,(function (p__32279){
var map__32280 = p__32279;
var map__32280__$1 = ((((!((map__32280 == null)))?((((map__32280.cljs$lang$protocol_mask$partition0$ & (64))) || (map__32280.cljs$core$ISeq$))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__32280):map__32280);
var match_length = cljs.core.get.call(null,map__32280__$1,new cljs.core.Keyword(null,"match-length","match-length",1101537310));
var current_url_length = cljs.core.get.call(null,map__32280__$1,new cljs.core.Keyword(null,"current-url-length","current-url-length",380404083));
return (current_url_length - match_length);
}),cljs.core.keep.call(null,(function (p1__32275_SHARP_){
return figwheel.client.file_reloading.matches_file_QMARK_.call(null,f_data,p1__32275_SHARP_);
}),figwheel.client.file_reloading.current_links.call(null))));
if(cljs.core.truth_(temp__4657__auto__)){
var res = temp__4657__auto__;
return new cljs.core.Keyword(null,"link","link",-1769163468).cljs$core$IFn$_invoke$arity$1(res);
} else {
return null;
}
});
figwheel.client.file_reloading.clone_link = (function figwheel$client$file_reloading$clone_link(link,url){
var clone = document.createElement("link");
clone.rel = "stylesheet";

clone.media = link.media;

clone.disabled = link.disabled;

clone.href = figwheel.client.file_reloading.add_cache_buster.call(null,url);

return clone;
});
figwheel.client.file_reloading.create_link = (function figwheel$client$file_reloading$create_link(url){
var link = document.createElement("link");
link.rel = "stylesheet";

link.href = figwheel.client.file_reloading.add_cache_buster.call(null,url);

return link;
});
figwheel.client.file_reloading.add_link_to_doc = (function figwheel$client$file_reloading$add_link_to_doc(var_args){
var args32282 = [];
var len__24201__auto___32285 = arguments.length;
var i__24202__auto___32286 = (0);
while(true){
if((i__24202__auto___32286 < len__24201__auto___32285)){
args32282.push((arguments[i__24202__auto___32286]));

var G__32287 = (i__24202__auto___32286 + (1));
i__24202__auto___32286 = G__32287;
continue;
} else {
}
break;
}

var G__32284 = args32282.length;
switch (G__32284) {
case 1:
return figwheel.client.file_reloading.add_link_to_doc.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return figwheel.client.file_reloading.add_link_to_doc.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error([cljs.core.str("Invalid arity: "),cljs.core.str(args32282.length)].join('')));

}
});

figwheel.client.file_reloading.add_link_to_doc.cljs$core$IFn$_invoke$arity$1 = (function (new_link){
return (document.getElementsByTagName("head")[(0)]).appendChild(new_link);
});

figwheel.client.file_reloading.add_link_to_doc.cljs$core$IFn$_invoke$arity$2 = (function (orig_link,klone){
var parent = orig_link.parentNode;
if(cljs.core._EQ_.call(null,orig_link,parent.lastChild)){
parent.appendChild(klone);
} else {
parent.insertBefore(klone,orig_link.nextSibling);
}

return setTimeout(((function (parent){
return (function (){
return parent.removeChild(orig_link);
});})(parent))
,(300));
});

figwheel.client.file_reloading.add_link_to_doc.cljs$lang$maxFixedArity = 2;
figwheel.client.file_reloading.distictify = (function figwheel$client$file_reloading$distictify(key,seqq){
return cljs.core.vals.call(null,cljs.core.reduce.call(null,(function (p1__32289_SHARP_,p2__32290_SHARP_){
return cljs.core.assoc.call(null,p1__32289_SHARP_,cljs.core.get.call(null,p2__32290_SHARP_,key),p2__32290_SHARP_);
}),cljs.core.PersistentArrayMap.EMPTY,seqq));
});
figwheel.client.file_reloading.reload_css_file = (function figwheel$client$file_reloading$reload_css_file(p__32291){
var map__32294 = p__32291;
var map__32294__$1 = ((((!((map__32294 == null)))?((((map__32294.cljs$lang$protocol_mask$partition0$ & (64))) || (map__32294.cljs$core$ISeq$))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__32294):map__32294);
var f_data = map__32294__$1;
var file = cljs.core.get.call(null,map__32294__$1,new cljs.core.Keyword(null,"file","file",-1269645878));
var temp__4657__auto__ = figwheel.client.file_reloading.get_correct_link.call(null,f_data);
if(cljs.core.truth_(temp__4657__auto__)){
var link = temp__4657__auto__;
return figwheel.client.file_reloading.add_link_to_doc.call(null,link,figwheel.client.file_reloading.clone_link.call(null,link,link.href));
} else {
return null;
}
});
figwheel.client.file_reloading.reload_css_files = (function figwheel$client$file_reloading$reload_css_files(p__32296,files_msg){
var map__32303 = p__32296;
var map__32303__$1 = ((((!((map__32303 == null)))?((((map__32303.cljs$lang$protocol_mask$partition0$ & (64))) || (map__32303.cljs$core$ISeq$))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__32303):map__32303);
var opts = map__32303__$1;
var on_cssload = cljs.core.get.call(null,map__32303__$1,new cljs.core.Keyword(null,"on-cssload","on-cssload",1825432318));
if(cljs.core.truth_(figwheel.client.utils.html_env_QMARK_.call(null))){
var seq__32305_32309 = cljs.core.seq.call(null,figwheel.client.file_reloading.distictify.call(null,new cljs.core.Keyword(null,"file","file",-1269645878),new cljs.core.Keyword(null,"files","files",-472457450).cljs$core$IFn$_invoke$arity$1(files_msg)));
var chunk__32306_32310 = null;
var count__32307_32311 = (0);
var i__32308_32312 = (0);
while(true){
if((i__32308_32312 < count__32307_32311)){
var f_32313 = cljs.core._nth.call(null,chunk__32306_32310,i__32308_32312);
figwheel.client.file_reloading.reload_css_file.call(null,f_32313);

var G__32314 = seq__32305_32309;
var G__32315 = chunk__32306_32310;
var G__32316 = count__32307_32311;
var G__32317 = (i__32308_32312 + (1));
seq__32305_32309 = G__32314;
chunk__32306_32310 = G__32315;
count__32307_32311 = G__32316;
i__32308_32312 = G__32317;
continue;
} else {
var temp__4657__auto___32318 = cljs.core.seq.call(null,seq__32305_32309);
if(temp__4657__auto___32318){
var seq__32305_32319__$1 = temp__4657__auto___32318;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__32305_32319__$1)){
var c__23946__auto___32320 = cljs.core.chunk_first.call(null,seq__32305_32319__$1);
var G__32321 = cljs.core.chunk_rest.call(null,seq__32305_32319__$1);
var G__32322 = c__23946__auto___32320;
var G__32323 = cljs.core.count.call(null,c__23946__auto___32320);
var G__32324 = (0);
seq__32305_32309 = G__32321;
chunk__32306_32310 = G__32322;
count__32307_32311 = G__32323;
i__32308_32312 = G__32324;
continue;
} else {
var f_32325 = cljs.core.first.call(null,seq__32305_32319__$1);
figwheel.client.file_reloading.reload_css_file.call(null,f_32325);

var G__32326 = cljs.core.next.call(null,seq__32305_32319__$1);
var G__32327 = null;
var G__32328 = (0);
var G__32329 = (0);
seq__32305_32309 = G__32326;
chunk__32306_32310 = G__32327;
count__32307_32311 = G__32328;
i__32308_32312 = G__32329;
continue;
}
} else {
}
}
break;
}

return setTimeout(((function (map__32303,map__32303__$1,opts,on_cssload){
return (function (){
return on_cssload.call(null,new cljs.core.Keyword(null,"files","files",-472457450).cljs$core$IFn$_invoke$arity$1(files_msg));
});})(map__32303,map__32303__$1,opts,on_cssload))
,(100));
} else {
return null;
}
});

//# sourceMappingURL=file_reloading.js.map