// Compiled by ClojureScript 1.7.170 {}
goog.provide('figwheel.client.heads_up');
goog.require('cljs.core');
goog.require('clojure.string');
goog.require('figwheel.client.socket');
goog.require('cljs.core.async');
goog.require('goog.string');
figwheel.client.heads_up.clear;

figwheel.client.heads_up.cljs_logo_svg;
figwheel.client.heads_up.node = (function figwheel$client$heads_up$node(var_args){
var args__24208__auto__ = [];
var len__24201__auto___32656 = arguments.length;
var i__24202__auto___32657 = (0);
while(true){
if((i__24202__auto___32657 < len__24201__auto___32656)){
args__24208__auto__.push((arguments[i__24202__auto___32657]));

var G__32658 = (i__24202__auto___32657 + (1));
i__24202__auto___32657 = G__32658;
continue;
} else {
}
break;
}

var argseq__24209__auto__ = ((((2) < args__24208__auto__.length))?(new cljs.core.IndexedSeq(args__24208__auto__.slice((2)),(0))):null);
return figwheel.client.heads_up.node.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),(arguments[(1)]),argseq__24209__auto__);
});

figwheel.client.heads_up.node.cljs$core$IFn$_invoke$arity$variadic = (function (t,attrs,children){
var e = document.createElement(cljs.core.name.call(null,t));
var seq__32648_32659 = cljs.core.seq.call(null,cljs.core.keys.call(null,attrs));
var chunk__32649_32660 = null;
var count__32650_32661 = (0);
var i__32651_32662 = (0);
while(true){
if((i__32651_32662 < count__32650_32661)){
var k_32663 = cljs.core._nth.call(null,chunk__32649_32660,i__32651_32662);
e.setAttribute(cljs.core.name.call(null,k_32663),cljs.core.get.call(null,attrs,k_32663));

var G__32664 = seq__32648_32659;
var G__32665 = chunk__32649_32660;
var G__32666 = count__32650_32661;
var G__32667 = (i__32651_32662 + (1));
seq__32648_32659 = G__32664;
chunk__32649_32660 = G__32665;
count__32650_32661 = G__32666;
i__32651_32662 = G__32667;
continue;
} else {
var temp__4657__auto___32668 = cljs.core.seq.call(null,seq__32648_32659);
if(temp__4657__auto___32668){
var seq__32648_32669__$1 = temp__4657__auto___32668;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__32648_32669__$1)){
var c__23946__auto___32670 = cljs.core.chunk_first.call(null,seq__32648_32669__$1);
var G__32671 = cljs.core.chunk_rest.call(null,seq__32648_32669__$1);
var G__32672 = c__23946__auto___32670;
var G__32673 = cljs.core.count.call(null,c__23946__auto___32670);
var G__32674 = (0);
seq__32648_32659 = G__32671;
chunk__32649_32660 = G__32672;
count__32650_32661 = G__32673;
i__32651_32662 = G__32674;
continue;
} else {
var k_32675 = cljs.core.first.call(null,seq__32648_32669__$1);
e.setAttribute(cljs.core.name.call(null,k_32675),cljs.core.get.call(null,attrs,k_32675));

var G__32676 = cljs.core.next.call(null,seq__32648_32669__$1);
var G__32677 = null;
var G__32678 = (0);
var G__32679 = (0);
seq__32648_32659 = G__32676;
chunk__32649_32660 = G__32677;
count__32650_32661 = G__32678;
i__32651_32662 = G__32679;
continue;
}
} else {
}
}
break;
}

var seq__32652_32680 = cljs.core.seq.call(null,children);
var chunk__32653_32681 = null;
var count__32654_32682 = (0);
var i__32655_32683 = (0);
while(true){
if((i__32655_32683 < count__32654_32682)){
var ch_32684 = cljs.core._nth.call(null,chunk__32653_32681,i__32655_32683);
e.appendChild(ch_32684);

var G__32685 = seq__32652_32680;
var G__32686 = chunk__32653_32681;
var G__32687 = count__32654_32682;
var G__32688 = (i__32655_32683 + (1));
seq__32652_32680 = G__32685;
chunk__32653_32681 = G__32686;
count__32654_32682 = G__32687;
i__32655_32683 = G__32688;
continue;
} else {
var temp__4657__auto___32689 = cljs.core.seq.call(null,seq__32652_32680);
if(temp__4657__auto___32689){
var seq__32652_32690__$1 = temp__4657__auto___32689;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__32652_32690__$1)){
var c__23946__auto___32691 = cljs.core.chunk_first.call(null,seq__32652_32690__$1);
var G__32692 = cljs.core.chunk_rest.call(null,seq__32652_32690__$1);
var G__32693 = c__23946__auto___32691;
var G__32694 = cljs.core.count.call(null,c__23946__auto___32691);
var G__32695 = (0);
seq__32652_32680 = G__32692;
chunk__32653_32681 = G__32693;
count__32654_32682 = G__32694;
i__32655_32683 = G__32695;
continue;
} else {
var ch_32696 = cljs.core.first.call(null,seq__32652_32690__$1);
e.appendChild(ch_32696);

var G__32697 = cljs.core.next.call(null,seq__32652_32690__$1);
var G__32698 = null;
var G__32699 = (0);
var G__32700 = (0);
seq__32652_32680 = G__32697;
chunk__32653_32681 = G__32698;
count__32654_32682 = G__32699;
i__32655_32683 = G__32700;
continue;
}
} else {
}
}
break;
}

return e;
});

figwheel.client.heads_up.node.cljs$lang$maxFixedArity = (2);

figwheel.client.heads_up.node.cljs$lang$applyTo = (function (seq32645){
var G__32646 = cljs.core.first.call(null,seq32645);
var seq32645__$1 = cljs.core.next.call(null,seq32645);
var G__32647 = cljs.core.first.call(null,seq32645__$1);
var seq32645__$2 = cljs.core.next.call(null,seq32645__$1);
return figwheel.client.heads_up.node.cljs$core$IFn$_invoke$arity$variadic(G__32646,G__32647,seq32645__$2);
});
if(typeof figwheel.client.heads_up.heads_up_event_dispatch !== 'undefined'){
} else {
figwheel.client.heads_up.heads_up_event_dispatch = (function (){var method_table__24056__auto__ = cljs.core.atom.call(null,cljs.core.PersistentArrayMap.EMPTY);
var prefer_table__24057__auto__ = cljs.core.atom.call(null,cljs.core.PersistentArrayMap.EMPTY);
var method_cache__24058__auto__ = cljs.core.atom.call(null,cljs.core.PersistentArrayMap.EMPTY);
var cached_hierarchy__24059__auto__ = cljs.core.atom.call(null,cljs.core.PersistentArrayMap.EMPTY);
var hierarchy__24060__auto__ = cljs.core.get.call(null,cljs.core.PersistentArrayMap.EMPTY,new cljs.core.Keyword(null,"hierarchy","hierarchy",-1053470341),cljs.core.get_global_hierarchy.call(null));
return (new cljs.core.MultiFn(cljs.core.symbol.call(null,"figwheel.client.heads-up","heads-up-event-dispatch"),((function (method_table__24056__auto__,prefer_table__24057__auto__,method_cache__24058__auto__,cached_hierarchy__24059__auto__,hierarchy__24060__auto__){
return (function (dataset){
return dataset.figwheelEvent;
});})(method_table__24056__auto__,prefer_table__24057__auto__,method_cache__24058__auto__,cached_hierarchy__24059__auto__,hierarchy__24060__auto__))
,new cljs.core.Keyword(null,"default","default",-1987822328),hierarchy__24060__auto__,method_table__24056__auto__,prefer_table__24057__auto__,method_cache__24058__auto__,cached_hierarchy__24059__auto__));
})();
}
cljs.core._add_method.call(null,figwheel.client.heads_up.heads_up_event_dispatch,new cljs.core.Keyword(null,"default","default",-1987822328),(function (_){
return cljs.core.PersistentArrayMap.EMPTY;
}));
cljs.core._add_method.call(null,figwheel.client.heads_up.heads_up_event_dispatch,"file-selected",(function (dataset){
return figwheel.client.socket.send_BANG_.call(null,new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"figwheel-event","figwheel-event",519570592),"file-selected",new cljs.core.Keyword(null,"file-name","file-name",-1654217259),dataset.fileName,new cljs.core.Keyword(null,"file-line","file-line",-1228823138),dataset.fileLine], null));
}));
cljs.core._add_method.call(null,figwheel.client.heads_up.heads_up_event_dispatch,"close-heads-up",(function (dataset){
return figwheel.client.heads_up.clear.call(null);
}));
figwheel.client.heads_up.ancestor_nodes = (function figwheel$client$heads_up$ancestor_nodes(el){
return cljs.core.iterate.call(null,(function (e){
return e.parentNode;
}),el);
});
figwheel.client.heads_up.get_dataset = (function figwheel$client$heads_up$get_dataset(el){
return cljs.core.first.call(null,cljs.core.keep.call(null,(function (x){
if(cljs.core.truth_(x.dataset.figwheelEvent)){
return x.dataset;
} else {
return null;
}
}),cljs.core.take.call(null,(4),figwheel.client.heads_up.ancestor_nodes.call(null,el))));
});
figwheel.client.heads_up.heads_up_onclick_handler = (function figwheel$client$heads_up$heads_up_onclick_handler(event){
var dataset = figwheel.client.heads_up.get_dataset.call(null,event.target);
event.preventDefault();

if(cljs.core.truth_(dataset)){
return figwheel.client.heads_up.heads_up_event_dispatch.call(null,dataset);
} else {
return null;
}
});
figwheel.client.heads_up.ensure_container = (function figwheel$client$heads_up$ensure_container(){
var cont_id = "figwheel-heads-up-container";
var content_id = "figwheel-heads-up-content-area";
if(cljs.core.not.call(null,document.querySelector([cljs.core.str("#"),cljs.core.str(cont_id)].join('')))){
var el_32701 = figwheel.client.heads_up.node.call(null,new cljs.core.Keyword(null,"div","div",1057191632),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"id","id",-1388402092),cont_id,new cljs.core.Keyword(null,"style","style",-496642736),[cljs.core.str("-webkit-transition: all 0.2s ease-in-out;"),cljs.core.str("-moz-transition: all 0.2s ease-in-out;"),cljs.core.str("-o-transition: all 0.2s ease-in-out;"),cljs.core.str("transition: all 0.2s ease-in-out;"),cljs.core.str("font-size: 13px;"),cljs.core.str("border-top: 1px solid #f5f5f5;"),cljs.core.str("box-shadow: 0px 0px 1px #aaaaaa;"),cljs.core.str("line-height: 18px;"),cljs.core.str("color: #333;"),cljs.core.str("font-family: monospace;"),cljs.core.str("padding: 0px 10px 0px 70px;"),cljs.core.str("position: fixed;"),cljs.core.str("bottom: 0px;"),cljs.core.str("left: 0px;"),cljs.core.str("height: 0px;"),cljs.core.str("opacity: 0.0;"),cljs.core.str("box-sizing: border-box;"),cljs.core.str("z-index: 10000;")].join('')], null));
el_32701.onclick = figwheel.client.heads_up.heads_up_onclick_handler;

el_32701.innerHTML = figwheel.client.heads_up.cljs_logo_svg;

el_32701.appendChild(figwheel.client.heads_up.node.call(null,new cljs.core.Keyword(null,"div","div",1057191632),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"id","id",-1388402092),content_id], null)));

document.body.appendChild(el_32701);
} else {
}

return new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"container-el","container-el",109664205),document.getElementById(cont_id),new cljs.core.Keyword(null,"content-area-el","content-area-el",742757187),document.getElementById(content_id)], null);
});
figwheel.client.heads_up.set_style_BANG_ = (function figwheel$client$heads_up$set_style_BANG_(p__32702,st_map){
var map__32707 = p__32702;
var map__32707__$1 = ((((!((map__32707 == null)))?((((map__32707.cljs$lang$protocol_mask$partition0$ & (64))) || (map__32707.cljs$core$ISeq$))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__32707):map__32707);
var container_el = cljs.core.get.call(null,map__32707__$1,new cljs.core.Keyword(null,"container-el","container-el",109664205));
return cljs.core.mapv.call(null,((function (map__32707,map__32707__$1,container_el){
return (function (p__32709){
var vec__32710 = p__32709;
var k = cljs.core.nth.call(null,vec__32710,(0),null);
var v = cljs.core.nth.call(null,vec__32710,(1),null);
return (container_el.style[cljs.core.name.call(null,k)] = v);
});})(map__32707,map__32707__$1,container_el))
,st_map);
});
figwheel.client.heads_up.set_content_BANG_ = (function figwheel$client$heads_up$set_content_BANG_(p__32711,dom_str){
var map__32714 = p__32711;
var map__32714__$1 = ((((!((map__32714 == null)))?((((map__32714.cljs$lang$protocol_mask$partition0$ & (64))) || (map__32714.cljs$core$ISeq$))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__32714):map__32714);
var c = map__32714__$1;
var content_area_el = cljs.core.get.call(null,map__32714__$1,new cljs.core.Keyword(null,"content-area-el","content-area-el",742757187));
return content_area_el.innerHTML = dom_str;
});
figwheel.client.heads_up.get_content = (function figwheel$client$heads_up$get_content(p__32716){
var map__32719 = p__32716;
var map__32719__$1 = ((((!((map__32719 == null)))?((((map__32719.cljs$lang$protocol_mask$partition0$ & (64))) || (map__32719.cljs$core$ISeq$))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__32719):map__32719);
var content_area_el = cljs.core.get.call(null,map__32719__$1,new cljs.core.Keyword(null,"content-area-el","content-area-el",742757187));
return content_area_el.innerHTML;
});
figwheel.client.heads_up.close_link = (function figwheel$client$heads_up$close_link(){
return [cljs.core.str("<a style=\""),cljs.core.str("float: right;"),cljs.core.str("font-size: 18px;"),cljs.core.str("text-decoration: none;"),cljs.core.str("text-align: right;"),cljs.core.str("width: 30px;"),cljs.core.str("height: 30px;"),cljs.core.str("color: rgba(84,84,84, 0.5);"),cljs.core.str("\" href=\"#\"  data-figwheel-event=\"close-heads-up\">"),cljs.core.str("x"),cljs.core.str("</a>")].join('');
});
figwheel.client.heads_up.display_heads_up = (function figwheel$client$heads_up$display_heads_up(style,msg){
var c__28489__auto__ = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__28489__auto__){
return (function (){
var f__28490__auto__ = (function (){var switch__28377__auto__ = ((function (c__28489__auto__){
return (function (state_32762){
var state_val_32763 = (state_32762[(1)]);
if((state_val_32763 === (1))){
var inst_32747 = (state_32762[(7)]);
var inst_32747__$1 = figwheel.client.heads_up.ensure_container.call(null);
var inst_32748 = [new cljs.core.Keyword(null,"paddingTop","paddingTop",-1088692345),new cljs.core.Keyword(null,"paddingBottom","paddingBottom",-916694489),new cljs.core.Keyword(null,"width","width",-384071477),new cljs.core.Keyword(null,"minHeight","minHeight",-1635998980),new cljs.core.Keyword(null,"opacity","opacity",397153780)];
var inst_32749 = ["10px","10px","100%","68px","1.0"];
var inst_32750 = cljs.core.PersistentHashMap.fromArrays(inst_32748,inst_32749);
var inst_32751 = cljs.core.merge.call(null,inst_32750,style);
var inst_32752 = figwheel.client.heads_up.set_style_BANG_.call(null,inst_32747__$1,inst_32751);
var inst_32753 = figwheel.client.heads_up.set_content_BANG_.call(null,inst_32747__$1,msg);
var inst_32754 = cljs.core.async.timeout.call(null,(300));
var state_32762__$1 = (function (){var statearr_32764 = state_32762;
(statearr_32764[(8)] = inst_32753);

(statearr_32764[(7)] = inst_32747__$1);

(statearr_32764[(9)] = inst_32752);

return statearr_32764;
})();
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_32762__$1,(2),inst_32754);
} else {
if((state_val_32763 === (2))){
var inst_32747 = (state_32762[(7)]);
var inst_32756 = (state_32762[(2)]);
var inst_32757 = [new cljs.core.Keyword(null,"height","height",1025178622)];
var inst_32758 = ["auto"];
var inst_32759 = cljs.core.PersistentHashMap.fromArrays(inst_32757,inst_32758);
var inst_32760 = figwheel.client.heads_up.set_style_BANG_.call(null,inst_32747,inst_32759);
var state_32762__$1 = (function (){var statearr_32765 = state_32762;
(statearr_32765[(10)] = inst_32756);

return statearr_32765;
})();
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_32762__$1,inst_32760);
} else {
return null;
}
}
});})(c__28489__auto__))
;
return ((function (switch__28377__auto__,c__28489__auto__){
return (function() {
var figwheel$client$heads_up$display_heads_up_$_state_machine__28378__auto__ = null;
var figwheel$client$heads_up$display_heads_up_$_state_machine__28378__auto____0 = (function (){
var statearr_32769 = [null,null,null,null,null,null,null,null,null,null,null];
(statearr_32769[(0)] = figwheel$client$heads_up$display_heads_up_$_state_machine__28378__auto__);

(statearr_32769[(1)] = (1));

return statearr_32769;
});
var figwheel$client$heads_up$display_heads_up_$_state_machine__28378__auto____1 = (function (state_32762){
while(true){
var ret_value__28379__auto__ = (function (){try{while(true){
var result__28380__auto__ = switch__28377__auto__.call(null,state_32762);
if(cljs.core.keyword_identical_QMARK_.call(null,result__28380__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__28380__auto__;
}
break;
}
}catch (e32770){if((e32770 instanceof Object)){
var ex__28381__auto__ = e32770;
var statearr_32771_32773 = state_32762;
(statearr_32771_32773[(5)] = ex__28381__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_32762);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e32770;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__28379__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__32774 = state_32762;
state_32762 = G__32774;
continue;
} else {
return ret_value__28379__auto__;
}
break;
}
});
figwheel$client$heads_up$display_heads_up_$_state_machine__28378__auto__ = function(state_32762){
switch(arguments.length){
case 0:
return figwheel$client$heads_up$display_heads_up_$_state_machine__28378__auto____0.call(this);
case 1:
return figwheel$client$heads_up$display_heads_up_$_state_machine__28378__auto____1.call(this,state_32762);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
figwheel$client$heads_up$display_heads_up_$_state_machine__28378__auto__.cljs$core$IFn$_invoke$arity$0 = figwheel$client$heads_up$display_heads_up_$_state_machine__28378__auto____0;
figwheel$client$heads_up$display_heads_up_$_state_machine__28378__auto__.cljs$core$IFn$_invoke$arity$1 = figwheel$client$heads_up$display_heads_up_$_state_machine__28378__auto____1;
return figwheel$client$heads_up$display_heads_up_$_state_machine__28378__auto__;
})()
;})(switch__28377__auto__,c__28489__auto__))
})();
var state__28491__auto__ = (function (){var statearr_32772 = f__28490__auto__.call(null);
(statearr_32772[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__28489__auto__);

return statearr_32772;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__28491__auto__);
});})(c__28489__auto__))
);

return c__28489__auto__;
});
figwheel.client.heads_up.heading = (function figwheel$client$heads_up$heading(s){
return [cljs.core.str("<div style=\""),cljs.core.str("font-size: 26px;"),cljs.core.str("line-height: 26px;"),cljs.core.str("margin-bottom: 2px;"),cljs.core.str("padding-top: 1px;"),cljs.core.str("\">"),cljs.core.str(s),cljs.core.str("</div>")].join('');
});
figwheel.client.heads_up.file_and_line_number = (function figwheel$client$heads_up$file_and_line_number(msg){
if(cljs.core.truth_(cljs.core.re_matches.call(null,/.*at\sline.*/,msg))){
return cljs.core.take.call(null,(2),cljs.core.reverse.call(null,clojure.string.split.call(null,msg," ")));
} else {
return null;
}
});
figwheel.client.heads_up.file_selector_div = (function figwheel$client$heads_up$file_selector_div(file_name,line_number,msg){
return [cljs.core.str("<div data-figwheel-event=\"file-selected\" data-file-name=\""),cljs.core.str(file_name),cljs.core.str("\" data-file-line=\""),cljs.core.str(line_number),cljs.core.str("\">"),cljs.core.str(msg),cljs.core.str("</div>")].join('');
});
figwheel.client.heads_up.format_line = (function figwheel$client$heads_up$format_line(msg){
var msg__$1 = goog.string.htmlEscape(msg);
var temp__4655__auto__ = figwheel.client.heads_up.file_and_line_number.call(null,msg__$1);
if(cljs.core.truth_(temp__4655__auto__)){
var vec__32776 = temp__4655__auto__;
var f = cljs.core.nth.call(null,vec__32776,(0),null);
var ln = cljs.core.nth.call(null,vec__32776,(1),null);
return figwheel.client.heads_up.file_selector_div.call(null,f,ln,msg__$1);
} else {
return [cljs.core.str("<div>"),cljs.core.str(msg__$1),cljs.core.str("</div>")].join('');
}
});
figwheel.client.heads_up.display_error = (function figwheel$client$heads_up$display_error(formatted_messages,cause){
var vec__32779 = (cljs.core.truth_(cause)?new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"file","file",-1269645878).cljs$core$IFn$_invoke$arity$1(cause),new cljs.core.Keyword(null,"line","line",212345235).cljs$core$IFn$_invoke$arity$1(cause),new cljs.core.Keyword(null,"column","column",2078222095).cljs$core$IFn$_invoke$arity$1(cause)], null):cljs.core.first.call(null,cljs.core.keep.call(null,figwheel.client.heads_up.file_and_line_number,formatted_messages)));
var file_name = cljs.core.nth.call(null,vec__32779,(0),null);
var file_line = cljs.core.nth.call(null,vec__32779,(1),null);
var file_column = cljs.core.nth.call(null,vec__32779,(2),null);
var msg = cljs.core.apply.call(null,cljs.core.str,cljs.core.map.call(null,((function (vec__32779,file_name,file_line,file_column){
return (function (p1__32777_SHARP_){
return [cljs.core.str("<div>"),cljs.core.str(goog.string.htmlEscape(p1__32777_SHARP_)),cljs.core.str("</div>")].join('');
});})(vec__32779,file_name,file_line,file_column))
,formatted_messages));
return figwheel.client.heads_up.display_heads_up.call(null,new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"backgroundColor","backgroundColor",1738438491),"rgba(255, 161, 161, 0.95)"], null),[cljs.core.str(figwheel.client.heads_up.close_link.call(null)),cljs.core.str(figwheel.client.heads_up.heading.call(null,"Compile Error")),cljs.core.str(figwheel.client.heads_up.file_selector_div.call(null,file_name,(function (){var or__23143__auto__ = file_line;
if(cljs.core.truth_(or__23143__auto__)){
return or__23143__auto__;
} else {
var and__23131__auto__ = cause;
if(cljs.core.truth_(and__23131__auto__)){
return new cljs.core.Keyword(null,"line","line",212345235).cljs$core$IFn$_invoke$arity$1(cause);
} else {
return and__23131__auto__;
}
}
})(),[cljs.core.str(msg),cljs.core.str((cljs.core.truth_(cause)?[cljs.core.str("Error on file "),cljs.core.str(goog.string.htmlEscape(new cljs.core.Keyword(null,"file","file",-1269645878).cljs$core$IFn$_invoke$arity$1(cause))),cljs.core.str(", line "),cljs.core.str(new cljs.core.Keyword(null,"line","line",212345235).cljs$core$IFn$_invoke$arity$1(cause)),cljs.core.str(", column "),cljs.core.str(new cljs.core.Keyword(null,"column","column",2078222095).cljs$core$IFn$_invoke$arity$1(cause))].join(''):""))].join('')))].join(''));
});
figwheel.client.heads_up.display_system_warning = (function figwheel$client$heads_up$display_system_warning(header,msg){
return figwheel.client.heads_up.display_heads_up.call(null,new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"backgroundColor","backgroundColor",1738438491),"rgba(255, 220, 110, 0.95)"], null),[cljs.core.str(figwheel.client.heads_up.close_link.call(null)),cljs.core.str(figwheel.client.heads_up.heading.call(null,header)),cljs.core.str(figwheel.client.heads_up.format_line.call(null,msg))].join(''));
});
figwheel.client.heads_up.display_warning = (function figwheel$client$heads_up$display_warning(msg){
return figwheel.client.heads_up.display_system_warning.call(null,"Compile Warning",msg);
});
figwheel.client.heads_up.append_message = (function figwheel$client$heads_up$append_message(message){
var map__32782 = figwheel.client.heads_up.ensure_container.call(null);
var map__32782__$1 = ((((!((map__32782 == null)))?((((map__32782.cljs$lang$protocol_mask$partition0$ & (64))) || (map__32782.cljs$core$ISeq$))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__32782):map__32782);
var content_area_el = cljs.core.get.call(null,map__32782__$1,new cljs.core.Keyword(null,"content-area-el","content-area-el",742757187));
var el = document.createElement("div");
el.innerHTML = figwheel.client.heads_up.format_line.call(null,message);

return content_area_el.appendChild(el);
});
figwheel.client.heads_up.clear = (function figwheel$client$heads_up$clear(){
var c__28489__auto__ = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__28489__auto__){
return (function (){
var f__28490__auto__ = (function (){var switch__28377__auto__ = ((function (c__28489__auto__){
return (function (state_32830){
var state_val_32831 = (state_32830[(1)]);
if((state_val_32831 === (1))){
var inst_32813 = (state_32830[(7)]);
var inst_32813__$1 = figwheel.client.heads_up.ensure_container.call(null);
var inst_32814 = [new cljs.core.Keyword(null,"opacity","opacity",397153780)];
var inst_32815 = ["0.0"];
var inst_32816 = cljs.core.PersistentHashMap.fromArrays(inst_32814,inst_32815);
var inst_32817 = figwheel.client.heads_up.set_style_BANG_.call(null,inst_32813__$1,inst_32816);
var inst_32818 = cljs.core.async.timeout.call(null,(300));
var state_32830__$1 = (function (){var statearr_32832 = state_32830;
(statearr_32832[(8)] = inst_32817);

(statearr_32832[(7)] = inst_32813__$1);

return statearr_32832;
})();
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_32830__$1,(2),inst_32818);
} else {
if((state_val_32831 === (2))){
var inst_32813 = (state_32830[(7)]);
var inst_32820 = (state_32830[(2)]);
var inst_32821 = [new cljs.core.Keyword(null,"width","width",-384071477),new cljs.core.Keyword(null,"height","height",1025178622),new cljs.core.Keyword(null,"minHeight","minHeight",-1635998980),new cljs.core.Keyword(null,"padding","padding",1660304693),new cljs.core.Keyword(null,"borderRadius","borderRadius",-1505621083),new cljs.core.Keyword(null,"backgroundColor","backgroundColor",1738438491)];
var inst_32822 = ["auto","0px","0px","0px 10px 0px 70px","0px","transparent"];
var inst_32823 = cljs.core.PersistentHashMap.fromArrays(inst_32821,inst_32822);
var inst_32824 = figwheel.client.heads_up.set_style_BANG_.call(null,inst_32813,inst_32823);
var inst_32825 = cljs.core.async.timeout.call(null,(200));
var state_32830__$1 = (function (){var statearr_32833 = state_32830;
(statearr_32833[(9)] = inst_32820);

(statearr_32833[(10)] = inst_32824);

return statearr_32833;
})();
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_32830__$1,(3),inst_32825);
} else {
if((state_val_32831 === (3))){
var inst_32813 = (state_32830[(7)]);
var inst_32827 = (state_32830[(2)]);
var inst_32828 = figwheel.client.heads_up.set_content_BANG_.call(null,inst_32813,"");
var state_32830__$1 = (function (){var statearr_32834 = state_32830;
(statearr_32834[(11)] = inst_32827);

return statearr_32834;
})();
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_32830__$1,inst_32828);
} else {
return null;
}
}
}
});})(c__28489__auto__))
;
return ((function (switch__28377__auto__,c__28489__auto__){
return (function() {
var figwheel$client$heads_up$clear_$_state_machine__28378__auto__ = null;
var figwheel$client$heads_up$clear_$_state_machine__28378__auto____0 = (function (){
var statearr_32838 = [null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_32838[(0)] = figwheel$client$heads_up$clear_$_state_machine__28378__auto__);

(statearr_32838[(1)] = (1));

return statearr_32838;
});
var figwheel$client$heads_up$clear_$_state_machine__28378__auto____1 = (function (state_32830){
while(true){
var ret_value__28379__auto__ = (function (){try{while(true){
var result__28380__auto__ = switch__28377__auto__.call(null,state_32830);
if(cljs.core.keyword_identical_QMARK_.call(null,result__28380__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__28380__auto__;
}
break;
}
}catch (e32839){if((e32839 instanceof Object)){
var ex__28381__auto__ = e32839;
var statearr_32840_32842 = state_32830;
(statearr_32840_32842[(5)] = ex__28381__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_32830);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e32839;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__28379__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__32843 = state_32830;
state_32830 = G__32843;
continue;
} else {
return ret_value__28379__auto__;
}
break;
}
});
figwheel$client$heads_up$clear_$_state_machine__28378__auto__ = function(state_32830){
switch(arguments.length){
case 0:
return figwheel$client$heads_up$clear_$_state_machine__28378__auto____0.call(this);
case 1:
return figwheel$client$heads_up$clear_$_state_machine__28378__auto____1.call(this,state_32830);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
figwheel$client$heads_up$clear_$_state_machine__28378__auto__.cljs$core$IFn$_invoke$arity$0 = figwheel$client$heads_up$clear_$_state_machine__28378__auto____0;
figwheel$client$heads_up$clear_$_state_machine__28378__auto__.cljs$core$IFn$_invoke$arity$1 = figwheel$client$heads_up$clear_$_state_machine__28378__auto____1;
return figwheel$client$heads_up$clear_$_state_machine__28378__auto__;
})()
;})(switch__28377__auto__,c__28489__auto__))
})();
var state__28491__auto__ = (function (){var statearr_32841 = f__28490__auto__.call(null);
(statearr_32841[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__28489__auto__);

return statearr_32841;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__28491__auto__);
});})(c__28489__auto__))
);

return c__28489__auto__;
});
figwheel.client.heads_up.display_loaded_start = (function figwheel$client$heads_up$display_loaded_start(){
return figwheel.client.heads_up.display_heads_up.call(null,new cljs.core.PersistentArrayMap(null, 6, [new cljs.core.Keyword(null,"backgroundColor","backgroundColor",1738438491),"rgba(211,234,172,1.0)",new cljs.core.Keyword(null,"width","width",-384071477),"68px",new cljs.core.Keyword(null,"height","height",1025178622),"68px",new cljs.core.Keyword(null,"paddingLeft","paddingLeft",262720813),"0px",new cljs.core.Keyword(null,"paddingRight","paddingRight",-1642313463),"0px",new cljs.core.Keyword(null,"borderRadius","borderRadius",-1505621083),"35px"], null),"");
});
figwheel.client.heads_up.flash_loaded = (function figwheel$client$heads_up$flash_loaded(){
var c__28489__auto__ = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__28489__auto__){
return (function (){
var f__28490__auto__ = (function (){var switch__28377__auto__ = ((function (c__28489__auto__){
return (function (state_32875){
var state_val_32876 = (state_32875[(1)]);
if((state_val_32876 === (1))){
var inst_32865 = figwheel.client.heads_up.display_loaded_start.call(null);
var state_32875__$1 = state_32875;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_32875__$1,(2),inst_32865);
} else {
if((state_val_32876 === (2))){
var inst_32867 = (state_32875[(2)]);
var inst_32868 = cljs.core.async.timeout.call(null,(400));
var state_32875__$1 = (function (){var statearr_32877 = state_32875;
(statearr_32877[(7)] = inst_32867);

return statearr_32877;
})();
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_32875__$1,(3),inst_32868);
} else {
if((state_val_32876 === (3))){
var inst_32870 = (state_32875[(2)]);
var inst_32871 = figwheel.client.heads_up.clear.call(null);
var state_32875__$1 = (function (){var statearr_32878 = state_32875;
(statearr_32878[(8)] = inst_32870);

return statearr_32878;
})();
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_32875__$1,(4),inst_32871);
} else {
if((state_val_32876 === (4))){
var inst_32873 = (state_32875[(2)]);
var state_32875__$1 = state_32875;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_32875__$1,inst_32873);
} else {
return null;
}
}
}
}
});})(c__28489__auto__))
;
return ((function (switch__28377__auto__,c__28489__auto__){
return (function() {
var figwheel$client$heads_up$flash_loaded_$_state_machine__28378__auto__ = null;
var figwheel$client$heads_up$flash_loaded_$_state_machine__28378__auto____0 = (function (){
var statearr_32882 = [null,null,null,null,null,null,null,null,null];
(statearr_32882[(0)] = figwheel$client$heads_up$flash_loaded_$_state_machine__28378__auto__);

(statearr_32882[(1)] = (1));

return statearr_32882;
});
var figwheel$client$heads_up$flash_loaded_$_state_machine__28378__auto____1 = (function (state_32875){
while(true){
var ret_value__28379__auto__ = (function (){try{while(true){
var result__28380__auto__ = switch__28377__auto__.call(null,state_32875);
if(cljs.core.keyword_identical_QMARK_.call(null,result__28380__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__28380__auto__;
}
break;
}
}catch (e32883){if((e32883 instanceof Object)){
var ex__28381__auto__ = e32883;
var statearr_32884_32886 = state_32875;
(statearr_32884_32886[(5)] = ex__28381__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_32875);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e32883;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__28379__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__32887 = state_32875;
state_32875 = G__32887;
continue;
} else {
return ret_value__28379__auto__;
}
break;
}
});
figwheel$client$heads_up$flash_loaded_$_state_machine__28378__auto__ = function(state_32875){
switch(arguments.length){
case 0:
return figwheel$client$heads_up$flash_loaded_$_state_machine__28378__auto____0.call(this);
case 1:
return figwheel$client$heads_up$flash_loaded_$_state_machine__28378__auto____1.call(this,state_32875);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
figwheel$client$heads_up$flash_loaded_$_state_machine__28378__auto__.cljs$core$IFn$_invoke$arity$0 = figwheel$client$heads_up$flash_loaded_$_state_machine__28378__auto____0;
figwheel$client$heads_up$flash_loaded_$_state_machine__28378__auto__.cljs$core$IFn$_invoke$arity$1 = figwheel$client$heads_up$flash_loaded_$_state_machine__28378__auto____1;
return figwheel$client$heads_up$flash_loaded_$_state_machine__28378__auto__;
})()
;})(switch__28377__auto__,c__28489__auto__))
})();
var state__28491__auto__ = (function (){var statearr_32885 = f__28490__auto__.call(null);
(statearr_32885[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__28489__auto__);

return statearr_32885;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__28491__auto__);
});})(c__28489__auto__))
);

return c__28489__auto__;
});
figwheel.client.heads_up.cljs_logo_svg = "<?xml version='1.0' encoding='utf-8'?>\n<!DOCTYPE svg PUBLIC '-//W3C//DTD SVG 1.1//EN' 'http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd'>\n<svg width='49px' height='49px' style='position:absolute; top:9px; left: 10px;' version='1.1'\n  xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' x='0px' y='0px'\n  viewBox='0 0 428 428' enable-background='new 0 0 428 428' xml:space='preserve'>\n<circle fill='#fff' cx='213' cy='214' r='213' />\n<g>\n<path fill='#96CA4B' d='M122,266.6c-12.7,0-22.3-3.7-28.9-11.1c-6.6-7.4-9.9-18-9.9-31.8c0-14.1,3.4-24.9,10.3-32.5\n  s16.8-11.4,29.9-11.4c8.8,0,16.8,1.6,23.8,4.9l-5.4,14.3c-7.5-2.9-13.7-4.4-18.6-4.4c-14.5,0-21.7,9.6-21.7,28.8\n  c0,9.4,1.8,16.4,5.4,21.2c3.6,4.7,8.9,7.1,15.9,7.1c7.9,0,15.4-2,22.5-5.9v15.5c-3.2,1.9-6.6,3.2-10.2,4\n  C131.5,266.2,127.1,266.6,122,266.6z'/>\n<path fill='#96CA4B' d='M194.4,265.1h-17.8V147.3h17.8V265.1z'/>\n<path fill='#5F7FBF' d='M222.9,302.3c-5.3,0-9.8-0.6-13.3-1.9v-14.1c3.4,0.9,6.9,1.4,10.5,1.4c7.6,0,11.4-4.3,11.4-12.9v-93.5h17.8\n  v94.7c0,8.6-2.3,15.2-6.8,19.6C237.9,300.1,231.4,302.3,222.9,302.3z M230.4,159.2c0-3.2,0.9-5.6,2.6-7.3c1.7-1.7,4.2-2.6,7.5-2.6\n  c3.1,0,5.6,0.9,7.3,2.6c1.7,1.7,2.6,4.2,2.6,7.3c0,3-0.9,5.4-2.6,7.2c-1.7,1.7-4.2,2.6-7.3,2.6c-3.2,0-5.7-0.9-7.5-2.6\n  C231.2,164.6,230.4,162.2,230.4,159.2z'/>\n<path fill='#5F7FBF' d='M342.5,241.3c0,8.2-3,14.4-8.9,18.8c-6,4.4-14.5,6.5-25.6,6.5c-11.2,0-20.1-1.7-26.9-5.1v-15.4\n  c9.8,4.5,19,6.8,27.5,6.8c10.9,0,16.4-3.3,16.4-9.9c0-2.1-0.6-3.9-1.8-5.3c-1.2-1.4-3.2-2.9-6-4.4c-2.8-1.5-6.6-3.2-11.6-5.1\n  c-9.6-3.7-16.2-7.5-19.6-11.2c-3.4-3.7-5.1-8.6-5.1-14.5c0-7.2,2.9-12.7,8.7-16.7c5.8-4,13.6-5.9,23.6-5.9c9.8,0,19.1,2,27.9,6\n  l-5.8,13.4c-9-3.7-16.6-5.6-22.8-5.6c-9.4,0-14.1,2.7-14.1,8c0,2.6,1.2,4.8,3.7,6.7c2.4,1.8,7.8,4.3,16,7.5\n  c6.9,2.7,11.9,5.1,15.1,7.3c3.1,2.2,5.4,4.8,7,7.7C341.7,233.7,342.5,237.2,342.5,241.3z'/>\n</g>\n<path fill='#96CA4B' stroke='#96CA4B' stroke-width='6' stroke-miterlimit='10' d='M197,392.7c-91.2-8.1-163-85-163-178.3\n  S105.8,44.3,197,36.2V16.1c-102.3,8.2-183,94-183,198.4s80.7,190.2,183,198.4V392.7z'/>\n<path fill='#5F7FBF' stroke='#5F7FBF' stroke-width='6' stroke-miterlimit='10' d='M229,16.1v20.1c91.2,8.1,163,85,163,178.3\n  s-71.8,170.2-163,178.3v20.1c102.3-8.2,183-94,183-198.4S331.3,24.3,229,16.1z'/>\n</svg>";

//# sourceMappingURL=heads_up.js.map