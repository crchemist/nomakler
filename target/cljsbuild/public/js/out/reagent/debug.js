// Compiled by ClojureScript 1.7.170 {}
goog.provide('reagent.debug');
goog.require('cljs.core');
reagent.debug.has_console = typeof console !== 'undefined';
reagent.debug.tracking = false;
if(typeof reagent.debug.warnings !== 'undefined'){
} else {
reagent.debug.warnings = cljs.core.atom.call(null,null);
}
if(typeof reagent.debug.track_console !== 'undefined'){
} else {
reagent.debug.track_console = (function (){var o = {};
o.warn = ((function (o){
return (function() { 
var G__24354__delegate = function (args){
return cljs.core.swap_BANG_.call(null,reagent.debug.warnings,cljs.core.update_in,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"warn","warn",-436710552)], null),cljs.core.conj,cljs.core.apply.call(null,cljs.core.str,args));
};
var G__24354 = function (var_args){
var args = null;
if (arguments.length > 0) {
var G__24355__i = 0, G__24355__a = new Array(arguments.length -  0);
while (G__24355__i < G__24355__a.length) {G__24355__a[G__24355__i] = arguments[G__24355__i + 0]; ++G__24355__i;}
  args = new cljs.core.IndexedSeq(G__24355__a,0);
} 
return G__24354__delegate.call(this,args);};
G__24354.cljs$lang$maxFixedArity = 0;
G__24354.cljs$lang$applyTo = (function (arglist__24356){
var args = cljs.core.seq(arglist__24356);
return G__24354__delegate(args);
});
G__24354.cljs$core$IFn$_invoke$arity$variadic = G__24354__delegate;
return G__24354;
})()
;})(o))
;

o.error = ((function (o){
return (function() { 
var G__24357__delegate = function (args){
return cljs.core.swap_BANG_.call(null,reagent.debug.warnings,cljs.core.update_in,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"error","error",-978969032)], null),cljs.core.conj,cljs.core.apply.call(null,cljs.core.str,args));
};
var G__24357 = function (var_args){
var args = null;
if (arguments.length > 0) {
var G__24358__i = 0, G__24358__a = new Array(arguments.length -  0);
while (G__24358__i < G__24358__a.length) {G__24358__a[G__24358__i] = arguments[G__24358__i + 0]; ++G__24358__i;}
  args = new cljs.core.IndexedSeq(G__24358__a,0);
} 
return G__24357__delegate.call(this,args);};
G__24357.cljs$lang$maxFixedArity = 0;
G__24357.cljs$lang$applyTo = (function (arglist__24359){
var args = cljs.core.seq(arglist__24359);
return G__24357__delegate(args);
});
G__24357.cljs$core$IFn$_invoke$arity$variadic = G__24357__delegate;
return G__24357;
})()
;})(o))
;

return o;
})();
}
reagent.debug.track_warnings = (function reagent$debug$track_warnings(f){
reagent.debug.tracking = true;

cljs.core.reset_BANG_.call(null,reagent.debug.warnings,null);

f.call(null);

var warns = cljs.core.deref.call(null,reagent.debug.warnings);
cljs.core.reset_BANG_.call(null,reagent.debug.warnings,null);

reagent.debug.tracking = false;

return warns;
});

//# sourceMappingURL=debug.js.map