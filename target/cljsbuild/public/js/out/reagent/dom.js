// Compiled by ClojureScript 1.7.170 {}
goog.provide('reagent.dom');
goog.require('cljs.core');
goog.require('cljsjs.react.dom');
goog.require('reagent.impl.util');
goog.require('reagent.impl.template');
goog.require('reagent.debug');
goog.require('reagent.interop');
if(typeof reagent.dom.dom !== 'undefined'){
} else {
reagent.dom.dom = (function (){var or__23143__auto__ = (function (){var and__23131__auto__ = typeof ReactDOM !== 'undefined';
if(and__23131__auto__){
return ReactDOM;
} else {
return and__23131__auto__;
}
})();
if(cljs.core.truth_(or__23143__auto__)){
return or__23143__auto__;
} else {
var and__23131__auto__ = typeof require !== 'undefined';
if(and__23131__auto__){
return require("react-dom");
} else {
return and__23131__auto__;
}
}
})();
}
if(cljs.core.truth_(reagent.dom.dom)){
} else {
throw (new Error([cljs.core.str("Assert failed: "),cljs.core.str("Could not find ReactDOM"),cljs.core.str("\n"),cljs.core.str(cljs.core.pr_str.call(null,new cljs.core.Symbol(null,"dom","dom",403993605,null)))].join('')));
}
if(typeof reagent.dom.roots !== 'undefined'){
} else {
reagent.dom.roots = cljs.core.atom.call(null,cljs.core.PersistentArrayMap.EMPTY);
}
reagent.dom.unmount_comp = (function reagent$dom$unmount_comp(container){
cljs.core.swap_BANG_.call(null,reagent.dom.roots,cljs.core.dissoc,container);

return (reagent.dom.dom["unmountComponentAtNode"])(container);
});
reagent.dom.render_comp = (function reagent$dom$render_comp(comp,container,callback){
var _STAR_always_update_STAR_24810 = reagent.impl.util._STAR_always_update_STAR_;
reagent.impl.util._STAR_always_update_STAR_ = true;

try{return (reagent.dom.dom["render"])(comp.call(null),container,((function (_STAR_always_update_STAR_24810){
return (function (){
var _STAR_always_update_STAR_24811 = reagent.impl.util._STAR_always_update_STAR_;
reagent.impl.util._STAR_always_update_STAR_ = false;

try{cljs.core.swap_BANG_.call(null,reagent.dom.roots,cljs.core.assoc,container,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [comp,container], null));

if(cljs.core.some_QMARK_.call(null,callback)){
return callback.call(null);
} else {
return null;
}
}finally {reagent.impl.util._STAR_always_update_STAR_ = _STAR_always_update_STAR_24811;
}});})(_STAR_always_update_STAR_24810))
);
}finally {reagent.impl.util._STAR_always_update_STAR_ = _STAR_always_update_STAR_24810;
}});
reagent.dom.re_render_component = (function reagent$dom$re_render_component(comp,container){
return reagent.dom.render_comp.call(null,comp,container,null);
});
/**
 * Render a Reagent component into the DOM. The first argument may be
 *   either a vector (using Reagent's Hiccup syntax), or a React element. The second argument should be a DOM node.
 * 
 *   Optionally takes a callback that is called when the component is in place.
 * 
 *   Returns the mounted component instance.
 */
reagent.dom.render = (function reagent$dom$render(var_args){
var args24812 = [];
var len__24201__auto___24815 = arguments.length;
var i__24202__auto___24816 = (0);
while(true){
if((i__24202__auto___24816 < len__24201__auto___24815)){
args24812.push((arguments[i__24202__auto___24816]));

var G__24817 = (i__24202__auto___24816 + (1));
i__24202__auto___24816 = G__24817;
continue;
} else {
}
break;
}

var G__24814 = args24812.length;
switch (G__24814) {
case 2:
return reagent.dom.render.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return reagent.dom.render.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error([cljs.core.str("Invalid arity: "),cljs.core.str(args24812.length)].join('')));

}
});

reagent.dom.render.cljs$core$IFn$_invoke$arity$2 = (function (comp,container){
return reagent.dom.render.call(null,comp,container,null);
});

reagent.dom.render.cljs$core$IFn$_invoke$arity$3 = (function (comp,container,callback){
var f = (function (){
return reagent.impl.template.as_element.call(null,((cljs.core.fn_QMARK_.call(null,comp))?comp.call(null):comp));
});
return reagent.dom.render_comp.call(null,f,container,callback);
});

reagent.dom.render.cljs$lang$maxFixedArity = 3;
reagent.dom.unmount_component_at_node = (function reagent$dom$unmount_component_at_node(container){
return reagent.dom.unmount_comp.call(null,container);
});
/**
 * Returns the root DOM node of a mounted component.
 */
reagent.dom.dom_node = (function reagent$dom$dom_node(this$){
return (reagent.dom.dom["findDOMNode"])(this$);
});
reagent.impl.template.find_dom_node = reagent.dom.dom_node;
/**
 * Force re-rendering of all mounted Reagent components. This is
 *   probably only useful in a development environment, when you want to
 *   update components in response to some dynamic changes to code.
 * 
 *   Note that force-update-all may not update root components. This
 *   happens if a component 'foo' is mounted with `(render [foo])` (since
 *   functions are passed by value, and not by reference, in
 *   ClojureScript). To get around this you'll have to introduce a layer
 *   of indirection, for example by using `(render [#'foo])` instead.
 */
reagent.dom.force_update_all = (function reagent$dom$force_update_all(){
var seq__24823_24827 = cljs.core.seq.call(null,cljs.core.vals.call(null,cljs.core.deref.call(null,reagent.dom.roots)));
var chunk__24824_24828 = null;
var count__24825_24829 = (0);
var i__24826_24830 = (0);
while(true){
if((i__24826_24830 < count__24825_24829)){
var v_24831 = cljs.core._nth.call(null,chunk__24824_24828,i__24826_24830);
cljs.core.apply.call(null,reagent.dom.re_render_component,v_24831);

var G__24832 = seq__24823_24827;
var G__24833 = chunk__24824_24828;
var G__24834 = count__24825_24829;
var G__24835 = (i__24826_24830 + (1));
seq__24823_24827 = G__24832;
chunk__24824_24828 = G__24833;
count__24825_24829 = G__24834;
i__24826_24830 = G__24835;
continue;
} else {
var temp__4657__auto___24836 = cljs.core.seq.call(null,seq__24823_24827);
if(temp__4657__auto___24836){
var seq__24823_24837__$1 = temp__4657__auto___24836;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__24823_24837__$1)){
var c__23946__auto___24838 = cljs.core.chunk_first.call(null,seq__24823_24837__$1);
var G__24839 = cljs.core.chunk_rest.call(null,seq__24823_24837__$1);
var G__24840 = c__23946__auto___24838;
var G__24841 = cljs.core.count.call(null,c__23946__auto___24838);
var G__24842 = (0);
seq__24823_24827 = G__24839;
chunk__24824_24828 = G__24840;
count__24825_24829 = G__24841;
i__24826_24830 = G__24842;
continue;
} else {
var v_24843 = cljs.core.first.call(null,seq__24823_24837__$1);
cljs.core.apply.call(null,reagent.dom.re_render_component,v_24843);

var G__24844 = cljs.core.next.call(null,seq__24823_24837__$1);
var G__24845 = null;
var G__24846 = (0);
var G__24847 = (0);
seq__24823_24827 = G__24844;
chunk__24824_24828 = G__24845;
count__24825_24829 = G__24846;
i__24826_24830 = G__24847;
continue;
}
} else {
}
}
break;
}

return "Updated";
});

//# sourceMappingURL=dom.js.map