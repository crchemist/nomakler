// Compiled by ClojureScript 1.7.170 {}
goog.provide('reagent.session');
goog.require('cljs.core');
goog.require('reagent.core');
reagent.session.state = reagent.core.atom.call(null,cljs.core.PersistentArrayMap.EMPTY);
/**
 * Get the key's value from the session, returns nil if it doesn't exist.
 */
reagent.session.get = (function reagent$session$get(var_args){
var args__24208__auto__ = [];
var len__24201__auto___25978 = arguments.length;
var i__24202__auto___25979 = (0);
while(true){
if((i__24202__auto___25979 < len__24201__auto___25978)){
args__24208__auto__.push((arguments[i__24202__auto___25979]));

var G__25980 = (i__24202__auto___25979 + (1));
i__24202__auto___25979 = G__25980;
continue;
} else {
}
break;
}

var argseq__24209__auto__ = ((((1) < args__24208__auto__.length))?(new cljs.core.IndexedSeq(args__24208__auto__.slice((1)),(0))):null);
return reagent.session.get.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),argseq__24209__auto__);
});

reagent.session.get.cljs$core$IFn$_invoke$arity$variadic = (function (k,p__25976){
var vec__25977 = p__25976;
var default$ = cljs.core.nth.call(null,vec__25977,(0),null);
return cljs.core.get.call(null,cljs.core.deref.call(null,reagent.session.state),k,default$);
});

reagent.session.get.cljs$lang$maxFixedArity = (1);

reagent.session.get.cljs$lang$applyTo = (function (seq25974){
var G__25975 = cljs.core.first.call(null,seq25974);
var seq25974__$1 = cljs.core.next.call(null,seq25974);
return reagent.session.get.cljs$core$IFn$_invoke$arity$variadic(G__25975,seq25974__$1);
});
reagent.session.put_BANG_ = (function reagent$session$put_BANG_(k,v){
return cljs.core.swap_BANG_.call(null,reagent.session.state,cljs.core.assoc,k,v);
});
/**
 * Gets the value at the path specified by the vector ks from the session,
 *   returns nil if it doesn't exist.
 */
reagent.session.get_in = (function reagent$session$get_in(var_args){
var args__24208__auto__ = [];
var len__24201__auto___25985 = arguments.length;
var i__24202__auto___25986 = (0);
while(true){
if((i__24202__auto___25986 < len__24201__auto___25985)){
args__24208__auto__.push((arguments[i__24202__auto___25986]));

var G__25987 = (i__24202__auto___25986 + (1));
i__24202__auto___25986 = G__25987;
continue;
} else {
}
break;
}

var argseq__24209__auto__ = ((((1) < args__24208__auto__.length))?(new cljs.core.IndexedSeq(args__24208__auto__.slice((1)),(0))):null);
return reagent.session.get_in.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),argseq__24209__auto__);
});

reagent.session.get_in.cljs$core$IFn$_invoke$arity$variadic = (function (ks,p__25983){
var vec__25984 = p__25983;
var default$ = cljs.core.nth.call(null,vec__25984,(0),null);
return cljs.core.get_in.call(null,cljs.core.deref.call(null,reagent.session.state),ks,default$);
});

reagent.session.get_in.cljs$lang$maxFixedArity = (1);

reagent.session.get_in.cljs$lang$applyTo = (function (seq25981){
var G__25982 = cljs.core.first.call(null,seq25981);
var seq25981__$1 = cljs.core.next.call(null,seq25981);
return reagent.session.get_in.cljs$core$IFn$_invoke$arity$variadic(G__25982,seq25981__$1);
});
/**
 * Replace the current session's value with the result of executing f with
 *   the current value and args.
 */
reagent.session.swap_BANG_ = (function reagent$session$swap_BANG_(var_args){
var args__24208__auto__ = [];
var len__24201__auto___25990 = arguments.length;
var i__24202__auto___25991 = (0);
while(true){
if((i__24202__auto___25991 < len__24201__auto___25990)){
args__24208__auto__.push((arguments[i__24202__auto___25991]));

var G__25992 = (i__24202__auto___25991 + (1));
i__24202__auto___25991 = G__25992;
continue;
} else {
}
break;
}

var argseq__24209__auto__ = ((((1) < args__24208__auto__.length))?(new cljs.core.IndexedSeq(args__24208__auto__.slice((1)),(0))):null);
return reagent.session.swap_BANG_.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),argseq__24209__auto__);
});

reagent.session.swap_BANG_.cljs$core$IFn$_invoke$arity$variadic = (function (f,args){
return cljs.core.apply.call(null,cljs.core.swap_BANG_,reagent.session.state,f,args);
});

reagent.session.swap_BANG_.cljs$lang$maxFixedArity = (1);

reagent.session.swap_BANG_.cljs$lang$applyTo = (function (seq25988){
var G__25989 = cljs.core.first.call(null,seq25988);
var seq25988__$1 = cljs.core.next.call(null,seq25988);
return reagent.session.swap_BANG_.cljs$core$IFn$_invoke$arity$variadic(G__25989,seq25988__$1);
});
/**
 * Remove all data from the session and start over cleanly.
 */
reagent.session.clear_BANG_ = (function reagent$session$clear_BANG_(){
return cljs.core.reset_BANG_.call(null,reagent.session.state,cljs.core.PersistentArrayMap.EMPTY);
});
reagent.session.reset_BANG_ = (function reagent$session$reset_BANG_(m){
return cljs.core.reset_BANG_.call(null,reagent.session.state,m);
});
/**
 * Remove a key from the session
 */
reagent.session.remove_BANG_ = (function reagent$session$remove_BANG_(k){
return cljs.core.swap_BANG_.call(null,reagent.session.state,cljs.core.dissoc,k);
});
/**
 * Associates a value in the session, where ks is a
 * sequence of keys and v is the new value and returns
 * a new nested structure. If any levels do not exist,
 * hash-maps will be created.
 */
reagent.session.assoc_in_BANG_ = (function reagent$session$assoc_in_BANG_(ks,v){
return cljs.core.swap_BANG_.call(null,reagent.session.state,(function (p1__25993_SHARP_){
return cljs.core.assoc_in.call(null,p1__25993_SHARP_,ks,v);
}));
});
/**
 * Destructive get from the session. This returns the current value of the key
 *   and then removes it from the session.
 */
reagent.session.get_BANG_ = (function reagent$session$get_BANG_(var_args){
var args__24208__auto__ = [];
var len__24201__auto___25998 = arguments.length;
var i__24202__auto___25999 = (0);
while(true){
if((i__24202__auto___25999 < len__24201__auto___25998)){
args__24208__auto__.push((arguments[i__24202__auto___25999]));

var G__26000 = (i__24202__auto___25999 + (1));
i__24202__auto___25999 = G__26000;
continue;
} else {
}
break;
}

var argseq__24209__auto__ = ((((1) < args__24208__auto__.length))?(new cljs.core.IndexedSeq(args__24208__auto__.slice((1)),(0))):null);
return reagent.session.get_BANG_.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),argseq__24209__auto__);
});

reagent.session.get_BANG_.cljs$core$IFn$_invoke$arity$variadic = (function (k,p__25996){
var vec__25997 = p__25996;
var default$ = cljs.core.nth.call(null,vec__25997,(0),null);
var cur = reagent.session.get.call(null,k,default$);
reagent.session.remove_BANG_.call(null,k);

return cur;
});

reagent.session.get_BANG_.cljs$lang$maxFixedArity = (1);

reagent.session.get_BANG_.cljs$lang$applyTo = (function (seq25994){
var G__25995 = cljs.core.first.call(null,seq25994);
var seq25994__$1 = cljs.core.next.call(null,seq25994);
return reagent.session.get_BANG_.cljs$core$IFn$_invoke$arity$variadic(G__25995,seq25994__$1);
});
/**
 * Destructive get from the session. This returns the current value of the path
 *   specified by the vector ks and then removes it from the session.
 */
reagent.session.get_in_BANG_ = (function reagent$session$get_in_BANG_(var_args){
var args__24208__auto__ = [];
var len__24201__auto___26005 = arguments.length;
var i__24202__auto___26006 = (0);
while(true){
if((i__24202__auto___26006 < len__24201__auto___26005)){
args__24208__auto__.push((arguments[i__24202__auto___26006]));

var G__26007 = (i__24202__auto___26006 + (1));
i__24202__auto___26006 = G__26007;
continue;
} else {
}
break;
}

var argseq__24209__auto__ = ((((1) < args__24208__auto__.length))?(new cljs.core.IndexedSeq(args__24208__auto__.slice((1)),(0))):null);
return reagent.session.get_in_BANG_.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),argseq__24209__auto__);
});

reagent.session.get_in_BANG_.cljs$core$IFn$_invoke$arity$variadic = (function (ks,p__26003){
var vec__26004 = p__26003;
var default$ = cljs.core.nth.call(null,vec__26004,(0),null);
var cur = cljs.core.get_in.call(null,cljs.core.deref.call(null,reagent.session.state),ks,default$);
reagent.session.assoc_in_BANG_.call(null,ks,null);

return cur;
});

reagent.session.get_in_BANG_.cljs$lang$maxFixedArity = (1);

reagent.session.get_in_BANG_.cljs$lang$applyTo = (function (seq26001){
var G__26002 = cljs.core.first.call(null,seq26001);
var seq26001__$1 = cljs.core.next.call(null,seq26001);
return reagent.session.get_in_BANG_.cljs$core$IFn$_invoke$arity$variadic(G__26002,seq26001__$1);
});
/**
 * Updates a value in session where k is a key and f
 * is the function that takes the old value along with any
 * supplied args and return the new value. If key is not
 * present it will be added.
 */
reagent.session.update_BANG_ = (function reagent$session$update_BANG_(var_args){
var args__24208__auto__ = [];
var len__24201__auto___26012 = arguments.length;
var i__24202__auto___26013 = (0);
while(true){
if((i__24202__auto___26013 < len__24201__auto___26012)){
args__24208__auto__.push((arguments[i__24202__auto___26013]));

var G__26014 = (i__24202__auto___26013 + (1));
i__24202__auto___26013 = G__26014;
continue;
} else {
}
break;
}

var argseq__24209__auto__ = ((((2) < args__24208__auto__.length))?(new cljs.core.IndexedSeq(args__24208__auto__.slice((2)),(0))):null);
return reagent.session.update_BANG_.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),(arguments[(1)]),argseq__24209__auto__);
});

reagent.session.update_BANG_.cljs$core$IFn$_invoke$arity$variadic = (function (k,f,args){
return cljs.core.swap_BANG_.call(null,reagent.session.state,(function (p1__26008_SHARP_){
return cljs.core.apply.call(null,cljs.core.partial.call(null,cljs.core.update,p1__26008_SHARP_,k,f),args);
}));
});

reagent.session.update_BANG_.cljs$lang$maxFixedArity = (2);

reagent.session.update_BANG_.cljs$lang$applyTo = (function (seq26009){
var G__26010 = cljs.core.first.call(null,seq26009);
var seq26009__$1 = cljs.core.next.call(null,seq26009);
var G__26011 = cljs.core.first.call(null,seq26009__$1);
var seq26009__$2 = cljs.core.next.call(null,seq26009__$1);
return reagent.session.update_BANG_.cljs$core$IFn$_invoke$arity$variadic(G__26010,G__26011,seq26009__$2);
});
/**
 * 'Updates a value in the session, where ks is a
 * sequence of keys and f is a function that will
 * take the old value along with any supplied args and return
 * the new value. If any levels do not exist, hash-maps
 * will be created.
 */
reagent.session.update_in_BANG_ = (function reagent$session$update_in_BANG_(var_args){
var args__24208__auto__ = [];
var len__24201__auto___26019 = arguments.length;
var i__24202__auto___26020 = (0);
while(true){
if((i__24202__auto___26020 < len__24201__auto___26019)){
args__24208__auto__.push((arguments[i__24202__auto___26020]));

var G__26021 = (i__24202__auto___26020 + (1));
i__24202__auto___26020 = G__26021;
continue;
} else {
}
break;
}

var argseq__24209__auto__ = ((((2) < args__24208__auto__.length))?(new cljs.core.IndexedSeq(args__24208__auto__.slice((2)),(0))):null);
return reagent.session.update_in_BANG_.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),(arguments[(1)]),argseq__24209__auto__);
});

reagent.session.update_in_BANG_.cljs$core$IFn$_invoke$arity$variadic = (function (ks,f,args){
return cljs.core.swap_BANG_.call(null,reagent.session.state,(function (p1__26015_SHARP_){
return cljs.core.apply.call(null,cljs.core.partial.call(null,cljs.core.update_in,p1__26015_SHARP_,ks,f),args);
}));
});

reagent.session.update_in_BANG_.cljs$lang$maxFixedArity = (2);

reagent.session.update_in_BANG_.cljs$lang$applyTo = (function (seq26016){
var G__26017 = cljs.core.first.call(null,seq26016);
var seq26016__$1 = cljs.core.next.call(null,seq26016);
var G__26018 = cljs.core.first.call(null,seq26016__$1);
var seq26016__$2 = cljs.core.next.call(null,seq26016__$1);
return reagent.session.update_in_BANG_.cljs$core$IFn$_invoke$arity$variadic(G__26017,G__26018,seq26016__$2);
});

//# sourceMappingURL=session.js.map