// Compiled by ClojureScript 1.7.170 {}
goog.provide('nomakler.maps');
goog.require('cljs.core');
goog.require('reagent.core');
goog.require('dommy.core');
goog.require('reagent.session');
goog.require('ajax.core');
goog.require('re_frame.core');
goog.require('nomakler.state');
nomakler.maps.littleton = L.marker([39.61,-105.02]).bindPopup("This is Littleton, CO.");
nomakler.maps.cities = L.layerGroup([nomakler.maps.littleton]);
nomakler.maps.overlay_map = new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"Cities","Cities",572924963),nomakler.maps.cities], null);
nomakler.maps.rent_marker = reagent.core.atom.call(null,null);
nomakler.maps.map_add = (function nomakler$maps$map_add(leaflet_map,leaflet_obj){
if(cljs.core.some_QMARK_.call(null,leaflet_obj)){
return leaflet_obj.addTo(leaflet_map);
} else {
return null;
}
});
nomakler.maps.add_marker_to_layer = (function nomakler$maps$add_marker_to_layer(layer_group,marker_data){
var wkt = cljs.core.get.call(null,marker_data,"location");
var latlng = cljs.core.get.call(null,cljs.core.js__GT_clj.call(null,wellknown.parse(cljs.core.clj__GT_js.call(null,wkt))),"coordinates");
var marker = L.marker(cljs.core.clj__GT_js.call(null,latlng));
return layer_group.addLayer(marker);
});
nomakler.maps.add_markers_to_map = (function nomakler$maps$add_markers_to_map(leaflet_map,markers,appartments){
appartments.clearLayers();

console.log("Clear appartments");

var seq__26935_26939 = cljs.core.seq.call(null,markers);
var chunk__26936_26940 = null;
var count__26937_26941 = (0);
var i__26938_26942 = (0);
while(true){
if((i__26938_26942 < count__26937_26941)){
var marker_26943 = cljs.core._nth.call(null,chunk__26936_26940,i__26938_26942);
nomakler.maps.add_marker_to_layer.call(null,appartments,marker_26943);

var G__26944 = seq__26935_26939;
var G__26945 = chunk__26936_26940;
var G__26946 = count__26937_26941;
var G__26947 = (i__26938_26942 + (1));
seq__26935_26939 = G__26944;
chunk__26936_26940 = G__26945;
count__26937_26941 = G__26946;
i__26938_26942 = G__26947;
continue;
} else {
var temp__4657__auto___26948 = cljs.core.seq.call(null,seq__26935_26939);
if(temp__4657__auto___26948){
var seq__26935_26949__$1 = temp__4657__auto___26948;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__26935_26949__$1)){
var c__23946__auto___26950 = cljs.core.chunk_first.call(null,seq__26935_26949__$1);
var G__26951 = cljs.core.chunk_rest.call(null,seq__26935_26949__$1);
var G__26952 = c__23946__auto___26950;
var G__26953 = cljs.core.count.call(null,c__23946__auto___26950);
var G__26954 = (0);
seq__26935_26939 = G__26951;
chunk__26936_26940 = G__26952;
count__26937_26941 = G__26953;
i__26938_26942 = G__26954;
continue;
} else {
var marker_26955 = cljs.core.first.call(null,seq__26935_26949__$1);
nomakler.maps.add_marker_to_layer.call(null,appartments,marker_26955);

var G__26956 = cljs.core.next.call(null,seq__26935_26949__$1);
var G__26957 = null;
var G__26958 = (0);
var G__26959 = (0);
seq__26935_26939 = G__26956;
chunk__26936_26940 = G__26957;
count__26937_26941 = G__26958;
i__26938_26942 = G__26959;
continue;
}
} else {
}
}
break;
}

return nomakler.maps.map_add.call(null,leaflet_map,appartments);
});
re_frame.core.register_handler.call(null,new cljs.core.Keyword(null,"initialize","initialize",609952913),(function (db,_){
return cljs.core.merge.call(null,db,nomakler.state.initial_state);
}));
re_frame.core.register_handler.call(null,new cljs.core.Keyword(null,"put-markers-on-map","put-markers-on-map",2029998803),(function (db,p__26960){
var vec__26961 = p__26960;
var _ = cljs.core.nth.call(null,vec__26961,(0),null);
var leaflet_map = cljs.core.nth.call(null,vec__26961,(1),null);
var resp = cljs.core.nth.call(null,vec__26961,(2),null);
var markers = cljs.core.get.call(null,resp,"data");
nomakler.maps.add_markers_to_map.call(null,leaflet_map,markers,new cljs.core.Keyword(null,"appartments","appartments",-907406252).cljs$core$IFn$_invoke$arity$1(db));

return cljs.core.assoc.call(null,db,new cljs.core.Keyword(null,"markers","markers",-246919693),resp);
}));
re_frame.core.register_handler.call(null,new cljs.core.Keyword(null,"update-appartments","update-appartments",-650701524),(function (db,p__26963){
var vec__26964 = p__26963;
var _ = cljs.core.nth.call(null,vec__26964,(0),null);
var leaflet_map = cljs.core.nth.call(null,vec__26964,(1),null);
ajax.core.GET.call(null,"/appartments/",new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"handler","handler",-195596612),((function (vec__26964,_,leaflet_map){
return (function (p1__26962_SHARP_){
return re_frame.core.dispatch.call(null,new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"put-markers-on-map","put-markers-on-map",2029998803),leaflet_map,p1__26962_SHARP_], null));
});})(vec__26964,_,leaflet_map))
,new cljs.core.Keyword(null,"response-format","response-format",1664465322),new cljs.core.Keyword(null,"json","json",1279968570)], null));

return db;
}));
re_frame.core.register_sub.call(null,new cljs.core.Keyword(null,"markers","markers",-246919693),(function (db,_){
return reagent.ratom.make_reaction.call(null,(function (){
return new cljs.core.Keyword(null,"markers","markers",-246919693).cljs$core$IFn$_invoke$arity$1(cljs.core.deref.call(null,db));
}));
}));
re_frame.core.register_sub.call(null,new cljs.core.Keyword(null,"appartments","appartments",-907406252),(function (db,_){
return reagent.ratom.make_reaction.call(null,(function (){
return new cljs.core.Keyword(null,"appartments","appartments",-907406252).cljs$core$IFn$_invoke$arity$1(cljs.core.deref.call(null,db));
}));
}));
nomakler.maps.set_rent_marker = (function nomakler$maps$set_rent_marker(marker){
if((cljs.core.deref.call(null,nomakler.maps.rent_marker) == null)){
return cljs.core.reset_BANG_.call(null,nomakler.maps.rent_marker,marker);
} else {
return null;
}
});
nomakler.maps.clear_rent_marker = (function nomakler$maps$clear_rent_marker(leaflet_map){
if(!((cljs.core.deref.call(null,nomakler.maps.rent_marker) == null))){
console.log("Layer removed: ",cljs.core.deref.call(null,nomakler.maps.rent_marker));

leaflet_map.removeLayer(cljs.core.clj__GT_js.call(null,cljs.core.deref.call(null,nomakler.maps.rent_marker)));
} else {
}

return cljs.core.reset_BANG_.call(null,nomakler.maps.rent_marker,null);
});
nomakler.maps.update_appartments = (function nomakler$maps$update_appartments(leaflet_map,resp){
console.log("Update markers: ",leaflet_map,resp);

return re_frame.core.dispatch.call(null,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"markers","markers",-246919693),cljs.core.get.call(null,resp,"data")], null));
});
nomakler.maps.map_did_mount = (function nomakler$maps$map_did_mount(){
var leaflet_map = L.map("map",cljs.core.clj__GT_js.call(null,new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"layers","layers",1944875032),nomakler.maps.cities], null))).setView([49.83,24.07],(12));
var geocoder = L.Control.Geocoder.nominatim();
var geocoder_control = L.Control.geocoder(cljs.core.clj__GT_js.call(null,new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"geocoder","geocoder",-368105851),geocoder], null)));
var markers = re_frame.core.subscribe.call(null,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"markers","markers",-246919693)], null));
var appartments = re_frame.core.subscribe.call(null,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"appartments","appartments",-907406252)], null));
re_frame.core.dispatch.call(null,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"update-appartments","update-appartments",-650701524),leaflet_map], null));

setInterval(((function (leaflet_map,geocoder,geocoder_control,markers,appartments){
return (function (){
return re_frame.core.dispatch.call(null,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"update-appartments","update-appartments",-650701524),leaflet_map], null));
});})(leaflet_map,geocoder,geocoder_control,markers,appartments))
,(5000));

leaflet_map.on("click",((function (leaflet_map,geocoder,geocoder_control,markers,appartments){
return (function (e){
return nomakler.maps.map_add.call(null,leaflet_map,(function (){var latlng = e.latlng;
return L.marker(cljs.core.clj__GT_js.call(null,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [latlng.lat,latlng.lng], null)));
})());
});})(leaflet_map,geocoder,geocoder_control,markers,appartments))
);

leaflet_map.on("layeradd",((function (leaflet_map,geocoder,geocoder_control,markers,appartments){
return (function (e){
var obj = e.layer;
if((cljs.core._EQ_.call(null,cljs.core.type.call(null,obj),L.Marker)) && (cljs.core.not.call(null,cljs.core.deref.call(null,appartments).hasLayer(obj)))){
console.log("Marker added: ",obj._latlng);

nomakler.maps.clear_rent_marker.call(null,leaflet_map);

return nomakler.maps.set_rent_marker.call(null,obj);
} else {
return null;
}
});})(leaflet_map,geocoder,geocoder_control,markers,appartments))
);

nomakler.maps.map_add.call(null,leaflet_map,L.tileLayer("https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}",cljs.core.clj__GT_js.call(null,new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword(null,"attribution","attribution",1937239286),"Map data &copy; <a href=\"http://openstreetmap.org\">OpenStreetMap</a> contributors, <a href=\"http://creativecommons.org/licenses/by-sa/2.0/\">CC-BY-SA</a>, Imagery \u00A9 <a href=\"http://mapbox.com\">Mapbox</a>",new cljs.core.Keyword(null,"maxZoom","maxZoom",566190639),(18),new cljs.core.Keyword(null,"id","id",-1388402092),"crchemist.odi3aamj",new cljs.core.Keyword(null,"accessToken","accessToken",1833707055),"pk.eyJ1IjoiY3JjaGVtaXN0IiwiYSI6ImNpaTE2MnF5cDAwMzF0bmtxMDFoYWVlNmYifQ.HvpKx0YEjteZ9m0P7G7ypg"], null))));

nomakler.maps.map_add.call(null,leaflet_map,geocoder_control);

return nomakler.maps.map_add.call(null,leaflet_map,L.control.layers(cljs.core.clj__GT_js.call(null,null),cljs.core.clj__GT_js.call(null,nomakler.maps.overlay_map),cljs.core.clj__GT_js.call(null,new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"collapsed","collapsed",-628494523),false], null))));
});
nomakler.maps.map_render = (function nomakler$maps$map_render(){
return new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div#map","div#map",173142408)], null);
});
nomakler.maps.leaflet_component = (function nomakler$maps$leaflet_component(){
return reagent.core.create_class.call(null,new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"reagent-render","reagent-render",-985383853),nomakler.maps.map_render,new cljs.core.Keyword(null,"component-did-mount","component-did-mount",-1126910518),nomakler.maps.map_did_mount], null));
});

//# sourceMappingURL=maps.js.map