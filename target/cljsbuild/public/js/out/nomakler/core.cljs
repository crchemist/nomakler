(ns nomakler.core
  (:require [reagent.core :as reagent :refer [atom]]
            [reagent.session :as session]
            [secretary.core :as secretary :include-macros true]
            [goog.events :as events]
            [dommy.core :refer-macros [sel sel1]]
            [goog.history.EventType :as HistoryEventType]
            [markdown.core :refer [md->html]]
            [re-frame.core :refer [register-handler subscribe
                                   dispatch dispatch-sync
                                   register-sub]]
            [ajax.core :refer [GET POST]]
            [nomakler.rent :refer [rent-wizard]]
            [nomakler.state :refer [app-state initial-state]]
            [nomakler.notifications :refer [notification]]
            [nomakler.maps :refer [leaflet-component]])
  (:import goog.History))


(defn nav-link [uri title page collapsed?]
  [:li {:class (when (= page (session/get :page)) "active")}
   [:a {:href uri
        :on-click #(reset! collapsed? true)}
    title]])


(defn navbar []
  (let [collapsed? (atom true)
        username (aget js/context_data "name")
        user-id (aget js/context_data "id")
        server-name (aget js/context_data "server-name")
        server-port (aget js/context_data "server-port")
        dd (.log js/console "Server-name: " server-name)
        auth-text (if username
                    username
                    "Login via VK")
        auth-url (if user-id
                   "/user/logout/"
                   (str "https://oauth.vk.com/authorize?client_id=4230443&display=page&redirect_uri=http%3A//"
                        server-name (if (empty? (str server-port)) "" (str "%3A" server-port))
                        "/vk-callback&response_type=code&v=5.41"))]
    (fn []
      [:nav.navbar.navbar-inverse.navbar-fixed-top
       [:div.container
        [:div.navbar-header
         [:button.navbar-toggle
          {:class         (when-not @collapsed? "collapsed")
           :data-toggle   "collapse"
           :aria-expanded @collapsed?
           :aria-controls "navbar"
           :on-click      #(swap! collapsed? not)}
          [:span.sr-only "Toggle Navigation"]
          [:span.icon-bar]
          [:span.icon-bar]
          [:span.icon-bar]]
         [:a.navbar-brand {:href "#/"} "nomakler"]]
        [:div.navbar-collapse.collapse
         (when-not @collapsed? {:class "in"})
         [:ul.nav.navbar-nav
          [nav-link "#/" "Home" :home collapsed?]]
         [:ul.pull-right.nav.navbar-nav
	  (if (not user-id)
            [nav-link auth-url auth-text :vk-login collapsed?]
	    [:li.dropdown
	      [:a.dropdown-toggle {:data-toggle "dropdown" :role "button" :href "#"}
	                          auth-text
	        [:span.caret]]
	      [:ul.dropdown-menu
	        [:li
		  [:a {:href "/user/logout/"}
		    "Logout"]]]])]]]])))


(defn toolbox []
  [:div.panel-group {:id "accordion"}
    [:div.panel.panel-default
      [:div.panel-heading
        [:h4.panel-title
          [:a {:data-toggle "collapse"
               :data-parent "#accordion"
               :href "#collapseRent"} "Rent appartment"]]]
      [:div.panel-collapse.collapse {:id "collapseRent"}
        [:div.panel-body
          [rent-wizard]]]]
    [:div.panel.panel-default
      [:div.panel-heading
        [:h4.panel-title
          [:a {:data-toggle "collapse"
               :data-parent "#accordion"
               :href "#collapseSearch"} "Find appartment"]]]
      [:div.panel-collapse.collapse {:id "collapseSearch"}
        [:div.panel-body
          [:p "Hello World"]]]]
  ])

;; -------------------------
;; Routes
(secretary/set-config! :prefix "#")


;; -------------------------
;; History
;; must be called after routes have been defined
(defn hook-browser-navigation! []
  (doto (History.)
        (events/listen
          HistoryEventType/NAVIGATE
          (fn [event]
              (secretary/dispatch! (.-token event))))
        (.setEnabled true)))

;; -------------------------
;; Initialize app

(defn mount-components []
  (reagent/render [#'leaflet-component] (.getElementById js/document "map-wrapper"))
  (reagent/render [#'toolbox] (.getElementById js/document "toolbox"))
  (reagent/render [#'notification] (.getElementById js/document "alerts"))
  (reagent/render [#'navbar] (.getElementById js/document "navbar")))

(defn init! []
  (hook-browser-navigation!)
  (dispatch-sync [:initialize])
  (mount-components)
  )
