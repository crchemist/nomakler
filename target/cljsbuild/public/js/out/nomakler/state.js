// Compiled by ClojureScript 1.7.170 {}
goog.provide('nomakler.state');
goog.require('cljs.core');
goog.require('reagent.core');
nomakler.state.app_state = reagent.core.atom.call(null,new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"rent-wizard","rent-wizard",-1564663757),reagent.core.atom.call(null,cljs.core.PersistentArrayMap.EMPTY),new cljs.core.Keyword(null,"map","map",1371690461),reagent.core.atom.call(null,new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"put-marker","put-marker",-635271597),true], null))], null));
nomakler.state.initial_state = new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"markers","markers",-246919693),cljs.core.PersistentVector.EMPTY,new cljs.core.Keyword(null,"appartments","appartments",-907406252),L.layerGroup(cljs.core.clj__GT_js.call(null,cljs.core.PersistentVector.EMPTY)),new cljs.core.Keyword(null,"notifications","notifications",1685638001),new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"message","message",-406056002),"",new cljs.core.Keyword(null,"class","class",-2030961996),"alert-suucess",new cljs.core.Keyword(null,"show?","show?",1543842127),false], null)], null);

//# sourceMappingURL=state.js.map