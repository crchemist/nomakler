(ns nomakler.state
  (:require [reagent.core :as r :refer [atom]]))


(def app-state (atom {:rent-wizard (atom {})
                      :map (atom {:put-marker true})}))

(def initial-state
  {:markers []
   :appartments (.layerGroup js/L (clj->js []))
   :notifications {:message ""
                   :class "alert-suucess"
                   :show? false}})
