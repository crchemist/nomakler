(ns nomakler.notifications
  (:require-macros [reagent.ratom :refer [reaction]])
  (:require [reagent.core :as reagent :refer [atom]]
            [re-frame.core :refer [register-handler subscribe
                                   dispatch dispatch-sync
                                   register-sub]]))

;; -- Event handlers
(register-handler
  :show-alert
  (fn [db [_ message alert-type show]]
    (assoc db :notifications {:show? show
                             :message message
                             :class alert-type})))

;; -- Subsription handlers --
(register-sub
  :notifications
  (fn [db _]
    (reaction  (:notifications @db))))

(defn notification []
  (let [data (subscribe [:notifications])]
    (fn []
      (do
        (.log js/console "Notification rendered")
        (if (:show? @data)
          [:div {:class (str "alert " (:class @data) " alert-dismissible")
                 :role "alert"}
           [:button {:type "button"
                     :class "close"
                     :aria-label "Close"
                     :on-click #(dispatch [:show-alert "" "" false])}
            [:span {:aria-hidden "true"} "x"]] (:message @data)]
          [:div {:class (str "alert " (:class @data) " alert-dismissible")
                 :role "alert"
                 :style {:display "none"}}
           [:button {:type "button"
                     :class "close"
                     :aria-label "Close"
                     :on-click #()}
            [:span {:aria-hidden "true"} "x"]] (:message @data)])))))
