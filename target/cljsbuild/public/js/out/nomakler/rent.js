// Compiled by ClojureScript 1.7.170 {}
goog.provide('nomakler.rent');
goog.require('cljs.core');
goog.require('reagent.core');
goog.require('ajax.core');
goog.require('nomakler.maps');
goog.require('reagent.session');
goog.require('reagent_forms.core');
goog.require('nomakler.state');
goog.require('re_frame.core');
nomakler.rent.steps_count = (2);
nomakler.rent.page_number = reagent.core.atom.call(null,(0));
nomakler.rent.page_put_marker = (function nomakler$rent$page_put_marker(){
return new cljs.core.PersistentVector(null, 4, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div","div",1057191632),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"p","p",151049309),"Please specify place where appartment located."], null),((cljs.core.some_QMARK_.call(null,cljs.core.deref.call(null,nomakler.maps.rent_marker)))?(function (){var latlng = cljs.core.deref.call(null,nomakler.maps.rent_marker)._latlng;
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div","div",1057191632),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"p","p",151049309),"Lat: ",new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"span","span",1394872991),latlng.lat], null)], null),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"p","p",151049309),"Lng: ",new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"span","span",1394872991),latlng.lng], null)], null)], null);
})():null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"p","p",151049309),"Note: put marker on map by left click."], null)], null);
});
nomakler.rent.page_add_details = (function nomakler$rent$page_add_details(){
var form = reagent_forms.core.bind_fields.call(null,new cljs.core.PersistentVector(null, 6, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div","div",1057191632),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"h4","h4",2004862993),"Appartment details:"], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"p","p",151049309),"Price: "], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"input","input",556931961),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"field","field",-1302436500),new cljs.core.Keyword(null,"numeric","numeric",-1495594714),new cljs.core.Keyword(null,"id","id",-1388402092),new cljs.core.Keyword(null,"price","price",22129180)], null)], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"p","p",151049309),"Description: "], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"textarea","textarea",-650375824),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"field","field",-1302436500),new cljs.core.Keyword(null,"textarea","textarea",-650375824),new cljs.core.Keyword(null,"id","id",-1388402092),new cljs.core.Keyword(null,"description","description",-1428560544)], null)], null)], null),new cljs.core.Keyword(null,"rent-wizard","rent-wizard",-1564663757).cljs$core$IFn$_invoke$arity$1(cljs.core.deref.call(null,nomakler.state.app_state)));
return form;
});
nomakler.rent.page_last = (function nomakler$rent$page_last(){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div","div",1057191632),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"p","p",151049309),"Appartment info already collected. Presss 'Finish' to publish data"], null)], null);
});
nomakler.rent.publish_rent_data = (function nomakler$rent$publish_rent_data(ev){
var latlng = cljs.core.deref.call(null,nomakler.maps.rent_marker)._latlng;
ajax.core.POST.call(null,"/appartments/",new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"params","params",710516235),cljs.core.assoc.call(null,cljs.core.deref.call(null,new cljs.core.Keyword(null,"rent-wizard","rent-wizard",-1564663757).cljs$core$IFn$_invoke$arity$1(cljs.core.deref.call(null,nomakler.state.app_state))),new cljs.core.Keyword(null,"location","location",1815599388),[cljs.core.str("POINT("),cljs.core.str(latlng.lat),cljs.core.str(" "),cljs.core.str(latlng.lng),cljs.core.str(")")].join('')),new cljs.core.Keyword(null,"handler","handler",-195596612),((function (latlng){
return (function (){
return re_frame.core.dispatch.call(null,new cljs.core.PersistentVector(null, 4, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"show-alert","show-alert",-906612167),"Data stored","alert-success",true], null));
});})(latlng))
], null));

return cljs.core.reset_BANG_.call(null,nomakler.rent.page_number,(0));
});
nomakler.rent.wizard_next = (function nomakler$rent$wizard_next(){
var d = console.log(cljs.core.clj__GT_js.call(null,cljs.core.deref.call(null,new cljs.core.Keyword(null,"rent-wizard","rent-wizard",-1564663757).cljs$core$IFn$_invoke$arity$1(cljs.core.deref.call(null,nomakler.state.app_state)))));
if((cljs.core.deref.call(null,nomakler.rent.page_number) < nomakler.rent.steps_count)){
if((cljs.core.deref.call(null,nomakler.maps.rent_marker) == null)){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"button.btn.btn-primary.disabled","button.btn.btn-primary.disabled",1819529140),"Continue"], null);
} else {
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"button.btn.btn-primary","button.btn.btn-primary",510358192),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"on-click","on-click",1632826543),((function (d){
return (function (){
return cljs.core.swap_BANG_.call(null,nomakler.rent.page_number,cljs.core.inc);
});})(d))
], null),"Continue"], null);
}
} else {
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"button.btn.btn-primary","button.btn.btn-primary",510358192),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"on-click","on-click",1632826543),nomakler.rent.publish_rent_data], null),"Finish!"], null);
}
});
nomakler.rent.rent_wizard = (function nomakler$rent$rent_wizard(){
var page = ((cljs.core._EQ_.call(null,cljs.core.deref.call(null,nomakler.rent.page_number),(0)))?new cljs.core.Var(function(){return nomakler.rent.page_put_marker;},new cljs.core.Symbol("nomakler.rent","page-put-marker","nomakler.rent/page-put-marker",1725470588,null),cljs.core.PersistentHashMap.fromArrays([new cljs.core.Keyword(null,"ns","ns",441598760),new cljs.core.Keyword(null,"name","name",1843675177),new cljs.core.Keyword(null,"file","file",-1269645878),new cljs.core.Keyword(null,"end-column","end-column",1425389514),new cljs.core.Keyword(null,"column","column",2078222095),new cljs.core.Keyword(null,"line","line",212345235),new cljs.core.Keyword(null,"end-line","end-line",1837326455),new cljs.core.Keyword(null,"arglists","arglists",1661989754),new cljs.core.Keyword(null,"doc","doc",1913296891),new cljs.core.Keyword(null,"test","test",577538877)],[new cljs.core.Symbol(null,"nomakler.rent","nomakler.rent",-1026353995,null),new cljs.core.Symbol(null,"page-put-marker","page-put-marker",-1181433704,null),"/home/crchemist/projects/nomakler/src-cljs/nomakler/rent.cljs",22,1,17,17,cljs.core.list(cljs.core.PersistentVector.EMPTY),null,(cljs.core.truth_(nomakler.rent.page_put_marker)?nomakler.rent.page_put_marker.cljs$lang$test:null)])):((cljs.core._EQ_.call(null,cljs.core.deref.call(null,nomakler.rent.page_number),(1)))?new cljs.core.Var(function(){return nomakler.rent.page_add_details;},new cljs.core.Symbol("nomakler.rent","page-add-details","nomakler.rent/page-add-details",-1065327814,null),cljs.core.PersistentHashMap.fromArrays([new cljs.core.Keyword(null,"ns","ns",441598760),new cljs.core.Keyword(null,"name","name",1843675177),new cljs.core.Keyword(null,"file","file",-1269645878),new cljs.core.Keyword(null,"end-column","end-column",1425389514),new cljs.core.Keyword(null,"column","column",2078222095),new cljs.core.Keyword(null,"line","line",212345235),new cljs.core.Keyword(null,"end-line","end-line",1837326455),new cljs.core.Keyword(null,"arglists","arglists",1661989754),new cljs.core.Keyword(null,"doc","doc",1913296891),new cljs.core.Keyword(null,"test","test",577538877)],[new cljs.core.Symbol(null,"nomakler.rent","nomakler.rent",-1026353995,null),new cljs.core.Symbol(null,"page-add-details","page-add-details",-1178326250,null),"/home/crchemist/projects/nomakler/src-cljs/nomakler/rent.cljs",23,1,29,29,cljs.core.list(cljs.core.PersistentVector.EMPTY),null,(cljs.core.truth_(nomakler.rent.page_add_details)?nomakler.rent.page_add_details.cljs$lang$test:null)])):new cljs.core.Var(function(){return nomakler.rent.page_last;},new cljs.core.Symbol("nomakler.rent","page-last","nomakler.rent/page-last",-1748060950,null),cljs.core.PersistentHashMap.fromArrays([new cljs.core.Keyword(null,"ns","ns",441598760),new cljs.core.Keyword(null,"name","name",1843675177),new cljs.core.Keyword(null,"file","file",-1269645878),new cljs.core.Keyword(null,"end-column","end-column",1425389514),new cljs.core.Keyword(null,"column","column",2078222095),new cljs.core.Keyword(null,"line","line",212345235),new cljs.core.Keyword(null,"end-line","end-line",1837326455),new cljs.core.Keyword(null,"arglists","arglists",1661989754),new cljs.core.Keyword(null,"doc","doc",1913296891),new cljs.core.Keyword(null,"test","test",577538877)],[new cljs.core.Symbol(null,"nomakler.rent","nomakler.rent",-1026353995,null),new cljs.core.Symbol(null,"page-last","page-last",-561329010,null),"/home/crchemist/projects/nomakler/src-cljs/nomakler/rent.cljs",16,1,39,39,cljs.core.list(cljs.core.PersistentVector.EMPTY),null,(cljs.core.truth_(nomakler.rent.page_last)?nomakler.rent.page_last.cljs$lang$test:null)]))
));
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div","div",1057191632),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [page], null),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [nomakler.rent.wizard_next], null)], null);
});

//# sourceMappingURL=rent.js.map