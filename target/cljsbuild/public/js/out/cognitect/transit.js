// Compiled by ClojureScript 1.7.170 {}
goog.provide('cognitect.transit');
goog.require('cljs.core');
goog.require('com.cognitect.transit');
goog.require('com.cognitect.transit.types');
goog.require('com.cognitect.transit.eq');
goog.require('goog.math.Long');
cljs.core.UUID.prototype.cljs$core$IEquiv$ = true;

cljs.core.UUID.prototype.cljs$core$IEquiv$_equiv$arity$2 = (function (this$,other){
var this$__$1 = this;
if((other instanceof cljs.core.UUID)){
return (this$__$1.uuid === other.uuid);
} else {
if((other instanceof com.cognitect.transit.types.UUID)){
return (this$__$1.uuid === other.toString());
} else {
return false;

}
}
});
cljs.core.UUID.prototype.cljs$core$IComparable$ = true;

cljs.core.UUID.prototype.cljs$core$IComparable$_compare$arity$2 = (function (this$,other){
var this$__$1 = this;
if(((other instanceof cljs.core.UUID)) || ((other instanceof com.cognitect.transit.types.UUID))){
return cljs.core.compare.call(null,this$__$1.toString(),other.toString());
} else {
throw (new Error([cljs.core.str("Cannot compare "),cljs.core.str(this$__$1),cljs.core.str(" to "),cljs.core.str(other)].join('')));
}
});

com.cognitect.transit.types.UUID.prototype.cljs$core$IComparable$ = true;

com.cognitect.transit.types.UUID.prototype.cljs$core$IComparable$_compare$arity$2 = (function (this$,other){
var this$__$1 = this;
if(((other instanceof cljs.core.UUID)) || ((other instanceof com.cognitect.transit.types.UUID))){
return cljs.core.compare.call(null,this$__$1.toString(),other.toString());
} else {
throw (new Error([cljs.core.str("Cannot compare "),cljs.core.str(this$__$1),cljs.core.str(" to "),cljs.core.str(other)].join('')));
}
});
goog.math.Long.prototype.cljs$core$IEquiv$ = true;

goog.math.Long.prototype.cljs$core$IEquiv$_equiv$arity$2 = (function (this$,other){
var this$__$1 = this;
return this$__$1.equiv(other);
});

com.cognitect.transit.types.UUID.prototype.cljs$core$IEquiv$ = true;

com.cognitect.transit.types.UUID.prototype.cljs$core$IEquiv$_equiv$arity$2 = (function (this$,other){
var this$__$1 = this;
if((other instanceof cljs.core.UUID)){
return cljs.core._equiv.call(null,other,this$__$1);
} else {
return this$__$1.equiv(other);
}
});

com.cognitect.transit.types.TaggedValue.prototype.cljs$core$IEquiv$ = true;

com.cognitect.transit.types.TaggedValue.prototype.cljs$core$IEquiv$_equiv$arity$2 = (function (this$,other){
var this$__$1 = this;
return this$__$1.equiv(other);
});
goog.math.Long.prototype.cljs$core$IHash$ = true;

goog.math.Long.prototype.cljs$core$IHash$_hash$arity$1 = (function (this$){
var this$__$1 = this;
return com.cognitect.transit.eq.hashCode.call(null,this$__$1);
});

com.cognitect.transit.types.UUID.prototype.cljs$core$IHash$ = true;

com.cognitect.transit.types.UUID.prototype.cljs$core$IHash$_hash$arity$1 = (function (this$){
var this$__$1 = this;
return cljs.core.hash.call(null,this$__$1.toString());
});

com.cognitect.transit.types.TaggedValue.prototype.cljs$core$IHash$ = true;

com.cognitect.transit.types.TaggedValue.prototype.cljs$core$IHash$_hash$arity$1 = (function (this$){
var this$__$1 = this;
return com.cognitect.transit.eq.hashCode.call(null,this$__$1);
});
com.cognitect.transit.types.UUID.prototype.cljs$core$IPrintWithWriter$ = true;

com.cognitect.transit.types.UUID.prototype.cljs$core$IPrintWithWriter$_pr_writer$arity$3 = (function (uuid,writer,_){
var uuid__$1 = this;
return cljs.core._write.call(null,writer,[cljs.core.str("#uuid \""),cljs.core.str(uuid__$1.toString()),cljs.core.str("\"")].join(''));
});
cognitect.transit.opts_merge = (function cognitect$transit$opts_merge(a,b){
var seq__25051_25055 = cljs.core.seq.call(null,cljs.core.js_keys.call(null,b));
var chunk__25052_25056 = null;
var count__25053_25057 = (0);
var i__25054_25058 = (0);
while(true){
if((i__25054_25058 < count__25053_25057)){
var k_25059 = cljs.core._nth.call(null,chunk__25052_25056,i__25054_25058);
var v_25060 = (b[k_25059]);
(a[k_25059] = v_25060);

var G__25061 = seq__25051_25055;
var G__25062 = chunk__25052_25056;
var G__25063 = count__25053_25057;
var G__25064 = (i__25054_25058 + (1));
seq__25051_25055 = G__25061;
chunk__25052_25056 = G__25062;
count__25053_25057 = G__25063;
i__25054_25058 = G__25064;
continue;
} else {
var temp__4657__auto___25065 = cljs.core.seq.call(null,seq__25051_25055);
if(temp__4657__auto___25065){
var seq__25051_25066__$1 = temp__4657__auto___25065;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__25051_25066__$1)){
var c__23946__auto___25067 = cljs.core.chunk_first.call(null,seq__25051_25066__$1);
var G__25068 = cljs.core.chunk_rest.call(null,seq__25051_25066__$1);
var G__25069 = c__23946__auto___25067;
var G__25070 = cljs.core.count.call(null,c__23946__auto___25067);
var G__25071 = (0);
seq__25051_25055 = G__25068;
chunk__25052_25056 = G__25069;
count__25053_25057 = G__25070;
i__25054_25058 = G__25071;
continue;
} else {
var k_25072 = cljs.core.first.call(null,seq__25051_25066__$1);
var v_25073 = (b[k_25072]);
(a[k_25072] = v_25073);

var G__25074 = cljs.core.next.call(null,seq__25051_25066__$1);
var G__25075 = null;
var G__25076 = (0);
var G__25077 = (0);
seq__25051_25055 = G__25074;
chunk__25052_25056 = G__25075;
count__25053_25057 = G__25076;
i__25054_25058 = G__25077;
continue;
}
} else {
}
}
break;
}

return a;
});

/**
* @constructor
 * @implements {cognitect.transit.Object}
*/
cognitect.transit.MapBuilder = (function (){
})
cognitect.transit.MapBuilder.prototype.init = (function (node){
var self__ = this;
var _ = this;
return cljs.core.transient$.call(null,cljs.core.PersistentArrayMap.EMPTY);
});

cognitect.transit.MapBuilder.prototype.add = (function (m,k,v,node){
var self__ = this;
var _ = this;
return cljs.core.assoc_BANG_.call(null,m,k,v);
});

cognitect.transit.MapBuilder.prototype.finalize = (function (m,node){
var self__ = this;
var _ = this;
return cljs.core.persistent_BANG_.call(null,m);
});

cognitect.transit.MapBuilder.prototype.fromArray = (function (arr,node){
var self__ = this;
var _ = this;
return cljs.core.PersistentArrayMap.fromArray.call(null,arr,true,true);
});

cognitect.transit.MapBuilder.getBasis = (function (){
return cljs.core.PersistentVector.EMPTY;
});

cognitect.transit.MapBuilder.cljs$lang$type = true;

cognitect.transit.MapBuilder.cljs$lang$ctorStr = "cognitect.transit/MapBuilder";

cognitect.transit.MapBuilder.cljs$lang$ctorPrWriter = (function (this__23741__auto__,writer__23742__auto__,opt__23743__auto__){
return cljs.core._write.call(null,writer__23742__auto__,"cognitect.transit/MapBuilder");
});

cognitect.transit.__GT_MapBuilder = (function cognitect$transit$__GT_MapBuilder(){
return (new cognitect.transit.MapBuilder());
});


/**
* @constructor
 * @implements {cognitect.transit.Object}
*/
cognitect.transit.VectorBuilder = (function (){
})
cognitect.transit.VectorBuilder.prototype.init = (function (node){
var self__ = this;
var _ = this;
return cljs.core.transient$.call(null,cljs.core.PersistentVector.EMPTY);
});

cognitect.transit.VectorBuilder.prototype.add = (function (v,x,node){
var self__ = this;
var _ = this;
return cljs.core.conj_BANG_.call(null,v,x);
});

cognitect.transit.VectorBuilder.prototype.finalize = (function (v,node){
var self__ = this;
var _ = this;
return cljs.core.persistent_BANG_.call(null,v);
});

cognitect.transit.VectorBuilder.prototype.fromArray = (function (arr,node){
var self__ = this;
var _ = this;
return cljs.core.PersistentVector.fromArray.call(null,arr,true);
});

cognitect.transit.VectorBuilder.getBasis = (function (){
return cljs.core.PersistentVector.EMPTY;
});

cognitect.transit.VectorBuilder.cljs$lang$type = true;

cognitect.transit.VectorBuilder.cljs$lang$ctorStr = "cognitect.transit/VectorBuilder";

cognitect.transit.VectorBuilder.cljs$lang$ctorPrWriter = (function (this__23741__auto__,writer__23742__auto__,opt__23743__auto__){
return cljs.core._write.call(null,writer__23742__auto__,"cognitect.transit/VectorBuilder");
});

cognitect.transit.__GT_VectorBuilder = (function cognitect$transit$__GT_VectorBuilder(){
return (new cognitect.transit.VectorBuilder());
});

/**
 * Return a transit reader. type may be either :json or :json-verbose.
 * opts may be a map optionally containing a :handlers entry. The value
 * of :handlers should be map from tag to a decoder function which returns
 * then in-memory representation of the semantic transit value.
 */
cognitect.transit.reader = (function cognitect$transit$reader(var_args){
var args25078 = [];
var len__24201__auto___25081 = arguments.length;
var i__24202__auto___25082 = (0);
while(true){
if((i__24202__auto___25082 < len__24201__auto___25081)){
args25078.push((arguments[i__24202__auto___25082]));

var G__25083 = (i__24202__auto___25082 + (1));
i__24202__auto___25082 = G__25083;
continue;
} else {
}
break;
}

var G__25080 = args25078.length;
switch (G__25080) {
case 1:
return cognitect.transit.reader.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return cognitect.transit.reader.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error([cljs.core.str("Invalid arity: "),cljs.core.str(args25078.length)].join('')));

}
});

cognitect.transit.reader.cljs$core$IFn$_invoke$arity$1 = (function (type){
return cognitect.transit.reader.call(null,type,null);
});

cognitect.transit.reader.cljs$core$IFn$_invoke$arity$2 = (function (type,opts){
return com.cognitect.transit.reader.call(null,cljs.core.name.call(null,type),cognitect.transit.opts_merge.call(null,{"handlers": cljs.core.clj__GT_js.call(null,cljs.core.merge.call(null,new cljs.core.PersistentArrayMap(null, 5, ["$",(function (v){
return cljs.core.symbol.call(null,v);
}),":",(function (v){
return cljs.core.keyword.call(null,v);
}),"set",(function (v){
return cljs.core.into.call(null,cljs.core.PersistentHashSet.EMPTY,v);
}),"list",(function (v){
return cljs.core.into.call(null,cljs.core.List.EMPTY,v.reverse());
}),"cmap",(function (v){
var i = (0);
var ret = cljs.core.transient$.call(null,cljs.core.PersistentArrayMap.EMPTY);
while(true){
if((i < v.length)){
var G__25085 = (i + (2));
var G__25086 = cljs.core.assoc_BANG_.call(null,ret,(v[i]),(v[(i + (1))]));
i = G__25085;
ret = G__25086;
continue;
} else {
return cljs.core.persistent_BANG_.call(null,ret);
}
break;
}
})], null),new cljs.core.Keyword(null,"handlers","handlers",79528781).cljs$core$IFn$_invoke$arity$1(opts))), "mapBuilder": (new cognitect.transit.MapBuilder()), "arrayBuilder": (new cognitect.transit.VectorBuilder()), "prefersStrings": false},cljs.core.clj__GT_js.call(null,cljs.core.dissoc.call(null,opts,new cljs.core.Keyword(null,"handlers","handlers",79528781)))));
});

cognitect.transit.reader.cljs$lang$maxFixedArity = 2;
/**
 * Read a transit encoded string into ClojureScript values given a 
 * transit reader.
 */
cognitect.transit.read = (function cognitect$transit$read(r,str){
return r.read(str);
});

/**
* @constructor
 * @implements {cognitect.transit.Object}
*/
cognitect.transit.KeywordHandler = (function (){
})
cognitect.transit.KeywordHandler.prototype.tag = (function (v){
var self__ = this;
var _ = this;
return ":";
});

cognitect.transit.KeywordHandler.prototype.rep = (function (v){
var self__ = this;
var _ = this;
return v.fqn;
});

cognitect.transit.KeywordHandler.prototype.stringRep = (function (v){
var self__ = this;
var _ = this;
return v.fqn;
});

cognitect.transit.KeywordHandler.getBasis = (function (){
return cljs.core.PersistentVector.EMPTY;
});

cognitect.transit.KeywordHandler.cljs$lang$type = true;

cognitect.transit.KeywordHandler.cljs$lang$ctorStr = "cognitect.transit/KeywordHandler";

cognitect.transit.KeywordHandler.cljs$lang$ctorPrWriter = (function (this__23741__auto__,writer__23742__auto__,opt__23743__auto__){
return cljs.core._write.call(null,writer__23742__auto__,"cognitect.transit/KeywordHandler");
});

cognitect.transit.__GT_KeywordHandler = (function cognitect$transit$__GT_KeywordHandler(){
return (new cognitect.transit.KeywordHandler());
});


/**
* @constructor
 * @implements {cognitect.transit.Object}
*/
cognitect.transit.SymbolHandler = (function (){
})
cognitect.transit.SymbolHandler.prototype.tag = (function (v){
var self__ = this;
var _ = this;
return "$";
});

cognitect.transit.SymbolHandler.prototype.rep = (function (v){
var self__ = this;
var _ = this;
return v.str;
});

cognitect.transit.SymbolHandler.prototype.stringRep = (function (v){
var self__ = this;
var _ = this;
return v.str;
});

cognitect.transit.SymbolHandler.getBasis = (function (){
return cljs.core.PersistentVector.EMPTY;
});

cognitect.transit.SymbolHandler.cljs$lang$type = true;

cognitect.transit.SymbolHandler.cljs$lang$ctorStr = "cognitect.transit/SymbolHandler";

cognitect.transit.SymbolHandler.cljs$lang$ctorPrWriter = (function (this__23741__auto__,writer__23742__auto__,opt__23743__auto__){
return cljs.core._write.call(null,writer__23742__auto__,"cognitect.transit/SymbolHandler");
});

cognitect.transit.__GT_SymbolHandler = (function cognitect$transit$__GT_SymbolHandler(){
return (new cognitect.transit.SymbolHandler());
});


/**
* @constructor
 * @implements {cognitect.transit.Object}
*/
cognitect.transit.ListHandler = (function (){
})
cognitect.transit.ListHandler.prototype.tag = (function (v){
var self__ = this;
var _ = this;
return "list";
});

cognitect.transit.ListHandler.prototype.rep = (function (v){
var self__ = this;
var _ = this;
var ret = [];
var seq__25087_25091 = cljs.core.seq.call(null,v);
var chunk__25088_25092 = null;
var count__25089_25093 = (0);
var i__25090_25094 = (0);
while(true){
if((i__25090_25094 < count__25089_25093)){
var x_25095 = cljs.core._nth.call(null,chunk__25088_25092,i__25090_25094);
ret.push(x_25095);

var G__25096 = seq__25087_25091;
var G__25097 = chunk__25088_25092;
var G__25098 = count__25089_25093;
var G__25099 = (i__25090_25094 + (1));
seq__25087_25091 = G__25096;
chunk__25088_25092 = G__25097;
count__25089_25093 = G__25098;
i__25090_25094 = G__25099;
continue;
} else {
var temp__4657__auto___25100 = cljs.core.seq.call(null,seq__25087_25091);
if(temp__4657__auto___25100){
var seq__25087_25101__$1 = temp__4657__auto___25100;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__25087_25101__$1)){
var c__23946__auto___25102 = cljs.core.chunk_first.call(null,seq__25087_25101__$1);
var G__25103 = cljs.core.chunk_rest.call(null,seq__25087_25101__$1);
var G__25104 = c__23946__auto___25102;
var G__25105 = cljs.core.count.call(null,c__23946__auto___25102);
var G__25106 = (0);
seq__25087_25091 = G__25103;
chunk__25088_25092 = G__25104;
count__25089_25093 = G__25105;
i__25090_25094 = G__25106;
continue;
} else {
var x_25107 = cljs.core.first.call(null,seq__25087_25101__$1);
ret.push(x_25107);

var G__25108 = cljs.core.next.call(null,seq__25087_25101__$1);
var G__25109 = null;
var G__25110 = (0);
var G__25111 = (0);
seq__25087_25091 = G__25108;
chunk__25088_25092 = G__25109;
count__25089_25093 = G__25110;
i__25090_25094 = G__25111;
continue;
}
} else {
}
}
break;
}

return com.cognitect.transit.tagged.call(null,"array",ret);
});

cognitect.transit.ListHandler.prototype.stringRep = (function (v){
var self__ = this;
var _ = this;
return null;
});

cognitect.transit.ListHandler.getBasis = (function (){
return cljs.core.PersistentVector.EMPTY;
});

cognitect.transit.ListHandler.cljs$lang$type = true;

cognitect.transit.ListHandler.cljs$lang$ctorStr = "cognitect.transit/ListHandler";

cognitect.transit.ListHandler.cljs$lang$ctorPrWriter = (function (this__23741__auto__,writer__23742__auto__,opt__23743__auto__){
return cljs.core._write.call(null,writer__23742__auto__,"cognitect.transit/ListHandler");
});

cognitect.transit.__GT_ListHandler = (function cognitect$transit$__GT_ListHandler(){
return (new cognitect.transit.ListHandler());
});


/**
* @constructor
 * @implements {cognitect.transit.Object}
*/
cognitect.transit.MapHandler = (function (){
})
cognitect.transit.MapHandler.prototype.tag = (function (v){
var self__ = this;
var _ = this;
return "map";
});

cognitect.transit.MapHandler.prototype.rep = (function (v){
var self__ = this;
var _ = this;
return v;
});

cognitect.transit.MapHandler.prototype.stringRep = (function (v){
var self__ = this;
var _ = this;
return null;
});

cognitect.transit.MapHandler.getBasis = (function (){
return cljs.core.PersistentVector.EMPTY;
});

cognitect.transit.MapHandler.cljs$lang$type = true;

cognitect.transit.MapHandler.cljs$lang$ctorStr = "cognitect.transit/MapHandler";

cognitect.transit.MapHandler.cljs$lang$ctorPrWriter = (function (this__23741__auto__,writer__23742__auto__,opt__23743__auto__){
return cljs.core._write.call(null,writer__23742__auto__,"cognitect.transit/MapHandler");
});

cognitect.transit.__GT_MapHandler = (function cognitect$transit$__GT_MapHandler(){
return (new cognitect.transit.MapHandler());
});


/**
* @constructor
 * @implements {cognitect.transit.Object}
*/
cognitect.transit.SetHandler = (function (){
})
cognitect.transit.SetHandler.prototype.tag = (function (v){
var self__ = this;
var _ = this;
return "set";
});

cognitect.transit.SetHandler.prototype.rep = (function (v){
var self__ = this;
var _ = this;
var ret = [];
var seq__25112_25116 = cljs.core.seq.call(null,v);
var chunk__25113_25117 = null;
var count__25114_25118 = (0);
var i__25115_25119 = (0);
while(true){
if((i__25115_25119 < count__25114_25118)){
var x_25120 = cljs.core._nth.call(null,chunk__25113_25117,i__25115_25119);
ret.push(x_25120);

var G__25121 = seq__25112_25116;
var G__25122 = chunk__25113_25117;
var G__25123 = count__25114_25118;
var G__25124 = (i__25115_25119 + (1));
seq__25112_25116 = G__25121;
chunk__25113_25117 = G__25122;
count__25114_25118 = G__25123;
i__25115_25119 = G__25124;
continue;
} else {
var temp__4657__auto___25125 = cljs.core.seq.call(null,seq__25112_25116);
if(temp__4657__auto___25125){
var seq__25112_25126__$1 = temp__4657__auto___25125;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__25112_25126__$1)){
var c__23946__auto___25127 = cljs.core.chunk_first.call(null,seq__25112_25126__$1);
var G__25128 = cljs.core.chunk_rest.call(null,seq__25112_25126__$1);
var G__25129 = c__23946__auto___25127;
var G__25130 = cljs.core.count.call(null,c__23946__auto___25127);
var G__25131 = (0);
seq__25112_25116 = G__25128;
chunk__25113_25117 = G__25129;
count__25114_25118 = G__25130;
i__25115_25119 = G__25131;
continue;
} else {
var x_25132 = cljs.core.first.call(null,seq__25112_25126__$1);
ret.push(x_25132);

var G__25133 = cljs.core.next.call(null,seq__25112_25126__$1);
var G__25134 = null;
var G__25135 = (0);
var G__25136 = (0);
seq__25112_25116 = G__25133;
chunk__25113_25117 = G__25134;
count__25114_25118 = G__25135;
i__25115_25119 = G__25136;
continue;
}
} else {
}
}
break;
}

return com.cognitect.transit.tagged.call(null,"array",ret);
});

cognitect.transit.SetHandler.prototype.stringRep = (function (){
var self__ = this;
var v = this;
return null;
});

cognitect.transit.SetHandler.getBasis = (function (){
return cljs.core.PersistentVector.EMPTY;
});

cognitect.transit.SetHandler.cljs$lang$type = true;

cognitect.transit.SetHandler.cljs$lang$ctorStr = "cognitect.transit/SetHandler";

cognitect.transit.SetHandler.cljs$lang$ctorPrWriter = (function (this__23741__auto__,writer__23742__auto__,opt__23743__auto__){
return cljs.core._write.call(null,writer__23742__auto__,"cognitect.transit/SetHandler");
});

cognitect.transit.__GT_SetHandler = (function cognitect$transit$__GT_SetHandler(){
return (new cognitect.transit.SetHandler());
});


/**
* @constructor
 * @implements {cognitect.transit.Object}
*/
cognitect.transit.VectorHandler = (function (){
})
cognitect.transit.VectorHandler.prototype.tag = (function (v){
var self__ = this;
var _ = this;
return "array";
});

cognitect.transit.VectorHandler.prototype.rep = (function (v){
var self__ = this;
var _ = this;
var ret = [];
var seq__25137_25141 = cljs.core.seq.call(null,v);
var chunk__25138_25142 = null;
var count__25139_25143 = (0);
var i__25140_25144 = (0);
while(true){
if((i__25140_25144 < count__25139_25143)){
var x_25145 = cljs.core._nth.call(null,chunk__25138_25142,i__25140_25144);
ret.push(x_25145);

var G__25146 = seq__25137_25141;
var G__25147 = chunk__25138_25142;
var G__25148 = count__25139_25143;
var G__25149 = (i__25140_25144 + (1));
seq__25137_25141 = G__25146;
chunk__25138_25142 = G__25147;
count__25139_25143 = G__25148;
i__25140_25144 = G__25149;
continue;
} else {
var temp__4657__auto___25150 = cljs.core.seq.call(null,seq__25137_25141);
if(temp__4657__auto___25150){
var seq__25137_25151__$1 = temp__4657__auto___25150;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__25137_25151__$1)){
var c__23946__auto___25152 = cljs.core.chunk_first.call(null,seq__25137_25151__$1);
var G__25153 = cljs.core.chunk_rest.call(null,seq__25137_25151__$1);
var G__25154 = c__23946__auto___25152;
var G__25155 = cljs.core.count.call(null,c__23946__auto___25152);
var G__25156 = (0);
seq__25137_25141 = G__25153;
chunk__25138_25142 = G__25154;
count__25139_25143 = G__25155;
i__25140_25144 = G__25156;
continue;
} else {
var x_25157 = cljs.core.first.call(null,seq__25137_25151__$1);
ret.push(x_25157);

var G__25158 = cljs.core.next.call(null,seq__25137_25151__$1);
var G__25159 = null;
var G__25160 = (0);
var G__25161 = (0);
seq__25137_25141 = G__25158;
chunk__25138_25142 = G__25159;
count__25139_25143 = G__25160;
i__25140_25144 = G__25161;
continue;
}
} else {
}
}
break;
}

return ret;
});

cognitect.transit.VectorHandler.prototype.stringRep = (function (v){
var self__ = this;
var _ = this;
return null;
});

cognitect.transit.VectorHandler.getBasis = (function (){
return cljs.core.PersistentVector.EMPTY;
});

cognitect.transit.VectorHandler.cljs$lang$type = true;

cognitect.transit.VectorHandler.cljs$lang$ctorStr = "cognitect.transit/VectorHandler";

cognitect.transit.VectorHandler.cljs$lang$ctorPrWriter = (function (this__23741__auto__,writer__23742__auto__,opt__23743__auto__){
return cljs.core._write.call(null,writer__23742__auto__,"cognitect.transit/VectorHandler");
});

cognitect.transit.__GT_VectorHandler = (function cognitect$transit$__GT_VectorHandler(){
return (new cognitect.transit.VectorHandler());
});


/**
* @constructor
 * @implements {cognitect.transit.Object}
*/
cognitect.transit.UUIDHandler = (function (){
})
cognitect.transit.UUIDHandler.prototype.tag = (function (v){
var self__ = this;
var _ = this;
return "u";
});

cognitect.transit.UUIDHandler.prototype.rep = (function (v){
var self__ = this;
var _ = this;
return v.uuid;
});

cognitect.transit.UUIDHandler.prototype.stringRep = (function (v){
var self__ = this;
var this$ = this;
return this$.rep(v);
});

cognitect.transit.UUIDHandler.getBasis = (function (){
return cljs.core.PersistentVector.EMPTY;
});

cognitect.transit.UUIDHandler.cljs$lang$type = true;

cognitect.transit.UUIDHandler.cljs$lang$ctorStr = "cognitect.transit/UUIDHandler";

cognitect.transit.UUIDHandler.cljs$lang$ctorPrWriter = (function (this__23741__auto__,writer__23742__auto__,opt__23743__auto__){
return cljs.core._write.call(null,writer__23742__auto__,"cognitect.transit/UUIDHandler");
});

cognitect.transit.__GT_UUIDHandler = (function cognitect$transit$__GT_UUIDHandler(){
return (new cognitect.transit.UUIDHandler());
});

/**
 * Return a transit writer. type maybe either :json or :json-verbose.
 *   opts is a map containing a :handlers entry. :handlers is a map of
 *   type constructors to handler instances.
 */
cognitect.transit.writer = (function cognitect$transit$writer(var_args){
var args25162 = [];
var len__24201__auto___25173 = arguments.length;
var i__24202__auto___25174 = (0);
while(true){
if((i__24202__auto___25174 < len__24201__auto___25173)){
args25162.push((arguments[i__24202__auto___25174]));

var G__25175 = (i__24202__auto___25174 + (1));
i__24202__auto___25174 = G__25175;
continue;
} else {
}
break;
}

var G__25164 = args25162.length;
switch (G__25164) {
case 1:
return cognitect.transit.writer.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return cognitect.transit.writer.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error([cljs.core.str("Invalid arity: "),cljs.core.str(args25162.length)].join('')));

}
});

cognitect.transit.writer.cljs$core$IFn$_invoke$arity$1 = (function (type){
return cognitect.transit.writer.call(null,type,null);
});

cognitect.transit.writer.cljs$core$IFn$_invoke$arity$2 = (function (type,opts){
var keyword_handler = (new cognitect.transit.KeywordHandler());
var symbol_handler = (new cognitect.transit.SymbolHandler());
var list_handler = (new cognitect.transit.ListHandler());
var map_handler = (new cognitect.transit.MapHandler());
var set_handler = (new cognitect.transit.SetHandler());
var vector_handler = (new cognitect.transit.VectorHandler());
var uuid_handler = (new cognitect.transit.UUIDHandler());
var handlers = cljs.core.merge.call(null,cljs.core.PersistentHashMap.fromArrays([cljs.core.PersistentHashMap,cljs.core.Cons,cljs.core.PersistentArrayMap,cljs.core.NodeSeq,cljs.core.PersistentQueue,cljs.core.IndexedSeq,cljs.core.Keyword,cljs.core.EmptyList,cljs.core.LazySeq,cljs.core.Subvec,cljs.core.PersistentQueueSeq,cljs.core.ArrayNodeSeq,cljs.core.ValSeq,cljs.core.PersistentArrayMapSeq,cljs.core.PersistentVector,cljs.core.List,cljs.core.RSeq,cljs.core.PersistentHashSet,cljs.core.PersistentTreeMap,cljs.core.KeySeq,cljs.core.ChunkedSeq,cljs.core.PersistentTreeSet,cljs.core.ChunkedCons,cljs.core.Symbol,cljs.core.UUID,cljs.core.Range,cljs.core.PersistentTreeMapSeq],[map_handler,list_handler,map_handler,list_handler,list_handler,list_handler,keyword_handler,list_handler,list_handler,vector_handler,list_handler,list_handler,list_handler,list_handler,vector_handler,list_handler,list_handler,set_handler,map_handler,list_handler,list_handler,set_handler,list_handler,symbol_handler,uuid_handler,list_handler,list_handler]),new cljs.core.Keyword(null,"handlers","handlers",79528781).cljs$core$IFn$_invoke$arity$1(opts));
return com.cognitect.transit.writer.call(null,cljs.core.name.call(null,type),cognitect.transit.opts_merge.call(null,{"objectBuilder": ((function (keyword_handler,symbol_handler,list_handler,map_handler,set_handler,vector_handler,uuid_handler,handlers){
return (function (m,kfn,vfn){
return cljs.core.reduce_kv.call(null,((function (keyword_handler,symbol_handler,list_handler,map_handler,set_handler,vector_handler,uuid_handler,handlers){
return (function (obj,k,v){
var G__25165 = obj;
G__25165.push(kfn.call(null,k),vfn.call(null,v));

return G__25165;
});})(keyword_handler,symbol_handler,list_handler,map_handler,set_handler,vector_handler,uuid_handler,handlers))
,["^ "],m);
});})(keyword_handler,symbol_handler,list_handler,map_handler,set_handler,vector_handler,uuid_handler,handlers))
, "handlers": (function (){var x25166 = cljs.core.clone.call(null,handlers);
x25166.forEach = ((function (x25166,keyword_handler,symbol_handler,list_handler,map_handler,set_handler,vector_handler,uuid_handler,handlers){
return (function (f){
var coll = this;
var seq__25167 = cljs.core.seq.call(null,coll);
var chunk__25168 = null;
var count__25169 = (0);
var i__25170 = (0);
while(true){
if((i__25170 < count__25169)){
var vec__25171 = cljs.core._nth.call(null,chunk__25168,i__25170);
var k = cljs.core.nth.call(null,vec__25171,(0),null);
var v = cljs.core.nth.call(null,vec__25171,(1),null);
f.call(null,v,k);

var G__25177 = seq__25167;
var G__25178 = chunk__25168;
var G__25179 = count__25169;
var G__25180 = (i__25170 + (1));
seq__25167 = G__25177;
chunk__25168 = G__25178;
count__25169 = G__25179;
i__25170 = G__25180;
continue;
} else {
var temp__4657__auto__ = cljs.core.seq.call(null,seq__25167);
if(temp__4657__auto__){
var seq__25167__$1 = temp__4657__auto__;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__25167__$1)){
var c__23946__auto__ = cljs.core.chunk_first.call(null,seq__25167__$1);
var G__25181 = cljs.core.chunk_rest.call(null,seq__25167__$1);
var G__25182 = c__23946__auto__;
var G__25183 = cljs.core.count.call(null,c__23946__auto__);
var G__25184 = (0);
seq__25167 = G__25181;
chunk__25168 = G__25182;
count__25169 = G__25183;
i__25170 = G__25184;
continue;
} else {
var vec__25172 = cljs.core.first.call(null,seq__25167__$1);
var k = cljs.core.nth.call(null,vec__25172,(0),null);
var v = cljs.core.nth.call(null,vec__25172,(1),null);
f.call(null,v,k);

var G__25185 = cljs.core.next.call(null,seq__25167__$1);
var G__25186 = null;
var G__25187 = (0);
var G__25188 = (0);
seq__25167 = G__25185;
chunk__25168 = G__25186;
count__25169 = G__25187;
i__25170 = G__25188;
continue;
}
} else {
return null;
}
}
break;
}
});})(x25166,keyword_handler,symbol_handler,list_handler,map_handler,set_handler,vector_handler,uuid_handler,handlers))
;

return x25166;
})(), "unpack": ((function (keyword_handler,symbol_handler,list_handler,map_handler,set_handler,vector_handler,uuid_handler,handlers){
return (function (x){
if((x instanceof cljs.core.PersistentArrayMap)){
return x.arr;
} else {
return false;
}
});})(keyword_handler,symbol_handler,list_handler,map_handler,set_handler,vector_handler,uuid_handler,handlers))
},cljs.core.clj__GT_js.call(null,cljs.core.dissoc.call(null,opts,new cljs.core.Keyword(null,"handlers","handlers",79528781)))));
});

cognitect.transit.writer.cljs$lang$maxFixedArity = 2;
/**
 * Encode an object into a transit string given a transit writer.
 */
cognitect.transit.write = (function cognitect$transit$write(w,o){
return w.write(o);
});
/**
 * Construct a read handler. Implemented as identity, exists primarily
 * for API compatiblity with transit-clj
 */
cognitect.transit.read_handler = (function cognitect$transit$read_handler(from_rep){
return from_rep;
});
/**
 * Creates a transit write handler whose tag, rep,
 * stringRep, and verboseWriteHandler methods
 * invoke the provided fns.
 */
cognitect.transit.write_handler = (function cognitect$transit$write_handler(var_args){
var args25189 = [];
var len__24201__auto___25195 = arguments.length;
var i__24202__auto___25196 = (0);
while(true){
if((i__24202__auto___25196 < len__24201__auto___25195)){
args25189.push((arguments[i__24202__auto___25196]));

var G__25197 = (i__24202__auto___25196 + (1));
i__24202__auto___25196 = G__25197;
continue;
} else {
}
break;
}

var G__25191 = args25189.length;
switch (G__25191) {
case 2:
return cognitect.transit.write_handler.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cognitect.transit.write_handler.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
case 4:
return cognitect.transit.write_handler.cljs$core$IFn$_invoke$arity$4((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]));

break;
default:
throw (new Error([cljs.core.str("Invalid arity: "),cljs.core.str(args25189.length)].join('')));

}
});

cognitect.transit.write_handler.cljs$core$IFn$_invoke$arity$2 = (function (tag_fn,rep_fn){
return cognitect.transit.write_handler.call(null,tag_fn,rep_fn,null,null);
});

cognitect.transit.write_handler.cljs$core$IFn$_invoke$arity$3 = (function (tag_fn,rep_fn,str_rep_fn){
return cognitect.transit.write_handler.call(null,tag_fn,rep_fn,str_rep_fn,null);
});

cognitect.transit.write_handler.cljs$core$IFn$_invoke$arity$4 = (function (tag_fn,rep_fn,str_rep_fn,verbose_handler_fn){
if(typeof cognitect.transit.t_cognitect$transit25192 !== 'undefined'){
} else {

/**
* @constructor
 * @implements {cognitect.transit.Object}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.IWithMeta}
*/
cognitect.transit.t_cognitect$transit25192 = (function (tag_fn,rep_fn,str_rep_fn,verbose_handler_fn,meta25193){
this.tag_fn = tag_fn;
this.rep_fn = rep_fn;
this.str_rep_fn = str_rep_fn;
this.verbose_handler_fn = verbose_handler_fn;
this.meta25193 = meta25193;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
})
cognitect.transit.t_cognitect$transit25192.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_25194,meta25193__$1){
var self__ = this;
var _25194__$1 = this;
return (new cognitect.transit.t_cognitect$transit25192(self__.tag_fn,self__.rep_fn,self__.str_rep_fn,self__.verbose_handler_fn,meta25193__$1));
});

cognitect.transit.t_cognitect$transit25192.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_25194){
var self__ = this;
var _25194__$1 = this;
return self__.meta25193;
});

cognitect.transit.t_cognitect$transit25192.prototype.tag = (function (o){
var self__ = this;
var _ = this;
return self__.tag_fn.call(null,o);
});

cognitect.transit.t_cognitect$transit25192.prototype.rep = (function (o){
var self__ = this;
var _ = this;
return self__.rep_fn.call(null,o);
});

cognitect.transit.t_cognitect$transit25192.prototype.stringRep = (function (o){
var self__ = this;
var _ = this;
if(cljs.core.truth_(self__.str_rep_fn)){
return self__.str_rep_fn.call(null,o);
} else {
return null;
}
});

cognitect.transit.t_cognitect$transit25192.prototype.getVerboseHandler = (function (){
var self__ = this;
var _ = this;
if(cljs.core.truth_(self__.verbose_handler_fn)){
return self__.verbose_handler_fn.call(null);
} else {
return null;
}
});

cognitect.transit.t_cognitect$transit25192.getBasis = (function (){
return new cljs.core.PersistentVector(null, 5, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"tag-fn","tag-fn",242055482,null),new cljs.core.Symbol(null,"rep-fn","rep-fn",-1724891035,null),new cljs.core.Symbol(null,"str-rep-fn","str-rep-fn",-1179615016,null),new cljs.core.Symbol(null,"verbose-handler-fn","verbose-handler-fn",547340594,null),new cljs.core.Symbol(null,"meta25193","meta25193",588235980,null)], null);
});

cognitect.transit.t_cognitect$transit25192.cljs$lang$type = true;

cognitect.transit.t_cognitect$transit25192.cljs$lang$ctorStr = "cognitect.transit/t_cognitect$transit25192";

cognitect.transit.t_cognitect$transit25192.cljs$lang$ctorPrWriter = (function (this__23741__auto__,writer__23742__auto__,opt__23743__auto__){
return cljs.core._write.call(null,writer__23742__auto__,"cognitect.transit/t_cognitect$transit25192");
});

cognitect.transit.__GT_t_cognitect$transit25192 = (function cognitect$transit$__GT_t_cognitect$transit25192(tag_fn__$1,rep_fn__$1,str_rep_fn__$1,verbose_handler_fn__$1,meta25193){
return (new cognitect.transit.t_cognitect$transit25192(tag_fn__$1,rep_fn__$1,str_rep_fn__$1,verbose_handler_fn__$1,meta25193));
});

}

return (new cognitect.transit.t_cognitect$transit25192(tag_fn,rep_fn,str_rep_fn,verbose_handler_fn,cljs.core.PersistentArrayMap.EMPTY));
});

cognitect.transit.write_handler.cljs$lang$maxFixedArity = 4;
/**
 * Construct a tagged value. tag must be a string and rep can
 * be any transit encodeable value.
 */
cognitect.transit.tagged_value = (function cognitect$transit$tagged_value(tag,rep){
return com.cognitect.transit.types.taggedValue.call(null,tag,rep);
});
/**
 * Returns true if x is a transit tagged value, false otherwise.
 */
cognitect.transit.tagged_value_QMARK_ = (function cognitect$transit$tagged_value_QMARK_(x){
return com.cognitect.transit.types.isTaggedValue.call(null,x);
});
/**
 * Construct a transit integer value. Returns JavaScript number if
 *   in the 53bit integer range, a goog.math.Long instance if above. s
 *   may be a string or a JavaScript number.
 */
cognitect.transit.integer = (function cognitect$transit$integer(s){
return com.cognitect.transit.types.intValue.call(null,s);
});
/**
 * Returns true if x is an integer value between the 53bit and 64bit
 *   range, false otherwise.
 */
cognitect.transit.integer_QMARK_ = (function cognitect$transit$integer_QMARK_(x){
return com.cognitect.transit.types.isInteger.call(null,x);
});
/**
 * Construct a big integer from a string.
 */
cognitect.transit.bigint = (function cognitect$transit$bigint(s){
return com.cognitect.transit.types.bigInteger.call(null,s);
});
/**
 * Returns true if x is a transit big integer value, false otherwise.
 */
cognitect.transit.bigint_QMARK_ = (function cognitect$transit$bigint_QMARK_(x){
return com.cognitect.transit.types.isBigInteger.call(null,x);
});
/**
 * Construct a big decimal from a string.
 */
cognitect.transit.bigdec = (function cognitect$transit$bigdec(s){
return com.cognitect.transit.types.bigDecimalValue.call(null,s);
});
/**
 * Returns true if x is a transit big decimal value, false otherwise.
 */
cognitect.transit.bigdec_QMARK_ = (function cognitect$transit$bigdec_QMARK_(x){
return com.cognitect.transit.types.isBigDecimal.call(null,x);
});
/**
 * Construct a URI from a string.
 */
cognitect.transit.uri = (function cognitect$transit$uri(s){
return com.cognitect.transit.types.uri.call(null,s);
});
/**
 * Returns true if x is a transit URI value, false otherwise.
 */
cognitect.transit.uri_QMARK_ = (function cognitect$transit$uri_QMARK_(x){
return com.cognitect.transit.types.isURI.call(null,x);
});
/**
 * Construct a UUID from a string.
 */
cognitect.transit.uuid = (function cognitect$transit$uuid(s){
return com.cognitect.transit.types.uuid.call(null,s);
});
/**
 * Returns true if x is a transit UUID value, false otherwise.
 */
cognitect.transit.uuid_QMARK_ = (function cognitect$transit$uuid_QMARK_(x){
var or__23143__auto__ = com.cognitect.transit.types.isUUID.call(null,x);
if(cljs.core.truth_(or__23143__auto__)){
return or__23143__auto__;
} else {
return (x instanceof cljs.core.UUID);
}
});
/**
 * Construct a transit binary value. s should be base64 encoded
 * string.
 */
cognitect.transit.binary = (function cognitect$transit$binary(s){
return com.cognitect.transit.types.binary.call(null,s);
});
/**
 * Returns true if x is a transit binary value, false otherwise.
 */
cognitect.transit.binary_QMARK_ = (function cognitect$transit$binary_QMARK_(x){
return com.cognitect.transit.types.isBinary.call(null,x);
});
/**
 * Construct a quoted transit value. x should be a transit
 * encodeable value.
 */
cognitect.transit.quoted = (function cognitect$transit$quoted(x){
return com.cognitect.transit.types.quoted.call(null,x);
});
/**
 * Returns true if x is a transit quoted value, false otherwise.
 */
cognitect.transit.quoted_QMARK_ = (function cognitect$transit$quoted_QMARK_(x){
return com.cognitect.transit.types.isQuoted.call(null,x);
});
/**
 * Construct a transit link value. x should be an IMap instance
 * containing at a minimum the following keys: :href, :rel. It
 * may optionall include :name, :render, and :prompt. :href must
 * be a transit URI, all other values are strings, and :render must
 * be either :image or :link.
 */
cognitect.transit.link = (function cognitect$transit$link(x){
return com.cognitect.transit.types.link.call(null,x);
});
/**
 * Returns true if x a transit link value, false if otherwise.
 */
cognitect.transit.link_QMARK_ = (function cognitect$transit$link_QMARK_(x){
return com.cognitect.transit.types.isLink.call(null,x);
});

//# sourceMappingURL=transit.js.map