// Compiled by ClojureScript 1.7.170 {}
goog.provide('markdown.core');
goog.require('cljs.core');
goog.require('markdown.common');
goog.require('markdown.links');
goog.require('markdown.transformers');
markdown.core.init_transformer = (function markdown$core$init_transformer(p__25847){
var map__25854 = p__25847;
var map__25854__$1 = ((((!((map__25854 == null)))?((((map__25854.cljs$lang$protocol_mask$partition0$ & (64))) || (map__25854.cljs$core$ISeq$))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__25854):map__25854);
var replacement_transformers = cljs.core.get.call(null,map__25854__$1,new cljs.core.Keyword(null,"replacement-transformers","replacement-transformers",-2028552897));
var custom_transformers = cljs.core.get.call(null,map__25854__$1,new cljs.core.Keyword(null,"custom-transformers","custom-transformers",1440601790));
return ((function (map__25854,map__25854__$1,replacement_transformers,custom_transformers){
return (function (html,line,next_line,state){
var _STAR_next_line_STAR_25856 = markdown.transformers._STAR_next_line_STAR_;
markdown.transformers._STAR_next_line_STAR_ = next_line;

try{var vec__25857 = cljs.core.reduce.call(null,((function (_STAR_next_line_STAR_25856,map__25854,map__25854__$1,replacement_transformers,custom_transformers){
return (function (p__25858,transformer){
var vec__25859 = p__25858;
var text = cljs.core.nth.call(null,vec__25859,(0),null);
var state__$1 = cljs.core.nth.call(null,vec__25859,(1),null);
return transformer.call(null,text,state__$1);
});})(_STAR_next_line_STAR_25856,map__25854,map__25854__$1,replacement_transformers,custom_transformers))
,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [line,state], null),(function (){var or__23143__auto__ = replacement_transformers;
if(cljs.core.truth_(or__23143__auto__)){
return or__23143__auto__;
} else {
return cljs.core.into.call(null,markdown.transformers.transformer_vector,custom_transformers);
}
})());
var text = cljs.core.nth.call(null,vec__25857,(0),null);
var new_state = cljs.core.nth.call(null,vec__25857,(1),null);
html.append(text);

return new_state;
}finally {markdown.transformers._STAR_next_line_STAR_ = _STAR_next_line_STAR_25856;
}});
;})(map__25854,map__25854__$1,replacement_transformers,custom_transformers))
});
/**
 * Removed from cljs.core 0.0-1885, Ref. http://goo.gl/su7Xkj
 */
markdown.core.format = (function markdown$core$format(var_args){
var args__24208__auto__ = [];
var len__24201__auto___25862 = arguments.length;
var i__24202__auto___25863 = (0);
while(true){
if((i__24202__auto___25863 < len__24201__auto___25862)){
args__24208__auto__.push((arguments[i__24202__auto___25863]));

var G__25864 = (i__24202__auto___25863 + (1));
i__24202__auto___25863 = G__25864;
continue;
} else {
}
break;
}

var argseq__24209__auto__ = ((((1) < args__24208__auto__.length))?(new cljs.core.IndexedSeq(args__24208__auto__.slice((1)),(0))):null);
return markdown.core.format.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),argseq__24209__auto__);
});

markdown.core.format.cljs$core$IFn$_invoke$arity$variadic = (function (fmt,args){
return cljs.core.apply.call(null,goog.string.format,fmt,args);
});

markdown.core.format.cljs$lang$maxFixedArity = (1);

markdown.core.format.cljs$lang$applyTo = (function (seq25860){
var G__25861 = cljs.core.first.call(null,seq25860);
var seq25860__$1 = cljs.core.next.call(null,seq25860);
return markdown.core.format.cljs$core$IFn$_invoke$arity$variadic(G__25861,seq25860__$1);
});
markdown.core.parse_references = (function markdown$core$parse_references(lines){
var references = cljs.core.atom.call(null,cljs.core.PersistentArrayMap.EMPTY);
var seq__25869_25873 = cljs.core.seq.call(null,lines);
var chunk__25870_25874 = null;
var count__25871_25875 = (0);
var i__25872_25876 = (0);
while(true){
if((i__25872_25876 < count__25871_25875)){
var line_25877 = cljs.core._nth.call(null,chunk__25870_25874,i__25872_25876);
markdown.links.parse_reference_link.call(null,line_25877,references);

var G__25878 = seq__25869_25873;
var G__25879 = chunk__25870_25874;
var G__25880 = count__25871_25875;
var G__25881 = (i__25872_25876 + (1));
seq__25869_25873 = G__25878;
chunk__25870_25874 = G__25879;
count__25871_25875 = G__25880;
i__25872_25876 = G__25881;
continue;
} else {
var temp__4657__auto___25882 = cljs.core.seq.call(null,seq__25869_25873);
if(temp__4657__auto___25882){
var seq__25869_25883__$1 = temp__4657__auto___25882;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__25869_25883__$1)){
var c__23946__auto___25884 = cljs.core.chunk_first.call(null,seq__25869_25883__$1);
var G__25885 = cljs.core.chunk_rest.call(null,seq__25869_25883__$1);
var G__25886 = c__23946__auto___25884;
var G__25887 = cljs.core.count.call(null,c__23946__auto___25884);
var G__25888 = (0);
seq__25869_25873 = G__25885;
chunk__25870_25874 = G__25886;
count__25871_25875 = G__25887;
i__25872_25876 = G__25888;
continue;
} else {
var line_25889 = cljs.core.first.call(null,seq__25869_25883__$1);
markdown.links.parse_reference_link.call(null,line_25889,references);

var G__25890 = cljs.core.next.call(null,seq__25869_25883__$1);
var G__25891 = null;
var G__25892 = (0);
var G__25893 = (0);
seq__25869_25873 = G__25890;
chunk__25870_25874 = G__25891;
count__25871_25875 = G__25892;
i__25872_25876 = G__25893;
continue;
}
} else {
}
}
break;
}

return cljs.core.deref.call(null,references);
});
markdown.core.parse_footnotes = (function markdown$core$parse_footnotes(lines){
var footnotes = cljs.core.atom.call(null,new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"next-fn-id","next-fn-id",738579636),(1),new cljs.core.Keyword(null,"processed","processed",800622264),cljs.core.PersistentArrayMap.EMPTY,new cljs.core.Keyword(null,"unprocessed","unprocessed",766771972),cljs.core.PersistentArrayMap.EMPTY], null));
var seq__25898_25902 = cljs.core.seq.call(null,lines);
var chunk__25899_25903 = null;
var count__25900_25904 = (0);
var i__25901_25905 = (0);
while(true){
if((i__25901_25905 < count__25900_25904)){
var line_25906 = cljs.core._nth.call(null,chunk__25899_25903,i__25901_25905);
markdown.links.parse_footnote_link.call(null,line_25906,footnotes);

var G__25907 = seq__25898_25902;
var G__25908 = chunk__25899_25903;
var G__25909 = count__25900_25904;
var G__25910 = (i__25901_25905 + (1));
seq__25898_25902 = G__25907;
chunk__25899_25903 = G__25908;
count__25900_25904 = G__25909;
i__25901_25905 = G__25910;
continue;
} else {
var temp__4657__auto___25911 = cljs.core.seq.call(null,seq__25898_25902);
if(temp__4657__auto___25911){
var seq__25898_25912__$1 = temp__4657__auto___25911;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__25898_25912__$1)){
var c__23946__auto___25913 = cljs.core.chunk_first.call(null,seq__25898_25912__$1);
var G__25914 = cljs.core.chunk_rest.call(null,seq__25898_25912__$1);
var G__25915 = c__23946__auto___25913;
var G__25916 = cljs.core.count.call(null,c__23946__auto___25913);
var G__25917 = (0);
seq__25898_25902 = G__25914;
chunk__25899_25903 = G__25915;
count__25900_25904 = G__25916;
i__25901_25905 = G__25917;
continue;
} else {
var line_25918 = cljs.core.first.call(null,seq__25898_25912__$1);
markdown.links.parse_footnote_link.call(null,line_25918,footnotes);

var G__25919 = cljs.core.next.call(null,seq__25898_25912__$1);
var G__25920 = null;
var G__25921 = (0);
var G__25922 = (0);
seq__25898_25902 = G__25919;
chunk__25899_25903 = G__25920;
count__25900_25904 = G__25921;
i__25901_25905 = G__25922;
continue;
}
} else {
}
}
break;
}

return cljs.core.deref.call(null,footnotes);
});
markdown.core.parse_metadata = (function markdown$core$parse_metadata(lines){
var vec__25925 = cljs.core.split_with.call(null,(function (p1__25923_SHARP_){
return cljs.core.not_empty.call(null,p1__25923_SHARP_.trim());
}),lines);
var metadata = cljs.core.nth.call(null,vec__25925,(0),null);
var lines__$1 = cljs.core.nth.call(null,vec__25925,(1),null);
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [markdown.transformers.parse_metadata_headers.call(null,metadata),lines__$1], null);
});
/**
 * processes input text line by line and outputs an HTML string
 */
markdown.core.md_to_html_string_STAR_ = (function markdown$core$md_to_html_string_STAR_(text,params){
var _STAR_substring_STAR_25933 = markdown.common._STAR_substring_STAR_;
var formatter25934 = markdown.transformers.formatter;
markdown.common._STAR_substring_STAR_ = ((function (_STAR_substring_STAR_25933,formatter25934){
return (function (s,n){
return cljs.core.apply.call(null,cljs.core.str,cljs.core.drop.call(null,n,s));
});})(_STAR_substring_STAR_25933,formatter25934))
;

markdown.transformers.formatter = markdown.core.format;

try{var params__$1 = (cljs.core.truth_(params)?cljs.core.apply.call(null,cljs.core.partial.call(null,cljs.core.assoc,cljs.core.PersistentArrayMap.EMPTY),params):null);
var lines = [cljs.core.str(text),cljs.core.str("\n")].join('').split("\n");
var html = (new goog.string.StringBuffer(""));
var references = (cljs.core.truth_(new cljs.core.Keyword(null,"reference-links?","reference-links?",-2003778981).cljs$core$IFn$_invoke$arity$1(params__$1))?markdown.core.parse_references.call(null,lines):null);
var footnotes = (cljs.core.truth_(new cljs.core.Keyword(null,"footnotes?","footnotes?",-1590157845).cljs$core$IFn$_invoke$arity$1(params__$1))?markdown.core.parse_footnotes.call(null,lines):null);
var vec__25935 = (cljs.core.truth_(new cljs.core.Keyword(null,"parse-meta?","parse-meta?",-1938948742).cljs$core$IFn$_invoke$arity$1(params__$1))?markdown.core.parse_metadata.call(null,lines):new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [null,lines], null));
var metadata = cljs.core.nth.call(null,vec__25935,(0),null);
var lines__$1 = cljs.core.nth.call(null,vec__25935,(1),null);
var transformer = markdown.core.init_transformer.call(null,params__$1);
var G__25937_25940 = lines__$1;
var vec__25938_25941 = G__25937_25940;
var line_25942 = cljs.core.nth.call(null,vec__25938_25941,(0),null);
var more_25943 = cljs.core.nthnext.call(null,vec__25938_25941,(1));
var state_25944 = cljs.core.merge.call(null,new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword(null,"clojurescript","clojurescript",-299769403),true,new cljs.core.Keyword(null,"references","references",882562509),references,new cljs.core.Keyword(null,"footnotes","footnotes",-1842778205),footnotes,new cljs.core.Keyword(null,"last-line-empty?","last-line-empty?",1279111527),true], null),params__$1);
var G__25937_25945__$1 = G__25937_25940;
var state_25946__$1 = state_25944;
while(true){
var vec__25939_25947 = G__25937_25945__$1;
var line_25948__$1 = cljs.core.nth.call(null,vec__25939_25947,(0),null);
var more_25949__$1 = cljs.core.nthnext.call(null,vec__25939_25947,(1));
var state_25950__$2 = state_25946__$1;
var state_25951__$3 = (cljs.core.truth_(new cljs.core.Keyword(null,"buf","buf",-213913340).cljs$core$IFn$_invoke$arity$1(state_25950__$2))?transformer.call(null,html,new cljs.core.Keyword(null,"buf","buf",-213913340).cljs$core$IFn$_invoke$arity$1(state_25950__$2),cljs.core.first.call(null,more_25949__$1),cljs.core.assoc.call(null,cljs.core.dissoc.call(null,state_25950__$2,new cljs.core.Keyword(null,"buf","buf",-213913340),new cljs.core.Keyword(null,"lists","lists",-884730684)),new cljs.core.Keyword(null,"last-line-empty?","last-line-empty?",1279111527),true)):state_25950__$2);
if(cljs.core.truth_(cljs.core.not_empty.call(null,more_25949__$1))){
var G__25952 = more_25949__$1;
var G__25953 = cljs.core.assoc.call(null,transformer.call(null,html,line_25948__$1,cljs.core.first.call(null,more_25949__$1),state_25951__$3),new cljs.core.Keyword(null,"last-line-empty?","last-line-empty?",1279111527),cljs.core.empty_QMARK_.call(null,line_25948__$1));
G__25937_25945__$1 = G__25952;
state_25946__$1 = G__25953;
continue;
} else {
transformer.call(null,html.append(markdown.transformers.footer.call(null,new cljs.core.Keyword(null,"footnotes","footnotes",-1842778205).cljs$core$IFn$_invoke$arity$1(state_25951__$3))),line_25948__$1,"",cljs.core.assoc.call(null,state_25951__$3,new cljs.core.Keyword(null,"eof","eof",-489063237),true));
}
break;
}

return new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"metadata","metadata",1799301597),metadata,new cljs.core.Keyword(null,"html","html",-998796897),html.toString()], null);
}finally {markdown.transformers.formatter = formatter25934;

markdown.common._STAR_substring_STAR_ = _STAR_substring_STAR_25933;
}});
markdown.core.md__GT_html = (function markdown$core$md__GT_html(var_args){
var args__24208__auto__ = [];
var len__24201__auto___25956 = arguments.length;
var i__24202__auto___25957 = (0);
while(true){
if((i__24202__auto___25957 < len__24201__auto___25956)){
args__24208__auto__.push((arguments[i__24202__auto___25957]));

var G__25958 = (i__24202__auto___25957 + (1));
i__24202__auto___25957 = G__25958;
continue;
} else {
}
break;
}

var argseq__24209__auto__ = ((((1) < args__24208__auto__.length))?(new cljs.core.IndexedSeq(args__24208__auto__.slice((1)),(0))):null);
return markdown.core.md__GT_html.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),argseq__24209__auto__);
});

markdown.core.md__GT_html.cljs$core$IFn$_invoke$arity$variadic = (function (text,params){
return new cljs.core.Keyword(null,"html","html",-998796897).cljs$core$IFn$_invoke$arity$1(markdown.core.md_to_html_string_STAR_.call(null,text,params));
});

markdown.core.md__GT_html.cljs$lang$maxFixedArity = (1);

markdown.core.md__GT_html.cljs$lang$applyTo = (function (seq25954){
var G__25955 = cljs.core.first.call(null,seq25954);
var seq25954__$1 = cljs.core.next.call(null,seq25954);
return markdown.core.md__GT_html.cljs$core$IFn$_invoke$arity$variadic(G__25955,seq25954__$1);
});
markdown.core.md__GT_html_with_meta = (function markdown$core$md__GT_html_with_meta(var_args){
var args__24208__auto__ = [];
var len__24201__auto___25961 = arguments.length;
var i__24202__auto___25962 = (0);
while(true){
if((i__24202__auto___25962 < len__24201__auto___25961)){
args__24208__auto__.push((arguments[i__24202__auto___25962]));

var G__25963 = (i__24202__auto___25962 + (1));
i__24202__auto___25962 = G__25963;
continue;
} else {
}
break;
}

var argseq__24209__auto__ = ((((1) < args__24208__auto__.length))?(new cljs.core.IndexedSeq(args__24208__auto__.slice((1)),(0))):null);
return markdown.core.md__GT_html_with_meta.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),argseq__24209__auto__);
});

markdown.core.md__GT_html_with_meta.cljs$core$IFn$_invoke$arity$variadic = (function (text,params){
return markdown.core.md_to_html_string_STAR_.call(null,text,cljs.core.into.call(null,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"parse-meta?","parse-meta?",-1938948742),true], null),params));
});

markdown.core.md__GT_html_with_meta.cljs$lang$maxFixedArity = (1);

markdown.core.md__GT_html_with_meta.cljs$lang$applyTo = (function (seq25959){
var G__25960 = cljs.core.first.call(null,seq25959);
var seq25959__$1 = cljs.core.next.call(null,seq25959);
return markdown.core.md__GT_html_with_meta.cljs$core$IFn$_invoke$arity$variadic(G__25960,seq25959__$1);
});
/**
 * Js accessible wrapper
 */
markdown.core.mdToHtml = (function markdown$core$mdToHtml(var_args){
var args__24208__auto__ = [];
var len__24201__auto___25965 = arguments.length;
var i__24202__auto___25966 = (0);
while(true){
if((i__24202__auto___25966 < len__24201__auto___25965)){
args__24208__auto__.push((arguments[i__24202__auto___25966]));

var G__25967 = (i__24202__auto___25966 + (1));
i__24202__auto___25966 = G__25967;
continue;
} else {
}
break;
}

var argseq__24209__auto__ = ((((0) < args__24208__auto__.length))?(new cljs.core.IndexedSeq(args__24208__auto__.slice((0)),(0))):null);
return markdown.core.mdToHtml.cljs$core$IFn$_invoke$arity$variadic(argseq__24209__auto__);
});
goog.exportSymbol('markdown.core.mdToHtml', markdown.core.mdToHtml);

markdown.core.mdToHtml.cljs$core$IFn$_invoke$arity$variadic = (function (params){
return cljs.core.apply.call(null,markdown.core.md__GT_html,params);
});

markdown.core.mdToHtml.cljs$lang$maxFixedArity = (0);

markdown.core.mdToHtml.cljs$lang$applyTo = (function (seq25964){
return markdown.core.mdToHtml.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq.call(null,seq25964));
});
/**
 * Js accessible wrapper
 */
markdown.core.mdToHtmlWithMeta = (function markdown$core$mdToHtmlWithMeta(var_args){
var args__24208__auto__ = [];
var len__24201__auto___25969 = arguments.length;
var i__24202__auto___25970 = (0);
while(true){
if((i__24202__auto___25970 < len__24201__auto___25969)){
args__24208__auto__.push((arguments[i__24202__auto___25970]));

var G__25971 = (i__24202__auto___25970 + (1));
i__24202__auto___25970 = G__25971;
continue;
} else {
}
break;
}

var argseq__24209__auto__ = ((((0) < args__24208__auto__.length))?(new cljs.core.IndexedSeq(args__24208__auto__.slice((0)),(0))):null);
return markdown.core.mdToHtmlWithMeta.cljs$core$IFn$_invoke$arity$variadic(argseq__24209__auto__);
});
goog.exportSymbol('markdown.core.mdToHtmlWithMeta', markdown.core.mdToHtmlWithMeta);

markdown.core.mdToHtmlWithMeta.cljs$core$IFn$_invoke$arity$variadic = (function (params){
return cljs.core.apply.call(null,markdown.core.md__GT_html_with_meta,params);
});

markdown.core.mdToHtmlWithMeta.cljs$lang$maxFixedArity = (0);

markdown.core.mdToHtmlWithMeta.cljs$lang$applyTo = (function (seq25968){
return markdown.core.mdToHtmlWithMeta.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq.call(null,seq25968));
});

//# sourceMappingURL=core.js.map