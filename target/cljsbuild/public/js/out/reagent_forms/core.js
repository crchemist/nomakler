// Compiled by ClojureScript 1.7.170 {}
goog.provide('reagent_forms.core');
goog.require('cljs.core');
goog.require('clojure.walk');
goog.require('clojure.string');
goog.require('goog.string');
goog.require('goog.string.format');
goog.require('reagent.core');
goog.require('reagent_forms.datepicker');
reagent_forms.core.value_of = (function reagent_forms$core$value_of(element){
return element.target.value;
});
reagent_forms.core.id__GT_path = cljs.core.memoize.call(null,(function (id){
if(cljs.core.sequential_QMARK_.call(null,id)){
return id;
} else {
var segments = clojure.string.split.call(null,cljs.core.subs.call(null,[cljs.core.str(id)].join(''),(1)),/\./);
return cljs.core.map.call(null,cljs.core.keyword,segments);
}
}));
reagent_forms.core.set_doc_value = (function reagent_forms$core$set_doc_value(doc,id,value,events){
var path = reagent_forms.core.id__GT_path.call(null,id);
return cljs.core.reduce.call(null,((function (path){
return (function (p1__27271_SHARP_,p2__27270_SHARP_){
var or__23143__auto__ = p2__27270_SHARP_.call(null,path,value,p1__27271_SHARP_);
if(cljs.core.truth_(or__23143__auto__)){
return or__23143__auto__;
} else {
return p1__27271_SHARP_;
}
});})(path))
,cljs.core.assoc_in.call(null,doc,path,value),events);
});
reagent_forms.core.mk_save_fn = (function reagent_forms$core$mk_save_fn(doc,events){
return (function (id,value){
return cljs.core.swap_BANG_.call(null,doc,reagent_forms.core.set_doc_value,id,value,events);
});
});
reagent_forms.core.wrap_get_fn = (function reagent_forms$core$wrap_get_fn(get,wrapper){
return (function (id){
return wrapper.call(null,get.call(null,id));
});
});
reagent_forms.core.wrap_save_fn = (function reagent_forms$core$wrap_save_fn(save_BANG_,wrapper){
return (function (id,value){
return save_BANG_.call(null,id,wrapper.call(null,value));
});
});
reagent_forms.core.wrap_fns = (function reagent_forms$core$wrap_fns(opts,node){
return new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"doc","doc",1913296891),new cljs.core.Keyword(null,"doc","doc",1913296891).cljs$core$IFn$_invoke$arity$1(opts),new cljs.core.Keyword(null,"get","get",1683182755),(function (){var temp__4655__auto__ = new cljs.core.Keyword(null,"in-fn","in-fn",-1938980694).cljs$core$IFn$_invoke$arity$1(cljs.core.second.call(null,node));
if(cljs.core.truth_(temp__4655__auto__)){
var in_fn = temp__4655__auto__;
return reagent_forms.core.wrap_get_fn.call(null,new cljs.core.Keyword(null,"get","get",1683182755).cljs$core$IFn$_invoke$arity$1(opts),in_fn);
} else {
return new cljs.core.Keyword(null,"get","get",1683182755).cljs$core$IFn$_invoke$arity$1(opts);
}
})(),new cljs.core.Keyword(null,"save!","save!",-1137373803),(function (){var temp__4655__auto__ = new cljs.core.Keyword(null,"out-fn","out-fn",747295447).cljs$core$IFn$_invoke$arity$1(cljs.core.second.call(null,node));
if(cljs.core.truth_(temp__4655__auto__)){
var out_fn = temp__4655__auto__;
return reagent_forms.core.wrap_save_fn.call(null,new cljs.core.Keyword(null,"save!","save!",-1137373803).cljs$core$IFn$_invoke$arity$1(opts),out_fn);
} else {
return new cljs.core.Keyword(null,"save!","save!",-1137373803).cljs$core$IFn$_invoke$arity$1(opts);
}
})()], null);
});
if(typeof reagent_forms.core.format_type !== 'undefined'){
} else {
reagent_forms.core.format_type = (function (){var method_table__24056__auto__ = cljs.core.atom.call(null,cljs.core.PersistentArrayMap.EMPTY);
var prefer_table__24057__auto__ = cljs.core.atom.call(null,cljs.core.PersistentArrayMap.EMPTY);
var method_cache__24058__auto__ = cljs.core.atom.call(null,cljs.core.PersistentArrayMap.EMPTY);
var cached_hierarchy__24059__auto__ = cljs.core.atom.call(null,cljs.core.PersistentArrayMap.EMPTY);
var hierarchy__24060__auto__ = cljs.core.get.call(null,cljs.core.PersistentArrayMap.EMPTY,new cljs.core.Keyword(null,"hierarchy","hierarchy",-1053470341),cljs.core.get_global_hierarchy.call(null));
return (new cljs.core.MultiFn(cljs.core.symbol.call(null,"reagent-forms.core","format-type"),((function (method_table__24056__auto__,prefer_table__24057__auto__,method_cache__24058__auto__,cached_hierarchy__24059__auto__,hierarchy__24060__auto__){
return (function (field_type,_){
if(cljs.core.truth_(cljs.core.some.call(null,cljs.core.PersistentHashSet.fromArray([field_type], true),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"range","range",1639692286),new cljs.core.Keyword(null,"numeric","numeric",-1495594714)], null)))){
return new cljs.core.Keyword(null,"numeric","numeric",-1495594714);
} else {
return field_type;
}
});})(method_table__24056__auto__,prefer_table__24057__auto__,method_cache__24058__auto__,cached_hierarchy__24059__auto__,hierarchy__24060__auto__))
,new cljs.core.Keyword(null,"default","default",-1987822328),hierarchy__24060__auto__,method_table__24056__auto__,prefer_table__24057__auto__,method_cache__24058__auto__,cached_hierarchy__24059__auto__));
})();
}
reagent_forms.core.valid_number_ending_QMARK_ = (function reagent_forms$core$valid_number_ending_QMARK_(n){
return ((cljs.core.not_EQ_.call(null,".",cljs.core.last.call(null,cljs.core.butlast.call(null,n)))) && (cljs.core._EQ_.call(null,".",cljs.core.last.call(null,n)))) || (cljs.core._EQ_.call(null,"0",cljs.core.last.call(null,n)));
});
reagent_forms.core.format_value = (function reagent_forms$core$format_value(fmt,value){
if(cljs.core.truth_((function (){var and__23131__auto__ = cljs.core.not.call(null,isNaN(parseFloat(value)));
if(and__23131__auto__){
return fmt;
} else {
return and__23131__auto__;
}
})())){
return goog.string.format(fmt,value);
} else {
return value;
}
});
cljs.core._add_method.call(null,reagent_forms.core.format_type,new cljs.core.Keyword(null,"numeric","numeric",-1495594714),(function (_,n){
if(cljs.core.truth_(cljs.core.not_empty.call(null,n))){
var parsed = parseFloat(n);
if(cljs.core.truth_(isNaN(parsed))){
return null;
} else {
return parsed;
}
} else {
return null;
}
}));
cljs.core._add_method.call(null,reagent_forms.core.format_type,new cljs.core.Keyword(null,"default","default",-1987822328),(function (_,value){
return value;
}));
if(typeof reagent_forms.core.bind !== 'undefined'){
} else {
reagent_forms.core.bind = (function (){var method_table__24056__auto__ = cljs.core.atom.call(null,cljs.core.PersistentArrayMap.EMPTY);
var prefer_table__24057__auto__ = cljs.core.atom.call(null,cljs.core.PersistentArrayMap.EMPTY);
var method_cache__24058__auto__ = cljs.core.atom.call(null,cljs.core.PersistentArrayMap.EMPTY);
var cached_hierarchy__24059__auto__ = cljs.core.atom.call(null,cljs.core.PersistentArrayMap.EMPTY);
var hierarchy__24060__auto__ = cljs.core.get.call(null,cljs.core.PersistentArrayMap.EMPTY,new cljs.core.Keyword(null,"hierarchy","hierarchy",-1053470341),cljs.core.get_global_hierarchy.call(null));
return (new cljs.core.MultiFn(cljs.core.symbol.call(null,"reagent-forms.core","bind"),((function (method_table__24056__auto__,prefer_table__24057__auto__,method_cache__24058__auto__,cached_hierarchy__24059__auto__,hierarchy__24060__auto__){
return (function (p__27272,_){
var map__27273 = p__27272;
var map__27273__$1 = ((((!((map__27273 == null)))?((((map__27273.cljs$lang$protocol_mask$partition0$ & (64))) || (map__27273.cljs$core$ISeq$))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__27273):map__27273);
var field = cljs.core.get.call(null,map__27273__$1,new cljs.core.Keyword(null,"field","field",-1302436500));
if(cljs.core.truth_(cljs.core.some.call(null,cljs.core.PersistentHashSet.fromArray([field], true),new cljs.core.PersistentVector(null, 7, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"text","text",-1790561697),new cljs.core.Keyword(null,"numeric","numeric",-1495594714),new cljs.core.Keyword(null,"password","password",417022471),new cljs.core.Keyword(null,"email","email",1415816706),new cljs.core.Keyword(null,"tel","tel",224138159),new cljs.core.Keyword(null,"range","range",1639692286),new cljs.core.Keyword(null,"textarea","textarea",-650375824)], null)))){
return new cljs.core.Keyword(null,"input-field","input-field",289353943);
} else {
return field;
}
});})(method_table__24056__auto__,prefer_table__24057__auto__,method_cache__24058__auto__,cached_hierarchy__24059__auto__,hierarchy__24060__auto__))
,new cljs.core.Keyword(null,"default","default",-1987822328),hierarchy__24060__auto__,method_table__24056__auto__,prefer_table__24057__auto__,method_cache__24058__auto__,cached_hierarchy__24059__auto__));
})();
}
cljs.core._add_method.call(null,reagent_forms.core.bind,new cljs.core.Keyword(null,"input-field","input-field",289353943),(function (p__27276,p__27277){
var map__27278 = p__27276;
var map__27278__$1 = ((((!((map__27278 == null)))?((((map__27278.cljs$lang$protocol_mask$partition0$ & (64))) || (map__27278.cljs$core$ISeq$))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__27278):map__27278);
var field = cljs.core.get.call(null,map__27278__$1,new cljs.core.Keyword(null,"field","field",-1302436500));
var id = cljs.core.get.call(null,map__27278__$1,new cljs.core.Keyword(null,"id","id",-1388402092));
var fmt = cljs.core.get.call(null,map__27278__$1,new cljs.core.Keyword(null,"fmt","fmt",332300772));
var map__27279 = p__27277;
var map__27279__$1 = ((((!((map__27279 == null)))?((((map__27279.cljs$lang$protocol_mask$partition0$ & (64))) || (map__27279.cljs$core$ISeq$))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__27279):map__27279);
var get = cljs.core.get.call(null,map__27279__$1,new cljs.core.Keyword(null,"get","get",1683182755));
var save_BANG_ = cljs.core.get.call(null,map__27279__$1,new cljs.core.Keyword(null,"save!","save!",-1137373803));
var doc = cljs.core.get.call(null,map__27279__$1,new cljs.core.Keyword(null,"doc","doc",1913296891));
return new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"value","value",305978217),(function (){var value = (function (){var or__23143__auto__ = get.call(null,id);
if(cljs.core.truth_(or__23143__auto__)){
return or__23143__auto__;
} else {
return "";
}
})();
return reagent_forms.core.format_value.call(null,fmt,value);
})(),new cljs.core.Keyword(null,"on-change","on-change",-732046149),((function (map__27278,map__27278__$1,field,id,fmt,map__27279,map__27279__$1,get,save_BANG_,doc){
return (function (p1__27275_SHARP_){
return save_BANG_.call(null,id,reagent_forms.core.format_type.call(null,field,reagent_forms.core.value_of.call(null,p1__27275_SHARP_)));
});})(map__27278,map__27278__$1,field,id,fmt,map__27279,map__27279__$1,get,save_BANG_,doc))
], null);
}));
cljs.core._add_method.call(null,reagent_forms.core.bind,new cljs.core.Keyword(null,"checkbox","checkbox",1612615655),(function (p__27282,p__27283){
var map__27284 = p__27282;
var map__27284__$1 = ((((!((map__27284 == null)))?((((map__27284.cljs$lang$protocol_mask$partition0$ & (64))) || (map__27284.cljs$core$ISeq$))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__27284):map__27284);
var id = cljs.core.get.call(null,map__27284__$1,new cljs.core.Keyword(null,"id","id",-1388402092));
var map__27285 = p__27283;
var map__27285__$1 = ((((!((map__27285 == null)))?((((map__27285.cljs$lang$protocol_mask$partition0$ & (64))) || (map__27285.cljs$core$ISeq$))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__27285):map__27285);
var get = cljs.core.get.call(null,map__27285__$1,new cljs.core.Keyword(null,"get","get",1683182755));
var save_BANG_ = cljs.core.get.call(null,map__27285__$1,new cljs.core.Keyword(null,"save!","save!",-1137373803));
return cljs.core.merge.call(null,new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"default-checked","default-checked",1039965863),get.call(null,id),new cljs.core.Keyword(null,"on-change","on-change",-732046149),((function (map__27284,map__27284__$1,id,map__27285,map__27285__$1,get,save_BANG_){
return (function (){
return save_BANG_.call(null,id,cljs.core.not.call(null,get.call(null,id)));
});})(map__27284,map__27284__$1,id,map__27285,map__27285__$1,get,save_BANG_))
], null),(cljs.core.truth_(get.call(null,id))?new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"checked","checked",-50955819),"checked"], null):null));
}));
cljs.core._add_method.call(null,reagent_forms.core.bind,new cljs.core.Keyword(null,"default","default",-1987822328),(function (_,___$1){
return null;
}));
reagent_forms.core.set_attrs = (function reagent_forms$core$set_attrs(var_args){
var args__24208__auto__ = [];
var len__24201__auto___27295 = arguments.length;
var i__24202__auto___27296 = (0);
while(true){
if((i__24202__auto___27296 < len__24201__auto___27295)){
args__24208__auto__.push((arguments[i__24202__auto___27296]));

var G__27297 = (i__24202__auto___27296 + (1));
i__24202__auto___27296 = G__27297;
continue;
} else {
}
break;
}

var argseq__24209__auto__ = ((((2) < args__24208__auto__.length))?(new cljs.core.IndexedSeq(args__24208__auto__.slice((2)),(0))):null);
return reagent_forms.core.set_attrs.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),(arguments[(1)]),argseq__24209__auto__);
});

reagent_forms.core.set_attrs.cljs$core$IFn$_invoke$arity$variadic = (function (p__27291,opts,p__27292){
var vec__27293 = p__27291;
var type = cljs.core.nth.call(null,vec__27293,(0),null);
var attrs = cljs.core.nth.call(null,vec__27293,(1),null);
var body = cljs.core.nthnext.call(null,vec__27293,(2));
var vec__27294 = p__27292;
var default_attrs = cljs.core.nth.call(null,vec__27294,(0),null);
return cljs.core.into.call(null,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [type,cljs.core.merge.call(null,default_attrs,reagent_forms.core.bind.call(null,attrs,opts),attrs)], null),body);
});

reagent_forms.core.set_attrs.cljs$lang$maxFixedArity = (2);

reagent_forms.core.set_attrs.cljs$lang$applyTo = (function (seq27288){
var G__27289 = cljs.core.first.call(null,seq27288);
var seq27288__$1 = cljs.core.next.call(null,seq27288);
var G__27290 = cljs.core.first.call(null,seq27288__$1);
var seq27288__$2 = cljs.core.next.call(null,seq27288__$1);
return reagent_forms.core.set_attrs.cljs$core$IFn$_invoke$arity$variadic(G__27289,G__27290,seq27288__$2);
});
if(typeof reagent_forms.core.init_field !== 'undefined'){
} else {
reagent_forms.core.init_field = (function (){var method_table__24056__auto__ = cljs.core.atom.call(null,cljs.core.PersistentArrayMap.EMPTY);
var prefer_table__24057__auto__ = cljs.core.atom.call(null,cljs.core.PersistentArrayMap.EMPTY);
var method_cache__24058__auto__ = cljs.core.atom.call(null,cljs.core.PersistentArrayMap.EMPTY);
var cached_hierarchy__24059__auto__ = cljs.core.atom.call(null,cljs.core.PersistentArrayMap.EMPTY);
var hierarchy__24060__auto__ = cljs.core.get.call(null,cljs.core.PersistentArrayMap.EMPTY,new cljs.core.Keyword(null,"hierarchy","hierarchy",-1053470341),cljs.core.get_global_hierarchy.call(null));
return (new cljs.core.MultiFn(cljs.core.symbol.call(null,"reagent-forms.core","init-field"),((function (method_table__24056__auto__,prefer_table__24057__auto__,method_cache__24058__auto__,cached_hierarchy__24059__auto__,hierarchy__24060__auto__){
return (function (p__27298,_){
var vec__27299 = p__27298;
var ___$1 = cljs.core.nth.call(null,vec__27299,(0),null);
var map__27300 = cljs.core.nth.call(null,vec__27299,(1),null);
var map__27300__$1 = ((((!((map__27300 == null)))?((((map__27300.cljs$lang$protocol_mask$partition0$ & (64))) || (map__27300.cljs$core$ISeq$))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__27300):map__27300);
var field = cljs.core.get.call(null,map__27300__$1,new cljs.core.Keyword(null,"field","field",-1302436500));
var field__$1 = cljs.core.keyword.call(null,field);
if(cljs.core.truth_(cljs.core.some.call(null,cljs.core.PersistentHashSet.fromArray([field__$1], true),new cljs.core.PersistentVector(null, 6, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"range","range",1639692286),new cljs.core.Keyword(null,"text","text",-1790561697),new cljs.core.Keyword(null,"password","password",417022471),new cljs.core.Keyword(null,"email","email",1415816706),new cljs.core.Keyword(null,"tel","tel",224138159),new cljs.core.Keyword(null,"textarea","textarea",-650375824)], null)))){
return new cljs.core.Keyword(null,"input-field","input-field",289353943);
} else {
return field__$1;
}
});})(method_table__24056__auto__,prefer_table__24057__auto__,method_cache__24058__auto__,cached_hierarchy__24059__auto__,hierarchy__24060__auto__))
,new cljs.core.Keyword(null,"default","default",-1987822328),hierarchy__24060__auto__,method_table__24056__auto__,prefer_table__24057__auto__,method_cache__24058__auto__,cached_hierarchy__24059__auto__));
})();
}
cljs.core._add_method.call(null,reagent_forms.core.init_field,new cljs.core.Keyword(null,"container","container",-1736937707),(function (p__27303,p__27304){
var vec__27305 = p__27303;
var type = cljs.core.nth.call(null,vec__27305,(0),null);
var map__27306 = cljs.core.nth.call(null,vec__27305,(1),null);
var map__27306__$1 = ((((!((map__27306 == null)))?((((map__27306.cljs$lang$protocol_mask$partition0$ & (64))) || (map__27306.cljs$core$ISeq$))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__27306):map__27306);
var attrs = map__27306__$1;
var valid_QMARK_ = cljs.core.get.call(null,map__27306__$1,new cljs.core.Keyword(null,"valid?","valid?",-212412379));
var body = cljs.core.nthnext.call(null,vec__27305,(2));
var map__27307 = p__27304;
var map__27307__$1 = ((((!((map__27307 == null)))?((((map__27307.cljs$lang$protocol_mask$partition0$ & (64))) || (map__27307.cljs$core$ISeq$))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__27307):map__27307);
var doc = cljs.core.get.call(null,map__27307__$1,new cljs.core.Keyword(null,"doc","doc",1913296891));
return ((function (vec__27305,type,map__27306,map__27306__$1,attrs,valid_QMARK_,body,map__27307,map__27307__$1,doc){
return (function (){
var temp__4655__auto__ = new cljs.core.Keyword(null,"visible?","visible?",2129863715).cljs$core$IFn$_invoke$arity$1(attrs);
if(cljs.core.truth_(temp__4655__auto__)){
var visible__27262__auto__ = temp__4655__auto__;
if(cljs.core.truth_(visible__27262__auto__.call(null,cljs.core.deref.call(null,doc)))){
return cljs.core.into.call(null,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [type,(function (){var temp__4655__auto____$1 = (cljs.core.truth_(valid_QMARK_)?valid_QMARK_.call(null,cljs.core.deref.call(null,doc)):null);
if(cljs.core.truth_(temp__4655__auto____$1)){
var valid_class = temp__4655__auto____$1;
return cljs.core.update_in.call(null,attrs,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"class","class",-2030961996)], null),((function (valid_class,temp__4655__auto____$1,visible__27262__auto__,temp__4655__auto__,vec__27305,type,map__27306,map__27306__$1,attrs,valid_QMARK_,body,map__27307,map__27307__$1,doc){
return (function (p1__27302_SHARP_){
if(!(cljs.core.empty_QMARK_.call(null,p1__27302_SHARP_))){
return [cljs.core.str(p1__27302_SHARP_),cljs.core.str(" "),cljs.core.str(valid_class)].join('');
} else {
return valid_class;
}
});})(valid_class,temp__4655__auto____$1,visible__27262__auto__,temp__4655__auto__,vec__27305,type,map__27306,map__27306__$1,attrs,valid_QMARK_,body,map__27307,map__27307__$1,doc))
);
} else {
return attrs;
}
})()], null),body);
} else {
return null;
}
} else {
return cljs.core.into.call(null,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [type,(function (){var temp__4655__auto____$1 = (cljs.core.truth_(valid_QMARK_)?valid_QMARK_.call(null,cljs.core.deref.call(null,doc)):null);
if(cljs.core.truth_(temp__4655__auto____$1)){
var valid_class = temp__4655__auto____$1;
return cljs.core.update_in.call(null,attrs,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"class","class",-2030961996)], null),((function (valid_class,temp__4655__auto____$1,temp__4655__auto__,vec__27305,type,map__27306,map__27306__$1,attrs,valid_QMARK_,body,map__27307,map__27307__$1,doc){
return (function (p1__27302_SHARP_){
if(!(cljs.core.empty_QMARK_.call(null,p1__27302_SHARP_))){
return [cljs.core.str(p1__27302_SHARP_),cljs.core.str(" "),cljs.core.str(valid_class)].join('');
} else {
return valid_class;
}
});})(valid_class,temp__4655__auto____$1,temp__4655__auto__,vec__27305,type,map__27306,map__27306__$1,attrs,valid_QMARK_,body,map__27307,map__27307__$1,doc))
);
} else {
return attrs;
}
})()], null),body);
}
});
;})(vec__27305,type,map__27306,map__27306__$1,attrs,valid_QMARK_,body,map__27307,map__27307__$1,doc))
}));
cljs.core._add_method.call(null,reagent_forms.core.init_field,new cljs.core.Keyword(null,"input-field","input-field",289353943),(function (p__27310,p__27311){
var vec__27312 = p__27310;
var _ = cljs.core.nth.call(null,vec__27312,(0),null);
var map__27313 = cljs.core.nth.call(null,vec__27312,(1),null);
var map__27313__$1 = ((((!((map__27313 == null)))?((((map__27313.cljs$lang$protocol_mask$partition0$ & (64))) || (map__27313.cljs$core$ISeq$))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__27313):map__27313);
var attrs = map__27313__$1;
var field = cljs.core.get.call(null,map__27313__$1,new cljs.core.Keyword(null,"field","field",-1302436500));
var component = vec__27312;
var map__27314 = p__27311;
var map__27314__$1 = ((((!((map__27314 == null)))?((((map__27314.cljs$lang$protocol_mask$partition0$ & (64))) || (map__27314.cljs$core$ISeq$))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__27314):map__27314);
var opts = map__27314__$1;
var doc = cljs.core.get.call(null,map__27314__$1,new cljs.core.Keyword(null,"doc","doc",1913296891));
return ((function (vec__27312,_,map__27313,map__27313__$1,attrs,field,component,map__27314,map__27314__$1,opts,doc){
return (function (){
var temp__4655__auto__ = new cljs.core.Keyword(null,"visible?","visible?",2129863715).cljs$core$IFn$_invoke$arity$1(attrs);
if(cljs.core.truth_(temp__4655__auto__)){
var visible__27262__auto__ = temp__4655__auto__;
if(cljs.core.truth_(visible__27262__auto__.call(null,cljs.core.deref.call(null,doc)))){
return reagent_forms.core.set_attrs.call(null,component,opts,new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"type","type",1174270348),field], null));
} else {
return null;
}
} else {
return reagent_forms.core.set_attrs.call(null,component,opts,new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"type","type",1174270348),field], null));
}
});
;})(vec__27312,_,map__27313,map__27313__$1,attrs,field,component,map__27314,map__27314__$1,opts,doc))
}));
cljs.core._add_method.call(null,reagent_forms.core.init_field,new cljs.core.Keyword(null,"numeric","numeric",-1495594714),(function (p__27319,p__27320){
var vec__27321 = p__27319;
var type = cljs.core.nth.call(null,vec__27321,(0),null);
var map__27322 = cljs.core.nth.call(null,vec__27321,(1),null);
var map__27322__$1 = ((((!((map__27322 == null)))?((((map__27322.cljs$lang$protocol_mask$partition0$ & (64))) || (map__27322.cljs$core$ISeq$))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__27322):map__27322);
var attrs = map__27322__$1;
var id = cljs.core.get.call(null,map__27322__$1,new cljs.core.Keyword(null,"id","id",-1388402092));
var fmt = cljs.core.get.call(null,map__27322__$1,new cljs.core.Keyword(null,"fmt","fmt",332300772));
var map__27323 = p__27320;
var map__27323__$1 = ((((!((map__27323 == null)))?((((map__27323.cljs$lang$protocol_mask$partition0$ & (64))) || (map__27323.cljs$core$ISeq$))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__27323):map__27323);
var doc = cljs.core.get.call(null,map__27323__$1,new cljs.core.Keyword(null,"doc","doc",1913296891));
var get = cljs.core.get.call(null,map__27323__$1,new cljs.core.Keyword(null,"get","get",1683182755));
var save_BANG_ = cljs.core.get.call(null,map__27323__$1,new cljs.core.Keyword(null,"save!","save!",-1137373803));
var input_value = reagent.core.atom.call(null,null);
return ((function (input_value,vec__27321,type,map__27322,map__27322__$1,attrs,id,fmt,map__27323,map__27323__$1,doc,get,save_BANG_){
return (function (){
var temp__4655__auto__ = new cljs.core.Keyword(null,"visible?","visible?",2129863715).cljs$core$IFn$_invoke$arity$1(attrs);
if(cljs.core.truth_(temp__4655__auto__)){
var visible__27262__auto__ = temp__4655__auto__;
if(cljs.core.truth_(visible__27262__auto__.call(null,cljs.core.deref.call(null,doc)))){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [type,cljs.core.merge.call(null,new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword(null,"type","type",1174270348),new cljs.core.Keyword(null,"text","text",-1790561697),new cljs.core.Keyword(null,"value","value",305978217),(function (){var or__23143__auto__ = cljs.core.deref.call(null,input_value);
if(cljs.core.truth_(or__23143__auto__)){
return or__23143__auto__;
} else {
return get.call(null,id,"");
}
})(),new cljs.core.Keyword(null,"on-change","on-change",-732046149),((function (visible__27262__auto__,temp__4655__auto__,input_value,vec__27321,type,map__27322,map__27322__$1,attrs,id,fmt,map__27323,map__27323__$1,doc,get,save_BANG_){
return (function (p1__27317_SHARP_){
return cljs.core.reset_BANG_.call(null,input_value,reagent_forms.core.value_of.call(null,p1__27317_SHARP_));
});})(visible__27262__auto__,temp__4655__auto__,input_value,vec__27321,type,map__27322,map__27322__$1,attrs,id,fmt,map__27323,map__27323__$1,doc,get,save_BANG_))
,new cljs.core.Keyword(null,"on-blur","on-blur",814300747),((function (visible__27262__auto__,temp__4655__auto__,input_value,vec__27321,type,map__27322,map__27322__$1,attrs,id,fmt,map__27323,map__27323__$1,doc,get,save_BANG_){
return (function (p1__27318_SHARP_){
cljs.core.reset_BANG_.call(null,input_value,null);

return save_BANG_.call(null,id,reagent_forms.core.format_type.call(null,new cljs.core.Keyword(null,"numeric","numeric",-1495594714),reagent_forms.core.format_value.call(null,fmt,reagent_forms.core.value_of.call(null,p1__27318_SHARP_))));
});})(visible__27262__auto__,temp__4655__auto__,input_value,vec__27321,type,map__27322,map__27322__$1,attrs,id,fmt,map__27323,map__27323__$1,doc,get,save_BANG_))
], null),attrs)], null);
} else {
return null;
}
} else {
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [type,cljs.core.merge.call(null,new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword(null,"type","type",1174270348),new cljs.core.Keyword(null,"text","text",-1790561697),new cljs.core.Keyword(null,"value","value",305978217),(function (){var or__23143__auto__ = cljs.core.deref.call(null,input_value);
if(cljs.core.truth_(or__23143__auto__)){
return or__23143__auto__;
} else {
return get.call(null,id,"");
}
})(),new cljs.core.Keyword(null,"on-change","on-change",-732046149),((function (temp__4655__auto__,input_value,vec__27321,type,map__27322,map__27322__$1,attrs,id,fmt,map__27323,map__27323__$1,doc,get,save_BANG_){
return (function (p1__27317_SHARP_){
return cljs.core.reset_BANG_.call(null,input_value,reagent_forms.core.value_of.call(null,p1__27317_SHARP_));
});})(temp__4655__auto__,input_value,vec__27321,type,map__27322,map__27322__$1,attrs,id,fmt,map__27323,map__27323__$1,doc,get,save_BANG_))
,new cljs.core.Keyword(null,"on-blur","on-blur",814300747),((function (temp__4655__auto__,input_value,vec__27321,type,map__27322,map__27322__$1,attrs,id,fmt,map__27323,map__27323__$1,doc,get,save_BANG_){
return (function (p1__27318_SHARP_){
cljs.core.reset_BANG_.call(null,input_value,null);

return save_BANG_.call(null,id,reagent_forms.core.format_type.call(null,new cljs.core.Keyword(null,"numeric","numeric",-1495594714),reagent_forms.core.format_value.call(null,fmt,reagent_forms.core.value_of.call(null,p1__27318_SHARP_))));
});})(temp__4655__auto__,input_value,vec__27321,type,map__27322,map__27322__$1,attrs,id,fmt,map__27323,map__27323__$1,doc,get,save_BANG_))
], null),attrs)], null);
}
});
;})(input_value,vec__27321,type,map__27322,map__27322__$1,attrs,id,fmt,map__27323,map__27323__$1,doc,get,save_BANG_))
}));
cljs.core._add_method.call(null,reagent_forms.core.init_field,new cljs.core.Keyword(null,"datepicker","datepicker",821741450),(function (p__27327,p__27328){
var vec__27329 = p__27327;
var _ = cljs.core.nth.call(null,vec__27329,(0),null);
var map__27330 = cljs.core.nth.call(null,vec__27329,(1),null);
var map__27330__$1 = ((((!((map__27330 == null)))?((((map__27330.cljs$lang$protocol_mask$partition0$ & (64))) || (map__27330.cljs$core$ISeq$))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__27330):map__27330);
var attrs = map__27330__$1;
var id = cljs.core.get.call(null,map__27330__$1,new cljs.core.Keyword(null,"id","id",-1388402092));
var date_format = cljs.core.get.call(null,map__27330__$1,new cljs.core.Keyword(null,"date-format","date-format",-557196721));
var inline = cljs.core.get.call(null,map__27330__$1,new cljs.core.Keyword(null,"inline","inline",1399884222));
var auto_close_QMARK_ = cljs.core.get.call(null,map__27330__$1,new cljs.core.Keyword(null,"auto-close?","auto-close?",-1675434568));
var lang = cljs.core.get.call(null,map__27330__$1,new cljs.core.Keyword(null,"lang","lang",-1819677104),new cljs.core.Keyword(null,"en-US","en-US",1221407245));
var map__27331 = p__27328;
var map__27331__$1 = ((((!((map__27331 == null)))?((((map__27331.cljs$lang$protocol_mask$partition0$ & (64))) || (map__27331.cljs$core$ISeq$))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__27331):map__27331);
var doc = cljs.core.get.call(null,map__27331__$1,new cljs.core.Keyword(null,"doc","doc",1913296891));
var get = cljs.core.get.call(null,map__27331__$1,new cljs.core.Keyword(null,"get","get",1683182755));
var save_BANG_ = cljs.core.get.call(null,map__27331__$1,new cljs.core.Keyword(null,"save!","save!",-1137373803));
var fmt = reagent_forms.datepicker.parse_format.call(null,date_format);
var selected_date = get.call(null,id);
var selected_month = (((new cljs.core.Keyword(null,"month","month",-1960248533).cljs$core$IFn$_invoke$arity$1(selected_date) > (0)))?(new cljs.core.Keyword(null,"month","month",-1960248533).cljs$core$IFn$_invoke$arity$1(selected_date) - (1)):new cljs.core.Keyword(null,"month","month",-1960248533).cljs$core$IFn$_invoke$arity$1(selected_date));
var today = (new Date());
var year = (function (){var or__23143__auto__ = new cljs.core.Keyword(null,"year","year",335913393).cljs$core$IFn$_invoke$arity$1(selected_date);
if(cljs.core.truth_(or__23143__auto__)){
return or__23143__auto__;
} else {
return today.getFullYear();
}
})();
var month = (function (){var or__23143__auto__ = selected_month;
if(cljs.core.truth_(or__23143__auto__)){
return or__23143__auto__;
} else {
return today.getMonth();
}
})();
var day = (function (){var or__23143__auto__ = new cljs.core.Keyword(null,"day","day",-274800446).cljs$core$IFn$_invoke$arity$1(selected_date);
if(cljs.core.truth_(or__23143__auto__)){
return or__23143__auto__;
} else {
return today.getDate();
}
})();
var expanded_QMARK_ = reagent.core.atom.call(null,false);
return ((function (fmt,selected_date,selected_month,today,year,month,day,expanded_QMARK_,vec__27329,_,map__27330,map__27330__$1,attrs,id,date_format,inline,auto_close_QMARK_,lang,map__27331,map__27331__$1,doc,get,save_BANG_){
return (function (){
var temp__4655__auto__ = new cljs.core.Keyword(null,"visible?","visible?",2129863715).cljs$core$IFn$_invoke$arity$1(attrs);
if(cljs.core.truth_(temp__4655__auto__)){
var visible__27262__auto__ = temp__4655__auto__;
if(cljs.core.truth_(visible__27262__auto__.call(null,cljs.core.deref.call(null,doc)))){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.datepicker-wrapper","div.datepicker-wrapper",2036556212),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.input-group.date","div.input-group.date",-987970676),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"input.form-control","input.form-control",-1123419636),cljs.core.merge.call(null,new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword(null,"read-only","read-only",-191706886),true,new cljs.core.Keyword(null,"type","type",1174270348),new cljs.core.Keyword(null,"text","text",-1790561697),new cljs.core.Keyword(null,"on-click","on-click",1632826543),((function (visible__27262__auto__,temp__4655__auto__,fmt,selected_date,selected_month,today,year,month,day,expanded_QMARK_,vec__27329,_,map__27330,map__27330__$1,attrs,id,date_format,inline,auto_close_QMARK_,lang,map__27331,map__27331__$1,doc,get,save_BANG_){
return (function (){
return cljs.core.swap_BANG_.call(null,expanded_QMARK_,cljs.core.not);
});})(visible__27262__auto__,temp__4655__auto__,fmt,selected_date,selected_month,today,year,month,day,expanded_QMARK_,vec__27329,_,map__27330,map__27330__$1,attrs,id,date_format,inline,auto_close_QMARK_,lang,map__27331,map__27331__$1,doc,get,save_BANG_))
,new cljs.core.Keyword(null,"value","value",305978217),(function (){var temp__4657__auto__ = get.call(null,id);
if(cljs.core.truth_(temp__4657__auto__)){
var date = temp__4657__auto__;
return reagent_forms.datepicker.format_date.call(null,date,fmt);
} else {
return null;
}
})()], null),attrs)], null),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"span.input-group-addon","span.input-group-addon",-1300181023),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"on-click","on-click",1632826543),((function (visible__27262__auto__,temp__4655__auto__,fmt,selected_date,selected_month,today,year,month,day,expanded_QMARK_,vec__27329,_,map__27330,map__27330__$1,attrs,id,date_format,inline,auto_close_QMARK_,lang,map__27331,map__27331__$1,doc,get,save_BANG_){
return (function (){
return cljs.core.swap_BANG_.call(null,expanded_QMARK_,cljs.core.not);
});})(visible__27262__auto__,temp__4655__auto__,fmt,selected_date,selected_month,today,year,month,day,expanded_QMARK_,vec__27329,_,map__27330,map__27330__$1,attrs,id,date_format,inline,auto_close_QMARK_,lang,map__27331,map__27331__$1,doc,get,save_BANG_))
], null),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"i.glyphicon.glyphicon-calendar","i.glyphicon.glyphicon-calendar",-1048928069)], null)], null)], null),new cljs.core.PersistentVector(null, 10, 5, cljs.core.PersistentVector.EMPTY_NODE, [reagent_forms.datepicker.datepicker,year,month,day,expanded_QMARK_,auto_close_QMARK_,((function (visible__27262__auto__,temp__4655__auto__,fmt,selected_date,selected_month,today,year,month,day,expanded_QMARK_,vec__27329,_,map__27330,map__27330__$1,attrs,id,date_format,inline,auto_close_QMARK_,lang,map__27331,map__27331__$1,doc,get,save_BANG_){
return (function (){
return get.call(null,id);
});})(visible__27262__auto__,temp__4655__auto__,fmt,selected_date,selected_month,today,year,month,day,expanded_QMARK_,vec__27329,_,map__27330,map__27330__$1,attrs,id,date_format,inline,auto_close_QMARK_,lang,map__27331,map__27331__$1,doc,get,save_BANG_))
,((function (visible__27262__auto__,temp__4655__auto__,fmt,selected_date,selected_month,today,year,month,day,expanded_QMARK_,vec__27329,_,map__27330,map__27330__$1,attrs,id,date_format,inline,auto_close_QMARK_,lang,map__27331,map__27331__$1,doc,get,save_BANG_){
return (function (p1__27326_SHARP_){
return save_BANG_.call(null,id,p1__27326_SHARP_);
});})(visible__27262__auto__,temp__4655__auto__,fmt,selected_date,selected_month,today,year,month,day,expanded_QMARK_,vec__27329,_,map__27330,map__27330__$1,attrs,id,date_format,inline,auto_close_QMARK_,lang,map__27331,map__27331__$1,doc,get,save_BANG_))
,inline,lang], null)], null);
} else {
return null;
}
} else {
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.datepicker-wrapper","div.datepicker-wrapper",2036556212),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.input-group.date","div.input-group.date",-987970676),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"input.form-control","input.form-control",-1123419636),cljs.core.merge.call(null,new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword(null,"read-only","read-only",-191706886),true,new cljs.core.Keyword(null,"type","type",1174270348),new cljs.core.Keyword(null,"text","text",-1790561697),new cljs.core.Keyword(null,"on-click","on-click",1632826543),((function (temp__4655__auto__,fmt,selected_date,selected_month,today,year,month,day,expanded_QMARK_,vec__27329,_,map__27330,map__27330__$1,attrs,id,date_format,inline,auto_close_QMARK_,lang,map__27331,map__27331__$1,doc,get,save_BANG_){
return (function (){
return cljs.core.swap_BANG_.call(null,expanded_QMARK_,cljs.core.not);
});})(temp__4655__auto__,fmt,selected_date,selected_month,today,year,month,day,expanded_QMARK_,vec__27329,_,map__27330,map__27330__$1,attrs,id,date_format,inline,auto_close_QMARK_,lang,map__27331,map__27331__$1,doc,get,save_BANG_))
,new cljs.core.Keyword(null,"value","value",305978217),(function (){var temp__4657__auto__ = get.call(null,id);
if(cljs.core.truth_(temp__4657__auto__)){
var date = temp__4657__auto__;
return reagent_forms.datepicker.format_date.call(null,date,fmt);
} else {
return null;
}
})()], null),attrs)], null),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"span.input-group-addon","span.input-group-addon",-1300181023),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"on-click","on-click",1632826543),((function (temp__4655__auto__,fmt,selected_date,selected_month,today,year,month,day,expanded_QMARK_,vec__27329,_,map__27330,map__27330__$1,attrs,id,date_format,inline,auto_close_QMARK_,lang,map__27331,map__27331__$1,doc,get,save_BANG_){
return (function (){
return cljs.core.swap_BANG_.call(null,expanded_QMARK_,cljs.core.not);
});})(temp__4655__auto__,fmt,selected_date,selected_month,today,year,month,day,expanded_QMARK_,vec__27329,_,map__27330,map__27330__$1,attrs,id,date_format,inline,auto_close_QMARK_,lang,map__27331,map__27331__$1,doc,get,save_BANG_))
], null),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"i.glyphicon.glyphicon-calendar","i.glyphicon.glyphicon-calendar",-1048928069)], null)], null)], null),new cljs.core.PersistentVector(null, 10, 5, cljs.core.PersistentVector.EMPTY_NODE, [reagent_forms.datepicker.datepicker,year,month,day,expanded_QMARK_,auto_close_QMARK_,((function (temp__4655__auto__,fmt,selected_date,selected_month,today,year,month,day,expanded_QMARK_,vec__27329,_,map__27330,map__27330__$1,attrs,id,date_format,inline,auto_close_QMARK_,lang,map__27331,map__27331__$1,doc,get,save_BANG_){
return (function (){
return get.call(null,id);
});})(temp__4655__auto__,fmt,selected_date,selected_month,today,year,month,day,expanded_QMARK_,vec__27329,_,map__27330,map__27330__$1,attrs,id,date_format,inline,auto_close_QMARK_,lang,map__27331,map__27331__$1,doc,get,save_BANG_))
,((function (temp__4655__auto__,fmt,selected_date,selected_month,today,year,month,day,expanded_QMARK_,vec__27329,_,map__27330,map__27330__$1,attrs,id,date_format,inline,auto_close_QMARK_,lang,map__27331,map__27331__$1,doc,get,save_BANG_){
return (function (p1__27326_SHARP_){
return save_BANG_.call(null,id,p1__27326_SHARP_);
});})(temp__4655__auto__,fmt,selected_date,selected_month,today,year,month,day,expanded_QMARK_,vec__27329,_,map__27330,map__27330__$1,attrs,id,date_format,inline,auto_close_QMARK_,lang,map__27331,map__27331__$1,doc,get,save_BANG_))
,inline,lang], null)], null);
}
});
;})(fmt,selected_date,selected_month,today,year,month,day,expanded_QMARK_,vec__27329,_,map__27330,map__27330__$1,attrs,id,date_format,inline,auto_close_QMARK_,lang,map__27331,map__27331__$1,doc,get,save_BANG_))
}));
cljs.core._add_method.call(null,reagent_forms.core.init_field,new cljs.core.Keyword(null,"checkbox","checkbox",1612615655),(function (p__27334,p__27335){
var vec__27336 = p__27334;
var _ = cljs.core.nth.call(null,vec__27336,(0),null);
var map__27337 = cljs.core.nth.call(null,vec__27336,(1),null);
var map__27337__$1 = ((((!((map__27337 == null)))?((((map__27337.cljs$lang$protocol_mask$partition0$ & (64))) || (map__27337.cljs$core$ISeq$))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__27337):map__27337);
var attrs = map__27337__$1;
var id = cljs.core.get.call(null,map__27337__$1,new cljs.core.Keyword(null,"id","id",-1388402092));
var field = cljs.core.get.call(null,map__27337__$1,new cljs.core.Keyword(null,"field","field",-1302436500));
var checked = cljs.core.get.call(null,map__27337__$1,new cljs.core.Keyword(null,"checked","checked",-50955819));
var default_checked = cljs.core.get.call(null,map__27337__$1,new cljs.core.Keyword(null,"default-checked","default-checked",1039965863));
var component = vec__27336;
var map__27338 = p__27335;
var map__27338__$1 = ((((!((map__27338 == null)))?((((map__27338.cljs$lang$protocol_mask$partition0$ & (64))) || (map__27338.cljs$core$ISeq$))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__27338):map__27338);
var opts = map__27338__$1;
var doc = cljs.core.get.call(null,map__27338__$1,new cljs.core.Keyword(null,"doc","doc",1913296891));
var get = cljs.core.get.call(null,map__27338__$1,new cljs.core.Keyword(null,"get","get",1683182755));
var save_BANG_ = cljs.core.get.call(null,map__27338__$1,new cljs.core.Keyword(null,"save!","save!",-1137373803));
if(cljs.core.truth_((function (){var or__23143__auto__ = checked;
if(cljs.core.truth_(or__23143__auto__)){
return or__23143__auto__;
} else {
return default_checked;
}
})())){
save_BANG_.call(null,id,true);
} else {
}

return ((function (vec__27336,_,map__27337,map__27337__$1,attrs,id,field,checked,default_checked,component,map__27338,map__27338__$1,opts,doc,get,save_BANG_){
return (function (){
var temp__4655__auto__ = new cljs.core.Keyword(null,"visible?","visible?",2129863715).cljs$core$IFn$_invoke$arity$1(attrs);
if(cljs.core.truth_(temp__4655__auto__)){
var visible__27262__auto__ = temp__4655__auto__;
if(cljs.core.truth_(visible__27262__auto__.call(null,cljs.core.deref.call(null,doc)))){
return reagent_forms.core.set_attrs.call(null,component,opts,new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"type","type",1174270348),field], null));
} else {
return null;
}
} else {
return reagent_forms.core.set_attrs.call(null,component,opts,new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"type","type",1174270348),field], null));
}
});
;})(vec__27336,_,map__27337,map__27337__$1,attrs,id,field,checked,default_checked,component,map__27338,map__27338__$1,opts,doc,get,save_BANG_))
}));
cljs.core._add_method.call(null,reagent_forms.core.init_field,new cljs.core.Keyword(null,"label","label",1718410804),(function (p__27341,p__27342){
var vec__27343 = p__27341;
var type = cljs.core.nth.call(null,vec__27343,(0),null);
var map__27344 = cljs.core.nth.call(null,vec__27343,(1),null);
var map__27344__$1 = ((((!((map__27344 == null)))?((((map__27344.cljs$lang$protocol_mask$partition0$ & (64))) || (map__27344.cljs$core$ISeq$))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__27344):map__27344);
var attrs = map__27344__$1;
var id = cljs.core.get.call(null,map__27344__$1,new cljs.core.Keyword(null,"id","id",-1388402092));
var preamble = cljs.core.get.call(null,map__27344__$1,new cljs.core.Keyword(null,"preamble","preamble",1641040241));
var postamble = cljs.core.get.call(null,map__27344__$1,new cljs.core.Keyword(null,"postamble","postamble",-500033366));
var placeholder = cljs.core.get.call(null,map__27344__$1,new cljs.core.Keyword(null,"placeholder","placeholder",-104873083));
var map__27345 = p__27342;
var map__27345__$1 = ((((!((map__27345 == null)))?((((map__27345.cljs$lang$protocol_mask$partition0$ & (64))) || (map__27345.cljs$core$ISeq$))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__27345):map__27345);
var doc = cljs.core.get.call(null,map__27345__$1,new cljs.core.Keyword(null,"doc","doc",1913296891));
var get = cljs.core.get.call(null,map__27345__$1,new cljs.core.Keyword(null,"get","get",1683182755));
return ((function (vec__27343,type,map__27344,map__27344__$1,attrs,id,preamble,postamble,placeholder,map__27345,map__27345__$1,doc,get){
return (function (){
var temp__4655__auto__ = new cljs.core.Keyword(null,"visible?","visible?",2129863715).cljs$core$IFn$_invoke$arity$1(attrs);
if(cljs.core.truth_(temp__4655__auto__)){
var visible__27262__auto__ = temp__4655__auto__;
if(cljs.core.truth_(visible__27262__auto__.call(null,cljs.core.deref.call(null,doc)))){
return new cljs.core.PersistentVector(null, 4, 5, cljs.core.PersistentVector.EMPTY_NODE, [type,attrs,preamble,(function (){var temp__4655__auto____$1 = get.call(null,id);
if(cljs.core.truth_(temp__4655__auto____$1)){
var value = temp__4655__auto____$1;
return [cljs.core.str(value),cljs.core.str(postamble)].join('');
} else {
return placeholder;
}
})()], null);
} else {
return null;
}
} else {
return new cljs.core.PersistentVector(null, 4, 5, cljs.core.PersistentVector.EMPTY_NODE, [type,attrs,preamble,(function (){var temp__4655__auto____$1 = get.call(null,id);
if(cljs.core.truth_(temp__4655__auto____$1)){
var value = temp__4655__auto____$1;
return [cljs.core.str(value),cljs.core.str(postamble)].join('');
} else {
return placeholder;
}
})()], null);
}
});
;})(vec__27343,type,map__27344,map__27344__$1,attrs,id,preamble,postamble,placeholder,map__27345,map__27345__$1,doc,get))
}));
cljs.core._add_method.call(null,reagent_forms.core.init_field,new cljs.core.Keyword(null,"alert","alert",-571950580),(function (p__27348,p__27349){
var vec__27350 = p__27348;
var type = cljs.core.nth.call(null,vec__27350,(0),null);
var map__27351 = cljs.core.nth.call(null,vec__27350,(1),null);
var map__27351__$1 = ((((!((map__27351 == null)))?((((map__27351.cljs$lang$protocol_mask$partition0$ & (64))) || (map__27351.cljs$core$ISeq$))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__27351):map__27351);
var attrs = map__27351__$1;
var id = cljs.core.get.call(null,map__27351__$1,new cljs.core.Keyword(null,"id","id",-1388402092));
var event = cljs.core.get.call(null,map__27351__$1,new cljs.core.Keyword(null,"event","event",301435442));
var touch_event = cljs.core.get.call(null,map__27351__$1,new cljs.core.Keyword(null,"touch-event","touch-event",-1071581783));
var body = cljs.core.nthnext.call(null,vec__27350,(2));
var map__27352 = p__27349;
var map__27352__$1 = ((((!((map__27352 == null)))?((((map__27352.cljs$lang$protocol_mask$partition0$ & (64))) || (map__27352.cljs$core$ISeq$))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__27352):map__27352);
var opts = map__27352__$1;
var doc = cljs.core.get.call(null,map__27352__$1,new cljs.core.Keyword(null,"doc","doc",1913296891));
var get = cljs.core.get.call(null,map__27352__$1,new cljs.core.Keyword(null,"get","get",1683182755));
var save_BANG_ = cljs.core.get.call(null,map__27352__$1,new cljs.core.Keyword(null,"save!","save!",-1137373803));
return ((function (vec__27350,type,map__27351,map__27351__$1,attrs,id,event,touch_event,body,map__27352,map__27352__$1,opts,doc,get,save_BANG_){
return (function (){
var temp__4655__auto__ = new cljs.core.Keyword(null,"visible?","visible?",2129863715).cljs$core$IFn$_invoke$arity$1(attrs);
if(cljs.core.truth_(temp__4655__auto__)){
var visible__27262__auto__ = temp__4655__auto__;
if(cljs.core.truth_(visible__27262__auto__.call(null,cljs.core.deref.call(null,doc)))){
if(cljs.core.truth_(event)){
if(cljs.core.truth_(event.call(null,get.call(null,id)))){
return cljs.core.into.call(null,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [type,cljs.core.dissoc.call(null,attrs,event)], null),body);
} else {
return null;
}
} else {
var temp__4655__auto____$1 = cljs.core.not_empty.call(null,get.call(null,id));
if(cljs.core.truth_(temp__4655__auto____$1)){
var message = temp__4655__auto____$1;
return new cljs.core.PersistentVector(null, 4, 5, cljs.core.PersistentVector.EMPTY_NODE, [type,attrs,new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"button.close","button.close",-1545560743),cljs.core.PersistentArrayMap.fromArray([new cljs.core.Keyword(null,"type","type",1174270348),"button",new cljs.core.Keyword(null,"aria-hidden","aria-hidden",399337029),true,(function (){var or__23143__auto__ = touch_event;
if(cljs.core.truth_(or__23143__auto__)){
return or__23143__auto__;
} else {
return new cljs.core.Keyword(null,"on-click","on-click",1632826543);
}
})(),((function (message,temp__4655__auto____$1,visible__27262__auto__,temp__4655__auto__,vec__27350,type,map__27351,map__27351__$1,attrs,id,event,touch_event,body,map__27352,map__27352__$1,opts,doc,get,save_BANG_){
return (function (){
return save_BANG_.call(null,id,null);
});})(message,temp__4655__auto____$1,visible__27262__auto__,temp__4655__auto__,vec__27350,type,map__27351,map__27351__$1,attrs,id,event,touch_event,body,map__27352,map__27352__$1,opts,doc,get,save_BANG_))
], true, false),"X"], null),message], null);
} else {
return null;
}
}
} else {
return null;
}
} else {
if(cljs.core.truth_(event)){
if(cljs.core.truth_(event.call(null,get.call(null,id)))){
return cljs.core.into.call(null,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [type,cljs.core.dissoc.call(null,attrs,event)], null),body);
} else {
return null;
}
} else {
var temp__4655__auto____$1 = cljs.core.not_empty.call(null,get.call(null,id));
if(cljs.core.truth_(temp__4655__auto____$1)){
var message = temp__4655__auto____$1;
return new cljs.core.PersistentVector(null, 4, 5, cljs.core.PersistentVector.EMPTY_NODE, [type,attrs,new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"button.close","button.close",-1545560743),cljs.core.PersistentArrayMap.fromArray([new cljs.core.Keyword(null,"type","type",1174270348),"button",new cljs.core.Keyword(null,"aria-hidden","aria-hidden",399337029),true,(function (){var or__23143__auto__ = touch_event;
if(cljs.core.truth_(or__23143__auto__)){
return or__23143__auto__;
} else {
return new cljs.core.Keyword(null,"on-click","on-click",1632826543);
}
})(),((function (message,temp__4655__auto____$1,temp__4655__auto__,vec__27350,type,map__27351,map__27351__$1,attrs,id,event,touch_event,body,map__27352,map__27352__$1,opts,doc,get,save_BANG_){
return (function (){
return save_BANG_.call(null,id,null);
});})(message,temp__4655__auto____$1,temp__4655__auto__,vec__27350,type,map__27351,map__27351__$1,attrs,id,event,touch_event,body,map__27352,map__27352__$1,opts,doc,get,save_BANG_))
], true, false),"X"], null),message], null);
} else {
return null;
}
}
}
});
;})(vec__27350,type,map__27351,map__27351__$1,attrs,id,event,touch_event,body,map__27352,map__27352__$1,opts,doc,get,save_BANG_))
}));
cljs.core._add_method.call(null,reagent_forms.core.init_field,new cljs.core.Keyword(null,"radio","radio",1323726374),(function (p__27355,p__27356){
var vec__27357 = p__27355;
var type = cljs.core.nth.call(null,vec__27357,(0),null);
var map__27358 = cljs.core.nth.call(null,vec__27357,(1),null);
var map__27358__$1 = ((((!((map__27358 == null)))?((((map__27358.cljs$lang$protocol_mask$partition0$ & (64))) || (map__27358.cljs$core$ISeq$))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__27358):map__27358);
var attrs = map__27358__$1;
var field = cljs.core.get.call(null,map__27358__$1,new cljs.core.Keyword(null,"field","field",-1302436500));
var name = cljs.core.get.call(null,map__27358__$1,new cljs.core.Keyword(null,"name","name",1843675177));
var value = cljs.core.get.call(null,map__27358__$1,new cljs.core.Keyword(null,"value","value",305978217));
var checked = cljs.core.get.call(null,map__27358__$1,new cljs.core.Keyword(null,"checked","checked",-50955819));
var default_checked = cljs.core.get.call(null,map__27358__$1,new cljs.core.Keyword(null,"default-checked","default-checked",1039965863));
var body = cljs.core.nthnext.call(null,vec__27357,(2));
var map__27359 = p__27356;
var map__27359__$1 = ((((!((map__27359 == null)))?((((map__27359.cljs$lang$protocol_mask$partition0$ & (64))) || (map__27359.cljs$core$ISeq$))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__27359):map__27359);
var doc = cljs.core.get.call(null,map__27359__$1,new cljs.core.Keyword(null,"doc","doc",1913296891));
var get = cljs.core.get.call(null,map__27359__$1,new cljs.core.Keyword(null,"get","get",1683182755));
var save_BANG_ = cljs.core.get.call(null,map__27359__$1,new cljs.core.Keyword(null,"save!","save!",-1137373803));
if(cljs.core.truth_((function (){var or__23143__auto__ = checked;
if(cljs.core.truth_(or__23143__auto__)){
return or__23143__auto__;
} else {
return default_checked;
}
})())){
save_BANG_.call(null,name,value);
} else {
}

return ((function (vec__27357,type,map__27358,map__27358__$1,attrs,field,name,value,checked,default_checked,body,map__27359,map__27359__$1,doc,get,save_BANG_){
return (function (){
var temp__4655__auto__ = new cljs.core.Keyword(null,"visible?","visible?",2129863715).cljs$core$IFn$_invoke$arity$1(attrs);
if(cljs.core.truth_(temp__4655__auto__)){
var visible__27262__auto__ = temp__4655__auto__;
if(cljs.core.truth_(visible__27262__auto__.call(null,cljs.core.deref.call(null,doc)))){
return cljs.core.into.call(null,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [type,cljs.core.merge.call(null,new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"type","type",1174270348),new cljs.core.Keyword(null,"radio","radio",1323726374),new cljs.core.Keyword(null,"default-checked","default-checked",1039965863),cljs.core._EQ_.call(null,value,get.call(null,name)),new cljs.core.Keyword(null,"on-change","on-change",-732046149),((function (visible__27262__auto__,temp__4655__auto__,vec__27357,type,map__27358,map__27358__$1,attrs,field,name,value,checked,default_checked,body,map__27359,map__27359__$1,doc,get,save_BANG_){
return (function (){
return save_BANG_.call(null,name,value);
});})(visible__27262__auto__,temp__4655__auto__,vec__27357,type,map__27358,map__27358__$1,attrs,field,name,value,checked,default_checked,body,map__27359,map__27359__$1,doc,get,save_BANG_))
], null),attrs)], null),body);
} else {
return null;
}
} else {
return cljs.core.into.call(null,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [type,cljs.core.merge.call(null,new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"type","type",1174270348),new cljs.core.Keyword(null,"radio","radio",1323726374),new cljs.core.Keyword(null,"default-checked","default-checked",1039965863),cljs.core._EQ_.call(null,value,get.call(null,name)),new cljs.core.Keyword(null,"on-change","on-change",-732046149),((function (temp__4655__auto__,vec__27357,type,map__27358,map__27358__$1,attrs,field,name,value,checked,default_checked,body,map__27359,map__27359__$1,doc,get,save_BANG_){
return (function (){
return save_BANG_.call(null,name,value);
});})(temp__4655__auto__,vec__27357,type,map__27358,map__27358__$1,attrs,field,name,value,checked,default_checked,body,map__27359,map__27359__$1,doc,get,save_BANG_))
], null),attrs)], null),body);
}
});
;})(vec__27357,type,map__27358,map__27358__$1,attrs,field,name,value,checked,default_checked,body,map__27359,map__27359__$1,doc,get,save_BANG_))
}));
cljs.core._add_method.call(null,reagent_forms.core.init_field,new cljs.core.Keyword(null,"typeahead","typeahead",-1364611797),(function (p__27365,p__27366){
var vec__27367 = p__27365;
var type = cljs.core.nth.call(null,vec__27367,(0),null);
var map__27368 = cljs.core.nth.call(null,vec__27367,(1),null);
var map__27368__$1 = ((((!((map__27368 == null)))?((((map__27368.cljs$lang$protocol_mask$partition0$ & (64))) || (map__27368.cljs$core$ISeq$))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__27368):map__27368);
var attrs = map__27368__$1;
var result_fn = cljs.core.get.call(null,map__27368__$1,new cljs.core.Keyword(null,"result-fn","result-fn",-726333573),cljs.core.identity);
var item_class = cljs.core.get.call(null,map__27368__$1,new cljs.core.Keyword(null,"item-class","item-class",1277553858));
var input_placeholder = cljs.core.get.call(null,map__27368__$1,new cljs.core.Keyword(null,"input-placeholder","input-placeholder",-965612860));
var highlight_class = cljs.core.get.call(null,map__27368__$1,new cljs.core.Keyword(null,"highlight-class","highlight-class",1738202282));
var list_class = cljs.core.get.call(null,map__27368__$1,new cljs.core.Keyword(null,"list-class","list-class",1412758252));
var data_source = cljs.core.get.call(null,map__27368__$1,new cljs.core.Keyword(null,"data-source","data-source",-658934676));
var input_class = cljs.core.get.call(null,map__27368__$1,new cljs.core.Keyword(null,"input-class","input-class",-62053138));
var clear_on_focus_QMARK_ = cljs.core.get.call(null,map__27368__$1,new cljs.core.Keyword(null,"clear-on-focus?","clear-on-focus?",330213424),true);
var id = cljs.core.get.call(null,map__27368__$1,new cljs.core.Keyword(null,"id","id",-1388402092));
var choice_fn = cljs.core.get.call(null,map__27368__$1,new cljs.core.Keyword(null,"choice-fn","choice-fn",-1053191627),cljs.core.identity);
var map__27369 = p__27366;
var map__27369__$1 = ((((!((map__27369 == null)))?((((map__27369.cljs$lang$protocol_mask$partition0$ & (64))) || (map__27369.cljs$core$ISeq$))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__27369):map__27369);
var doc = cljs.core.get.call(null,map__27369__$1,new cljs.core.Keyword(null,"doc","doc",1913296891));
var get = cljs.core.get.call(null,map__27369__$1,new cljs.core.Keyword(null,"get","get",1683182755));
var save_BANG_ = cljs.core.get.call(null,map__27369__$1,new cljs.core.Keyword(null,"save!","save!",-1137373803));
var typeahead_hidden_QMARK_ = reagent.core.atom.call(null,true);
var mouse_on_list_QMARK_ = reagent.core.atom.call(null,false);
var selected_index = reagent.core.atom.call(null,(-1));
var selections = reagent.core.atom.call(null,cljs.core.PersistentVector.EMPTY);
var choose_selected = ((function (typeahead_hidden_QMARK_,mouse_on_list_QMARK_,selected_index,selections,vec__27367,type,map__27368,map__27368__$1,attrs,result_fn,item_class,input_placeholder,highlight_class,list_class,data_source,input_class,clear_on_focus_QMARK_,id,choice_fn,map__27369,map__27369__$1,doc,get,save_BANG_){
return (function (){
if(cljs.core.truth_((function (){var and__23131__auto__ = cljs.core.not_empty.call(null,cljs.core.deref.call(null,selections));
if(cljs.core.truth_(and__23131__auto__)){
return (cljs.core.deref.call(null,selected_index) > (-1));
} else {
return and__23131__auto__;
}
})())){
var choice = cljs.core.nth.call(null,cljs.core.deref.call(null,selections),cljs.core.deref.call(null,selected_index));
save_BANG_.call(null,id,choice);

choice_fn.call(null,choice);

return cljs.core.reset_BANG_.call(null,typeahead_hidden_QMARK_,true);
} else {
return null;
}
});})(typeahead_hidden_QMARK_,mouse_on_list_QMARK_,selected_index,selections,vec__27367,type,map__27368,map__27368__$1,attrs,result_fn,item_class,input_placeholder,highlight_class,list_class,data_source,input_class,clear_on_focus_QMARK_,id,choice_fn,map__27369,map__27369__$1,doc,get,save_BANG_))
;
return ((function (typeahead_hidden_QMARK_,mouse_on_list_QMARK_,selected_index,selections,choose_selected,vec__27367,type,map__27368,map__27368__$1,attrs,result_fn,item_class,input_placeholder,highlight_class,list_class,data_source,input_class,clear_on_focus_QMARK_,id,choice_fn,map__27369,map__27369__$1,doc,get,save_BANG_){
return (function (){
var temp__4655__auto__ = new cljs.core.Keyword(null,"visible?","visible?",2129863715).cljs$core$IFn$_invoke$arity$1(attrs);
if(cljs.core.truth_(temp__4655__auto__)){
var visible__27262__auto__ = temp__4655__auto__;
if(cljs.core.truth_(visible__27262__auto__.call(null,cljs.core.deref.call(null,doc)))){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [type,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"input","input",556931961),new cljs.core.PersistentArrayMap(null, 8, [new cljs.core.Keyword(null,"type","type",1174270348),new cljs.core.Keyword(null,"text","text",-1790561697),new cljs.core.Keyword(null,"placeholder","placeholder",-104873083),input_placeholder,new cljs.core.Keyword(null,"class","class",-2030961996),input_class,new cljs.core.Keyword(null,"value","value",305978217),(function (){var v = get.call(null,id);
if(cljs.core.not.call(null,cljs.core.iterable_QMARK_.call(null,v))){
return v;
} else {
return cljs.core.first.call(null,v);
}
})(),new cljs.core.Keyword(null,"on-focus","on-focus",-13737624),((function (visible__27262__auto__,temp__4655__auto__,typeahead_hidden_QMARK_,mouse_on_list_QMARK_,selected_index,selections,choose_selected,vec__27367,type,map__27368,map__27368__$1,attrs,result_fn,item_class,input_placeholder,highlight_class,list_class,data_source,input_class,clear_on_focus_QMARK_,id,choice_fn,map__27369,map__27369__$1,doc,get,save_BANG_){
return (function (){
if(cljs.core.truth_(clear_on_focus_QMARK_)){
return save_BANG_.call(null,id,null);
} else {
return null;
}
});})(visible__27262__auto__,temp__4655__auto__,typeahead_hidden_QMARK_,mouse_on_list_QMARK_,selected_index,selections,choose_selected,vec__27367,type,map__27368,map__27368__$1,attrs,result_fn,item_class,input_placeholder,highlight_class,list_class,data_source,input_class,clear_on_focus_QMARK_,id,choice_fn,map__27369,map__27369__$1,doc,get,save_BANG_))
,new cljs.core.Keyword(null,"on-blur","on-blur",814300747),((function (visible__27262__auto__,temp__4655__auto__,typeahead_hidden_QMARK_,mouse_on_list_QMARK_,selected_index,selections,choose_selected,vec__27367,type,map__27368,map__27368__$1,attrs,result_fn,item_class,input_placeholder,highlight_class,list_class,data_source,input_class,clear_on_focus_QMARK_,id,choice_fn,map__27369,map__27369__$1,doc,get,save_BANG_){
return (function (){
if(cljs.core.truth_(cljs.core.deref.call(null,mouse_on_list_QMARK_))){
return null;
} else {
cljs.core.reset_BANG_.call(null,typeahead_hidden_QMARK_,true);

return cljs.core.reset_BANG_.call(null,selected_index,(-1));
}
});})(visible__27262__auto__,temp__4655__auto__,typeahead_hidden_QMARK_,mouse_on_list_QMARK_,selected_index,selections,choose_selected,vec__27367,type,map__27368,map__27368__$1,attrs,result_fn,item_class,input_placeholder,highlight_class,list_class,data_source,input_class,clear_on_focus_QMARK_,id,choice_fn,map__27369,map__27369__$1,doc,get,save_BANG_))
,new cljs.core.Keyword(null,"on-change","on-change",-732046149),((function (visible__27262__auto__,temp__4655__auto__,typeahead_hidden_QMARK_,mouse_on_list_QMARK_,selected_index,selections,choose_selected,vec__27367,type,map__27368,map__27368__$1,attrs,result_fn,item_class,input_placeholder,highlight_class,list_class,data_source,input_class,clear_on_focus_QMARK_,id,choice_fn,map__27369,map__27369__$1,doc,get,save_BANG_){
return (function (p1__27362_SHARP_){
var temp__4657__auto__ = clojure.string.trim.call(null,reagent_forms.core.value_of.call(null,p1__27362_SHARP_));
if(cljs.core.truth_(temp__4657__auto__)){
var value = temp__4657__auto__;
cljs.core.reset_BANG_.call(null,selections,data_source.call(null,value.toLowerCase()));

save_BANG_.call(null,id,reagent_forms.core.value_of.call(null,p1__27362_SHARP_));

cljs.core.reset_BANG_.call(null,typeahead_hidden_QMARK_,false);

return cljs.core.reset_BANG_.call(null,selected_index,(-1));
} else {
return null;
}
});})(visible__27262__auto__,temp__4655__auto__,typeahead_hidden_QMARK_,mouse_on_list_QMARK_,selected_index,selections,choose_selected,vec__27367,type,map__27368,map__27368__$1,attrs,result_fn,item_class,input_placeholder,highlight_class,list_class,data_source,input_class,clear_on_focus_QMARK_,id,choice_fn,map__27369,map__27369__$1,doc,get,save_BANG_))
,new cljs.core.Keyword(null,"on-key-down","on-key-down",-1374733765),((function (visible__27262__auto__,temp__4655__auto__,typeahead_hidden_QMARK_,mouse_on_list_QMARK_,selected_index,selections,choose_selected,vec__27367,type,map__27368,map__27368__$1,attrs,result_fn,item_class,input_placeholder,highlight_class,list_class,data_source,input_class,clear_on_focus_QMARK_,id,choice_fn,map__27369,map__27369__$1,doc,get,save_BANG_){
return (function (p1__27363_SHARP_){
var G__27372 = p1__27363_SHARP_.which;
switch (G__27372) {
case (38):
p1__27363_SHARP_.preventDefault();

if(cljs.core._EQ_.call(null,cljs.core.deref.call(null,selected_index),(0))){
return null;
} else {
return cljs.core.swap_BANG_.call(null,selected_index,cljs.core.dec);
}

break;
case (40):
p1__27363_SHARP_.preventDefault();

if(cljs.core._EQ_.call(null,cljs.core.deref.call(null,selected_index),(cljs.core.count.call(null,cljs.core.deref.call(null,selections)) - (1)))){
return null;
} else {
save_BANG_.call(null,id,reagent_forms.core.value_of.call(null,p1__27363_SHARP_));

return cljs.core.swap_BANG_.call(null,selected_index,cljs.core.inc);
}

break;
case (9):
return choose_selected.call(null);

break;
case (13):
return choose_selected.call(null);

break;
case (27):
cljs.core.reset_BANG_.call(null,typeahead_hidden_QMARK_,true);

return cljs.core.reset_BANG_.call(null,selected_index,(0));

break;
default:
return "default";

}
});})(visible__27262__auto__,temp__4655__auto__,typeahead_hidden_QMARK_,mouse_on_list_QMARK_,selected_index,selections,choose_selected,vec__27367,type,map__27368,map__27368__$1,attrs,result_fn,item_class,input_placeholder,highlight_class,list_class,data_source,input_class,clear_on_focus_QMARK_,id,choice_fn,map__27369,map__27369__$1,doc,get,save_BANG_))
], null)], null),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"ul","ul",-1349521403),new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword(null,"style","style",-496642736),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"display","display",242065432),(cljs.core.truth_((function (){var or__23143__auto__ = cljs.core.empty_QMARK_.call(null,cljs.core.deref.call(null,selections));
if(or__23143__auto__){
return or__23143__auto__;
} else {
return cljs.core.deref.call(null,typeahead_hidden_QMARK_);
}
})())?new cljs.core.Keyword(null,"none","none",1333468478):new cljs.core.Keyword(null,"block","block",664686210))], null),new cljs.core.Keyword(null,"class","class",-2030961996),list_class,new cljs.core.Keyword(null,"on-mouse-enter","on-mouse-enter",-1664921661),((function (visible__27262__auto__,temp__4655__auto__,typeahead_hidden_QMARK_,mouse_on_list_QMARK_,selected_index,selections,choose_selected,vec__27367,type,map__27368,map__27368__$1,attrs,result_fn,item_class,input_placeholder,highlight_class,list_class,data_source,input_class,clear_on_focus_QMARK_,id,choice_fn,map__27369,map__27369__$1,doc,get,save_BANG_){
return (function (){
return cljs.core.reset_BANG_.call(null,mouse_on_list_QMARK_,true);
});})(visible__27262__auto__,temp__4655__auto__,typeahead_hidden_QMARK_,mouse_on_list_QMARK_,selected_index,selections,choose_selected,vec__27367,type,map__27368,map__27368__$1,attrs,result_fn,item_class,input_placeholder,highlight_class,list_class,data_source,input_class,clear_on_focus_QMARK_,id,choice_fn,map__27369,map__27369__$1,doc,get,save_BANG_))
,new cljs.core.Keyword(null,"on-mouse-leave","on-mouse-leave",-1864319528),((function (visible__27262__auto__,temp__4655__auto__,typeahead_hidden_QMARK_,mouse_on_list_QMARK_,selected_index,selections,choose_selected,vec__27367,type,map__27368,map__27368__$1,attrs,result_fn,item_class,input_placeholder,highlight_class,list_class,data_source,input_class,clear_on_focus_QMARK_,id,choice_fn,map__27369,map__27369__$1,doc,get,save_BANG_){
return (function (){
return cljs.core.reset_BANG_.call(null,mouse_on_list_QMARK_,false);
});})(visible__27262__auto__,temp__4655__auto__,typeahead_hidden_QMARK_,mouse_on_list_QMARK_,selected_index,selections,choose_selected,vec__27367,type,map__27368,map__27368__$1,attrs,result_fn,item_class,input_placeholder,highlight_class,list_class,data_source,input_class,clear_on_focus_QMARK_,id,choice_fn,map__27369,map__27369__$1,doc,get,save_BANG_))
], null),cljs.core.doall.call(null,cljs.core.map_indexed.call(null,((function (visible__27262__auto__,temp__4655__auto__,typeahead_hidden_QMARK_,mouse_on_list_QMARK_,selected_index,selections,choose_selected,vec__27367,type,map__27368,map__27368__$1,attrs,result_fn,item_class,input_placeholder,highlight_class,list_class,data_source,input_class,clear_on_focus_QMARK_,id,choice_fn,map__27369,map__27369__$1,doc,get,save_BANG_){
return (function (index,result){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"li","li",723558921),new cljs.core.PersistentArrayMap(null, 5, [new cljs.core.Keyword(null,"tab-index","tab-index",895755393),index,new cljs.core.Keyword(null,"key","key",-1516042587),index,new cljs.core.Keyword(null,"class","class",-2030961996),((cljs.core._EQ_.call(null,cljs.core.deref.call(null,selected_index),index))?highlight_class:item_class),new cljs.core.Keyword(null,"on-mouse-over","on-mouse-over",-858472552),((function (visible__27262__auto__,temp__4655__auto__,typeahead_hidden_QMARK_,mouse_on_list_QMARK_,selected_index,selections,choose_selected,vec__27367,type,map__27368,map__27368__$1,attrs,result_fn,item_class,input_placeholder,highlight_class,list_class,data_source,input_class,clear_on_focus_QMARK_,id,choice_fn,map__27369,map__27369__$1,doc,get,save_BANG_){
return (function (p1__27364_SHARP_){
return cljs.core.reset_BANG_.call(null,selected_index,parseInt(p1__27364_SHARP_.target.getAttribute("tabIndex")));
});})(visible__27262__auto__,temp__4655__auto__,typeahead_hidden_QMARK_,mouse_on_list_QMARK_,selected_index,selections,choose_selected,vec__27367,type,map__27368,map__27368__$1,attrs,result_fn,item_class,input_placeholder,highlight_class,list_class,data_source,input_class,clear_on_focus_QMARK_,id,choice_fn,map__27369,map__27369__$1,doc,get,save_BANG_))
,new cljs.core.Keyword(null,"on-click","on-click",1632826543),((function (visible__27262__auto__,temp__4655__auto__,typeahead_hidden_QMARK_,mouse_on_list_QMARK_,selected_index,selections,choose_selected,vec__27367,type,map__27368,map__27368__$1,attrs,result_fn,item_class,input_placeholder,highlight_class,list_class,data_source,input_class,clear_on_focus_QMARK_,id,choice_fn,map__27369,map__27369__$1,doc,get,save_BANG_){
return (function (){
cljs.core.reset_BANG_.call(null,typeahead_hidden_QMARK_,true);

save_BANG_.call(null,id,result);

return choice_fn.call(null,result);
});})(visible__27262__auto__,temp__4655__auto__,typeahead_hidden_QMARK_,mouse_on_list_QMARK_,selected_index,selections,choose_selected,vec__27367,type,map__27368,map__27368__$1,attrs,result_fn,item_class,input_placeholder,highlight_class,list_class,data_source,input_class,clear_on_focus_QMARK_,id,choice_fn,map__27369,map__27369__$1,doc,get,save_BANG_))
], null),result_fn.call(null,result)], null);
});})(visible__27262__auto__,temp__4655__auto__,typeahead_hidden_QMARK_,mouse_on_list_QMARK_,selected_index,selections,choose_selected,vec__27367,type,map__27368,map__27368__$1,attrs,result_fn,item_class,input_placeholder,highlight_class,list_class,data_source,input_class,clear_on_focus_QMARK_,id,choice_fn,map__27369,map__27369__$1,doc,get,save_BANG_))
,cljs.core.deref.call(null,selections)))], null)], null);
} else {
return null;
}
} else {
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [type,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"input","input",556931961),new cljs.core.PersistentArrayMap(null, 8, [new cljs.core.Keyword(null,"type","type",1174270348),new cljs.core.Keyword(null,"text","text",-1790561697),new cljs.core.Keyword(null,"placeholder","placeholder",-104873083),input_placeholder,new cljs.core.Keyword(null,"class","class",-2030961996),input_class,new cljs.core.Keyword(null,"value","value",305978217),(function (){var v = get.call(null,id);
if(cljs.core.not.call(null,cljs.core.iterable_QMARK_.call(null,v))){
return v;
} else {
return cljs.core.first.call(null,v);
}
})(),new cljs.core.Keyword(null,"on-focus","on-focus",-13737624),((function (temp__4655__auto__,typeahead_hidden_QMARK_,mouse_on_list_QMARK_,selected_index,selections,choose_selected,vec__27367,type,map__27368,map__27368__$1,attrs,result_fn,item_class,input_placeholder,highlight_class,list_class,data_source,input_class,clear_on_focus_QMARK_,id,choice_fn,map__27369,map__27369__$1,doc,get,save_BANG_){
return (function (){
if(cljs.core.truth_(clear_on_focus_QMARK_)){
return save_BANG_.call(null,id,null);
} else {
return null;
}
});})(temp__4655__auto__,typeahead_hidden_QMARK_,mouse_on_list_QMARK_,selected_index,selections,choose_selected,vec__27367,type,map__27368,map__27368__$1,attrs,result_fn,item_class,input_placeholder,highlight_class,list_class,data_source,input_class,clear_on_focus_QMARK_,id,choice_fn,map__27369,map__27369__$1,doc,get,save_BANG_))
,new cljs.core.Keyword(null,"on-blur","on-blur",814300747),((function (temp__4655__auto__,typeahead_hidden_QMARK_,mouse_on_list_QMARK_,selected_index,selections,choose_selected,vec__27367,type,map__27368,map__27368__$1,attrs,result_fn,item_class,input_placeholder,highlight_class,list_class,data_source,input_class,clear_on_focus_QMARK_,id,choice_fn,map__27369,map__27369__$1,doc,get,save_BANG_){
return (function (){
if(cljs.core.truth_(cljs.core.deref.call(null,mouse_on_list_QMARK_))){
return null;
} else {
cljs.core.reset_BANG_.call(null,typeahead_hidden_QMARK_,true);

return cljs.core.reset_BANG_.call(null,selected_index,(-1));
}
});})(temp__4655__auto__,typeahead_hidden_QMARK_,mouse_on_list_QMARK_,selected_index,selections,choose_selected,vec__27367,type,map__27368,map__27368__$1,attrs,result_fn,item_class,input_placeholder,highlight_class,list_class,data_source,input_class,clear_on_focus_QMARK_,id,choice_fn,map__27369,map__27369__$1,doc,get,save_BANG_))
,new cljs.core.Keyword(null,"on-change","on-change",-732046149),((function (temp__4655__auto__,typeahead_hidden_QMARK_,mouse_on_list_QMARK_,selected_index,selections,choose_selected,vec__27367,type,map__27368,map__27368__$1,attrs,result_fn,item_class,input_placeholder,highlight_class,list_class,data_source,input_class,clear_on_focus_QMARK_,id,choice_fn,map__27369,map__27369__$1,doc,get,save_BANG_){
return (function (p1__27362_SHARP_){
var temp__4657__auto__ = clojure.string.trim.call(null,reagent_forms.core.value_of.call(null,p1__27362_SHARP_));
if(cljs.core.truth_(temp__4657__auto__)){
var value = temp__4657__auto__;
cljs.core.reset_BANG_.call(null,selections,data_source.call(null,value.toLowerCase()));

save_BANG_.call(null,id,reagent_forms.core.value_of.call(null,p1__27362_SHARP_));

cljs.core.reset_BANG_.call(null,typeahead_hidden_QMARK_,false);

return cljs.core.reset_BANG_.call(null,selected_index,(-1));
} else {
return null;
}
});})(temp__4655__auto__,typeahead_hidden_QMARK_,mouse_on_list_QMARK_,selected_index,selections,choose_selected,vec__27367,type,map__27368,map__27368__$1,attrs,result_fn,item_class,input_placeholder,highlight_class,list_class,data_source,input_class,clear_on_focus_QMARK_,id,choice_fn,map__27369,map__27369__$1,doc,get,save_BANG_))
,new cljs.core.Keyword(null,"on-key-down","on-key-down",-1374733765),((function (temp__4655__auto__,typeahead_hidden_QMARK_,mouse_on_list_QMARK_,selected_index,selections,choose_selected,vec__27367,type,map__27368,map__27368__$1,attrs,result_fn,item_class,input_placeholder,highlight_class,list_class,data_source,input_class,clear_on_focus_QMARK_,id,choice_fn,map__27369,map__27369__$1,doc,get,save_BANG_){
return (function (p1__27363_SHARP_){
var G__27373 = p1__27363_SHARP_.which;
switch (G__27373) {
case (38):
p1__27363_SHARP_.preventDefault();

if(cljs.core._EQ_.call(null,cljs.core.deref.call(null,selected_index),(0))){
return null;
} else {
return cljs.core.swap_BANG_.call(null,selected_index,cljs.core.dec);
}

break;
case (40):
p1__27363_SHARP_.preventDefault();

if(cljs.core._EQ_.call(null,cljs.core.deref.call(null,selected_index),(cljs.core.count.call(null,cljs.core.deref.call(null,selections)) - (1)))){
return null;
} else {
save_BANG_.call(null,id,reagent_forms.core.value_of.call(null,p1__27363_SHARP_));

return cljs.core.swap_BANG_.call(null,selected_index,cljs.core.inc);
}

break;
case (9):
return choose_selected.call(null);

break;
case (13):
return choose_selected.call(null);

break;
case (27):
cljs.core.reset_BANG_.call(null,typeahead_hidden_QMARK_,true);

return cljs.core.reset_BANG_.call(null,selected_index,(0));

break;
default:
return "default";

}
});})(temp__4655__auto__,typeahead_hidden_QMARK_,mouse_on_list_QMARK_,selected_index,selections,choose_selected,vec__27367,type,map__27368,map__27368__$1,attrs,result_fn,item_class,input_placeholder,highlight_class,list_class,data_source,input_class,clear_on_focus_QMARK_,id,choice_fn,map__27369,map__27369__$1,doc,get,save_BANG_))
], null)], null),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"ul","ul",-1349521403),new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword(null,"style","style",-496642736),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"display","display",242065432),(cljs.core.truth_((function (){var or__23143__auto__ = cljs.core.empty_QMARK_.call(null,cljs.core.deref.call(null,selections));
if(or__23143__auto__){
return or__23143__auto__;
} else {
return cljs.core.deref.call(null,typeahead_hidden_QMARK_);
}
})())?new cljs.core.Keyword(null,"none","none",1333468478):new cljs.core.Keyword(null,"block","block",664686210))], null),new cljs.core.Keyword(null,"class","class",-2030961996),list_class,new cljs.core.Keyword(null,"on-mouse-enter","on-mouse-enter",-1664921661),((function (temp__4655__auto__,typeahead_hidden_QMARK_,mouse_on_list_QMARK_,selected_index,selections,choose_selected,vec__27367,type,map__27368,map__27368__$1,attrs,result_fn,item_class,input_placeholder,highlight_class,list_class,data_source,input_class,clear_on_focus_QMARK_,id,choice_fn,map__27369,map__27369__$1,doc,get,save_BANG_){
return (function (){
return cljs.core.reset_BANG_.call(null,mouse_on_list_QMARK_,true);
});})(temp__4655__auto__,typeahead_hidden_QMARK_,mouse_on_list_QMARK_,selected_index,selections,choose_selected,vec__27367,type,map__27368,map__27368__$1,attrs,result_fn,item_class,input_placeholder,highlight_class,list_class,data_source,input_class,clear_on_focus_QMARK_,id,choice_fn,map__27369,map__27369__$1,doc,get,save_BANG_))
,new cljs.core.Keyword(null,"on-mouse-leave","on-mouse-leave",-1864319528),((function (temp__4655__auto__,typeahead_hidden_QMARK_,mouse_on_list_QMARK_,selected_index,selections,choose_selected,vec__27367,type,map__27368,map__27368__$1,attrs,result_fn,item_class,input_placeholder,highlight_class,list_class,data_source,input_class,clear_on_focus_QMARK_,id,choice_fn,map__27369,map__27369__$1,doc,get,save_BANG_){
return (function (){
return cljs.core.reset_BANG_.call(null,mouse_on_list_QMARK_,false);
});})(temp__4655__auto__,typeahead_hidden_QMARK_,mouse_on_list_QMARK_,selected_index,selections,choose_selected,vec__27367,type,map__27368,map__27368__$1,attrs,result_fn,item_class,input_placeholder,highlight_class,list_class,data_source,input_class,clear_on_focus_QMARK_,id,choice_fn,map__27369,map__27369__$1,doc,get,save_BANG_))
], null),cljs.core.doall.call(null,cljs.core.map_indexed.call(null,((function (temp__4655__auto__,typeahead_hidden_QMARK_,mouse_on_list_QMARK_,selected_index,selections,choose_selected,vec__27367,type,map__27368,map__27368__$1,attrs,result_fn,item_class,input_placeholder,highlight_class,list_class,data_source,input_class,clear_on_focus_QMARK_,id,choice_fn,map__27369,map__27369__$1,doc,get,save_BANG_){
return (function (index,result){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"li","li",723558921),new cljs.core.PersistentArrayMap(null, 5, [new cljs.core.Keyword(null,"tab-index","tab-index",895755393),index,new cljs.core.Keyword(null,"key","key",-1516042587),index,new cljs.core.Keyword(null,"class","class",-2030961996),((cljs.core._EQ_.call(null,cljs.core.deref.call(null,selected_index),index))?highlight_class:item_class),new cljs.core.Keyword(null,"on-mouse-over","on-mouse-over",-858472552),((function (temp__4655__auto__,typeahead_hidden_QMARK_,mouse_on_list_QMARK_,selected_index,selections,choose_selected,vec__27367,type,map__27368,map__27368__$1,attrs,result_fn,item_class,input_placeholder,highlight_class,list_class,data_source,input_class,clear_on_focus_QMARK_,id,choice_fn,map__27369,map__27369__$1,doc,get,save_BANG_){
return (function (p1__27364_SHARP_){
return cljs.core.reset_BANG_.call(null,selected_index,parseInt(p1__27364_SHARP_.target.getAttribute("tabIndex")));
});})(temp__4655__auto__,typeahead_hidden_QMARK_,mouse_on_list_QMARK_,selected_index,selections,choose_selected,vec__27367,type,map__27368,map__27368__$1,attrs,result_fn,item_class,input_placeholder,highlight_class,list_class,data_source,input_class,clear_on_focus_QMARK_,id,choice_fn,map__27369,map__27369__$1,doc,get,save_BANG_))
,new cljs.core.Keyword(null,"on-click","on-click",1632826543),((function (temp__4655__auto__,typeahead_hidden_QMARK_,mouse_on_list_QMARK_,selected_index,selections,choose_selected,vec__27367,type,map__27368,map__27368__$1,attrs,result_fn,item_class,input_placeholder,highlight_class,list_class,data_source,input_class,clear_on_focus_QMARK_,id,choice_fn,map__27369,map__27369__$1,doc,get,save_BANG_){
return (function (){
cljs.core.reset_BANG_.call(null,typeahead_hidden_QMARK_,true);

save_BANG_.call(null,id,result);

return choice_fn.call(null,result);
});})(temp__4655__auto__,typeahead_hidden_QMARK_,mouse_on_list_QMARK_,selected_index,selections,choose_selected,vec__27367,type,map__27368,map__27368__$1,attrs,result_fn,item_class,input_placeholder,highlight_class,list_class,data_source,input_class,clear_on_focus_QMARK_,id,choice_fn,map__27369,map__27369__$1,doc,get,save_BANG_))
], null),result_fn.call(null,result)], null);
});})(temp__4655__auto__,typeahead_hidden_QMARK_,mouse_on_list_QMARK_,selected_index,selections,choose_selected,vec__27367,type,map__27368,map__27368__$1,attrs,result_fn,item_class,input_placeholder,highlight_class,list_class,data_source,input_class,clear_on_focus_QMARK_,id,choice_fn,map__27369,map__27369__$1,doc,get,save_BANG_))
,cljs.core.deref.call(null,selections)))], null)], null);
}
});
;})(typeahead_hidden_QMARK_,mouse_on_list_QMARK_,selected_index,selections,choose_selected,vec__27367,type,map__27368,map__27368__$1,attrs,result_fn,item_class,input_placeholder,highlight_class,list_class,data_source,input_class,clear_on_focus_QMARK_,id,choice_fn,map__27369,map__27369__$1,doc,get,save_BANG_))
}));
cljs.core._add_method.call(null,reagent_forms.core.init_field,new cljs.core.Keyword(null,"file","file",-1269645878),(function (p__27377,p__27378){
var vec__27379 = p__27377;
var type = cljs.core.nth.call(null,vec__27379,(0),null);
var map__27380 = cljs.core.nth.call(null,vec__27379,(1),null);
var map__27380__$1 = ((((!((map__27380 == null)))?((((map__27380.cljs$lang$protocol_mask$partition0$ & (64))) || (map__27380.cljs$core$ISeq$))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__27380):map__27380);
var attrs = map__27380__$1;
var id = cljs.core.get.call(null,map__27380__$1,new cljs.core.Keyword(null,"id","id",-1388402092));
var map__27381 = p__27378;
var map__27381__$1 = ((((!((map__27381 == null)))?((((map__27381.cljs$lang$protocol_mask$partition0$ & (64))) || (map__27381.cljs$core$ISeq$))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__27381):map__27381);
var doc = cljs.core.get.call(null,map__27381__$1,new cljs.core.Keyword(null,"doc","doc",1913296891));
var save_BANG_ = cljs.core.get.call(null,map__27381__$1,new cljs.core.Keyword(null,"save!","save!",-1137373803));
return ((function (vec__27379,type,map__27380,map__27380__$1,attrs,id,map__27381,map__27381__$1,doc,save_BANG_){
return (function (){
var temp__4655__auto__ = new cljs.core.Keyword(null,"visible?","visible?",2129863715).cljs$core$IFn$_invoke$arity$1(attrs);
if(cljs.core.truth_(temp__4655__auto__)){
var visible__27262__auto__ = temp__4655__auto__;
if(cljs.core.truth_(visible__27262__auto__.call(null,cljs.core.deref.call(null,doc)))){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [type,cljs.core.merge.call(null,new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"type","type",1174270348),new cljs.core.Keyword(null,"file","file",-1269645878),new cljs.core.Keyword(null,"on-change","on-change",-732046149),((function (visible__27262__auto__,temp__4655__auto__,vec__27379,type,map__27380,map__27380__$1,attrs,id,map__27381,map__27381__$1,doc,save_BANG_){
return (function (p1__27376_SHARP_){
return save_BANG_.call(null,id,cljs.core.first.call(null,cljs.core.array_seq.call(null,p1__27376_SHARP_.target.files)));
});})(visible__27262__auto__,temp__4655__auto__,vec__27379,type,map__27380,map__27380__$1,attrs,id,map__27381,map__27381__$1,doc,save_BANG_))
], null),attrs)], null);
} else {
return null;
}
} else {
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [type,cljs.core.merge.call(null,new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"type","type",1174270348),new cljs.core.Keyword(null,"file","file",-1269645878),new cljs.core.Keyword(null,"on-change","on-change",-732046149),((function (temp__4655__auto__,vec__27379,type,map__27380,map__27380__$1,attrs,id,map__27381,map__27381__$1,doc,save_BANG_){
return (function (p1__27376_SHARP_){
return save_BANG_.call(null,id,cljs.core.first.call(null,cljs.core.array_seq.call(null,p1__27376_SHARP_.target.files)));
});})(temp__4655__auto__,vec__27379,type,map__27380,map__27380__$1,attrs,id,map__27381,map__27381__$1,doc,save_BANG_))
], null),attrs)], null);
}
});
;})(vec__27379,type,map__27380,map__27380__$1,attrs,id,map__27381,map__27381__$1,doc,save_BANG_))
}));
cljs.core._add_method.call(null,reagent_forms.core.init_field,new cljs.core.Keyword(null,"files","files",-472457450),(function (p__27385,p__27386){
var vec__27387 = p__27385;
var type = cljs.core.nth.call(null,vec__27387,(0),null);
var map__27388 = cljs.core.nth.call(null,vec__27387,(1),null);
var map__27388__$1 = ((((!((map__27388 == null)))?((((map__27388.cljs$lang$protocol_mask$partition0$ & (64))) || (map__27388.cljs$core$ISeq$))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__27388):map__27388);
var attrs = map__27388__$1;
var id = cljs.core.get.call(null,map__27388__$1,new cljs.core.Keyword(null,"id","id",-1388402092));
var map__27389 = p__27386;
var map__27389__$1 = ((((!((map__27389 == null)))?((((map__27389.cljs$lang$protocol_mask$partition0$ & (64))) || (map__27389.cljs$core$ISeq$))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__27389):map__27389);
var doc = cljs.core.get.call(null,map__27389__$1,new cljs.core.Keyword(null,"doc","doc",1913296891));
var save_BANG_ = cljs.core.get.call(null,map__27389__$1,new cljs.core.Keyword(null,"save!","save!",-1137373803));
return ((function (vec__27387,type,map__27388,map__27388__$1,attrs,id,map__27389,map__27389__$1,doc,save_BANG_){
return (function (){
var temp__4655__auto__ = new cljs.core.Keyword(null,"visible?","visible?",2129863715).cljs$core$IFn$_invoke$arity$1(attrs);
if(cljs.core.truth_(temp__4655__auto__)){
var visible__27262__auto__ = temp__4655__auto__;
if(cljs.core.truth_(visible__27262__auto__.call(null,cljs.core.deref.call(null,doc)))){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [type,cljs.core.merge.call(null,new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"type","type",1174270348),new cljs.core.Keyword(null,"file","file",-1269645878),new cljs.core.Keyword(null,"multiple","multiple",1244445549),true,new cljs.core.Keyword(null,"on-change","on-change",-732046149),((function (visible__27262__auto__,temp__4655__auto__,vec__27387,type,map__27388,map__27388__$1,attrs,id,map__27389,map__27389__$1,doc,save_BANG_){
return (function (p1__27384_SHARP_){
return save_BANG_.call(null,id,p1__27384_SHARP_.target.files);
});})(visible__27262__auto__,temp__4655__auto__,vec__27387,type,map__27388,map__27388__$1,attrs,id,map__27389,map__27389__$1,doc,save_BANG_))
], null),attrs)], null);
} else {
return null;
}
} else {
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [type,cljs.core.merge.call(null,new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"type","type",1174270348),new cljs.core.Keyword(null,"file","file",-1269645878),new cljs.core.Keyword(null,"multiple","multiple",1244445549),true,new cljs.core.Keyword(null,"on-change","on-change",-732046149),((function (temp__4655__auto__,vec__27387,type,map__27388,map__27388__$1,attrs,id,map__27389,map__27389__$1,doc,save_BANG_){
return (function (p1__27384_SHARP_){
return save_BANG_.call(null,id,p1__27384_SHARP_.target.files);
});})(temp__4655__auto__,vec__27387,type,map__27388,map__27388__$1,attrs,id,map__27389,map__27389__$1,doc,save_BANG_))
], null),attrs)], null);
}
});
;})(vec__27387,type,map__27388,map__27388__$1,attrs,id,map__27389,map__27389__$1,doc,save_BANG_))
}));
reagent_forms.core.group_item = (function reagent_forms$core$group_item(p__27392,p__27393,selections,field,id){
var vec__27399 = p__27392;
var type = cljs.core.nth.call(null,vec__27399,(0),null);
var map__27400 = cljs.core.nth.call(null,vec__27399,(1),null);
var map__27400__$1 = ((((!((map__27400 == null)))?((((map__27400.cljs$lang$protocol_mask$partition0$ & (64))) || (map__27400.cljs$core$ISeq$))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__27400):map__27400);
var attrs = map__27400__$1;
var key = cljs.core.get.call(null,map__27400__$1,new cljs.core.Keyword(null,"key","key",-1516042587));
var touch_event = cljs.core.get.call(null,map__27400__$1,new cljs.core.Keyword(null,"touch-event","touch-event",-1071581783));
var body = cljs.core.nthnext.call(null,vec__27399,(2));
var map__27401 = p__27393;
var map__27401__$1 = ((((!((map__27401 == null)))?((((map__27401.cljs$lang$protocol_mask$partition0$ & (64))) || (map__27401.cljs$core$ISeq$))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__27401):map__27401);
var save_BANG_ = cljs.core.get.call(null,map__27401__$1,new cljs.core.Keyword(null,"save!","save!",-1137373803));
var multi_select = cljs.core.get.call(null,map__27401__$1,new cljs.core.Keyword(null,"multi-select","multi-select",-1298511287));
var handle_click_BANG_ = ((function (vec__27399,type,map__27400,map__27400__$1,attrs,key,touch_event,body,map__27401,map__27401__$1,save_BANG_,multi_select){
return (function reagent_forms$core$group_item_$_handle_click_BANG_(){
if(cljs.core.truth_(multi_select)){
cljs.core.swap_BANG_.call(null,selections,cljs.core.update_in,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [key], null),cljs.core.not);

return save_BANG_.call(null,id,cljs.core.map.call(null,cljs.core.first,cljs.core.filter.call(null,cljs.core.second,cljs.core.deref.call(null,selections))));
} else {
var value = cljs.core.get.call(null,cljs.core.deref.call(null,selections),key);
cljs.core.reset_BANG_.call(null,selections,cljs.core.PersistentArrayMap.fromArray([key,cljs.core.not.call(null,value)], true, false));

return save_BANG_.call(null,id,(cljs.core.truth_(cljs.core.get.call(null,cljs.core.deref.call(null,selections),key))?key:null));
}
});})(vec__27399,type,map__27400,map__27400__$1,attrs,key,touch_event,body,map__27401,map__27401__$1,save_BANG_,multi_select))
;
return ((function (vec__27399,type,map__27400,map__27400__$1,attrs,key,touch_event,body,map__27401,map__27401__$1,save_BANG_,multi_select){
return (function (){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [type,cljs.core.merge.call(null,cljs.core.PersistentArrayMap.fromArray([new cljs.core.Keyword(null,"class","class",-2030961996),(cljs.core.truth_(cljs.core.get.call(null,cljs.core.deref.call(null,selections),key))?"active":null),(function (){var or__23143__auto__ = touch_event;
if(cljs.core.truth_(or__23143__auto__)){
return or__23143__auto__;
} else {
return new cljs.core.Keyword(null,"on-click","on-click",1632826543);
}
})(),handle_click_BANG_], true, false),attrs),body], null);
});
;})(vec__27399,type,map__27400,map__27400__$1,attrs,key,touch_event,body,map__27401,map__27401__$1,save_BANG_,multi_select))
});
reagent_forms.core.mk_selections = (function reagent_forms$core$mk_selections(id,selectors,p__27404){
var map__27411 = p__27404;
var map__27411__$1 = ((((!((map__27411 == null)))?((((map__27411.cljs$lang$protocol_mask$partition0$ & (64))) || (map__27411.cljs$core$ISeq$))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__27411):map__27411);
var get = cljs.core.get.call(null,map__27411__$1,new cljs.core.Keyword(null,"get","get",1683182755));
var multi_select = cljs.core.get.call(null,map__27411__$1,new cljs.core.Keyword(null,"multi-select","multi-select",-1298511287));
var value = get.call(null,id);
return cljs.core.reduce.call(null,((function (value,map__27411,map__27411__$1,get,multi_select){
return (function (m,p__27413){
var vec__27414 = p__27413;
var _ = cljs.core.nth.call(null,vec__27414,(0),null);
var map__27415 = cljs.core.nth.call(null,vec__27414,(1),null);
var map__27415__$1 = ((((!((map__27415 == null)))?((((map__27415.cljs$lang$protocol_mask$partition0$ & (64))) || (map__27415.cljs$core$ISeq$))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__27415):map__27415);
var key = cljs.core.get.call(null,map__27415__$1,new cljs.core.Keyword(null,"key","key",-1516042587));
return cljs.core.assoc.call(null,m,key,cljs.core.boolean$.call(null,cljs.core.some.call(null,cljs.core.PersistentHashSet.fromArray([key], true),(cljs.core.truth_(multi_select)?value:new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [value], null)))));
});})(value,map__27411,map__27411__$1,get,multi_select))
,cljs.core.PersistentArrayMap.EMPTY,selectors);
});
/**
 * selectors might be passed in inline or as a collection
 */
reagent_forms.core.extract_selectors = (function reagent_forms$core$extract_selectors(selectors){
if((cljs.core.ffirst.call(null,selectors) instanceof cljs.core.Keyword)){
return selectors;
} else {
return cljs.core.first.call(null,selectors);
}
});
reagent_forms.core.selection_group = (function reagent_forms$core$selection_group(p__27419,p__27420){
var vec__27428 = p__27419;
var type = cljs.core.nth.call(null,vec__27428,(0),null);
var map__27429 = cljs.core.nth.call(null,vec__27428,(1),null);
var map__27429__$1 = ((((!((map__27429 == null)))?((((map__27429.cljs$lang$protocol_mask$partition0$ & (64))) || (map__27429.cljs$core$ISeq$))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__27429):map__27429);
var attrs = map__27429__$1;
var field = cljs.core.get.call(null,map__27429__$1,new cljs.core.Keyword(null,"field","field",-1302436500));
var id = cljs.core.get.call(null,map__27429__$1,new cljs.core.Keyword(null,"id","id",-1388402092));
var selection_items = cljs.core.nthnext.call(null,vec__27428,(2));
var map__27430 = p__27420;
var map__27430__$1 = ((((!((map__27430 == null)))?((((map__27430.cljs$lang$protocol_mask$partition0$ & (64))) || (map__27430.cljs$core$ISeq$))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__27430):map__27430);
var opts = map__27430__$1;
var get = cljs.core.get.call(null,map__27430__$1,new cljs.core.Keyword(null,"get","get",1683182755));
var selection_items__$1 = reagent_forms.core.extract_selectors.call(null,selection_items);
var selections = reagent.core.atom.call(null,reagent_forms.core.mk_selections.call(null,id,selection_items__$1,opts));
var selectors = cljs.core.map.call(null,((function (selection_items__$1,selections,vec__27428,type,map__27429,map__27429__$1,attrs,field,id,selection_items,map__27430,map__27430__$1,opts,get){
return (function (item){
return new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"visible?","visible?",2129863715),new cljs.core.Keyword(null,"visible?","visible?",2129863715).cljs$core$IFn$_invoke$arity$1(cljs.core.second.call(null,item)),new cljs.core.Keyword(null,"selector","selector",762528866),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [reagent_forms.core.group_item.call(null,item,opts,selections,field,id)], null)], null);
});})(selection_items__$1,selections,vec__27428,type,map__27429,map__27429__$1,attrs,field,id,selection_items,map__27430,map__27430__$1,opts,get))
,selection_items__$1);
return ((function (selection_items__$1,selections,selectors,vec__27428,type,map__27429,map__27429__$1,attrs,field,id,selection_items,map__27430,map__27430__$1,opts,get){
return (function (){
if(cljs.core.truth_(get.call(null,id))){
} else {
cljs.core.swap_BANG_.call(null,selections,((function (selection_items__$1,selections,selectors,vec__27428,type,map__27429,map__27429__$1,attrs,field,id,selection_items,map__27430,map__27430__$1,opts,get){
return (function (p1__27417_SHARP_){
return cljs.core.into.call(null,cljs.core.PersistentArrayMap.EMPTY,cljs.core.map.call(null,((function (selection_items__$1,selections,selectors,vec__27428,type,map__27429,map__27429__$1,attrs,field,id,selection_items,map__27430,map__27430__$1,opts,get){
return (function (p__27433){
var vec__27434 = p__27433;
var k = cljs.core.nth.call(null,vec__27434,(0),null);
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [k,false], null);
});})(selection_items__$1,selections,selectors,vec__27428,type,map__27429,map__27429__$1,attrs,field,id,selection_items,map__27430,map__27430__$1,opts,get))
,p1__27417_SHARP_));
});})(selection_items__$1,selections,selectors,vec__27428,type,map__27429,map__27429__$1,attrs,field,id,selection_items,map__27430,map__27430__$1,opts,get))
);
}

return cljs.core.into.call(null,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [type,attrs], null),cljs.core.map.call(null,new cljs.core.Keyword(null,"selector","selector",762528866),cljs.core.filter.call(null,((function (selection_items__$1,selections,selectors,vec__27428,type,map__27429,map__27429__$1,attrs,field,id,selection_items,map__27430,map__27430__$1,opts,get){
return (function (p1__27418_SHARP_){
var temp__4655__auto__ = new cljs.core.Keyword(null,"visible?","visible?",2129863715).cljs$core$IFn$_invoke$arity$1(p1__27418_SHARP_);
if(cljs.core.truth_(temp__4655__auto__)){
var visible_QMARK_ = temp__4655__auto__;
return visible_QMARK_.call(null,cljs.core.deref.call(null,new cljs.core.Keyword(null,"doc","doc",1913296891).cljs$core$IFn$_invoke$arity$1(opts)));
} else {
return true;
}
});})(selection_items__$1,selections,selectors,vec__27428,type,map__27429,map__27429__$1,attrs,field,id,selection_items,map__27430,map__27430__$1,opts,get))
,selectors)));
});
;})(selection_items__$1,selections,selectors,vec__27428,type,map__27429,map__27429__$1,attrs,field,id,selection_items,map__27430,map__27430__$1,opts,get))
});
cljs.core._add_method.call(null,reagent_forms.core.init_field,new cljs.core.Keyword(null,"single-select","single-select",1327691774),(function (p__27435,p__27436){
var vec__27437 = p__27435;
var _ = cljs.core.nth.call(null,vec__27437,(0),null);
var attrs = cljs.core.nth.call(null,vec__27437,(1),null);
var field = vec__27437;
var map__27438 = p__27436;
var map__27438__$1 = ((((!((map__27438 == null)))?((((map__27438.cljs$lang$protocol_mask$partition0$ & (64))) || (map__27438.cljs$core$ISeq$))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__27438):map__27438);
var opts = map__27438__$1;
var doc = cljs.core.get.call(null,map__27438__$1,new cljs.core.Keyword(null,"doc","doc",1913296891));
return ((function (vec__27437,_,attrs,field,map__27438,map__27438__$1,opts,doc){
return (function (){
var temp__4655__auto__ = new cljs.core.Keyword(null,"visible?","visible?",2129863715).cljs$core$IFn$_invoke$arity$1(attrs);
if(cljs.core.truth_(temp__4655__auto__)){
var visible__27262__auto__ = temp__4655__auto__;
if(cljs.core.truth_(visible__27262__auto__.call(null,cljs.core.deref.call(null,doc)))){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [reagent_forms.core.selection_group,field,opts], null);
} else {
return null;
}
} else {
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [reagent_forms.core.selection_group,field,opts], null);
}
});
;})(vec__27437,_,attrs,field,map__27438,map__27438__$1,opts,doc))
}));
cljs.core._add_method.call(null,reagent_forms.core.init_field,new cljs.core.Keyword(null,"multi-select","multi-select",-1298511287),(function (p__27440,p__27441){
var vec__27442 = p__27440;
var _ = cljs.core.nth.call(null,vec__27442,(0),null);
var attrs = cljs.core.nth.call(null,vec__27442,(1),null);
var field = vec__27442;
var map__27443 = p__27441;
var map__27443__$1 = ((((!((map__27443 == null)))?((((map__27443.cljs$lang$protocol_mask$partition0$ & (64))) || (map__27443.cljs$core$ISeq$))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__27443):map__27443);
var opts = map__27443__$1;
var doc = cljs.core.get.call(null,map__27443__$1,new cljs.core.Keyword(null,"doc","doc",1913296891));
return ((function (vec__27442,_,attrs,field,map__27443,map__27443__$1,opts,doc){
return (function (){
var temp__4655__auto__ = new cljs.core.Keyword(null,"visible?","visible?",2129863715).cljs$core$IFn$_invoke$arity$1(attrs);
if(cljs.core.truth_(temp__4655__auto__)){
var visible__27262__auto__ = temp__4655__auto__;
if(cljs.core.truth_(visible__27262__auto__.call(null,cljs.core.deref.call(null,doc)))){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [reagent_forms.core.selection_group,field,cljs.core.assoc.call(null,opts,new cljs.core.Keyword(null,"multi-select","multi-select",-1298511287),true)], null);
} else {
return null;
}
} else {
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [reagent_forms.core.selection_group,field,cljs.core.assoc.call(null,opts,new cljs.core.Keyword(null,"multi-select","multi-select",-1298511287),true)], null);
}
});
;})(vec__27442,_,attrs,field,map__27443,map__27443__$1,opts,doc))
}));
reagent_forms.core.map_options = (function reagent_forms$core$map_options(options){
return cljs.core.into.call(null,cljs.core.PersistentArrayMap.EMPTY,(function (){var iter__23915__auto__ = (function reagent_forms$core$map_options_$_iter__27461(s__27462){
return (new cljs.core.LazySeq(null,(function (){
var s__27462__$1 = s__27462;
while(true){
var temp__4657__auto__ = cljs.core.seq.call(null,s__27462__$1);
if(temp__4657__auto__){
var s__27462__$2 = temp__4657__auto__;
if(cljs.core.chunked_seq_QMARK_.call(null,s__27462__$2)){
var c__23913__auto__ = cljs.core.chunk_first.call(null,s__27462__$2);
var size__23914__auto__ = cljs.core.count.call(null,c__23913__auto__);
var b__27464 = cljs.core.chunk_buffer.call(null,size__23914__auto__);
if((function (){var i__27463 = (0);
while(true){
if((i__27463 < size__23914__auto__)){
var vec__27471 = cljs.core._nth.call(null,c__23913__auto__,i__27463);
var _ = cljs.core.nth.call(null,vec__27471,(0),null);
var map__27472 = cljs.core.nth.call(null,vec__27471,(1),null);
var map__27472__$1 = ((((!((map__27472 == null)))?((((map__27472.cljs$lang$protocol_mask$partition0$ & (64))) || (map__27472.cljs$core$ISeq$))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__27472):map__27472);
var key = cljs.core.get.call(null,map__27472__$1,new cljs.core.Keyword(null,"key","key",-1516042587));
var label = cljs.core.nth.call(null,vec__27471,(2),null);
cljs.core.chunk_append.call(null,b__27464,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [[cljs.core.str(label)].join(''),key], null));

var G__27477 = (i__27463 + (1));
i__27463 = G__27477;
continue;
} else {
return true;
}
break;
}
})()){
return cljs.core.chunk_cons.call(null,cljs.core.chunk.call(null,b__27464),reagent_forms$core$map_options_$_iter__27461.call(null,cljs.core.chunk_rest.call(null,s__27462__$2)));
} else {
return cljs.core.chunk_cons.call(null,cljs.core.chunk.call(null,b__27464),null);
}
} else {
var vec__27474 = cljs.core.first.call(null,s__27462__$2);
var _ = cljs.core.nth.call(null,vec__27474,(0),null);
var map__27475 = cljs.core.nth.call(null,vec__27474,(1),null);
var map__27475__$1 = ((((!((map__27475 == null)))?((((map__27475.cljs$lang$protocol_mask$partition0$ & (64))) || (map__27475.cljs$core$ISeq$))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__27475):map__27475);
var key = cljs.core.get.call(null,map__27475__$1,new cljs.core.Keyword(null,"key","key",-1516042587));
var label = cljs.core.nth.call(null,vec__27474,(2),null);
return cljs.core.cons.call(null,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [[cljs.core.str(label)].join(''),key], null),reagent_forms$core$map_options_$_iter__27461.call(null,cljs.core.rest.call(null,s__27462__$2)));
}
} else {
return null;
}
break;
}
}),null,null));
});
return iter__23915__auto__.call(null,options);
})());
});
reagent_forms.core.default_selection = (function reagent_forms$core$default_selection(options,v){
return cljs.core.last.call(null,cljs.core.first.call(null,cljs.core.filter.call(null,(function (p1__27478_SHARP_){
return cljs.core._EQ_.call(null,v,cljs.core.get_in.call(null,p1__27478_SHARP_,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [(1),new cljs.core.Keyword(null,"key","key",-1516042587)], null)));
}),options)));
});
cljs.core._add_method.call(null,reagent_forms.core.init_field,new cljs.core.Keyword(null,"list","list",765357683),(function (p__27481,p__27482){
var vec__27483 = p__27481;
var type = cljs.core.nth.call(null,vec__27483,(0),null);
var map__27484 = cljs.core.nth.call(null,vec__27483,(1),null);
var map__27484__$1 = ((((!((map__27484 == null)))?((((map__27484.cljs$lang$protocol_mask$partition0$ & (64))) || (map__27484.cljs$core$ISeq$))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__27484):map__27484);
var attrs = map__27484__$1;
var field = cljs.core.get.call(null,map__27484__$1,new cljs.core.Keyword(null,"field","field",-1302436500));
var id = cljs.core.get.call(null,map__27484__$1,new cljs.core.Keyword(null,"id","id",-1388402092));
var options = cljs.core.nthnext.call(null,vec__27483,(2));
var map__27485 = p__27482;
var map__27485__$1 = ((((!((map__27485 == null)))?((((map__27485.cljs$lang$protocol_mask$partition0$ & (64))) || (map__27485.cljs$core$ISeq$))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__27485):map__27485);
var doc = cljs.core.get.call(null,map__27485__$1,new cljs.core.Keyword(null,"doc","doc",1913296891));
var get = cljs.core.get.call(null,map__27485__$1,new cljs.core.Keyword(null,"get","get",1683182755));
var save_BANG_ = cljs.core.get.call(null,map__27485__$1,new cljs.core.Keyword(null,"save!","save!",-1137373803));
var options__$1 = reagent_forms.core.extract_selectors.call(null,options);
var options_lookup = reagent_forms.core.map_options.call(null,options__$1);
var selection = reagent.core.atom.call(null,(function (){var or__23143__auto__ = get.call(null,id);
if(cljs.core.truth_(or__23143__auto__)){
return or__23143__auto__;
} else {
return cljs.core.get_in.call(null,cljs.core.first.call(null,options__$1),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [(1),new cljs.core.Keyword(null,"key","key",-1516042587)], null));
}
})());
save_BANG_.call(null,id,cljs.core.deref.call(null,selection));

return ((function (options__$1,options_lookup,selection,vec__27483,type,map__27484,map__27484__$1,attrs,field,id,options,map__27485,map__27485__$1,doc,get,save_BANG_){
return (function (){
var temp__4655__auto__ = new cljs.core.Keyword(null,"visible?","visible?",2129863715).cljs$core$IFn$_invoke$arity$1(attrs);
if(cljs.core.truth_(temp__4655__auto__)){
var visible__27262__auto__ = temp__4655__auto__;
if(cljs.core.truth_(visible__27262__auto__.call(null,cljs.core.deref.call(null,doc)))){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [type,cljs.core.merge.call(null,attrs,new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"default-value","default-value",232220170),reagent_forms.core.default_selection.call(null,options__$1,cljs.core.deref.call(null,selection)),new cljs.core.Keyword(null,"on-change","on-change",-732046149),((function (visible__27262__auto__,temp__4655__auto__,options__$1,options_lookup,selection,vec__27483,type,map__27484,map__27484__$1,attrs,field,id,options,map__27485,map__27485__$1,doc,get,save_BANG_){
return (function (p1__27479_SHARP_){
return save_BANG_.call(null,id,cljs.core.get.call(null,options_lookup,reagent_forms.core.value_of.call(null,p1__27479_SHARP_)));
});})(visible__27262__auto__,temp__4655__auto__,options__$1,options_lookup,selection,vec__27483,type,map__27484,map__27484__$1,attrs,field,id,options,map__27485,map__27485__$1,doc,get,save_BANG_))
], null)),cljs.core.doall.call(null,cljs.core.filter.call(null,((function (visible__27262__auto__,temp__4655__auto__,options__$1,options_lookup,selection,vec__27483,type,map__27484,map__27484__$1,attrs,field,id,options,map__27485,map__27485__$1,doc,get,save_BANG_){
return (function (p1__27480_SHARP_){
var temp__4655__auto____$1 = new cljs.core.Keyword(null,"visible?","visible?",2129863715).cljs$core$IFn$_invoke$arity$1(cljs.core.second.call(null,p1__27480_SHARP_));
if(cljs.core.truth_(temp__4655__auto____$1)){
var visible_QMARK_ = temp__4655__auto____$1;
return visible_QMARK_.call(null,cljs.core.deref.call(null,doc));
} else {
return true;
}
});})(visible__27262__auto__,temp__4655__auto__,options__$1,options_lookup,selection,vec__27483,type,map__27484,map__27484__$1,attrs,field,id,options,map__27485,map__27485__$1,doc,get,save_BANG_))
,options__$1))], null);
} else {
return null;
}
} else {
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [type,cljs.core.merge.call(null,attrs,new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"default-value","default-value",232220170),reagent_forms.core.default_selection.call(null,options__$1,cljs.core.deref.call(null,selection)),new cljs.core.Keyword(null,"on-change","on-change",-732046149),((function (temp__4655__auto__,options__$1,options_lookup,selection,vec__27483,type,map__27484,map__27484__$1,attrs,field,id,options,map__27485,map__27485__$1,doc,get,save_BANG_){
return (function (p1__27479_SHARP_){
return save_BANG_.call(null,id,cljs.core.get.call(null,options_lookup,reagent_forms.core.value_of.call(null,p1__27479_SHARP_)));
});})(temp__4655__auto__,options__$1,options_lookup,selection,vec__27483,type,map__27484,map__27484__$1,attrs,field,id,options,map__27485,map__27485__$1,doc,get,save_BANG_))
], null)),cljs.core.doall.call(null,cljs.core.filter.call(null,((function (temp__4655__auto__,options__$1,options_lookup,selection,vec__27483,type,map__27484,map__27484__$1,attrs,field,id,options,map__27485,map__27485__$1,doc,get,save_BANG_){
return (function (p1__27480_SHARP_){
var temp__4655__auto____$1 = new cljs.core.Keyword(null,"visible?","visible?",2129863715).cljs$core$IFn$_invoke$arity$1(cljs.core.second.call(null,p1__27480_SHARP_));
if(cljs.core.truth_(temp__4655__auto____$1)){
var visible_QMARK_ = temp__4655__auto____$1;
return visible_QMARK_.call(null,cljs.core.deref.call(null,doc));
} else {
return true;
}
});})(temp__4655__auto__,options__$1,options_lookup,selection,vec__27483,type,map__27484,map__27484__$1,attrs,field,id,options,map__27485,map__27485__$1,doc,get,save_BANG_))
,options__$1))], null);
}
});
;})(options__$1,options_lookup,selection,vec__27483,type,map__27484,map__27484__$1,attrs,field,id,options,map__27485,map__27485__$1,doc,get,save_BANG_))
}));
reagent_forms.core.field_QMARK_ = (function reagent_forms$core$field_QMARK_(node){
return (cljs.core.coll_QMARK_.call(null,node)) && (cljs.core.map_QMARK_.call(null,cljs.core.second.call(null,node))) && (cljs.core.contains_QMARK_.call(null,cljs.core.second.call(null,node),new cljs.core.Keyword(null,"field","field",-1302436500)));
});
/**
 * creates data bindings between the form fields and the supplied atom
 * form - the form template with the fields
 * doc - the document that the fields will be bound to
 * events - any events that should be triggered when the document state changes
 */
reagent_forms.core.bind_fields = (function reagent_forms$core$bind_fields(var_args){
var args__24208__auto__ = [];
var len__24201__auto___27492 = arguments.length;
var i__24202__auto___27493 = (0);
while(true){
if((i__24202__auto___27493 < len__24201__auto___27492)){
args__24208__auto__.push((arguments[i__24202__auto___27493]));

var G__27494 = (i__24202__auto___27493 + (1));
i__24202__auto___27493 = G__27494;
continue;
} else {
}
break;
}

var argseq__24209__auto__ = ((((2) < args__24208__auto__.length))?(new cljs.core.IndexedSeq(args__24208__auto__.slice((2)),(0))):null);
return reagent_forms.core.bind_fields.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),(arguments[(1)]),argseq__24209__auto__);
});

reagent_forms.core.bind_fields.cljs$core$IFn$_invoke$arity$variadic = (function (form,doc,events){
var opts = new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"doc","doc",1913296891),doc,new cljs.core.Keyword(null,"get","get",1683182755),(function (p1__27488_SHARP_){
return cljs.core.get_in.call(null,cljs.core.deref.call(null,doc),reagent_forms.core.id__GT_path.call(null,p1__27488_SHARP_));
}),new cljs.core.Keyword(null,"save!","save!",-1137373803),reagent_forms.core.mk_save_fn.call(null,doc,events)], null);
var form__$1 = clojure.walk.postwalk.call(null,((function (opts){
return (function (node){
if(cljs.core.truth_(reagent_forms.core.field_QMARK_.call(null,node))){
var opts__$1 = reagent_forms.core.wrap_fns.call(null,opts,node);
var field = reagent_forms.core.init_field.call(null,node,opts__$1);
if(cljs.core.fn_QMARK_.call(null,field)){
return new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [field], null);
} else {
return field;
}
} else {
return node;
}
});})(opts))
,form);
return ((function (opts,form__$1){
return (function (){
return form__$1;
});
;})(opts,form__$1))
});

reagent_forms.core.bind_fields.cljs$lang$maxFixedArity = (2);

reagent_forms.core.bind_fields.cljs$lang$applyTo = (function (seq27489){
var G__27490 = cljs.core.first.call(null,seq27489);
var seq27489__$1 = cljs.core.next.call(null,seq27489);
var G__27491 = cljs.core.first.call(null,seq27489__$1);
var seq27489__$2 = cljs.core.next.call(null,seq27489__$1);
return reagent_forms.core.bind_fields.cljs$core$IFn$_invoke$arity$variadic(G__27490,G__27491,seq27489__$2);
});

//# sourceMappingURL=core.js.map