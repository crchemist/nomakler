{:profiles/dev  {:env {:database-url #=(eval  (if  (System/getenv  "OPENSHIFT_POSTGRESQL_DB_URL")  (str  "jdbc:postgresql://"  (System/getenv  "OPENSHIFT_POSTGRESQL_DB_HOST")  ":"  (System/getenv  "OPENSHIFT_POSTGRESQL_DB_PORT")  "/"  (System/getenv  "OPENSHIFT_APP_NAME")  "?user="  (System/getenv  "OPENSHIFT_POSTGRESQL_DB_USERNAME")  "&password="  (System/getenv  "OPENSHIFT_POSTGRESQL_DB_PASSWORD"))  "jdbc:postgresql://localhost/nm_dev?user=nm&password=qaz123"))
                       :media-path "/tmp/"}}
 :profiles/test {:env {:database-url "jdbc:postgresql://localhost/nm_test?user=nm&password=qaz123"
                       :media-path "/tmp/"}}}
