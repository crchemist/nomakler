(ns nomakler.test.db.core
  (:require [nomakler.db.core :as db]
            [nomakler.db.migrations :as migrations]
            [clojure.test :refer :all]
            [clojure.java.jdbc :as jdbc]
            [conman.core :refer [with-transaction]]
            [environ.core :refer [env]]
            [mount.core :as mount]))

(use-fixtures
  :once
  (fn [f]
    (mount/start #'nomakler.db.core/*db*)
    (migrations/migrate ["migrate"])
    (f)))

(deftest test-users
  (with-transaction [t-conn db/*db*]
    (jdbc/db-set-rollback-only! t-conn)
    (is (= 1 (db/create-user!
               {:vk_id "id1"
                :name  "Smith"})))
    (is (= [{:name  "Smith"
             :admin      nil
             :last_login nil
             :is_active  nil
	     :vk_id "id1"
	     :data nil}]
           [(dissoc (first (db/get-vk-user {:vk_id "id1"})) :id)]))))
