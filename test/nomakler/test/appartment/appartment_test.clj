(ns nomakler.test.appartment.appartment-test
  (:require [nomakler.routes.appartment :as ap]
            [clojure.test :refer :all]
            [ring.mock.request :refer :all]
            [nomakler.handler :refer :all]))


(deftest test-appartments
  (testing "create appartment"
    (let [response (app (request :post "/appartments/"
                                 {:description "descr"
                                  :price 56
                                  :location "POINT(1 2)"}))]
      (is (= 200 (:status response))))))
